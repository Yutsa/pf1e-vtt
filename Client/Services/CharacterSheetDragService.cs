using Microsoft.Extensions.Logging;
using pf1e_vtt.Client.Models;

namespace pf1e_vtt.Client.Services
{
    public class CharacterSheetDragService<T>
    {
        private readonly ILogger logger;
        private T draggableObject;

        public CharacterSheetDragService(ILogger<CharacterSheetDragService<T>> logger)
        {
            this.logger = logger;
        }

        public void DragObject(T draggedObject)
        {
            draggableObject = draggedObject;
            logger.LogInformation($"Dragging {draggableObject}");
        }

        public T DropObject()
        {
            logger.LogInformation($"Dropped {draggableObject}");
            return draggableObject;
        }
    }
}