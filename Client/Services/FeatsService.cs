using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using pf1e_vtt.Shared.Models;
using pf1e_vtt.Shared.Services;

namespace pf1e_vtt.Client.Services
{
    class FeatsService : IFeatsService
    {
        private readonly HttpClient _http;

        public FeatsService(HttpClient http)
        {
            _http = http;
        }

        public Task<Feat[]> GetFeatsAsync()
        {
            return _http.GetFromJsonAsync<Feat[]>("api/Feat");
        }
    }
}