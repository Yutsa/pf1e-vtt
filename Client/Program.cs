using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using pf1e_vtt.Client.Services;
using pf1e_vtt.Shared.Services;

namespace pf1e_vtt.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddScoped<IFeatsService, FeatsService>();
            builder.Services.AddScoped(typeof(CharacterSheetDragService<>), typeof(CharacterSheetDragService<>));

            await builder.Build().RunAsync();
        }
    }
}
