using pf1e_vtt.Shared.Models;
using Xunit;

namespace Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var feat = new Feat() { Name = "Test" };
            var character = new Character();
            character.Feats.Add(feat);
            var prerequisite = new FeatPrerequisite() { Feat = feat };
            Assert.True(prerequisite.IsSatisfied(character));
        }
    }
}
