using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pf1e_vtt.Models;
using pf1e_vtt.Shared.Models;
using pf1e_vtt.Shared.Services;

namespace pf1e_vtt.Server.Services
{
    class FeatsService : IFeatsService
    {
        private readonly FeatContext _context;

        public FeatsService(FeatContext context)
        {
            _context = context;
        }

        public Task<Feat[]> GetFeatsAsync()
        {
            return _context.Feats.ToArrayAsync();
        }
    }
}