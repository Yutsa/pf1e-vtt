﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace pf1evtt.Server.Migrations
{
    public partial class Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Feats",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Description = table.Column<string>(type: "TEXT", nullable: true),
                    Advantage = table.Column<string>(type: "TEXT", nullable: true),
                    Normal = table.Column<string>(type: "TEXT", nullable: true),
                    Special = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feats", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "adepte-de-la-matraque", null, "Le personnage sait exactement où frapper pour assommer ses adversaires.", "Adepte de la matraque", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-croc-en-jambe-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manoeuvre offensive de croc-en-jambe et ajoute ce même bonus à son DMD lorsqu’un adversaire tente une telle manoeuvre contre lui. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science du croc-en-jambe. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente une manœuvre de croc-en-jambe contre lui, à moins qu’elle possède également ce don.", "Le personnage est passé maître dans l’art de faire tomber ses adversaires.", "Science du croc-en-jambe (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-croc-en-jambe", "Le personnage ne provoque pas d’attaque d’opportunité lorsqu’il tente de faire un croc-en-jambe. Il bénéficie de plus d’un bonus de +2 aux tests de combat destinés à faire un croc-en-jambe à un adversaire et un bonus de +2 à la manœuvre défensive quand un ennemi essaie de faire de même.", "Le personnage est très doué pour envoyer son adversaire à terre.", "Science du croc-en-jambe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-désarmement-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manœuvre offensive de désarmement et ce même bonus est ajouté à son DMD lorsqu’un adversaire tente de le désarmer. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science du désarmement. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente de le désarmer, à moins qu’elle possède également ce don.", "Le personnage est passé maître dans l’art de désarmer ses adversaires.", "Science du désarmement (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-désarmement", "Le personnage ne provoque pas d’attaque d’opportunité quand il tente de désarmer son ennemi. De plus, il reçoit un bonus de +2 aux tests destinés à désarmer un adversaire et un bonus de +2 à la manœuvre défensive quand ses ennemis essaient de le désarmer.", "Le personnage a un don pour ôter l’arme des mains de son adversaire.", "Science du désarmement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-lancer-lors-dune-charge", "Le personnage peut se trouver à n’importe quelle distance de sa cible quand il utilise Lancer lors d’une charge, dans la limite qu’autorise l’arme. Si la cible se trouve à moins de 9 m (6 c), il gagne un bonus de +2 au jet de dégâts.", "Le personnage mobilise chacun de ses muscles pour projeter une arme.", "Science du lancer lors d'une charge", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-renversement-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manœuvre offensive de renversement et ajoute ce même bonus à son DMD lorsqu’un adversaire tente de le renverser. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science du renversement. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente de le renverser, à moins qu’elle possède également ce don.", "Le personnage est une force qu’on ne peut arrêter sur le champ de bataille, capable de renverser avec aisance ses ennemis inférieurs.", "Science du renversement (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-renversement", "Le personnage ne provoque pas d’attaque d’opportunité quand il tente de renverser un adversaire lors d’une manœuvre offensive. Il bénéficie de plus d’un bonus de +2 aux tests de combat destinés à renverser un adversaire et un bonus de +2 à la manœuvre défensive quand un ennemi essaie de le renverser. Les cibles du personnage ne peuvent pas choisir de l’éviter.", "Le personnage sait renverser ses adversaires.", "Science du renversement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-repositionnement-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manœuvre offensive de repositionnement et ajoute ce même bonus à son DMD lorsqu’un adversaire tente de le repositionner. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science du repositionnement. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente de le repositionner, à moins qu’elle possède également ce don.", "Le personnage positionne ses ennemis là où il le souhaite.", "Science du repositionnement (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-sale-coup-(mythique)", "Le personnage gagne un bonus égal à son grade aux tests effectués pour réaliser un sale coup et à son DMD lorsqu’un adversaire tente d’effectuer un sale coup contre lui. Ces bonus se cumulent avec ceux conférés par Science du sale coup. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui effectue une manœuvre de sale coup contre lui, à moins qu’elle possède également ce don.", "Le personnage n’a pas son pareil lorsqu’il s’agit de se battre sournoisement.", "Science du sale coup (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-tir-de-précision", "Les attaques à distance du personnage ignorent le bonus à la classe d’armure dont bénéficient les créatures cachées derrière un abri autre que total ainsi que les chances d’échec liées à un camouflage autre que total. Un abri total ou un camouflage total conserve ses effets habituels.", "Les attaques du personnage ignorent tout sauf le camouflage et l’abri total.", "Science du tir de précision", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "serviteur-céleste", "Le compagnon animal, le familier ou la monture de l’aasimar gagne l’archétype céleste et devient une créature magique, mais l’aasimar le considère toujours comme un animal lorsqu’il utilise Dressage, empathie sauvage ou tout autre sort ou capacité de classe affectant spécifiquement les animaux.", "Plutôt que d’être un animal ou une créature normal, le compagnon ou familier de l’aasimar vient des royaumes célestes.", "Serviteur céleste", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "signes-secrets", null, "Le personnage aime tout particulièrement communiquer avec les autres par des sous-entendus, des gestes et des signes de mains secrets.", "Signes secrets", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sociable-(mythique)", "Le personnage confère et bénéficie constamment du bonus de +2 aux tests de Diplomatie octroyé par le don Sociable sans qu’il y consacre une action de mouvement. Le personnage peut effectuer une action de mouvement pour que le bonus s’élève à +4 pendant un nombre de rounds égal à son grade.", "La nature affable du personnage transparaît au moindre de ses actes et met son entourage à l’aise.", "Sociable (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-ascendant-(métamagie)", "Le personnage peut modifier un sort pour produire sa version mythique. Un sort ascendant utilise la version mythique du sort, mais il n’est pas considéré comme un sort mythique dans le cadre des effets qui interagissent avec lui, à moins que le personnage lui-même ne soit une créature mythique. Il ne peut pas se servir de la version amplifiée du sort mythique ni utiliser des effets de sort qui nécessite une dépense de pouvoir mythique (même s’il lui reste des utilisations de pouvoir mythique). Un sort ascendant utilise un emplacement de sort de 5 niveaux de plus que son niveau réel.", "Le personnage a appris à reproduire les effets des sorts mythiques à partir d’énergie non-mythique.", "Sort ascendant (métamagie)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-de-recherche", "Un sort de recherche peut contourner les obstacles pour atteindre la cible désignée. Le personnage peut définir lui-même le chemin à emprunter ou identifier la cible de manière non-ambiguë afin de laisser le sort déterminer son propre chemin. Cependant, le sort échoue si pour atteindre sa cible il doit se déplacer sur une distance qui excède sa portée maximale. Un jet d’attaque à distance réalisé pour lancer un tel sort ne prend pas en compte les bonus d’abris et  de camouflage de la cible. Afin de bénéficier de ce don, le sort sélectionné doit avoir une portée supérieur à un sort de contact et cibler au moins une créature, ou il doit requérir une attaque de contact à distance. Un sort de recherche utilise un emplacement de sort d’un niveau égal au niveau actuel du sort + 2.", "Le personnage peut lancer un sort qui contourne les barrières pour atteindre la destination voulue.", "Sort de recherche", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-mélodieux", null, "Le personnage peut intégrer des effets de musique de barde dans ses sorts, au point qu’il est impossible de distinguer le sort de la représentation bardique.", "Sort mélodieux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-ténébreux", null, "Le personnage intègre l’ombre dans ses sorts, ce qui augmente leur efficacité, mais également leur vulnérabilité à la lumière.", "Sort ténébreux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "survivant-tenace", "Quand le personnage se fait tuer parce qu’il reçoit trop de points de dégâts, son esprit s’attarde dans son corps un nombre de rounds égal à son bonus de Constitution. Il est bien mort, mais si une créature fait un test de Premiers secours DD 10, par une action simple, elle se rend compte qu’elle peut encore le sauver. Elle peut le soigner par magie comme s’il était encore vivant. Si cela fait repasser ses points de vie au-dessus du seuil de la mort, il revient parmi les vivants, mais il gagne un niveau négatif permanent.", "L’esprit du personnage s’attarde bien plus longtemps qu’il ne devrait.", "Survivant tenace", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "surprise", ". Le personnage ne subit pas de malus quand il se sert d’une arme de corps à corps improvisée. Les adversaires sans armes sont pris au dépourvu quand le personnage les attaque avec une arme de corps à corps improvisée.", "Les adversaires sont surpris par l’adresse avec laquelle le personnage se sert d’armes improvisées peu orthodoxes.", "Surprise", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "surprise-(mythique)", "Les dégâts infligés quand le personnage frappe avec des armes improvisées augmentent d’un nombre égal à son grade. Il gagne également un bonus au DMD égal à son grade quand un adversaire tente une manoeuvre de destruction ou de désarmement sur son arme improvisée.", "Chaque objet devient une arme mortelle entre les mains du personnage.", "Surprise (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "succès-éclatant", "Quand le personnage confirme un coup critique ou qu’il fait un 20 naturel sur un jet de sauvegarde, il gagne un bonus de circonstances de +2 à un unique jet d’attaque, jet de sauvegarde, test de compétence ou test de caractéristique avant la fin de son prochain tour. Le personnage doit annoncer qu’il utilise le bonus avant de faire le jet ou le test associé.", "Le succès du personnage l’aide dans ses prochaines actions.", "Succès éclatant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "succession-denchaînements", "Par une action simple, le personnage porte une unique attaque avec la totalité de son bonus de base à l’attaque à un adversaire situé à sa portée. S’il le touche, il lui inflige les dégâts habituels et peut faire une attaque supplémentaire (toujours avec son bonus de base à l’attaque maximal) contre un adversaire adjacent au premier ennemi et situé à portée du personnage. S’il le touche, il peut faire une nouvelle attaque sur un adversaire à sa portée et adjacent au dernier ennemi touché. Il n’y a pas de limite au nombre d’ennemis que le personnage peut attaquer ainsi mais il ne peut pas attaquer deux fois le même pendant cette action. Quand le personnage utilise ce don, il subit un malus de -2 à la CA jusqu’à son prochain tour.", "Le personnage peut frapper plusieurs adversaires adjacents d’un seul coup.", "Succession d'enchaînements", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sténo", null, "À force d’étudier d’anciens écrits, le personnage a découvert une méthode bien plus efficace pour noter ses sorts.", "Sténo", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-critique", "La zone de critique de l’arme choisie est doublée.", "Les attaques du personnage avec une arme de son choix sont particulièrement redoutables.", "Science du critique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "stoïque", null, "Le personnage sait d’expérience que la vie est une suite d’épreuves impitoyables et que la peur doit être vaincue. ", "Stoïque", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "spécialisation-martiale-supérieure", "Le personnage obtient un bonus de +2 sur les jets de dégâts de l’arme choisie. Ce bonus se cumule avec tous les autres bonus aux jets de dégâts, y compris celui du don Spécialisation martiale.", "Le personnage choisit une arme pour laquelle il possède le don Spécialisation martiale. Il peut aussi choisir l’attaque à mains nues ou la lutte. Ses attaques seront alors bien plus destructrices que la normale.", "Spécialisation martiale supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "spécialisation-martiale-(mythique)", "Lorsque le personnage utilise l’arme dans laquelle il s’est spécialisé, il gagne un bonus égal à la moitié de son grade aux jets de dégâts. Ce bonus se cumule avec celui conféré par Spécialisation martiale et Spécialisation martiale supérieure.", "Les dégâts que le personnage inflige avec son arme préférée sont véritablement stupéfiants.", "Spécialisation martiale (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "souplesse-du-serpent", "Le personnage bénéficie d’un bonus d’esquive de +4 à la CA contre les attaques d’opportunité déclenchées par un déplacement dans un espace contrôlé (ou lorsqu’il en sort). Il perd automatiquement ce bonus s’il perd son bonus de Dextérité à la CA.\r\nContrairement à la plupart des bonus, le bonus d’esquive est cumulatif.", "Le personnage n’a aucun mal à traverser la mêlée.", "Souplesse du serpent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "souplesse-du-serpent-(mythique)", "Lorsqu’il utilise Souplesse du serpent, le personnage gagne un bonus d’esquive de +6 à la CA à la place du bonus normal de +4. De plus, une fois par round lorsqu’une attaque d’opportunité provoquée par son déplacement le rate, il peut se déplacer de 1,50 mètre par une action libre. Ce déplacement n’est pas compté dans la distance totale que le personnage parcourt durant le round en cours, mais il provoque tout de même des attaques d’opportunité.", "Le personnage a perfectionné son aptitude à se faufiler sur le champ de bataille en utilisant ses capacités de déplacement pour pousser son '''Avantage.''' ", "Souplesse du serpent (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "souffle-intérieur", "Le sylphe n’a plus besoin de respirer. Il est immunisé aux effets nécessitant que la créature respire (comme les poisons inhalés). Ceci ne l’immunise pas aux attaques de nuages ou de gaz qui n’ont pas besoin que la créature respire, comme brume mortelle.", "Le corps du sylphe est empreint d’air élémentaire pourvoyant à tous ses besoins respiratoires.", "Souffle intérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-umbral", null, "Les cibles des sorts d’obscurité lancés par le personnage irradient elles-mêmes l’obscurité.", "Sort umbral", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "spécialisation-martiale", "Le personnage obtient un bonus de +2 sur les jets de dégâts de l’arme choisie.", "Le personnage est doué pour infliger des dégâts avec son arme. Il choisit une arme pour laquelle il possède le don Arme de prédilection. Il peut aussi choisir l’ attaque à mains nues ou la lutte. Il inflige des dégâts supplémentaires avec cette arme.", "Spécialisation martiale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "survivant", null, "Dans le pays du personnage, seuls les plus forts survivent et il n’a rien d’un faible, même pour les siens. ", "Survivant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-critique-(mythique)", "Le multiplicateur de critique que le personnage applique lorsqu’il manipule l’arme choisie augmente de 1 ( jusqu’à un maximum de ×6).", "Les coups critiques que le personnage inflige avec l’arme choisie sont d’une puissance dévastatrice.", "Science du critique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-contresort", "Lorsque le personnage tente de contrer un sort, il peut remplacer le sort d’origine par n’importe quel sort de la même école ayant au moins un niveau de sort de plus.", "Le personnage sait contrer les sorts ennemis en utilisant des sorts proches.", "Science du contresort", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sang-de-la-vie", "Par une action complexe, à volonté, le samsaran peut effectuer un rituel de sang spécial au cours duquel il sacrifie une partie de sa force vitale pour guérir une autre créature. Lorsqu’il utilise ce don, il reçoit 1d4 points de dégâts et applique son sang sur les blessures d’une créature vivante, la soignant d’un nombre de points de vie égal au nombre de dégâts qu’il a reçus du rituel. Il s’agit d’une capacité surnaturelle. Seul le samsaran peut réaliser cette saignée. Il n’est pas possible de soigner une créature grâce à cette capacité plus d’une fois par jour.", "La vie éternelle coule dans les veines du samsaran et ses pouvoirs de guérison lui permettent d’utiliser son sang pour guérir les autres.", "Sang de la vie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sang-remarquable", null, "Le personnage descend d’un personnage remarquable choisi pour ses dispositions.", "Sang remarquable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "saut-vaillant-(mythique)", "On considère que la monture du personnage bénéficie toujours d’une course d’élan quand elle utilise la compétence Acrobaties pour sauter. Le personnage peut dépenser une utilisation de pouvoir mythique pour conférer à sa monture un bonus de +10 à un test d’Acrobaties ou un bonus de +20 si le test est effectué pour sauter. Lorsqu’il effectue une charge montée, le personnage peut pousser sa monture à sauter par-dessus un adversaire situé entre sa position de départ et la cible de sa charge. Le personnage effectue un test d’Équitation ou sa monture effectue un test d’Acrobaties (sélectionnez la compétence la moins élevée) pendant la charge, en appliquant les règles pour sauter un obstacle décrites dans la compétence Équitation. Si la hauteur sautée grâce au test dépasse la taille de la créature, le personnage et sa monture passent au-dessus d’elle et poursuivent leur charge de l’autre côté. Ce déplacement provoque des attaques d’opportunité. Par ailleurs, la monture du personnage peut bondir au-dessus de sa cible pendant qu’il effectue une Attaque au galop, ce qui lui permet de se déplacer au travers de l’espace occupé par la cible et poursuivre le déplacement de l’autre côté. Si la hauteur de ce saut dépasse la taille de la cible, le personnage gagne un bonus de +1 au jet d’attaque grâce à sa position surélevée. Si ses alliés menacent la cible, le personnage considère qu’elle est prise en tenaille dans le cadre de l’Attaque au galop.", "La monture du personnage peut réaliser des sauts héroïques.", "Saut vaillant (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "saut-élémentaire", "Une fois par jour, le personnage peut utiliser le pouvoir magique changement de plan avec un niveau de lanceur de sorts égal à son niveau pour se téléporter, avec les cibles volontaires, sur le plan élémentaire lié à sa race (plan du Feu pour les ifrits, plan de la Terre pour les oréades, plan de l’Air pour les sylphes, plan de l’Eau pour les ondins). Lorsqu’il est sur ce plan, le personnage (mais aucun de ceux qu’il emmène avec lui) est considéré comme étant sous les effets du sort adaptation planaire.", "Les esprits du plan ancestral du personnage appellent ce dernier, l’enjoignant à revenir.", "Saut élémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "savancer", "Quand un ennemi adjacent tente de faire un pas de 1,5 m pour s’éloigner du personnage, ce dernier peut aussi faire un pas de 1,5 m (par une action immédiate) tant qu’il termine son déplacement sur une case adjacente à celle de l’adversaire qui a provoqué ce mouvement. En revanche, le personnage ne pourra pas faire de pas de 1,5 m lors de son prochain tour. Si le personnage fait une action de mouvement lors de son prochain tour, retranchez 1,5 m à son total de mouvement.", "Le personnage se rapproche des adversaires qui tentent de s’éloigner.", "S'avancer", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-bousculade-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manoeuvre offensive de bousculade et ajoute ce même bonus à son DMD lorsqu’un adversaire tente de le bousculer. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science de la bousculade. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente de le bousculer, à moins qu’elle possède également ce don.", "Le personnage bouscule ses adversaires avec une facilité déconcertante.", "Science de la bousculade (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-bousculade", "Lors d’une manœuvre offensive, le personnage peut tenter une bousculade sans provoquer d’attaque d’opportunité de la part de sa cible. Le personnage bénéficie de plus d’un bonus de +2 aux tests destinés à bousculer un adversaire et un bonus de +2 à sa manœuvre défensive quand un ennemi essaie de le bousculer.", "Le personnage est doué pour repousser ses adversaires.", "Science de la bousculade", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-canalisation-(mythique)", "Les créatures non-mythiques qui subissent des dégâts infligés par la canalisation d’énergie du personnage doivent effectuer deux jets de sauvegarde et conserver le pire.", "Le personnage est un intermédiaire sans pareil du pouvoir divin.", "Science de la canalisation (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-canalisation-de-force", "Lorsqu’il utilise Canalisation de force, l’aasimar peut affecter toutes les créatures situées dans une ligne de 18 mètres ou dans un cône de 9 mètres. Il peut décider de repousser ou d’attirer tous les êtres situés dans la zone affectée qui échouent à leur jet de sauvegarde.", "L’aasimar déplace ses ennemis dans un rayon d’énergie vertueuse.", "Science de la canalisation de force", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-canalisation", "Le personnage ajoute 2 au DD des jets de sauvegarde destinés à résister à l’énergie qu’il canalise.", "Il est plus difficile de résister à l’énergie canalisée par le personnage.", "Science de la canalisation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-destruction-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manœuvre offensive de destruction et ajoute ce même bonus à son DMD lorsqu’un adversaire tente une telle manœuvre contre lui. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science de la destruction. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente une manœuvre de destruction contre l’un des objets qu’il porte, à moins qu’elle possède également ce don.", "Les puissants coups donnés par le personnage mettent les objets en pièces.", "Science de la destruction (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-destruction", "Lorsque le personnage essaye de détruire un objet tenu ou porté par un adversaire (comme une arme ou un bouclier), il ne s’expose pas à une attaque d’opportunité. Il bénéficie de plus d’un bonus de +2 aux tests de combat destinés à briser un objet et un bonus de +2 à son score de manœuvre défensive quand un ennemi essaie de détruire son équipement.", "Le personnage est doué pour endommager les armes et l'armure de ses adversaires.", "Science de la destruction", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-feinte", "Le personnage peut faire un test de Bluff pour tenter une feinte par une action de mouvement.", "Le personnage est très doué pour tromper ses adversaires au combat.", "Science de la feinte", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-frappe-décisive", "Quand le personnage recourt à cette action, il porte une unique attaque avec son bonus de base à l’attaque le plus élevé. Cette attaque inflige alors des dégâts supplémentaires : le personnage lance trois fois les dés de dégâts et additionne leurs résultats avant d'ajouter les bonus découlant de sa Force ou des propriétés des armes (comme une arme de feu), les dégâts de précision et les autres bonus de dégâts. Ces dés de dégâts supplémentaires ne sont pas multipliés en cas de critique mais sont ajoutés au total.", "Le personnage peut porter une unique attaque qui inflige des dégâts importants.", "Science de la frappe décisive", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-lutte-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manœuvre offensive de lutte et ce même bonus est ajouté à son DMD lorsqu’un adversaire tente de lutter contre lui. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science de la lutte. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente de lutter contre lui, à moins qu’elle possède également ce don.", "Les mouvements de lutte du personnage sont difficiles à contrer.", "Science de la lutte (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-lutte", "Le personnage ne provoque pas d’attaque d’opportunité lorsqu’il fait une manœuvre de combat pour engager une lutte. Il bénéficie de plus d’un bonus de +2 aux tests de combat destinés à lutter avec un adversaire et un bonus de +2 à la manœuvre défensive quand un ennemi essaie de lutter avec lui.", "Le personnage a un don pour la lutte.", "Science de la lutte", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-noblesse-drow", "Le drow peut utiliser les pouvoirs magiques lumières dansantes, lueur féerique, feuille morte et lévitation deux fois par jour. Son pouvoir magique de ténèbres est remplacé par celui de ténèbres profondes, qu’il peut utiliser deux fois par jour.", "Le drow a un héritage magique plus puissant que celui de ses pairs, comme le prouvent ses pouvoirs magiques supérieurs.", "Science de la noblesse drow", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-contresort-(mythique)", "Lorsqu’il contre un sort, le personnage peut utiliser un sort de la même école de magie dont le niveau est supérieur ou égal à celui du sort ciblé.", "Le personnage comprend instinctivement le fonctionnement de la magie, ce qui lui permet de dissiper des sorts à l’aide d’énergie magique pure.", "Science du contresort (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-combat-à-mains-nues", "Le personnage est considéré comme armé même s’il n’a pas d’arme. Cela signifie qu’il ne provoque pas d’attaque d’opportunité quand il attaque un adversaire armé à mains nues. Les attaques à mains nues du personnage peuvent infliger des dégâts létaux ou non-létaux, à sa guise.", "Le personnage sait se battre sans armes.", "Science du combat à mains nues", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-combat-à-mains-nues-(mythique)", "Le personnage ajoute la moitié de son grade aux dégâts qu’il inflige à mains nues. Par une action rapide, il peut dépenser une utilisation de pouvoir mythique pour ignorer la solidité des objets qu’il frappe à mains nues. Cet effet dure un nombre de rounds égal à son grade. Si la solidité d’un objet est supérieure à 15, le personnage ne peut pas l’ignorer, même en partie.", "Les frappes à mains nues du personnage sont plus puissantes que celles d’autrui.", "Science du combat à mains nues (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-combat-à-deux-armes", "Lorsque le personnage se bat à deux armes, il peut porter deux attaques supplémentaires à l’aide de son arme secondaire au lieu d’une seule. Cependant, cette deuxième attaque secondaire s’accompagne d’un malus de -5 au jet d’attaque.", "Le personnage est doué pour le combat à deux armes.", "Science du combat à deux armes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-des-réflexes-surhumains", "Une fois par jour, le personnage peut relancer un jet de Réflexes. Il doit décider de le faire avant que le résultat du premier jet soit connu et doit accepter le second résultat, même s’il est pire que le premier.", "Le personnage a un don pour éviter les dangers qui l’entourent.", "Science des réflexes surhumains", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-l’initiative-(mythique)", "Le bonus aux tests d’initiative dont bénéficie le personnage grâce à Science de l’initiative augmente d’un montant égal à son grade. De plus, au lieu de lancer le dé d’initiative, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer que son jet est un 20 naturel.", "Le personnage engage le combat avec une rapidité et une assurance incroyables.", "Science de l’initiative (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-coup-de-bouclier", "Lorsque le personnage donne un coup de bouclier, il peut tout de même appliquer son bonus de bouclier à sa classe d’armure.", "Le personnage peut se protéger à l’aide de son bouclier, même s’il s’en sert pour attaquer.", "Science du coup de bouclier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-l’entraînement-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manœuvre offensive d’entraînement et ce même bonus est ajouté à son DMD lorsqu’un adversaire tente de l’entraîner. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science de l’entraînement. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente de l’entraîner, à moins qu’elle possède également ce don.", "Le personnage traîne ses ennemis sur le champ de bataille avec une grande facilité.", "Science de l’entraînement (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-limprovisation", "Le personnage réduit de moitié le malus lié à l’absence de formation dans le maniement d’une arme ou d’un bouclier ou le port d’une armure. Son bonus aux tests quand il utilise une compétence pour laquelle il ne possède aucun rang passe de +2 à +4.", "Le personnage est un maître de l’improvisation.", "Science de l'improvisation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-lenchaînement-surprise", "Quand le personnage utilise Succession d’enchaînements, tous les adversaires qu’il attaque à son tour (à part le premier) sont privés de leur bonus de Dextérité contre lui.", "Le personnage fait suivre son attaque d’une étonnante série de coups.", "Science de l'enchaînement surprise", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-larme-de-lélu", "Ce don fonctionne comme Arme de l’Élu, à la seule différence que le personnage bénéficie des avantages sur toutes ses attaques jusqu’au début de son tour suivant. Ses attaques gagnent une composante d’alignement de sa divinité (Bonne, Chaotique, Loyale ou Mauvaise) quand il s’agit de passer la réduction de dégâts. Si sa divinité est Neutre, sans autre composante d’alignement, les attaques du personnage passent alors la réduction de dégâts comme si l’arme était en argent et en fer froid.", "Le personnage bénéficie un peu plus encore des faveurs de son dieu lorsqu’il utilise son arme de prédilection.", "Science de l'arme de l'élu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-volonté-de-fer", "Une fois par jour, le personnage peut relancer un jet de Volonté. Il doit décider de le faire avant que le résultat du premier jet soit connu et doit accepter le second résultat, même s’il est pire que le premier.", "Les pensées limpides du personnage lui permettent de résister aux attaques mentales.", "Science de la volonté de fer", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-vigueur-surhumaine", "Une fois par jour, le personnage peut relancer un jet de Vigueur. Il doit décider de le faire avant que le résultat du premier jet soit connu et doit accepter le second résultat, même s’il est pire que le premier.", "Le personnage peut puiser dans ses réserves intérieures pour résister aux maladies, aux poisons et autres effets néfastes.", "Science de la vigueur surhumaine", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-subtilisation-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests de manoeuvre offensive de subtilisation et ce même bonus est ajouté à son DMD lorsqu’un adversaire tente de le subtiliser. Ces bonus se cumulent avec ceux conférés par la version non-mythique de Science de la subtilisation. De plus, le personnage peut effectuer une attaque d’opportunité contre une créature qui tente de le subtiliser, à moins qu’elle possède également ce don.", "Les doigts lestes du personnage lui permettent de subtiliser tous les objets, même si ses ennemis les agrippent fermement.", "Science de la subtilisation (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-linitiative", "Le personnage bénéficie d’un bonus de +4 au test d’initiative.", "Les réflexes du personnage lui permettent de réagir rapidement en cas de danger.", "Science de l'initiative", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tacticien-rusé", null, "À force de négocier avec des pirates, des voleurs et des assassins, le personnage a appris à être très prudent.", "Tacticien rusé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "talent-(mythique)", "Le personnage peut toujours faire 10 ou 20 aux tests de la compétence concernée par le don Talent, même s’il est menacé ou ne peut pas prendre son temps.", "La maîtrise du personnage dans la compétence concernée, même dans les situations les plus compliquées, est sans pareille.", "Talent (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "talent-sauvage-supplémentaire", "Le personnage obtient un talent sauvage d’au moins\r\n2 niveaux de moins que le talent sauvage le plus puissant qu’il\r\nconnaît actuellement. Il peut choisir un talent d’injection ou\r\nnon, mais pas un talent d’explosion ni de défense. S’il possède le\r\npouvoir d’extension élémentaire, il peut choisir un talent sauvage\r\nde n’importe quel élément d’au moins deux niveaux de moins\r\nque le talent sauvage de plus haut niveau qu’il peut actuellement\r\nmanier dans son élément primaire.", "Le personnage bénéficie d’un talent sauvage de plus.", "Talent sauvage supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "viser-juste-(mythique)", "Le personnage peut utiliser ce don même s’il se déplace au cours du round, mais seulement si la distance parcourue est égale ou inférieure à 1,50 mètre par grade.", "Les attaques du personnage trouvent généralement les failles de son adversaire et ignorent de nombreux moyens défensifs.", "Viser juste (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "viser-juste", "Par une action simple, le personnage fait une unique attaque à distance. La cible ne bénéficie plus de son bonus d’armure, d’armure naturelle ni de bouclier à la CA. Le personnage ne peut pas utiliser ce don s’il se déplace au cours du même round.", "Le personnage vise le défaut de l’armure de son adversaire.", "Viser juste", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "viser", "Le personnage peut s’imposer un malus de -1 aux jets d’attaque à distance pour gagner un bonus de +2 aux jets de dégâts sur ces mêmes attaques. Quand son bonus de base à l’attaque atteint +4, et, par la suite, pour chaque tranche de +4 points de bonus, le malus augmente de -1 et le bonus aux dégâts de +2. Le personnage doit choisir s’il veut utiliser ce don avant de lancer les dés pour son jet d’attaque et les effets persistent jusqu’à son prochain tour. Ce bonus aux dégâts ne s’applique pas aux attaques de contact ni aux effets qui n’infligent pas de points de dégâts.", "Le personnage effectue de dangereuses attaques à distance en visant les points faibles de son adversaire mais il diminue ainsi ses chances de toucher.", "Viser", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "visible-et-invisible", "Le personnage gagne un bonus de +2 à tous les jets de sauvegarde contre les effets de scrutation et de divination. De plus, il gagne un bonus de +2 à tous les tests de Discrétion et les elfes, les demi-elfes et les humains subissent un malus de -4 quand ils tentent de suivre sa piste avec la compétence Survie.", "L’anonymat du personnage le rend très difficile à repérer, que ce soit par magie ou par des moyens ordinaires, et il a appris comment combattre ses deux races génitrices.", "Visible et invisible", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-des-profondeurs-(mythique)", "La portée de la vision dans le noir du personnage augmente de 3 mètres par grade.", "La vue aiguisée du personnage peut percer les ténèbres les plus profondes.", "Vision des profondeurs (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-fiélone", "Le tieffelin gagne vision nocturne et la portée de sa vision dans le noir passe à 36 mètres.", "Les yeux du tieffelin voient mieux à faible luminosité et dans les ténèbres.", "Vision fiélone", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-obscure", "Le fetchelin gagne vision dans le noir à 27 mètres (18 c), mais gagne la faiblesse sensibilité à la lumière.", "Suite à une combinaison d’étrange magie des ombres et de chirurgie, les yeux du fetchelin sont modifiés de manière permanente et lui permettent de voir plus loin dans les ténèbres.", "Vision obscure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-prophétique-(mythique)", "Le personnage peut utiliser l’aptitude augure conférée par Vision prophétique à volonté, en plongeant dans une transe profonde pendant dix minutes. Ses chances de réussite augmentent de 1% par grade. Un augure réussi et réalisé par le biais de ce don confère également au personnage les avantages d’une assistance divine mais avec un bonus égal à\r\nson grade. Le bonus de compétence est remplacé par un bonus d’intuition.", "Les pouvoirs prémonitoires du personnage sont d’une aide encore plus précieuse.", "Vision prophétique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-ténébreuse-supérieure", "Le fetchelin gagne la capacité vision dans les ténèbres et perd sa faiblesse sensibilité à la lumière mais gagne la faiblesse aveuglé par la lumière.", "Grâce à ses modifications, le fetchelin a des yeux qui lui permettent de voir clairement non seulement dans les ténèbres normales, mais aussi dans les ténèbres magiques.", "Vision ténébreuse supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-ténébreuse", "Le fetchelin gagne la capacité de voir clairement jusqu’à 4,5 mètres dans les ténèbres magiques telles que celles que crée le sort ténèbres profondes.", "Le fetchelin a des yeux qui lui permettent de percer l’obscurité, même celle des ténèbres magiques.", "Vision ténébreuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-à-travers-le-feu", "L’ifrit peut voir à travers le feu et la fumée sans malus, ignorant tous les bonus de camouflage ou d’abri qu’offrent le feu et la fumée. Cela ne lui permet pas de voir ce qu’il ne peut normalement pas voir (par exemple, les créatures invisibles restent invisibles). Il est immunisé à l’état préjudiciable ébloui11.", "Le feu n’aveugle plus l’ifrit et la fumée ne lui cache pas la vue.", "Vision à travers le feu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "voie-de-lexilé", "Une fois par jour, quand le personnage rate un jet de Volonté contre un effet ou un sort  d’enchantement, il peut le refaire, mais il doit conserver le second résultat, même s’il est pire que le premier.", "Le personnage a passé sa vie à voir les autres l’éviter et à cacher son héritage, il est devenu très résistant à toute tentative d’intrusion dans son esprit.", "Voie de l'exilé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "voie-duale-(mythique)", "Choisissez une voie mythique différente de celle empruntée par le personnage au moment de l’ascension. Il gagne les aptitudes du 1er grade de cette voie (soit l’arcane de l’archimage, la frappe du champion, la montée en puissance divine du hiérophante, le ralliement du protecteur, l’ordre du maréchal ou l’attaque du filou). Chaque fois qu’il gagne une nouvelle aptitude de voie, il peut la sélectionner dans la liste proposée par l’une ou l’autre de ses deux voies, ainsi que dans celle des aptitudes de voie universelles.", "Le personnage suit deux voies mythiques.", "Voie duale (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "voix-de-la-sibylle-(mythique)", "Le personnage gagne un bonus de +1 aux tests de Bluff, Diplomatie et Représentation (déclamation) pour chaque tranche de 3 grades possédés. Ce bonus se cumule avec celui conféré par le don Voix de la sibylle. Si le personnage possède 10 rangs ou plus dans au moins deux de ces compétences, il gagne un bonus de +2 au DD des sorts dépendant du langage qu’il lance.", "Le personnage domine les esprits faibles et vulnérables par le simple son de sa voix.", "Voix de la sibylle (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vol-stationnaire", "Une créature qui possède ce don peut arrêter son mouvement lorsqu’elle est en vol, ce qui lui permet de faire du sur-place sans avoir à réussir un test de Vol.\r\nSi une créature de taille supérieure ou égale à G avec ce don effectue un vol stationnaire à 6 m (4 c) ou moins d’un sol recouvert de nombreux débris, le souffle de ses ailes crée un nuage hémisphérique de 18 m (12 c) de rayon. Les vents ainsi engendrés peuvent éteindre les torches, les petits feux de camp, les lanternes exposées ainsi que les autres flammes non protégées et d’origine non magique. À l’intérieur du nuage, on ne voit bien qu’à 3 m (2 c). De 4,5 m (3 c) à 6 m (4 c), les créatures bénéficient d’un camouflage simple (20% de chances d’échec). À partir de 7,5 m (5 c), les créatures bénéficient d’un camouflage total (50% de chances d’échec et il est impossible d’utiliser la vision pour les repérer).", "La créature peut facilement faire du sur-place ou envoyer dans les airs des nuages de poussières et de gravats.", "Vol stationnaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "volonté-de-fer-(mythique)", "Chaque fois que le personnage effectue un jet de Volonté contre un sort, un pouvoir magique ou un pouvoir surnaturel provenant d’une source non-mythique, il lance deux dés et conserve le meilleur résultat.", "La volonté du personnage est inébranlable.", "Volonté de fer (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "volonté-de-fer", "Le personnage bénéficie d’un bonus de +2 à tous ses jets de Volonté.", "Le personnage est plus résistant aux effets mentaux.", "Volonté de fer", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-renforcée", "Le Degré de Difficulté des jets de sauvegarde contre tous les sorts de l’école choisie lancés par le personnage augmente de +1.", "Le personnage choisit une école de magie contre laquelle ses adversaires auront plus de mal à résister.", "École renforcée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-renforcée-(mythique)", "Le personnage sélectionne une école de magie déjà choisie avec le don École renforcée. Le bonus aux DD des jets de sauvegarde conféré par École renforcée et École supérieure dans l’école de magie choisie augmente de +1. Le personnage peut dépenser une utilisation de pouvoir mythique lors de l’incantation d’un sort issu de l’école choisie pour obliger les cibles du sort à lancer deux fois leur jet de sauvegarde et à conserver le pire.", "Le personnage est particulièrement efficace lorsqu’il manipule sa magie spécialisée.", "École renforcée (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-scorpion", "Pour utiliser ce don, le personnage doit faire une unique attaque à mains nues par une action simple. S’il touche, il inflige des dégâts normaux et réduit la vitesse de déplacement de base de sa cible à 1,5 m pendant un nombre de rounds égal à son modificateur de Sagesse, à moins que la victime réussisse un jet de Vigueur (DD 10 + 1/2 niveau du personnage + modificateur de Sagesse).", "Le personnage frappe son adversaire de ses poings nus et entrave considérablement ses mouvements.", "École du scorpion", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-scorpion-(mythique)", "Lorsqu’il utilise École du scorpion, le personnage ajoute son grade au nombre de rounds pendant lesquels il réduit la vitesse de déplacement de base de la cible. Le personnage peut dépenser une utilisation de pouvoir mythique lorsqu’il effectue son attaque pour ralentir sa cible (effet identique au sort de lenteur), à moins qu’elle réussisse un jet de Vigueur (DD 10 + ½ du niveau du personnage + modificateur de Sagesse du personnage). L’effet de lenteur remplace la réduction normale de la vitesse de déplacement provoquée par École du scorpion, mais sa durée en rounds reste la même.", "Les coups à mains nues du personnage peuvent plonger ses ennemis dans une léthargie proche de la paralysie.", "École du scorpion (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-cogneur", "Par une action complexe, le personnage peut concentrer tout son potentiel d’attaque dans un seul coup de poing dévastateur. Il fait un nombre de jets égal au nombre d’attaques qu’il peut effectuer en une attaque à outrance ou un déluge de coups (à lui de choisir) avec le bonus d’attaque normal pour chaque attaque.\r\nÀ chaque fois qu’il réussit un jet, il comptabilise le nombre normal de dégâts et ajoute les dégâts de tous les jets d’attaque réussis pour connaître les dégâts totaux de son unique coup de poing.\r\nSi l’un des jets d’attaque est un critique potentiel, faites un seul jet de confirmation pour toute l’attaque avec le plus haut bonus de base à l’attaque. S’il est réussi, toute l’attaque est un coup critique confirmé.", "Le personnage concentre toute sa puissance dans un seul coup de poing vicieux et débilitant.", "École du cogneur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-boxeur", "Une fois par round, quand le personnage touche une seule cible avec deux attaques à mains nues, il lui inflige 1d6 points de dégâts supplémentaires, qui se rajoutent aux dégâts de la seconde attaque. S’il touche une seule cible avec trois attaques à mains nues ou plus, il lui inflige 2d6 points de dégâts supplémentaires, qui se rajoutent aux dégâts de sa dernière attaque du round contre la cible.", "Une série de coups rapides inflige plus de dégâts qu’un seul coup de poing circulaire.", "École du boxeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "viser-(mythique)", "Lorsqu’il utilise Viser, le personnage gagne un bonus de +3 aux jets de dégâts à distance à la place d’un +2. Lorsque son bonus de base à l’attaque atteint +4, et toutes les tranches de 4 points de bonus par la suite, le bonus aux jets de dégâts à distance s’élève à +3 au lieu de +2.", "Lorsque le personnage vise, ses tirs sont particulièrement dévastateurs.", "Viser (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-de-lempoigneur", "Quand il utilise ce don, le personnage ne reçoit pas le malus de -4 aux tests de manoeuvre offensive pour agripper un ennemi avec une seule main. De plus, il ne perd pas son bonus de Dextérité à la CA quand il immobilise un adversaire.", "Le personnage est un expert de l’étreinte à une main.", "École de l'empoigneur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "âme-animale", "Le personnage peut se laisser affecter par des sorts et effets normalement réservés aux animaux, aux compagnons animaux et aux montures spéciales, même si ces sorts n’affectent normalement pas les créatures de son type. Il peut, par exemple, lancer croissance animale ou rapetissement d’animal sur sa personne, même si ces sorts n’affectent en principe que les animaux. Un allié peut lancer rappel de compagnon animal sur le personnage afin de le ramener d’entre les morts. Un adversaire ne peut pas lancer charme-animal ou domination d’animal sur le personnage, à moins que ce dernier ne décide d’autoriser le sort à l’affecter comme s’il était un animal.", "Du fait de ses liens étroits avec un animal, le personnage peut utiliser sur lui-même la magie qui affecte les animaux.", "Âme animale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "yeux-de-lynx-(mythique)", "Le personnage ignore le malus aux tests de Perception visuelle provoqué par la distance, tant que celui-ci ne dépasse pas les -10 à la place des -5 habituels. Par une action rapide, le personnage peut dépenser une utilisation de pouvoir mythique pour ignorer la totalité des malus infligés par la distance aux tests de Perception visuel pendant un round.", "La perception visuelle du personnage rivalise en efficacité avec celle des rapaces les plus vigilants.", "Yeux de lynx (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "xénoglossie", "Si le personnage réussit un test de Linguistique DD 25, il peut parler avec un unique individu avec qui il ne partage pourtant pas de langage commun. Il ne peut pas faire 10 sur ce test et ne peut pas recevoir l’aide d’autrui. Le personnage est persuadé de parler sa langue natale et son interlocuteur croit qu’il parle la sienne, en revanche, pour tous les autres auditeurs, il semble que le personnage ne débite que du charabia. Les créatures sous l’effet d’un don des langues ou dotées de langage universel comprennent la Xénoglossie et celles qui réussissent un test de Linguistique DD 25 comprennent l’essentiel de ce que dit le personnage. La Xénoglossie dure jusqu’à la fin de la conversation ou une minute par niveau, selon ce qui se produit en premier. Le personnage peut utiliser de nouveau ce don pour poursuivre la conversation si elle est plus longue et gagne un bonus de +2 au test de Linguistique quand il l’utilise avec quelqu’un avec qui il a déjà communiqué grâce à lui. S’il rate un test de Xénoglossie, il ne peut plus utiliser ce don pour communiquer avec cette créature précise tant qu’il n’a pas gagné un nouveau rang en Linguistique.", "Partout les gens partagent une langue commune, seulement, ils ne s’en souviennent pas toujours.", "Xénoglossie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "voltigeur", "Le personnage obtient un bonus de +2 sur tous ses tests d’Acrobaties et de Vol. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus de cette compétence passe à +4.", "Le personnage est doué pour bondir, sauter et voltiger.", "Voltigeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "voltigeur-(mythique)", "Le bonus aux tests d’Acrobaties et de Vol conféré par Voltigeur augmente de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il a obtenu un 20 naturel à l’un de ces tests. Le personnage doit décider s’il utilise cette aptitude avant de lancer le dé.", "La grâce et la fluidité gestuelles du personnage sont incomparables.", "Voltigeur (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "volonté-héroïque", "Une fois par jour, par une action simple, le personnage peut faire un nouveau jet de sauvegarde contre une condition néfaste qui l’affecte actuellement et à laquelle il aurait pu résister en réussissant un jet de Volonté. Si le personnage est dominé, contrôlé ou s’il ne peut pas faire d’action à cause de l’effet contre lequel il veut tenter un nouveau jet de sauvegarde, il peut faire ce nouveau jet au début de son tour, sans dépenser d’action. En cas de succès, son tour se termine immédiatement. Il ne peut pas utiliser ce don pour se débarrasser d’un effet instantané ou d’un effet qui ne nécessite pas de jet de Volonté ou n’autorise pas de jet de sauvegarde.", "La volonté du personnage est si indomptable qu’elle lui permet de briser ses chaînes mentales.", "Volonté héroïque", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "âme-commune", "Tant que le fantôme du personnage est confiné dans sa conscience, le personnage gagne un bonus de +2 aux jets de sauvegarde contre les effets de mort, l’absorption d’énergie et la possession. De plus, il peut, une fois par jour, quand il rate un jet de sauvegarde contre un effet de mort ou de possession ou quand il reçoit un niveau négatif, dépenser une action immédiate pour dériver l’effet vers la partie fantôme de son âme. L’effet n’affecte pas le personnage mais il est privé des avantages habituels du pouvoir de partage de conscience et ne peut plus manifester son fantôme pendant toute la durée normale du sort ou de l’effet dévié sur le fantôme. Le fantôme subit la totalité de l’effet dévié et tant qu’il en souffre, le personnage ne peut plus dévier d’effet de mort ou de possession ni de niveau négatif sur lui.", "Le personnage et son fantôme sont étroitement liés de coeur, d’esprit et d’âme.", "Âme commune", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "virage-sur-laile", "Une fois par round, une créature possédant ce don peut virer de 180 degrés par une action libre sans avoir à réussir un test de Vol. Ce virage ne réduit pas non plus la distance que la créature peut parcourir.", "La créature peut virer facilement lorsqu’elle est en vol.", "Virage sur l'aile", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vigueur-surhumaine", "Le personnage bénéficie d’un bonus de +2 à tous ses jets de Vigueur.", "Le personnage est plus résistant aux poisons, aux maladies et autres effets néfastes.", "Vigueur surhumaine", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vigueur-surhumaine-(mythique)", "Chaque fois que le personnage effectue un jet de Vigueur contre un sort, un pouvoir magique ou un pouvoir surnaturel produit par une source non-mythique, il lance deux fois le dé et conserve le meilleur résultat.", "La vigueur du personnage est sans pareil.", "Vigueur surhumaine (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-monté", "Le malus que subit normalement un personnage monté qui utilise une arme à distance est divisé par deux : -2 au lieu de -4 si la monture effectue un déplacement double, et -4 au lieu de -8 si elle galope.", "Le personnage sait attaquer à distance du haut d’une monture.", "Tir monté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-monté-(mythique)", "Lorsqu’il utilise la compétence Équitation pour que sa monture lui serve d’abri, le personnage peut réaliser une unique attaque d’arme à distance par une action simple. Pendant le déplacement de sa monture, il peut dépenser une utilisation de pouvoir mythique pour annuler les malus infligés par le fait de réaliser des attaques d’arme à distance sur une monture, et ce jusqu’au début de son prochain tour.", "Les compétences de cavalier du personnage lui permettent de s’abriter et de tirer avec une précision stupéfiante.", "Tir monté (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-lobé", "Lorsque le personnage fait une attaque à distance, il peut ignorer tous les bonus que confèrent un abri à la CA, ce qui inclut l’abri total, sauf si la cible bénéficie d’un abri au-dessus d’elle tels qu’un toit ou la canopée d’une forêt. Lorsque ce don est utilisé, le personnage doit considérer sa cible comme si elle se situait à un facteur de portée plus loin. Ce don ne peut pas être utilisé tant qu’il n’y a pas un espace au-dessus du personnage égal au minimum à la moitié de la portée de son attaque.", "Le personnage peut réaliser un haut tir parabolique pour toucher un ennemi par dessus.", "Tir lobé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-impitoyable", "Lorsque le personnage réussit un croc-en-jambe avec une attaque à distance sur un adversaire situé dans un rayon de 9m (6c), le personnage le menace jusqu’à son prochain tour. Il peut alors effectuer des attaques d’opportunités contre cet adversaire avec ses armes à distances. Cet effet prend fin si le personnage attaque une autre cible ou si l’adversaire se déplace d’1,5m (1c).", "Le personnage garde une visée stable lorsque son adversaire tombe au sol.", "Tir impitoyable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-en-salve", "Le personnage gagne un bonus de +1 àses jets d’attaques à distance pour chaque allié qui possède ce don et qui a effectué une attaque à distance depuis la fin de votre dernier tour sur une cible qui se situe à moins de 4.5m (3c) de la votre, pour un bonus maximum de +4. Ces alliés ne fournissent pas d’abri aux adversaires contre les attaques du personnage.", "Le personnage s’est entrainé à travailler en rang d’archers afin de lancer une pluie de flèches sur ses adversaires.", "Tir en salve", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-en-mouvement", "Au prix d'une action complexe, le personnage peut effectuer une unique attaque à l'aide d'une arme à distance et peut se déplacer avant et après son attaque, du moment que la distance totale parcourue pendant le round ne dépasse pas sa vitesse de déplacement.", "Le personnage peut se déplacer, tirer et se déplacer à nouveau avant même que son ennemi puisse réagir.", "Tir en mouvement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-rapide-(mythique)", "Lorsqu’il utilise Tir rapide, le personnage peut soit ignorer le malus de -2 aux jets d’attaque imposé par le don, soit effectuer deux attaques supplémentaires au lieu d’une seule.", "Chaque round, le personnage peut tirer de nombreuses fois avec une précision stupéfiante.", "Tir rapide (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-en-mouvement-(mythique)", "Lorsqu’il utilise Tir en mouvement, le personnage peut effectuer deux attaques à distance au lieu d’une seule, à n’importe quel moment lors de son déplacement, en appliquant son bonus de base à l’attaque (BBA) le plus élevé.", "Le personnage peut facilement décocher deux tirs tout en se déplaçant rapidement sur le champ de bataille.", "Tir en mouvement (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-de-précision", "Le personnage peut tirer ou lancer une arme de jet sur un adversaire engagé dans un combat au corps à corps sans subir de malus à son jet d'attaque.", "Le personnage sait viser au cœur d’une mêlée.", "Tir de précision", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-de-loin", "Si le personnage utilise une arme à projectiles, il souffre seulement d’un malus de -1 par facteur de portée qui le sépare de sa cible.", "Le personnage est plus précis quand il tire de loin.", "Tir de loin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-de-loin-(mythique)", "Par une action rapide, le personnage peut dépenser une utilisation de pouvoir mythique pour ignorer tous les malus de facteur de portée lors des attaques à distance effectuées jusqu’à la fin de son tour.", "Les tirs à longue distance du personnage sont réalisés avec une précision remarquable.", "Tir de loin (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-de-couverture", "Le personnage peut utiliser une action aider quelqu'un combinée à une attaque à distance contre un adversaire, même si celui-ci ne menace pas ses alliés. Les pénalités liées au facteur de portée s’appliquent au jet d'attaque. Le personnage doit désigner un allié avant l’attaque. Si celle-ci réussit, cet allié gagne un bonus de +2 à sa CA contre la prochaine attaque de l’ennemi ciblé, tant que celle-ci s’effectue avant le début du prochain tour du personnage qui le couvre. Tous les alliés qui possèdent ce don obtiennent également le bonus contre cet ennemi.", "Le personnage distrait ses ennemies avec des attaques à distances afin de protéger ses alliés.", "Tir de couverture", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tatouage-mystique", null, "Le personnage possède des tatouages complexes qui l’inspirent et renforcent ses aptitudes magiques naturelles. Ils le désignent comme un adepte des anciennes traditions magiques. Le tatouage se compose généralement d’une longue suite de caractères complexes issus d'une langue ancienne.", "Tatouage mystique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "talent", "Le personnage bénéficie d’un bonus de +3 à tous les tests concernant la compétence choisie. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence passe à +6.", "Le personnage choisit une compétence pour laquelle il sera particulièrement doué.", "Talent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-de-sommation", "Le personnage peut effectuer une attaque de contact à distance par une action simple en utilisant une arme à distance de prédilection. S’il réussit, au lieu d’infliger des dégâts ou d’autres effets liés à l’attaque, il peut tenter un jet d’intimidation par une action libre afin de démoraliser sa cible. La cible n’a pas besoin de se situer à moins de 9m (6c) du tireur, comme elle n’a pas besoin de le voir ni de l’entendre.", "Un tir intentionnellement raté de justesse démontre le talent effroyable du personnage avec une arme à distance.", "Tir de sommation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sang-angélique", "L’aasimar gagne un bonus de +2 à ses jets de sauvegarde contre les effets du registre Mal et à ses tests de Constitution pour se stabiliser lorsqu’il est réduit à un nombre de points de vie négatif (mais pas mort). De plus, chaque fois qu’il reçoit des dégâts de saignement ou d’absorption de sang, toute créature morte-vivante ou de sous-type Mal adjacente à l’aasimar reçoit également 1 point de dégâts.", "Le sang du personnage est empreint d’une puissance sacrée.", "Sang angélique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-rapide", "Lorsqu’il effectue une action d’attaque à outrance avec une arme à distance, le personnage a droit à une attaque supplémentaire. Toutes les attaques du round subissent un malus de -2.", "Le personnage peut faire une attaque à distance supplémentaire.", "Tir rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-à-bout-portant", "Le personnage bénéficie d’un bonus de +1 aux jets d’attaque et de dégâts avec n’importe quelle arme à distance, tant que sa cible est distante de 9 m ou moins.", "Le personnage est encore plus précis quand il tire sur des cibles proches.", "Tir à bout portant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vigilance", "Le personnage obtient un bonus de +2 sur tous ses tests de Perception et de Psychologie. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence augmente de +4.", "Le personnage remarque plus facilement les détails que les autres personnes.", "Vigilance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vigilance-(mythique)", "Le bonus aux tests de Perception et Psychologie conféré par Vigilance augmente de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il a obtenu un 20 naturel à l’un de ces tests. Le personnage doit décider s’il utilise cette aptitude avant de lancer le dé.", "Les capacités de perception du personnage dépassent de loin celles du commun des mortels, qu’il les utilise pour observer son environnement ou pour deviner les intentions d’autrui.", "Vigilance (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "venin-de-sommeil", "Par une action rapide, le vishkanya peut altérer les effets de son venin afin que sa cible tombe inconsciente.\r\nIl modifie ainsi les effets principal et secondaire de son venin pour donner ceci : effet principal chancelant pendant 1d4 rounds ; effet secondaire inconscient pendant 1 minute. Il doit prendre la décision d’altérer son venin avant de l’appliquer à son arme.", "Le vishkanya peut modifier la nature de sa salive toxique pour endormir ses ennemis.", "Venin de sommeil", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vengeance-sanguinaire", "Quand le personnage voit un de ses alliés mourir ou passer en dessous de 0 point de vie, il peut, à son tour et par une action libre, entrer dans un état de rage similaire à celui du barbare, en moins puissant. Si le personnage possède déjà la rage comme pouvoir de classe et qu’il est déjà en rage, ses bonus de moral à la Force et à la Constitution augmentent de +2 pendant le reste de sa rage.\r\nS’il n’a pas le pouvoir de classe de rage ou s’il ne lui reste plus de rounds quotidiens, cette forme de rage amoindrie lui donne les mêmes bonus et les mêmes malus que la rage du barbare, sauf que ses bonus de moral à la Force et à la Constitution ne sont que de +2. Dans tous les cas, ce pouvoir fait effet pendant 4 rounds. Comme avec la rage de barbare, quand celle-ci se termine, le personnage est fatigué, mais si un autre allié tombe avant que la rage se dissipe, celle-ci se prolonge de 4 rounds.\r\nCe don ne lui permet pas d’entrer en rage quand il est fatigué. Le personnage peut utiliser ce pouvoir uniquement si l’allié tombé a au moins autant de DV que lui (à l’exclusion des alliés invoqués ou convoqués).", "Quand le personnage voit un allié tomber au combat, il se laisse gagner par une fureur meurtrière.", "Vengeance sanguinaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vendetta-arcanique", null, "Les mauvais traitements qu’un groupe usant de magie ont infligés au personnage l’ont poussé à chercher activement à faire du mal à ceux qui utilisent la magie.", "Vendetta arcanique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ténacité-féroce", "Une fois par jour, quand le personnage est enragé et qu’il se fait toucher par une attaque qui devrait le tuer (en faisant passer son total de points de vie négatifs à une valeur égale à sa valeur de Constitution), il peut dépenser 1 round de rage ou plus, par une action immédiate, pour annuler une partie de ces dégâts et rester en vie. Chaque round de rage sacrifié annule 1 point de dégâts, mais ce pouvoir ne permet pas de réduire les dégâts subits en dessous de 1 point.<br>\r\nPar exemple, si le personnage est enragé, qu’il a une valeur de Constitution de 18 avec la rage et qu’il se trouve actuellement à 2 pv alors qu’il reçoit un coup à 20 points de dégâts (qui suffit donc à le faire passer à -18 pv et à le tuer), il peut dépenser 1 round de rage pour réduire ces dégâts de 1 (ce qui le laisse dangereusement proche de la mort, à -17 pv). S’il dépense 19 rounds de rage, il réduit les dégâts reçus à 1 point et se retrouve donc avec 1 pv.", "Le personnage crache au visage de la mort.", "Ténacité féroce", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-à-bout-portant-(mythique)", "Le bonus aux jets d’attaque et de dégâts conféré par Tir à bout portant s’élève désormais à +2. Par une action rapide, le personnage peut dépenser une utilisation de pouvoir mythique pour gagner un bonus supplémentaire sur ces jets égal à son grade.", "La précision dont fait preuve le personnage lorsqu’il tire à bout portant est sans pareil.", "Tir à bout portant (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "travail-en-équipe-légendaire-(mythique)", "La totalité des bonus numériques fixes conférés au personnage par l’utilisation des dons de travail en équipe augmente de 1. Ceci ne s’applique pas aux bonus numériques variables ou aux effets qui ne confèrent aucun bonus numérique, tels que des attaques d’opportunité, un déplacement ou divers autres actions supplémentaires.", "Le personnage et ses alliés combattent en faisant preuve d’une grande coordination.", "Travail en équipe légendaire (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "transfert-de-sort-distant", "La portée du transfert de sort de la créature alliée devient proche (7.5m (5c) + 1.5m (1c) par tranche de 2 niveaux) tant que le personnage posséde une ligne d’effet jusqu’à sa créature lorsqu’il lance le sort. Si sa créature a le pouvoir Conduit, il peut la désigner en tant que source du sort de contact qu’il lance tant qu’il reste proche d’elle. La créature doit toujours utiliser le sort de contact en utilisant sa propre portée de contact.", "Le personnage peut réaliser un transfert de sort et octroyer des sorts de contact à son compagnon sur une plus grande distance.", "Transfert de sort distant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "traction-exceptionnelle", "Lorsque le personnage tient en main une arme à distance dont il connaît le maniement et qui possède un facteur de Force, ce facteur de Force augmente de 2. De plus, s'il connaît le maniement d'une arme, le personnage ne subit pas de pénalité sur ses jets d’attaque lorsqu'il possède un modificateur de Force inférieur au facteur de Force de cette arme.\r\nEn vo : Exceptional Pull (Combat)\r\nYou have mastered techniques to get the most out of composite bows.\r\nPrerequisites: Dex 13, Deadly Aim, base attack bonus +3.\r\nBenefit: When you wield a ranged weapon that you are\r\nproficient with and that has a strength rating, add 2 to\r\nthe weapon’s strength rating. You don’t take a penalty on\r\nattack rolls for having a Strength modif ier lower than the\r\nstrength rating of a weapon, provided you’re proficient\r\nwith that weapon.", "Le personnage a maîtrisé des techniques qui lui permettent de tirer le meilleur des arcs composites.", "Traction exceptionnelle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "traction-de-lempoigneur", "Quand le personnage utilise École de l’empoigneur et qu’il entreprend une action de mouvement en lutte, il peut se déplacer tout en traînant une unique cible agrippée à sa vitesse maximale plutôt qu’à la moitié de sa vitesse. Après cela, il peut dépenser une action de mouvement pour se déplacer, tout en traînant la cible agrippée, à la moitié de sa vitesse sans avoir besoin de faire de test de manoeuvre offensive supplémentaire. Il ne peut pas utiliser ce don s’il agrippe deux cibles.", "Le personnage se déplace facilement en traînant un ennemi agrippé.", "Traction de l'empoigneur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "touche-d’ombre", "Quand le wayang est dans une zone de faible lumière ou de ténèbres, il peut se déplacer à sa vitesse maximale en utilisant [Pathfinder-RPG.Discrétion|Discrétion] sans subir le malus normal de -5. Quand il se déplace à une vitesse allant de la moitié de sa vitesse normale à sa vitesse normale, le wayang subit un malus de -5 à ses tests de [Pathfinder-RPG.Discrétion|Discrétion].", "À faible luminosité, le wayang est à peine plus distinct qu’une ombre.", "Touche d’ombre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "totem-spirituel", null, "Le personnage possède des liens mystiques avec le totem sacré de sa tribu.", "Totem spirituel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tireur-astucieux", "Le personnage gagne un bonus de +2 au manoeuvre de combat à distance. Lorsqu’il tente une telle manoeuvre, le personnage peut s’abstenir de faire des dégâts à sa cible sans qu’aucune pénalité au jet de manoeuvre ne soit appliquée.", "La visée fantastique du personnage permet de mettre hors jeu son adversaire sans le blesser.", "Tireur astucieux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "transmettre-sa-chance", "Le personnage peut utiliser la Chance insolente une fois de plus par jour. Par une action immédiate, il peut aussi faire bénéficier un allié qui le voit et l’entend de sa Chance inexplicable.", "Le personnage est extrêmement chanceux et, parfois, cela déteint sur ses alliés.", "Transmettre sa chance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "révélation-supplémentaire", "Le personnage gagne une révélation supplémentaire. Il doit en remplir les conditions d’accès.", "Le personnage a découvert un nouvel aspect de son mystère.", "Révélation supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "résolution-féroce", "Le personnage gagne le pouvoir de monstre universel férocité, qui lui permet de continuer à se battre même s’il est en dessous de 0 point de vie. Quand il utilise ce pouvoir, il gagne un bonus de +2 aux tests d’Intimidation.", "Le sang orque du personnage lui permet de continuer à se battre.", "Résolution féroce", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "résolution-courageuse", "Si le personnage a le trait racial sans peur, son bonus racial aux jets de sauvegarde contre les effets de terreur passe à +4. S’il a le trait veule, il subit toujours un malus de -2 à ces mêmes jets de sauvegarde, mais il peut profiter des bonus de moral aux jets de sauvegarde contre les effets de terreur.", "Le personnage défend sa position, même quand les autres s’enfuient.", "Résolution courageuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maléfice-maudit-(mythique)", "Quand le personnage cible pour la deuxième fois une créature avec l’un de ces maléfices, celle-ci doit lancer deux jets de sauvegarde et conserver le pire.", "Les maléfices du personnage irradient de puissance persistante.", "Maléfice maudit (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maniement-des-armes-courantes", "Lorsqu’il utilise une arme courante, le personnage fait ses jets d’attaque normalement.\r\nNormal. Un personnage qui utilise une arme sans être formé à son maniement subit un malus de -4 aux jets d’attaque.", "Le personnage est formé au maniement des armes courantes.", "Maniement des armes courantes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maniement-des-boucliers", "Lorsque le personnage utilise un bouclier (autre qu’un pavois) au combat, le malus d’armure du bouclier s’applique alors uniquement aux compétences basées sur la Force et la Dextérité.\r\nNormal. Le malus d’armure aux tests imposé par un bouclier au maniement duquel le personnage n’est pas formé s’applique aux jets d’attaque et à tous les tests de caractéristique ou de compétence qui demandent de se déplacer.", "Le personnage sait manier un bouclier.", "Maniement des boucliers", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maniement-du-pavois", "Lorsque le personnage utilise un pavois, le malus d’armure du bouclier s’applique alors uniquement aux compétences basées sur la Force et la Dextérité.\r\nNormal. Le malus d’armure aux tests qu’impose un bouclier au port duquel on n’est pas formé s’applique aux jets d’attaque et à tous les tests de caractéristique ou de compétence qui demandent de se déplacer, y compris aux tests d’Équitation.", "Le personnage sait se servir des pavois.", "Maniement du pavois", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maniement-dune-arme-de-guerre", "Lorsqu’il utilise l’arme de guerre choisie, le personnage fait ses jets d’attaque normalement.\r\nNormal. Un personnage qui utilise une arme sans être formé à son maniement subit un malus de -4 aux jets d’attaque.", "Le personnage choisit une arme de guerre et sait se battre avec.", "Maniement d'une arme de guerre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maniement-dune-arme-exotique", "Lorsqu’il utilise l’arme exotique choisie, le personnage fait ses jets d’attaque normalement.\r\nNormal. Un personnage qui utilise une arme sans être formé à son maniement subit un malus de -4 aux jets d’attaque.", "Le personnage choisit une arme exotique, comme la chaîne cloutée ou le fouet. Il sait alors comment s’en servir au combat et sait comment utiliser ses aptitudes particulières.", "Maniement d'une arme exotique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "manipulation-partagée", "Par une action de mouvement, le personnage donne un bonus de +2 aux tests de Bluff ou d’Intimidation (choisir la compétence à chaque utilisation) de tous les alliés qui se trouvent à moins de 9 m (6 c) et peuvent le voir ou l’entendre. L’effet dure un nombre de rounds égal au modificateur de Charisme du personnage (1 round au minimum).", "Le personnage peut renforcer les capacités de ses alliés pour qu’ils bernent et énervent plus facilement leurs ennemis.", "Manipulation partagée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "manœuvres-agiles", "Le personnage ajoute son bonus de Dextérité à son bonus de base à l’attaque et à son bonus de taille quand il s’agit de déterminer son bonus de manœuvre offensive au lieu de son bonus de Force.", "Quand le personnage exécute une manœuvre offensive, il sait utiliser sa rapidité au lieu de recourir à la force brute.", "Manœuvres agiles", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "manœuvres-de-tir-ami", "Les alliés qui possèdent également ce don ne fournissent pas d’abri mou aux ennemis, permettant au personnage de faire des attaques d’opportunités contre l’un d’eux même si ses alliés le font bénéficier d’un abri mou contre les attaques de cet adversaire. Dans le cas où un allié qui possède ce don lance un sort à zone d’effet et que le personnage se trouve dans son aire d’effet, si ce sort permet un jet de sauvegarde de réflexes pour éviter l’effet (tel que Boule de feu), le personnage gagne un bonus d’esquive de +4 pour ce jet de sauvegarde.", "Le personnage et ses équipiers anticipent les attaques à distances de chacun d’entre eux et ainsi évitent de se gêner lors de leurs attaques.", "Manœuvres de tir ami", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-artisan", "Le personnage choisit une compétence d’Artisanat ou de Profession dans laquelle il a au moins 5 rangs. Il reçoit un bonus de +2 aux tests de cette compétence. Les rangs dont il dispose dans cette compétence comptent comme niveau de lanceur de sorts quand il s’agit de savoir s’il répond aux critères des dons de Création d’armes et armures magiques et de Création d’objets merveilleux.\r\nIl peut créer des objets magiques à l’aide de ces dons en utilisant son rang dans la compétence choisie comme niveau de lanceur de sorts. Il doit se servir de la compétence choisie pour faire le test de création de l’objet. Le DD de création augmente tout de même en fonction des conditions nécessaires pour lancer le sort (voir les règles de création d’objets magiques). Le personnage ne peut pas utiliser ce don pour créer un déclencheur de sorts ou un objet qui s’active avec un sort.", "Les talents d’artisan du personnage lui permettent de créer des objets magiques simples.", "Maître artisan", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-boxeur", "Quand le personnage utilise École du boxeur, les dégâts supplémentaires qu’il inflige quand il touche une cible avec deux attaques à mains nues passent à 2d6, et à 4d6 quand il la touche avec trois attaques à mains nues ou plus.", "Les coups de poing rapides du personnage sont encore plus fatals.", "Maître boxeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-brasseur", "Le personnage gagne un bonus de +2 aux tests d’Artisanat (alchimie) et de profession (brasseur) et ajoute 1 au DD de tous les poisons ingérés qu’il crée.", "Le personnage sait concocter de puissants breuvages.", "Maître brasseur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-des-duels", null, "Le personnage maîtrise l’épuisant style de combat des seigneurs des épées de duel.", "Maître des duels", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-des-morts-vivants-(mythique)", "Lorsqu’il lance animation des morts ou utilise le don Contrôle des morts-vivants, le personnage ajoute son grade à son niveau de lanceur de sorts pour déterminer le nombre de dés de vie de morts-vivants animés. Cet effet se cumule avec le niveau de lanceur de sorts augmenté par le don Maître des morts-vivants. Lorsqu’il lance animation des morts, la durée du sort est triplée au lieu d’être doublée.", "Grâce à sa nature mythique effrayante, le personnage règne sans partage sur les morts-vivants.", "Maître des morts-vivants (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-empoigneur", "Quand le personnage agrippe deux adversaires en utilisant École de l’empoigneur, il peut utiliser sa prise pour déplacer ou blesser l’un ou les deux adversaires agrippés plutôt qu’un seul.", "Le personnage peut agripper deux ennemis aussi facilement qu’un seul.", "Maître empoigneur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-de-larmure-magique", "Par une action rapide, le personnage réduit les risques d’échec des sorts dus à l’armure de 20% pour tout sort lancé au cours de ce round. Ce bonus remplace celui accordé par le Port de l’armure magique et ne se cumule pas avec lui.", "Le personnage maîtrise l’art de lancer des sorts en armure.", "Maîtrise de l'armure magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-des-armes-improvisées", "Le personnage ne subit aucun malus quand il utilise une arme improvisée. Il augmente les dégâts de cette arme d’un cran (par exemple de 1d4 à 1d6) jusqu’à un maximum de 1d8 (2d6 si l’arme improvisée est à deux mains). Cette arme inflige un éventuel coup critique sur 19-20 et possède un multiplicateur de critique de x2.", "Le personnage peut transformer n’importe quel objet en arme, qu’il s’agisse d’un pied de chaise tranchant ou d’un sac de farine.", "Maîtrise des armes improvisées", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "métamagie-spontanée-(mythique)", "Le personnage peut changer le sort sur lequel s’applique le don Métamagie spontanée chaque matin dès qu’il peut à nouveau utiliser les emplacements de sort dépensés. Le sort concerné peut être de n’importe quel niveau, mais il doit être valide au regard du don Métamagie spontanée.", "Le personnage applique les techniques métamagiques sur ses sorts avec bien plus d’aisance que les autres lanceurs de sorts spontanés.", "Métamagie spontanée (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "musique-flamboyante-(mythique)", "Lorsque le personnage utilise Musique flamboyante pour transformer les dégâts d’un sort de barde en dégâts de feu, ces dégâts ignorent les résistances au feu dont la valeur est inférieure ou égale à son grade. Si le personnage lance convocation de monstres avec un sort de barde et choisit de conférer à la créature convoquée une apparence flamboyante, la résistance au feu de cette créature augmente de 5 (et s’ajoute à celle déjà conférée par Musique flamboyante). Ses attaques naturelles infligent 1d4 points de dégâts de feu supplémentaires à la place du point de dégâts de feu supplémentaire normalement octroyé par Musique flamboyante. Les créatures dotées du sous-type de feu gagnent également ces avantages.", "Le personnage mêle les antiques mélodies du feu à l’incantation de ses sorts.", "Musique flamboyante (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mort-venue-du-ciel-(mythique)", "Lorsque le personnage charge un adversaire depuis une position surélevée ou s’il vole, le multiplicateur de critique de son arme augmente de 2 (jusqu’à un maximum de ×6). Cet effet ne se cumule pas avec celui d’autres aptitudes qui augmentent par ailleurs le multiplicateur de critique d’une arme. Si l’attaque de charge touche au but, le personnage peut accomplir une manœuvre offensive de croc-en-jambe contre la cible de la charge. Cette action est une action libre qui ne provoque aucune attaque d’opportunité.", "Lorsqu’il attaque d’une position surélevée ou directement depuis les airs, le personnage atterrit sur ses ennemis en produisant un effet dévastateur.", "Mort venue du ciel (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "montée-en-puissance-chanceuse-(mythique)", "Chaque fois que le personnage utilise son aptitude de montée en puissance, il peut lancer deux fois son dé de montée en puissance et conserver le meilleur résultat. S’il dispose déjà de cette capacité, il peut lancer trois fois son dé de montée en puissance et conserver le meilleur résultat.", "Les montées en puissance du personnage défient le destin.", "Montée en puissance chanceuse (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "menace-viscérale", "Le personnage peut utiliser l’Intimidation au lieu du Bluff pour tenter une feinte contre une créature à portée d’allonge.", "Le personnage est si doué pour effrayer ses adversaires que ces derniers hésitent.", "Menace viscérale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-martiale", "Chaque don de combat que le personnage possède et qui s’applique à une arme spécifique seulement (comme Arme de prédilection) s’applique à présent à toutes les armes du même groupe.", "Le personnage améliore ses connaissances en armes pour les appliquer à toutes les armes de même type.", "Maîtrise martiale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mains-du-croyant", "Une fois par jour, le personnage peut utiliser le pouvoir d’imposition des mains du paladin. Son niveau effectif de paladin est égal à 1/2 son niveau de personnage (1 au minimum). S’il possède déjà le pouvoir d’imposition des mains (ou le gagne plus tard), il peut, à la place, gagner une utilisation supplémentaire par jour d’imposition des mains.", "Le personnage peut soigner les autres d’un simple contact de ses mains.", "Mains du croyant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-critique", "Quand le personnage assène un coup critique, il peut ajouter les effets de deux dons de critique aux dégâts infligés.", "Les coups critiques du personnage provoquent deux effets supplémentaires.", "Maîtrise du critique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-combat-défensif", "Le personnage remplace son bonus de base à l’attaque par son nombre total de DV quand il calcule son degré de manœuvre défensive.", "Le personnage excelle à se défendre contre toutes les manœuvres offensives.", "Maîtrise du combat défensif", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-combat-défensif-(mythique)", "Ajoutez la moitié du grade mythique du personnage à son DMD.", "Au combat, les capacités défensives du personnage sont sans pareil.", "Maîtrise du combat défensif (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-bouclier", "Le personnage ne subit pas le moindre malus aux jets d’attaque portés avec un bouclier quand il manie une autre arme. Il ajoute le bonus d'altération du bouclier aux jets d’attaque et de dégâts du bouclier comme s’il s’agissait du bonus d’altération d'une arme.", "Le personnage sait si bien manier son bouclier qu’il ne le gêne pas au combat.", "Maîtrise du bouclier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-des-talents-multiples", "Le personnage considère toutes ses classes comme des classes de prédilection. Quand il gagne un niveau dans l’une d’elles, il gagne soit un point de vie, soit un point de compétence. Ces bonus s’appliquent de façon rétroactive à tous les niveaux de classe qui n’en avaient pas encore bénéficié.", "Le personnage est un adepte de nombreuses disciplines.", "Maîtrise des talents multiples", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-des-sorts", "À chaque fois que le personnage choisit ce don, il peut sélectionner un nombre de sorts égal à son modificateur d’Intelligence parmi les sorts de magicien qu’il connaît déjà. Il peut alors préparer les sorts sélectionnés sans consulter son grimoire.", "Le personnage maîtrise une petite poignée de sorts pour lesquels il n’a pas besoin de se référer à son grimoire.", "Maîtrise des sorts", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-des-sorts-(mythique)", "Le personnage peut préparer les sorts sélectionnés avec le don Maîtrise des sorts par une action complexe. Pour les autres, il doit toujours passer autant de temps que la normale. Par une action complexe, il peut dépenser une utilisation de pouvoir mythique pour préparer la totalité des sorts sélectionnés avec le don Maîtrise des sorts.", "Le personnage connaît si bien certains sorts qu’il peut les préparer avec une grande facilité.", "Maîtrise des sorts (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-critique-(mythique)", "Lorsqu’il utilise Maîtrise du critique, le personnage augmente le nombre de dons de critique qu’il peut appliquer pour chaque tranche de trois grades.", "Les coups critiques du personnage sont d’une nature polyvalente et dévastatrice.", "Maîtrise du critique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "main-de-feu", "Le gobelin peut manier une torche comme une arme sans subir le malus qu’impose la non-maîtrise de l’arme et gagne un bonus de +1 à ses jets d’attaque avec les armes de corps à corps infligeant des dégâts de feu.", "Né avec une torche dans la main, le gobelin a un don pour tout ce qui brûle.", "Main de feu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "magie-des-nains-gris", null, "Le personnage obtient un nouveau pouvoir magique issu de la liste des traits raciaux duergars.", "Magie des nains gris", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "magie-des-glyphes", null, "L’intimité du personnage avec les runes et les mystères des anciens arcanes lui a appris comment améliorer la puissance de nombre d’écrits magiques. ", "Magie des glyphes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "incantation-rapide", "L’incantation du sort ne prend qu’une action rapide et le personnage peut se livrer à une autre action, y compris jeter un second sort, au cours du même round. Si le temps d’incantation du sort est supérieur à un round, on ne peut pas l’accélérer avec ce don.\r\nUn sort à incantation rapide nécessite un emplacement de sort de quatre niveaux de plus que son niveau réel. Lancer un sort à incantation rapide ne provoque pas d’attaque d’opportunité.", "Le personnage peut lancer un sort en une fraction de seconde.", "Incantation rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "incantation-animale", "Le personnage peut utiliser un sort à composantes verbales et gestuelles alors qu’il est sous forme animale. Il les remplace par divers bruits et gestes.\r\nIl est aussi possible d’utiliser des composantes matérielles ou des focaliseurs en possession du personnage, même si ceux-ci sont fondus dans sa forme actuelle. Ce don ne permet pas d’utiliser des objets magiques qu’un animal ne pourrait pas utiliser normalement, pas plus qu’il ne permet de parler sous forme animale.", "Le personnage peut lancer des sorts même s’il est sous une forme qui ne devrait pas le lui permettre.", "Incantation animale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "incantation-animale-(mythique)", "Lorsqu’il utilise forme animale, le personnage peut utiliser les objets à fin d’incantation et à potentiel magique qu’il portait au moment de son changement de forme. Il n’a pas besoin de manipuler physiquement ces objets quand il les utilise sous forme animale. De plus, il peut parler normalement lorsqu’il est sous forme animale.", "Le personnage peut parler et utiliser certains objets magiques lorsqu’il est sous forme animale.", "Incantation animale (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "improvisation", "Le personnage gagne un bonus de +2 aux tests quand il utilise une compétence pour laquelle il ne possède aucun rang. De plus, il peut utiliser toutes les compétences sans formation.", "Le personnage est capable d’accomplir n’importe quoi grâce à ses facultés d’improvisation.", "Improvisation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "imposition-des-mains-supplémentaire", "Le personnage utilise imposition des mains deux fois de plus par jour.", "Le personnage utilise l’imposition des mains plus souvent.", "Imposition des mains supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "immobiliser", "Quand un adversaire déclenche une attaque d’opportunité en traversant une case adjacente à celle du personnage, ce dernier peut faire un test de manœuvre offensive en tant qu’attaque d’opportunité. S’il réussit, son adversaire ne peut plus se déplacer pendant le reste de son tour. Il peut toujours effectuer le reste de ses actions mais il ne peut plus bouger. Ce don s’applique aussi aux créatures qui essaient de quitter une case adjacente si ce mouvement provoque une attaque d’opportunité.", "Le personnage arrête les ennemis qui tentent de le dépasser.", "Immobiliser", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "incantation-silencieuse", "Un sort à incantation silencieuse ne nécessite pas de composante verbale. Les sorts sans composante verbale ne sont donc pas affectés. Un sort à incantation silencieuse nécessite un emplacement de sort d’un niveau de plus que son niveau réel.", "Le personnage peut lancer un sort sans émettre le moindre son.", "Incantation silencieuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "immense-haine", "Le personnage choisit deux types de créatures (et leur sous-type si besoin) issus de la liste des ennemis jurés du rôdeur et leur applique son bonus d’attaque de +1 issu de son trait racial haine.", "La haine du personnage sort de l’ordinaire.", "Immense haine", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "héritage-racial-(mythique)", "Le personnage gagne un unique trait racial qu’il sélectionne dans la liste de la race qu’il a choisie lorsqu’il a pris le don non-mythique d’Héritage racial. Ce trait racial ne peut modifier la taille ou les valeurs de caractéristique du personnage.Il apprend également le langage racial de la race (le cas échéant) s’il ne le connaît pas déjà. Dans le cas des races possédant plusieurs langages raciaux, le personnage les apprend tous.", "L’héritage racial du personnage s’intensifie en se mêlant à son pouvoir mythique.", "Héritage racial (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "homme-félin-modèle", "L’homme-félin peut prendre le don Aspect bestial même s’il ne remplit pas les conditions requises. De plus, sa nature féline se manifeste de l’une des manières suivantes. Il choisit sa manifestation lorsqu’il prend ce don et ne peut plus en changer.<br/>\r\n* Griffes acérées (Ext). Si le personnage n’a pas le trait racial griffes du félin ou la manifestation griffes bestiales lui venant du don Aspect bestial, il gagne le trait racial griffes du félin. S’il a le trait racial griffes du félin ou la manifestation griffes bestiales, les dégâts de son attaque de griffe passent à 1d6.<br/>\r\n* Sens améliorés (Ext). Si le personnage a vision nocturne, il gagne le trait racial d’homme-félin odorat. S’il a le trait racial odorat, il gagne vision nocturne.<br/>\r\n* Super sprinter (Ext). Le personnage gagne un bonus racial de 3 mètres (2 c) à sa vitesse de déplacement lorsqu’il charge, court ou bat en retraite. S’il a le trait racial sprinter, son bonus racial de vitesse quand il charge, court ou bat en retraite est augmenté à 6 mètres (4 c).", "Les caractéristiques félines de l’homme-félin sont plus précises et marquées que celles des autres membres de sa race.", "Homme-félin modèle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "harmonie-sauvage", "Le personnage choisit un type d’environnement de prédilection issu de la liste du rôdeur (à l'exception de l'environnement urbain). Tant qu’il se trouve dans l’environnement choisi, son rythme de guérison naturel est doublé (il s’agit du montant de points de vie et d’affaiblissement de caractéristique qu’il soigne après une nuit de repos complet).", "Le personnage développe un lien mystique très puissant avec un type d’environnement naturel.", "Harmonie Sauvage", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "hamatulatsu", null, "Le personnage maîtrise une forme de combat mortelle inspirée des immondes attaques des diables barbelés.", "Hamatulatsu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "halfelin-porte-chance-(mythique)", "Lorsqu’il utilise Halfelin porte-chance, le personnage ajoute son grade en bonus au jet de sauvegarde. Il peut dépenser une utilisation de pouvoir mythique lorsqu’un allié utilise son jet de sauvegarde mais souffre toujours d’un effet néfaste après avoir réussi le jet. Dans ce cas, cet allié ne subit aucun effet néfaste après avoir réussi son jet de sauvegarde.", "Le personnage partage sa chance mythique avec son entourage.", "Halfelin porte-chance (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "habitant-du-désert", null, "Le personnage a passé tant de temps dans un impitoyable désert qu’il résiste mieux à la chaleur extrême, à la faim et à la soif.", "Habitant du désert", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "illusionniste-amateur", "Tant que le personnage n’a pas utilisé tous ses pouvoirs magiques quotidiens issus du trait racial magie gnome, il gagne un bonus racial de +2 aux tests de Bluff, de Déguisement et d’Escamotage. Au niveau 10, ces bonus\r\npassent à +4.", "Le personnage utilise sa magie innée pour créer des illusions mineures qui améliorent ses tentatives de duperie et de fourberie.", "Illusionniste amateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ni-elfe-ni-humain", "Le personnage n’est pas considéré comme un humain ni comme un elfe vis-à-vis de tous les sorts et effets néfastes basés sur le type, comme une arme tueuse ou le pouvoir de classe de rôdeur ennemi juré.", "Le personnage a pris tant de distance avec son héritage que même la magie ne le reconnaît plus.", "Ni elfe ni humain", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "incantation-statique", "Un sort à incantation statique ne nécessite pas de composante gestuelle. Les sorts sans composante gestuelle ne sont pas affectés. Un sort à incantation statique nécessite un emplacement de sort d’un niveau de plus que son niveau réel.", "Le personnage peut lancer un sort en restant immobile.", "Incantation statique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "interférence-divine-(mythique)", "Lorsque le personnage utilise Interférence divine, le malus au jet d’attaque de son adversaire est égal au double du niveau du sort sacrifié. Le personnage peut dépenser une utilisation de pouvoir mythique au moment où il sacrifie le sort pour que le malus s’élève au triple du niveau du sort sacrifié.", "La puissance divine du personnage repousse les ennemis de sa foi.", "Interférence divine (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "magie-de-guerre", "Le personnage bénéficie d’un bonus de +4 aux tests de concentration lorsqu’il essaye de lancer un sort ou d’utiliser une capacité équivalente sur la défensive, ou lorsqu’il est agrippé ou immobilisé en situation de lutte.", "Le personnage est doué pour lancer des sorts même s’il est menacé ou distrait.", "Magie de guerre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mage-ténébreux", "Lorsqu’il lance des sorts de la branche de l’ombre ou du registre de ténèbres, le drow est considéré comme ayant deux niveaux de plus quand il s’agit de déterminer la durée de ces sorts.", "Le contrôle du drow sur l’ombre et les ténèbres lui permet de créer des effets de sort prolongés.", "Mage ténébreux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mage-des-étendues-sauvages", "Quand le personnage se trouve dans un environnement choisi pour le don Harmonie sauvage, il gagne un bonus de +2 aux tests de niveau de lanceur de sorts, de Concentration de Connaissances (mystères) et d’Art de la magie. S’il se trouve dans une zone qui compte comme plusieurs environnements, les bonus ne se cumulent pas.", "Le lien mystique qui unit le personnage à la nature renforce ses incantations.", "Mage des étendues sauvages", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lutte-supérieure", "Le personnage reçoit un bonus de +2 aux tests destinés à agripper un adversaire. Ce bonus s’ajoute à celui de la Science de la lutte. Une fois que le personnage a agrippé un adversaire, il lui suffit d’une action de mouvement pour maintenir sa prise. Ce don permet au personnage de faire deux tests de lutte par round (pour déplacer, blesser ou immobiliser son adversaire) mais il n’est pas obligé d’en faire deux. Il lui suffit d’en réussir un pour maintenir sa prise.", "Pour le personnage, continuer à lutter est une seconde nature.", "Lutte supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "linceul-dombres", "Par une action immédiate, le personnage peut créer un linceul de ténèbres brumeuses autour de sa personne et ainsi bénéficier d’un camouflage contre une attaque. La brume se dissipe dès l’attaque résolue.\r\nLe personnage peut utiliser ce pouvoir une fois par jour et une fois de plus quand le niveau de lanceur de sorts de son pouvoir racial ténèbres atteint 5, 10, 15 et 20.", "Le personnage utilise une partie de sa magie innée pour s’envelopper de ténèbres et esquiver les attaques.", "Linceul d'ombres", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lasso-étrangleur", "Quand le personnage réussit à enchevêtrer une cible avec un lasso et dépasse le DMD de sa cible de 5 ou plus, cette dernière ne peut émettre que des murmures et doit réussir un test de concentration (DD = 20 + BMO du personnage + niveau du sort) pour lancer un sort à composante verbale, utiliser un objet à mot de commande ou user d’une magie exigeant des paroles.\r\nDe plus, le DD du test d’Évasion pour échapper au lasso du personnage est de 15 ou DMD du personnage (prendre le plus élevé).\r\n{s:Desambi|Ce don est dénommé Strangulation dans le codex monstrueux. Cependant, cette dénomination désigne déjà un autre don existant}", "Le personnage étouffe ses adversaires avec un lasso.", "Lasso étrangleur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "individu-chanceux", "Le personnage peut utiliser chance adaptable une fois de plus par jour.", "Le personnage est encore plus doué que les autres halfelins pour adapter sa chance.", "Individu chanceux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lanterne-du-sniper", "Le personnage peut utiliser son pouvoir magique racial lumières dansantes pour créer un orbe de lumière vive. Cette lumière lui donne un bonus d’intuition de +2 aux attaques à distance contre les cibles situées dans un rayon de 1,50 mètre autour de l’orbe. Le personnage peut déplacer la lumière par une action de mouvement, dans les limites de portée du sort. En dehors de cela, l’orbe fonctionne comme lumières dansantes.\r\nLe personnage peut utiliser ce pouvoir une fois par jour et une fois de plus quand le niveau de lanceur de sorts de son pouvoir racial lumières dansantes atteint 5, 10, 15 et 20.", "Le personnage utilise sa magie innée pour viser ses ennemis et repérer leurs faiblesses.", "Lanterne du sniper", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lancer-improvisé", "Le personnage ne souffre d’aucun malus quand il utilise une arme de jet improvisée. Il reçoit un bonus de circonstances de +1 à ses jets d'attaque quand il utilise une arme à aspersion.", "Le personnage est habitué à lancer tout ce qui lui tombe sous la main.", "Lancer improvisé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lancer-improvisé-(mythique)", "Le personnage gagne un bonus de +2 aux jets d’attaque et de dégâts effectués en utilisant une arme à aspersion ou une arme de jet improvisée. Ce bonus se cumule avec celui du don Lancer improvisé non-mythique. S’il rate son attaque en lançant une arme à aspersion, celle-ci atterrit toujours dans une case adjacente à la cible, quel que soit le nombre de facteurs de portée qui séparent le personnage de sa cible.", "Le talent du personnage en matière d’armes de jet est impressionnant, quel que soit ce qu’il lance.", "Lancer improvisé (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ki-supplémentaire", "La réserve de ki du personnage est augmentée de 2 points.", "Le personnage peut utiliser sa réserve de ki de plus nombreuses fois que la normale.", "Ki supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "jouet-du-destin", null, "Dans la jeunesse du personnage, de nombreuses divinations semblent avoir fait mouche et ont renforcé sa conviction : il est voué à accomplir un grand destin. ", "Jouet du destin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "invocation-de-conduits", "Quand le personnage lance un sort de contact, il peut décider d’utiliser une créature adjacente (eidolon compris) comme conduit (il doit la toucher au moment de l’incantation). La créature peut alors porter l’attaque de contact à la place de son maître.. Une fois ce sort lancé, le personnage et n’importe quelle créature qu’il invoque gagnent le pouvoir transfert de sort. Ce don ne qualifie pas le personnage pour d’autres dons ou règles optionnelles qui requièrent la possession d’un compagnon animal, d’un familier ou d’une créature alliée similaire qui possède le pouvoir transfert de sort.", "Les créatures que le personnage invoque peuvent servir de conduit et lancer des sorts sur ses adversaires.", "Invocation de conduits", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "invocateur-daraignées", "Lorsqu’il lance les sorts convocation de monstres ou convocation d’alliés naturels, les options du drow augmentent. En fonction du niveau du sort, il peut invoquer les araignées suivantes.\r\n* Convocation de monstres. Niveau 1 – araignée crabe géante*; Niveau 4 – veuve noire géante*; Niveau 5 – araignée ogre*; Niveau 7 – tarentule géante*.\r\n* Convocation d’alliés naturels. Niveau 1 – araignée crabe géante; Niveau 4 – veuve noire géante; Niveau 5 – araignée ogre; Niveau 7 – tarentule géante.\r\nLes créatures marquées d’un astérisque (*) sont convoquées avec l’archétype céleste si le drow est d’alignement Bon, et avec l’archétype fiélon si le drow est d’alignement Mauvais. S’il est d’alignement Neutre, le drow peut choisir quel archétype appliquer à la créature.\r\nEn outre, lorsqu’il convoque des araignées avec convocation de monstres ou convocation d’alliés naturels, le DD du poison de la créature convoquée et des effets de toile augmente de 2.", "Le drow gagne la capacité à convoquer de puissantes araignées.", "Invocateur d'araignées", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lanceur-à-distance-(mythique)", "Les armes de jet propulsées par le personnage voient leur portée normale doubler.", "Grâce à la force du personnage, la portée des armes de jet qu’il lance est pratiquement illimitée.", "Lanceur à distance (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-supérieure", "Le degré de difficulté des jets de sauvegarde contre les sorts de l’école choisie par le personnage augmente de +1. Ce bonus s’ajoute à celui d’École renforcée.", "Le personnage choisit une école de magie pour laquelle il dispose déjà du don École renforcée. Quand le personnage lance un sort de cette école, il est très difficile d’y résister.", "École supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "noble-résistance-à-la-magie", "La résistance à la magie du drow est égale à 11 + son niveau.", "Le drow a terminé son ascension ; sa résistance à la magie approche celle des démons.", "Noble résistance à la magie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "noblesse-drow", "Le drow peut utiliser le pouvoir magique de détection de la magie à volonté, et rajoute les pouvoirs magiques de feuille morte et de lévitation à ceux qu’il peut utiliser une fois par jour. Son niveau de lanceur de sorts est égal à son niveau.", "Le pouvoir coule dans les veines du drow, lui conférant de plus grands talents magiques.", "Noblesse drow", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prêtre-guerrier-(mythique)", "Le personnage gagne un bonus égal à la moitié de son grade aux tests d’initiative et aux tests de concentration pour lancer un sort ou utiliser un pouvoir magique sur la défensive ou lorsqu’il est agrippé en situation de lutte. Ces bonus se cumulent avec ceux conférés par le don Prêtre guerrier.", "La foi du personnage aiguise ses réflexes et renforce davantage son esprit et son assurance.", "Prêtre guerrier (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "queue-magique", "Le kitsune gagne un nouveau pouvoir magique, utilisable deux fois par jour, choisi dans l’ordre parmi cette liste : déguisement, charme-personne, détection faussée, invisibilité, suggestion, déplacement, confusion, domination. Par exemple, la première fois qu’il choisit ce don, le kitsune gagne déguisement 2/jour ; la deuxième fois qu’il le choisit, il gagne charme-personne 2/jour. Son niveau de lanceur de sorts pour ces sorts est égal à ses dés de vie. Le DD de ces capacités est basé sur le Charisme.", "Le kitsune se fait pousser une queue supplémentaire, représentative du développement de ses pouvoirs magiques.", "Queue magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "quintessence-de-la-montée-en-puissance-(mythique)", "Une fois par jour lorsque le personnage utilise montée en puissance, il peut dépenser deux utilisations de pouvoir mythique pour considérer que son dé de montée en puissance obtient le résultat maximum.", "En cas d’urgence, le personnage peut consommer une part plus importante de pouvoir mythique pour s’assurer la victoire.", "Quintessence de la montée en puissance (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "quintessence-des-sorts", "Tous les effets numériques aléatoires de la quintessence d’un sort prennent automatiquement leur valeur maximale. Les jets de sauvegarde et les jets opposés (comme celui que l’on joue en cas de dissipation de la magie) ne sont pas affectés, pas plus que les sorts sans données numériques et aléatoires. La quintessence d’un sort nécessite un emplacement de sort de trois niveaux de plus que son niveau réel.", "Les sorts du personnage donnent un effet maximal.", "Quintessence des sorts", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rage-empathique", "Quand le personnage est adjacent à un allié enragé, il peut utiliser une action libre, à son tour, pour entrer dans un état similaire, mais moins efficace. Cette forme de rage plus faible lui donne tous les avantages et tous les malus de la rage de barbare, sauf que le bonus de moral à la Force et la Constitution est seulement de +2. Le personnage peut rester enragé aussi longtemps qu’il le désire tant qu’il reste adjacent à un allié enragé (par exemple, il peut faire un pas de 1,50 m (1 c) pour s’éloigner d’un allié enragé et se placer à côté d’un autre allié enragé tout en maintenant sa rage). Une fois que cette rage amoindrie se termine, le personnage est fatigué, comme avec la rage de barbare. Le personnage ne peut pas utiliser ce don s’il est fatigué.", "Quand le personnage voit un allié devenir enragé, il se laisse lui aussi gagner par la rage.", "Rage empathique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rage-supplémentaire", "Le personnage peut entrer en rage pendant six rounds supplémentaires par jour.", "Le personnage peut devenir enragé plus souvent.", "Rage supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rallonge", "Une fois par round, quand le personnage blesse une créature déjà secouée, effrayée ou paniquée, il peut lui infliger seulement la moitié des dégâts normaux pour augmenter la durée de son état préjudiciable de 1 round.", "Le personnage oblige son ennemi à rester prisonnier de sa peur.", "Rallonge", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rapide-(mythique)", "La vitesse de déplacement de base du personnage augmente de 1,50 mètre, quelle que soit l’armure portée ou la charge transportée. Ce bonus se cumule avec celui conféré par le don Rapide.", "Le personnage semble aussi léger qu’une plume lorsqu’il se déplace.", "Rapide (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rapide", "Quand le personnage porte une armure légère ou ne porte aucune armure, sa vitesse de base augmente de 1,5 m. Il perd ce bonus s’il porte une charge intermédiaire ou lourde.", "Le personnage est plus rapide que la plupart des gens.", "Rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rayon-dénergie-canalisée", "Lorsque le personnage canalise de l’énergie, il peut projeter un rayon d’énergie depuis son symbole sacré au lieu de créer une vague d’énergie. Il doit réussir une attaque de contact à distance pour toucher une cible non consentante ; la cible est alors normalement affectée par l’énergie canalisée et peut effectuer un jet de sauvegarde. Il n’y a pas besoin d’effectuer un jet d’attaque lorsque le rayon est dirigé sur une cible consentante. Le rayon a une portée de 9m (6c) par dés de canalisation d’énergie, et son DD du jet de sauvegarde augmente de +2.", "Le personnage peut concentrer l’énergie qu’il canalise sur une cible unique.", "Rayon d'énergie canalisée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rayonnement-divin", "L’aasimar gagne une utilisation supplémentaire par jour de lumière du jour. Il choisit un sort dans la table ci-dessous ; en dépensant une utilisation de lumière du jour, il peut utiliser ce sort comme un pouvoir magique. Pour choisir un sort, il doit avoir le niveau de personnage minimum indiqué dans la table. Le DD du jet de sauvegarde pour ce sort est basé sur le Charisme.\r\n<div align=\"center\">\r\n{| CLASS=\"tablo autoalt\"\r\n|-\r\n|  Pouvoir magique\r\n|  Niveau de personnage <BR>minimum \r\n|-\r\n| Grandes illuminations || Niveau 1\r\n|-\r\n| Sillage de lumière || Niveau 3\r\n|-\r\n| Lumière brûlante || Niveau 5\r\n|-\r\n| Poussière d’étoile || Niveau 7\r\n|-\r\n| Rayon de soleil (un rayon uniquement) || Niveau 9\r\n|}\r\n</div>", "L’aasimar peut utiliser sa lumière divine de bien des façons.", "Rayonnement divin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rechargement-rapide-(mythique)", "Le personnage profite des avantages de Rechargement rapide avec tous les types d’arbalètes et d’armes à feu, et plus seulement avec l’arme choisie lorsqu’il a sélectionné ce don. Par une action rapide, il peut dépenser une utilisation de pouvoir mythique pour pouvoir recharger les arbalètes et les armes à feu sans provoquer d’attaque d’opportunité pendant une minute.", "Le personnage peut recharger tous types d’armes avec une rapidité stupéfiante.", "Rechargement rapide (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "regard-perspicace", "Le personnage reçoit un bonus racial de +2 aux jets de sauvegarde contre les sorts et effets d’illusion et un bonus de +2 aux tests de Linguistique pour repérer un faux. Il peut utiliser la compétence Linguistique pour détecter un faux même s’il n’est pas entraîné.", "Le personnage ne se laisse pas facilement berner par les illusions et les faux.", "Regard perspicace", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "renversement-supérieur", "Le personnage reçoit un bonus de +2 aux tests destinés à renverser un adversaire. Ce bonus s’ajoute à celui de la Science du renversement. Quand le personnage renverse un adversaire, ce dernier déclenche une attaque d’opportunité s’il tombe à terre.", "Les ennemis du personnage doivent plonger pour éviter ses dangereux mouvements.", "Renversement supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "renvoi-de-projectile", "Lorsque le personnage utilise le don Capture de projectiles, il peut lancer le projectile attrapé (une flèche, un carreau ou une bille de fronde, mais pas les balles d’armes à feu) comme s’il s’agissait d’une arme de jet possédant un facteur de portée de 6m (4c), même si ce n’est pas son tour. Le projectile renvoyé inflige des dégâts correspondant au type de projectile utilisé + le modificateur de force du personnage qui ne subit pas de pénalité pour avoir lancé une arme qui n’est pas désignée comme étant une arme de jet. Cette attaque à distance provoque naturellement une attaque d’opportunité. Le personnage doit avoir au moins une main libre (qui ne tient rien) pour utiliser ce don.", "Lorsque le personnage attrape des flèches ou d’autres projectiles en plein vol, il peut les renvoyer immédiatement sur ses ennemis.", "Renvoi de projectile", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "renvoi-des-morts-vivants-(mythique)", "Lorsque le personnage utilise Renvoi des morts-vivants, la portée du don augmente de 3 mètres par grade et les morts-vivants non-mythiques subissent un malus aux jets de sauvegarde égal à son grade. Il peut dépenser une utilisation de pouvoir mythique lorsqu’il utilise Renvoi des morts-vivants pour détruire ces créatures. Au lieu de fuir, les morts-vivants qui ratent leur jet de sauvegarde sont détruits. Le personnage peut détruire de cette façon 2d4 DV de morts-vivants par grade. Les morts-vivants avec le moins de DV sont affectés les premiers ; s’il y a plusieurs morts-vivants avec le même nombre de DV, les plus proches du centre de la zone d’effet du sort sont affectés les premiers. Les morts-vivants possédant 9 DV ou plus ne peuvent être détruits de cette façon. S’il reste des DV à utiliser mais qu’ils ne suffisent pas à détruire un mort-vivant supplémentaire, ils sont perdus.", "L’énergie divine que manipule le personnage est si puissante qu’elle peut non seulement faire fuir les morts-vivants, mais également les détruire en un clin d’oeil.", "Renvoi des morts-vivants (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "renvoi-des-morts-vivants", "Par une action simple, le personnage peut utiliser l’une de ses utilisations journalières de canalisation d’énergie positive pour faire fuir tous les morts-vivants qui se trouvent dans un rayon de neuf mètres, comme s’ils étaient paniqués. Ils ont droit à un jet de Volonté pour annuler cet effet. Le DD de ce jet est égal à 10 + 1/2 niveau de prêtre + modificateur de Charisme. Les morts-vivants qui ratent leur jet fuient pendant une minute. Les morts-vivants intelligents ont droit à un jet de sauvegarde supplémentaire par round. Si le personnage choisit de canaliser l’énergie dans ce but, elle n’a pas d’autre effet (elle ne soigne ni ne blesse les créatures proches).", "Le personnage appelle les puissances supérieures : les morts-vivants fuient devant l’énergie divine qu’il dégage.", "Renvoi des morts-vivants", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "résistance-étendue", "Le personnage choisit une école de magie autre que l’illusion et gagne un bonus racial de +2 aux jets de sauvegarde contre les sorts et effets de cette école.", "Le personnage a développé sa résistance à la magie de manière à ce qu’elle englobe plusieurs écoles.", "Résistance étendue", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "résistance-héroïque-(mythique)", "Une fois par jour, par une action immédiate, le personnage peut tenter d’annuler le déclenchement d’un état préjudiciable ou d’une affliction (comme paniqué, paralysé, étourdi, etc.) qui autorise un jet de sauvegarde au moment de son déclenchement. Les états et les afflictions instantanées ou permanentes ne peuvent être annulés de cette façon. Le personnage effectue un jet de Volonté avec un bonus égal à son grade contre le DD original de l’effet. En cas de réussite, l’état est annulé. En cas d’échec, le déclenchement est retardé jusqu’à la fin du prochain tour, du personnage, moment où l’état produira ses effets normaux. Ce don ne produit aucun effet sur les pertes de points de vie et les affaiblissements de caractéristique. Cet effet est indépendant de l’utilisation normale de la version non-mythique de Résistance héroïque.", "Le personnage ignore les effets néfastes qui viendraient à bout de la plupart des gens.", "Résistance héroïque (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "réflexes-surhumains", "Le personnage bénéficie d’un bonus de +2 aux jets de Réflexes.", "Le personnage a des réflexes plus rapides que la normale.", "Réflexes surhumains", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "réflexes-surhumains-(mythique)", "Lorsque le personnage effectue un jet de Réflexes contre un sort, ou pouvoir magique ou surnaturel provenant d’une source non-mythique, lancez deux fois le dé et conservez le meilleur résultat.", "Les réflexes du personnage sont véritablement surhumains.", "Réflexes surhumains (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "récupération-héroïque-(mythique)", "Par une action rapide, le personnage peut dépenser une utilisation de pouvoir mythique pour utiliser une nouvelle fois Récupération héroïque. Le cas échéant, il ajoute son grade au résultat du jet de Vigueur.", "Le personnage peut dissiper les effets des états préjudiciables déjà déclenchés.", "Récupération héroïque (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "récupération-de-sang", "Le dhampir peut utiliser son don Buveur de sang sur une créature morte du sous-type humanoïde approprié. La créature doit être morte depuis moins de 6 heures au maximum.", "Le dhampir n’est pas obligé de boire du sang d’une créature vivante pour bénéficier de ses vertus de guérison.", "Récupération de sang", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "préparation-de-potions", null, "Le personnage sait fabriquer des potions magiques.", "Préparation de potions", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rogneur", "Les dégâts que l’homme-rat inflige avec son attaque de morsure ignorent un nombre de points de solidité égal à 3 + la moitié de son niveau de personnage. Il peut dépenser une action complexe pour infliger le double de ses dégâts de morsure à un objet inanimé inutilisé.", "Grâce à la force de ses dents et à sa rapidité, l’homme-rat est capable de ronger presque n’importe quel obstacle s’il en a le temps.", "Rogneur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "robustesse-(mythique)", "Le don Robustesse confère deux fois plus de points de vie. De plus, lorsque son total des points de vie tombe en dessous de 0, le personnage gagne une RD 10/épique. Cette RD se cumule avec toutes les éventuelles autres RD/épique possédées par le personnage.", "La longévité et la résistance du personnage sont légendaires.", "Robustesse (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "robustesse-(alternatif)", "Le personnage gagne 1 point de blessure pour\r\nchaque niveau ou dé de vie qu’il possède.\r\n(((Ce don est une version modifiée du don Robustesse pour utilisation avec les règles alternatives «&nbsp;Blessures et vitalité&nbsp;» de l’Art de la Guerre)))", "L’endurance du personnage est au-dessus de la moyenne.", "Robustesse (alternatif)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "riposte", "Le personnage peut préparer son action pour faire une attaque de corps à corps contre un adversaire qui l’attaque en mêlée, même si ce dernier devrait être hors de portée.", "Le personnage peut frapper un adversaire qui l’attaque grâce à une allonge supérieure. Il vise alors l’arme ou le membre qui se tend vers lui.", "Riposte", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "riposte-(mythique)", "Lorsqu’il effectue une attaque au corps à corps lors d’une action préparée contre un adversaire situé en dehors de sa zone de menace au corps à corps, le personnage peut effectuer un pas de placement vers cet ennemi. S’il dépense une utilisation de pouvoir mythique, il peut à la place se déplacer d’une distance égale à sa vitesse de déplacement vers l’ennemi. Aucun de ces deux types de déplacement n’est défalqué du potentiel de déplacement que le personnage peut effectuer pendant son tour.", "Les ripostes précises du personnage réduisent les avantages de ses ennemis pendant le combat.", "Riposte (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "retour-en-force-(mythique)", "Chaque fois que le personnage a la possibilité de relancer un test de caractéristique, de compétence ou un jet de sauvegarde, il lance deux dés et conserve le meilleur résultat, avant d’ajouter le bonus conféré par Retour en force.", "Le personnage ne fait jamais deux fois la même erreur.", "Retour en force (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "représentation-supplémentaire", "Le personnage peut donner une représentation bardique pendant six rounds de plus par jour.", "Le personnage donne quelques représentations bardiques de plus.", "Représentation supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "robustesse", "Le personnage gagne 3 points de vie supplémentaires. Il gagne 1 point de vie supplémentaire pour chaque Dé de vie qu’il possède au-dessus de 3. S’il possède plus de 3 DV, il gagne 1 point de vie supplémentaire à chaque fois qu’il gagne un DV (quand il prend un niveau par exemple).\r\n(((Ce don possède une variante Robustesse (alternatif) pour utilisation avec les règles alternatives «&nbsp;Blessures et vitalité&nbsp;» de l’Art de la Guerre)))", "L’endurance du personnage est au-dessus de la moyenne.", "Robustesse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prédisposition-pour-laltitude", null, "Par magie et par des méthodes plus classiques, le personnage a renforcé son corps pour survivre aux rigueurs de l’altitude.", "Prédisposition pour laltitude", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "précision-elfique-(mythique)", "Lorsque le personnage effectue une attaque avec un arc long ou court (y compris avec des arcs composites), il ignore le camouflage, à l’exception du camouflage total. S’il rate son tir à cause d’un camouflage total, il peut relancer le jet du risque d’échec comme indiqué dans la description de Précision elfique.", "Les yeux elfiques du personnage font fi des camouflages.", "Précision elfique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prestige", "Ce don permet au personnage de gagner les services d’un compagnon d’armes et de suivants qui l’aident de leur mieux. Le compagnon d’armes est souvent un PNJ avec des niveaux de classe tandis que les suivants sont des PNJ de bas niveau. Consultez la Table ci-après pour connaître le niveau du compagnon d’armes et le nombre de suivants sur lesquels le personnage peut compter.\r\n<div style=\"float:right; background-color: #fff; padding: 0 0 16px 16px\">\r\n<div style=\"border: 1px solid #4b3124; padding: 4px\">\r\n<center>\r\n{| CLASS=\"tablo\"\r\n|+ Prestige\r\n|- CLASS=\"titre\"\r\n| ROWSPAN=\"2\" | Valeur de <br/>Prestige\r\n| ROWSPAN=\"2\" | Niveau du <br/>compagnon d'armes\r\n| CLASS=\"avecsoustitres\" COLSPAN=\"6\" | Nombre de suivants par niveau\r\n|- CLASS=\"soustitre\"\r\n| 1 || 2 || 3 || 4 || 5 || 6\r\n|- CLASS=\"premier\"\r\n| 1 ou moins || — || — || — || — || — || — || —\r\n|- CLASS=\"alt\"\r\n| 2 || 1 || — || — || — || — || — || —\r\n|-\r\n| 3 || 2 || — || — || — || — || — || —\r\n|- CLASS=\"alt\"\r\n| 4 || 3 || — || — || — || — || — || —\r\n|-\r\n| 5 || 3 || — || — || — || — || — || —\r\n|- CLASS=\"alt\"\r\n| 6 || 4 || — || — || — || — || — || —\r\n|-\r\n| 7 || 5 || — || — || — || — || — || —\r\n|- CLASS=\"alt\"\r\n| 8 || 5 || — || — || — || — || — || —\r\n|-\r\n| 9 || 6 || — || — || — || — || — || —\r\n|- CLASS=\"alt\"\r\n| 10 || 7 || 5 || — || — || — || — || —\r\n|-\r\n| 11 || 7 || 6 || — || — || — || — || —\r\n|- CLASS=\"alt\"\r\n| 12 || 8 || 8 || — || — || — || — || —\r\n|-\r\n| 13 || 9 || 10 || 1 || — || — || — || —\r\n|- CLASS=\"alt\"\r\n| 14 || 10 || 15 || 1 || — || — || — || —\r\n|-\r\n| 15 || 10 || 20 || 2 || 1 || — || — || —\r\n|- CLASS=\"alt\"\r\n| 16 || 11 || 25 || 2 || 1 || — || — || —\r\n|-\r\n| 17 || 12 || 30 || 3 || 1 || 1 || — || —\r\n|- CLASS=\"alt\"\r\n| 18 || 12 || 35 || 3 || 1 || 1 || — || —\r\n|-\r\n| 19 || 13 || 40 || 4 || 2 || 1 || 1 || —\r\n|- CLASS=\"alt\"\r\n| 20 || 14 || 50 || 5 || 3 || 2 || 1 || —\r\n|-\r\n| 21 || 15 || 60 || 6 || 3 || 2 || 1 || 1\r\n|- CLASS=\"alt\"\r\n| 22 || 15 || 75 || 7 || 4 || 2 || 2 || 1\r\n|-\r\n| 23 || 16 || 90 || 9 || 5 || 3 || 2 || 1\r\n|- CLASS=\"alt\"\r\n| 24 || 17 || 110 || 11 || 6 || 3 || 2 || 1\r\n|-\r\n| 25 ou plus || 17 || 135 || 13 || 7 || 4 || 2 || 2\r\n|}\r\n</center></div></div>\r\nValeur de Prestige. La valeur de Prestige du personnage est égale à son niveau plus son modificateur de Charisme. La table propose des valeurs de Prestige très basses pour tenir compte de plus basses valeurs de Charisme, mais le personnage doit avoir atteint le niveau 7 au moins pour acquérir ce don. Des facteurs extérieurs peuvent modifier la valeur de Prestige, comme indiqué ci-dessous.\r\nNiveau du compagnon d’armes. Le personnage peut gagner les services d’un compagnon d’armes de ce niveau ou moins. À noter que, quelle que soit la valeur de Prestige du personnage, le niveau de son compagnon d’armes doit toujours être inférieur au sien de deux niveaux au moins. Le compagnon d’armes est équipé en fonction de son niveau (voir la table d'équipement des PNJ). Le compagnon d’armes peut être de n’importe quelle race ou classe. Son alignement ne doit pas être opposé à celui du personnage, que ce soit sur l’axe Bien/Mal ou Loyal/Chaotique. Le personnage subit un malus de -1 à sa valeur de Prestige s’il recrute un compagnon d’armes dont l’alignement diffère du sien.\r\nLe compagnon d’armes ne compte pas au nombre des membres du groupe quand il s’agit de répartir l’expérience. Le personnage divise le niveau de son compagnon d’armes par le sien et multiplie le résultat par le nombre total de points d'expérience qui lui ont été accordés. Il ajoute ensuite ce nombre au total d’expérience de son compagnon. Si le compagnon d’armes du personnage gagne suffisamment d’expérience pour accéder à un niveau inférieur de un à celui du PJ, il ne peut pas gagner ce niveau : on considère qu’il lui manque un point pour l’atteindre.\r\nNombre de suivants par niveau. Le personnage peut diriger un total de suivants ne dépassant pas le nombre indiqué. Les suivants ressemblent aux compagnons d’armes mais ils sont d’un niveau bien inférieur à celui du personnage : comme ils ont généralement cinq niveaux de moins, ils ne sont pas très efficaces au combat.\r\nLes suivants ne gagnent pas d’expérience et ne gagnent donc pas de niveaux. Quand le personnage gagne un niveau, consultez la table ci-contre pour savoir s’il acquiert de nouveaux suivants dont certains peuvent être de plus haut niveau que les précédents. En revanche, il n’est pas nécessaire de consulter la table pour savoir si les compagnons d'armes gagnent des niveaux car ils gagnent leur expérience de leur côté.", "Le personnage attire des suivants et un compagnon d’armes qui se joignent à sa cause et l’accompagnent au cours de ses aventures.", "Prestige", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pieds-emmêlés", "Lorsqu’il réussit un test d’Acrobaties pour éviter de provoquer une attaque d’opportunité de la part d’un adversaire plus grand alors qu’il traverse la zone qu’il menace ou l’espace qu’il occupe, le gobelin peut déséquilibrer son adversaire jusqu’à la fin de son tour par une action libre. Tandis que la créature est déséquilibrée, si elle tente de se déplacer, elle doit réussir un test d’Acrobaties DD 15 ou tomber à terre, gâchant son action de mouvement. Le gobelin ne peut affecter qu’une créature par round avec cet effet.", "Les créatures qui croisent la route du gobelin s’emmêlent les pieds et trébuchent.", "Pieds emmêlés", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "perturbateur", "Le DD qui permet de lancer des sorts sur la défensive alors qu’il se trouve dans une case contrôlée par le personnage augmente de +4. Cette augmentation du DD s’applique uniquement aux lanceurs de sorts dont le personnage connaît la position et contre lesquels il peut faire une attaque d’opportunité. Si le personnage a droit à une seule attaque d’opportunité par round et qu’il l’a déjà utilisée, le DD n’augmente pas.", "L’entraînement du personnage lui permet de perturber les lanceurs de sorts qui ont alors bien du mal à utiliser la magie près de lui.", "Perturbateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "perturbateur-(mythique)", "Le DD pour lancer des sorts sur la défensive dans la zone de menace du personnage augmente de la moitié de son grade. Ce bonus se cumule avec celui conféré par Perturbateur. De plus, même si le personnage ne peut pas réaliser d’attaques d’opportunité, les lanceurs de sorts non-mythiques se méfient toujours de lui et continuent de subir les effets du don Perturbateur.", "Le personnage est passé maître dans l’art de perturber les lanceurs de sorts à proximité.", "Perturbateur (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "persuasion", "Le personnage gagne un bonus de +2 aux tests de Diplomatie et d’Intimidation. S’il possède au moins 10 rangs dans l’une de ces compétences, le bonus passe à +4 pour celle-ci.", "Le personnage est très doué pour faire changer les autres d’attitude et les intimider afin de les rallier à son opinion.", "Persuasion", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "persuasif-(mythique)", "Le bonus aux tests de Diplomatie et d’Intimidation conféré par Persuasion augmente de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il obtient un 20 naturel à l’un de ces tests. Le personnage doit décider s’il utilise ou non cette aptitude avant de lancer le dé.", "Le personnage est passé maître dans l’art de la persuasion, que ce soit par le biais de paroles apaisantes ou d’ordres hurlés brusquement.", "Persuasif (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pas-aérien", "Le sylphe gagne un bonus de +2 à ses jets de sauvegarde contre les effets du registre air ou électricité et contre les effets infligeant des dégâts d’électricité. Il peut ignorer les 30 premiers points de dégâts de n’importe quelle chute lorsqu’il doit déterminer les dégâts qu’il reçoit d’une chute.", "L’air répond à la nature élémentaire innée du sylphe, le protège et amortit ses chutes.", "Pas aérien", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pistolier-gobelin", "Le gobelin peut manier les armes à feu taille M sans subir le malus qu’impose le maniement des armes de taille inappropriée.", "Le gobelin a appris à tirer avec de gros pistolets.", "Pistolier gobelin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "parangon-mythique-(mythique)", "On considère que le grade du personnage est de 2 niveaux de plus que la normale lorsqu’il s’agit de déterminer la puissance de ses aptitudes, dons et sorts mythiques. Ceci ne lui permet pas d’accéder à des aptitudes mythiques ou des versions mythiques des sorts tant que son niveau réel ne l’y autorise pas. Cela ne lui confère pas non plus d’utilisations supplémentaires de pouvoir mythique et ne modifie pas le dé utilisé par sa montée en puissance.", "Le pouvoir mythique du personnage est encore plus puissant que celui de la plupart des autres entités mythiques.", "Parangon mythique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "parade-de-projectiles", "Le personnage doit avoir au moins une main libre pour utiliser ce don. Une fois par round, quand il devrait être touché par une arme à distance, il peut dévier le projectile au dernier moment et éviter tout dégât. Le personnage doit être conscient de l’attaque. Il ne doit pas être pris au dépourvu. Il effectue un geste extrêmement rapide qui ne compte pas comme une action. Il est impossible de parer les projectiles massifs (comme un rocher lancé par un géant ou un carreau de baliste) ni les attaques à distance générées par des sorts ou des attaques naturelles (comme les souffles).", "Le personnage peut détourner les flèches et d’autres projectiles de leur trajectoire afin de les éviter.", "Parade de projectiles", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "parade-de-projectiles-(mythique)", "Lorsqu’il utilise Parade des projectiles, le personnage peut, chaque round, parer un nombre d’attaques à distance supplémentaires égal à la moitié de son grade. Il peut dépenser une utilisation de pouvoir mythique par une action immédiate pour parer un unique rayon produit par un sort ou un effet de rayon qui le prend pour cible.", "Le personnage pare les projectiles et les rayons des sorts en faisant preuve d’une maîtrise stupéfiante.", "Parade de projectiles (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "orateur-prudent", null, "À force de vivre dans une région où le gouvernement est dangereux, le personnage a appris à surveiller chacune de ses paroles.", "Orateur prudent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ombre-druidique", null, "Le personnage est un sinistre druide albinos. Il garde une puissante effigie de poils, de brindilles et de sang qui suinte du froid pénétrant du territoire de chasse impie d'où il provient et dédié à une divinité neutre mauvaise des Ténèbres.", "Ombre druidique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "oeil-mystique", null, "Le personnage a si bien compris les enseignements ésotériques et mystiques qu’il peut ouvrir un troisième oeil qui lui permet de mieux percevoir le monde magique.", "Œil mystique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "nordique", null, "Le personnage a mené une vie rude sous un climat froid et en a tiré quelques avantages.", "Nordique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "parangon-demi-drow", "Le personnage est assimilé à un drow vis-à-vis de tous les effets liés à la race. De plus, les pouvoirs magiques du trait racial magie drow comptent comme des pouvoirs magiques drows quand il s’agit de remplir des conditions requises.", "Le sang drow du personnage chante particulièrement fort.", "Parangon demi-drow", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "noblesse-drow-supérieure", "Le pouvoir magique détection de la magie du drow est maintenant permanent. Il peut utiliser les pouvoirs magiques lumières dansantes, ténèbres profondes, lueur féerique, feuille morte et lévitation à volonté.", "Le drow maîtrise ses pouvoirs magiques mineurs, ce qui prouve sa véritable noblesse.", "Noblesse drow supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "piège-de-rodeur-étendu", "Le personnage peut placer n’importe quel piège de rôdeur sur une zone contigu de 4 cases (6m) de son choix. Lorsque le piège est déclenché, l’effet est centré sur la première case où le piège s'est activé. Si plus d’une case sont traversées en même temps, le personnage effectue une sélection au hasard parmi les cases visitées.", "Les pièges de rôdeur occupent une zone plus large.", "Piège de rôdeur étendu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "piétinement", "Lors d’une tentative de renversement monté, la cible du personnage ne peut choisir de l’éviter. La monture du personnage peut porter un coup de sabot contre un adversaire renversé, en bénéficiant du bonus habituel au jet d'attaque de +4 contre les créatures à terre.", "Quand le personnage est à cheval, il peut renverser ses adversaires et les piétiner sous les sabots de sa monture.", "Piétinement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prestidigitateur-gnome-(mythique)", "Le personnage gagne les pouvoirs magiques suivants : 1/jour – flou et disparition. Chaque jour, le personnage peut utiliser ses pouvoirs magiques de gnome un nombre de fois supplémentaires égal à la moitié de son grade. Il peut dépenser ces utilisations supplémentaires pour utiliser n’importe lequel de ses pouvoirs magiques de gnome. Le niveau de lanceur de sorts de ses pouvoirs magiques de gnome est désormais égal à son niveau de personnage plus son grade.", "Grâce au pouvoir mythique, la magie qui imprègne le sang du personnage s’est diversifiée et intensifiée.", "Prestidigitateur gnome (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prendre-le-coup", "Tant qu'un allié avec le don esprit lié est situé sur une case adjacente, vous pouvez choisir d'absorber jusqu'à la moitié des points de dégâts d'une attaque qui le touche, comme sous l'effet du sort protection dautrui. Utiliser cette capacité est une action immédiate, et ne s'applique qu'à une attaque, même si l'allié est victime de plusieurs attaques au cours d'une même action.", "Vous pouvez protéger votre partenaire avec votre corps", "Prendre le coup", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pouvoir-mythique-supplémentaire-(mythique)", "Le personnage gagne deux utilisations quotidiennes supplémentaires de pouvoir mythique.", "La réserve de pouvoir mythique du personnage augmente au delà des limites normalement imposées par son grade.", "Pouvoir mythique supplémentaire (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pouvoir-magique-rapide", "Ce don s’applique à un des pouvoirs magiques de la créature, qui doit vérifier les conditions décrites ci-dessous. Trois fois par jour (ou moins souvent si la créature est normalement limitée à une ou deux utilisations quotidiennes du pouvoir), le pouvoir choisi peut être utilisé de manière rapide.\r\nUne action rapide suffit pour activer le pouvoir, et celle-ci ne provoque pas d’attaque d’opportunité. La créature peut entreprendre d’autres actions au cours du même round (utiliser un autre pouvoir magique par exemple, mais pas une autre action rapide). Elle ne peut utiliser qu’un seul pouvoir magique rapide par round.\r\nLe pouvoir magique choisi doit imiter un sort dont le niveau est inférieur ou égal à la moitié du NLS de la créature (arrondi vers le bas) moins 4. Le tableau ci-dessous présente les restrictions imposées par cette condition.", "La créature peut utiliser un de ses pouvoirs magiques quasiment sans effort.", "Pouvoir magique rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "portrait-craché", "Le kitsune peut imiter précisément les traits physiques de tout individu qu’il rencontre. Quand il utilise sa capacité raciale de changement de forme, il peut essayer de prendre la forme d’un individu, ce qui lui accorde un bonus de circonstances de +10 à ses tests de Déguisement pour faire croire aux autres qu’il est vraiment cette personne.", "Lorsqu’il est sous sa forme humaine, le kitsune peut prendre l’apparence d’un individu spécifique.", "Portrait craché", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "porte-étendard", null, "Quand le personnage brandit le drapeau d’une organisation à laquelle il a prêté allégeance, il inspire les membres de cette même organisation. ", "Porte-étendard", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "piétinement-(mythique)", "La monture du personnage peut réaliser deux attaques de sabot au lieu d’une seule contre un adversaire mis à terre suite à un renversement effectué par le personnage. Il peut dépenser une utilisation de pouvoir mythique lorsqu’il obtient une possibilité de critique avec une ou plusieurs de ces attaques de sabot. Le cas échéant, le coup critique est automatiquement confirmé. Si les deux attaques sont des possibilités de critique, la dépense d’une utilisation de pouvoir mythique confirme automatiquement les deux.", "La monture déchaînée du personnage peut piétiner les adversaires à terre tout en continuant sa course.", "Piétinement (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "port-des-armures-légères", "Lorsqu’un personnage est formé au port d’une armure, le malus d’armure aux tests associés à celle-ci s’applique uniquement aux tests basés sur la Dextérité et la Force.\r\nNormal. Le malus d’armure aux tests imposés par une armure pour laquelle le personnage n’est pas formé s’applique aux jets d’attaque et à tous les tests de compétence associés à la Force ou à la Dextérité.", "Le personnage est formé au port des armures légères.", "Port des armures légères", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "port-des-armures-intermédiaires", "Voir « Port des armures légères ».\r\nNormal. Voir « Port des armures légères ».", "Le personnage est formé au port des armures intermédiaires.", "Port des armures intermédiaires", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "port-de-l’armure-magique-(mythique)", "Le personnage n’a plus à réaliser une action rapide pour bénéficier d’une réduction des risques d’échec des sorts profanes conférée par Port de l’armure magique. De plus, s’il porte une armure légère, réduisez les chances d’échec des sorts profanes du personnage de 20%.", "Le personnage a adapté la gestuelle de ses incantations pour ignorer les restrictions imposées par le port des armures.", "Port de l’armure magique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "port-de-larmure-magique", "Par une action rapide, le personnage réduit les risques d’échec des sorts dus à l’armure de 10% pour tout sort lancé au cours de ce round.", "Le personnage sait lancer des sorts en armure.", "Port de l'armure magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "poing-élémentaire-(mythique)", "Les dégâts d’énergie destructive supplémentaires infligés par Poing élémentaire s’élèvent à 1d8 et le personnage gagne un nombre d’utilisations quotidiennes supplémentaires de ce don égal à son grade. Il peut dépenser une utilisation de pouvoir mythique par une action immédiate pour ajouter des dégâts supplémentaires infligés par son Poing élémentaire à tous les coups à mains nues effectués jusqu’au début de son prochain tour. On considère que Poing élémentaire est utilisé une seule fois, quel que soit le nombre d’attaques effectuées de cette façon.", "La capacité du personnage à canaliser la puissance élémentaire dans ses attaques à mains nues est férocement efficace.", "Poing élémentaire (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "poing-de-la-gorgone", "Par une action simple, le personnage fait une unique attaque de corps à corps contre un adversaire dont la vitesse est réduite (s’il est victime de l’École du scorpion par exemple). S’il touche son adversaire, le personnage lui inflige les dégâts habituels. La cible est chancelante jusqu’à la fin du prochain tour du personnage, à moins qu’elle ne réussisse un jet de Vigueur (DD 10 + 1/2 niveau du personnage + modificateur de Sagesse). Ce don n’a aucun effet sur les cibles qui chancellent déjà.", "Le personnage peut projeter son adversaire en arrière d’un seul coup de poing.", "Poing de la gorgone", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "poing-de-la-gorgone-(mythique)", "Lorsque le personnage touche une créature en utilisant Poing de la gorgone, si la cible rate son jet de Vigueur, elle est hébétée pendant un round au lieu d’être simplement chancelante. Le personnage peut dépenser une utilisation de pouvoir mythique lorsqu’il touche avec une attaque de Poing de la gorgone mais avant que le jet de sauvegarde ne soit effectué, pour augmenter le DD de cette sauvegarde de la moitié de son grade.", "Les coups du personnage affaiblissent et désorientent ses adversaires.", "Poing de la gorgone (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "port-des-armures-lourdes", "Voir « Port des armures légères ».\r\nNormal. Voir « Port des armures légères ».", "Le personnage est formé au port des armures lourdes.", "Port des armures lourdes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "écriture-de-parchemins", "Le personnage peut se servir de tout sort connu pour écrire un parchemin, ce qui lui prend deux heures pour un parchemin dont le prix de base est de 250 po ou moins, ou un jour par tranche de 1000 po du prix de base. Le personnage doit acheter les matières premières nécessaires pour la moitié du prix de base.\r\nConsultez les règles de création des objets magiques ici pour plus d’informations.", "Le personnage fabrique des parchemins magiques.", "Écriture de parchemins", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "éliminer-les-toxines", "Quand le personnage réussit un jet de sauvegarde contre le poison, il guérit 1 point d’affaiblissement de caractéristique infligé par ce même poison. Quand il soigne des affaiblissements de caractéristiques (naturellement ou par magie) il guérit toujours de 1 point de plus. Ce don n’a aucun effet sur les malus aux valeurs de caractéristiques ni sur les diminutions permanentes.", "Le corps du personnage se remet des effets des poisons à une vitesse époustouflante.", "Éliminer les toxines", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "élément-renforcé-(mythique)", "Le personnage choisit un type d’énergie destructive pour lequel il possède Élément renforcé. L’augmentation du DD des jets de sauvegarde conférée par Élément renforcé et Élément supérieur pour les sorts du type d’énergie destructive sélectionné est majorée de 1. Lorsque le personnage lance un sort doté du registre correspondant au type d’énergie destructive choisi, il peut dépenser une utilisation de pouvoir mythique pour obliger les cibles du sort à lancer deux jets de sauvegarde et à conserver le pire.", "Les sorts élémentaires du personnage sont efficaces et destructeurs.", "Élément renforcé (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "gros-buveur", "Chaque fois que le personnage gagne du ki temporaire grâce à sa capacité de ki alcoolisé, il obtient 2 points temporaires de ki au lieu d’un seul.", "Le personnage acquiert une plus grande quantité de ki lors de ses libations.", "Gros buveur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "guérison-rapide", "Lorsque le personnage regagne des points de vie en se reposant ou grâce à de la magie curative, il récupère des points de vie supplémentaires en quantité égale à la moitié de son modificateur de Constitution (minimum +1).", "Le personnage guérit facilement et les effets des sorts ou de la guérison naturelle sont augmentés.", "Guérison rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "halfelin-porte-chance", "Une fois par jour, lorsqu’un des alliés du personnage rate un jet de sauvegarde alors qu’il se trouve à au plus 9 mètres (6 cases) du personnage, ce dernier (le personnage donc) peut tenter d’effectuer le même jet de sauvegarde, comme s’il était la cible de l’effet associé. Le personnage doit utiliser cette capacité après que son allié a effectué son jet de sauvegarde mais avant que le MJ n’ait déclaré s’il s’agissait d’une réussite ou d’un échec. L’allié peut alors choisir d’utiliser le résultat du personnage au lieu du sien.", "Le personnage porte chance à ses compagnons de voyage.", "Halfelin porte-chance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "homme-de-main", "Chaque fois que le personnage inflige des dégâts non létaux avec une arme de corps à corps, il peut effectuer un test d’Intimidation pour démoraliser sa cible par une action libre. Si ce test réussit, la cible est secouée pendant un nombre de rounds égal aux dégâts infligés. Si l’attaque s’est soldée par un coup critique, la cible est effrayée pendant 1 round en cas de test d’Intimidation réussi, en plus d’être secouée pendant un nombre de rounds égal aux dégâts infligés.", "Le personnage sait comment susciter la peur chez ceux qu’il brutalise.", "Homme de main", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "héritage-racial", "Le personnage choisit une race humanoïde autre que humain. Pour tous les effets liés à la race, le personnage fonctionne à la fois comme humain et comme membre de cette autre race. Par exemple, s’il choisit la race des nains, le personnage fonctionne à la fois comme un humain et comme un nain lorsqu’il s’agit de choisir des traits ou des dons ou de déterminer la manière dont un sort ou un objet magique l’affecte, et ainsi de suite.", "Le sang d’un ancêtre non humain coule dans les veines du personnage.", "Héritage racial", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "inaperçu", "Au cours du premier round de combat, on considère que les ennemis du personnage qui sont pris au dépourvu ne l’ont pas encore aperçu. Il peut donc tenter un test de Discrétion pour se cacher.", "La petite taille du personnage lui permet de se dissimuler rapidement.", "Inaperçu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "interception-de-coups", "Quand le personnage utilise l’action « aider quelqu’un » pour améliorer la CA d’un allié adjacent, il peut intercepter une attaque réussie contre cet allié en utilisant une action immédiate qui lui permet de subir l’intégralité des dégâts de cette attaque, ainsi que ses effets spéciaux le cas échéant (saignement, empoisonnement, etc.) Une même créature ne peut pas bénéficier de ce don plus d’une fois par attaque.", "Le personnage se jette au devant du danger pour sauver ses alliés.", "Interception de coups", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "intuition-partagée", "Par une action de mouvement, le personnage peut donner à toutes les créatures amicales situées dans un rayon de 9 mètres (6 cases) et capables de le voir ou de l’entendre un bonus de +2 aux tests de Perception pendant un nombre de rounds égal à son modificateur de Sagesse (au minimum 1 round).", "Le personnage peut attirer l’attention des autres là où il veut.", "Intuition partagée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lancer-ki", "Lorsque le personnage réussit une manœuvre de croc-en-jambe à mains nues contre une cible de taille inférieure ou égale à la sienne, il peut la lancer et la faire tomber au sol dans n’importe quelle case de la zone qu’il contrôle. Le mouvement de la cible ne provoque pas d’attaque d’opportunité et le personnage ne peut pas la lancer dans une case occupée par une autre créature.", "Les aptitudes physiques du personnage et sa maîtrise de l’inertie lui permettent de lancer ses adversaires.", "Lancer ki", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lanceur-de-sort-allié", "Chaque fois que le personnage est adjacent à un allié possédant ce même don, il reçoit un bonus de compétences de +2 aux tests de NLS effectués pour percer la résistance à la magie d’une cible. Si l’allié a préparé le même sort (ou s’il connaît ce sort et dispose d’un emplacement de sort adéquat pour le lancer spontanément), ce bonus passe à +4 et le personnage reçoit un bonus de +1 au NLS pour le calcul de toutes les variables qui en dépendent (comme la durée, la portée et l’effet).", "Le personnage sait comment percer les protections magiques des autres créatures avec l’aide d’un allié.", "Lanceur de sort allié", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lanceur-de-sort-protégé", "Chaque fois que le personnage est adjacent à un allié qui possède également ce don, il reçoit un bonus de compétence de +4 aux tests de Concentration. Si l’allié porte une targe ou une rondache, ce bonus augmente de +1. S’il porte un écu ou un pavois, il augmente de +2. Enfin, si un ennemi contrôle à la fois l’espace où le personnage se trouve et celui où son allié se trouve et si cet ennemi possède le don Perturbateur ou une autre capacité qui augmente le DD des tests de Concentration, l’accroissement du DD est réduite de 50%.", "Les alliés du personnage le protègent quand il lance des sorts complexes.", "Lanceur de sort protégé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "manœuvres-coordonnées", "Chaque fois que le personnage est adjacent à un allié qui possède également ce don, il reçoit un bonus de compétence de +2 à tous ses tests de BMO. Ce bonus passe à +4 lorsqu’il tente de se libérer d’une lutte.", "Le personnage sait comment collaborer avec ses alliés pour accomplir de dangereuses manœuvres lors des combats.", "Manœuvres coordonnées", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "marche-sur-les-nuages", "Par une action de mouvement, le personnage peut marcher dans les airs (comme le sort) sur une distance maximale égale à la moitié de sa distance de chute ralentie avec un maximum de 15 mètres. Il doit atteindre une surface solide et suffisamment horizontale à la fin de son tour, sous peine de tomber.", "Le personnage a le pas extraordinairement léger.", "Marche sur les nuages", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-alchimiste", "Le personnage reçoit un bonus de +2 aux tests d’Artisanat (Alchimie) et il peut créer des objets alchimiques communs beaucoup plus vite que la normale. Lorsqu’il fabrique des poisons, il peut créer un nombre de doses égal à son modificateur d’Intelligence (au minimum 1) en même temps. Ces doses supplémentaires n’augmentent pas le temps requis mais bien le coût en matières premières.\r\nDe plus, chaque fois que le personnage fabrique des objets alchimiques ou des poisons à l’aide de la compétence d’Artisanat (Alchimie), on utilise la valeur de l’objet en pièces d’or au lieu de sa valeur en pièces d’argent pour calculer ses progrès (ne multipliez pas la valeur en pièces d’or par 10 pour déterminer la valeur en pièces d’argent).", "Le personnage possède une maîtrise quasi surnaturelle de l’alchimie.", "Maître alchimiste", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-des-arbalètes", "Quel que soit le type d’arbalète utilisé, il ne faut au personnage qu’une action libre pour la recharger. Lors d’une attaque à outrance, il peut effectuer autant d’attaques à l’arbalète que s’il utilisait un arc. Lorsque le personnage recharge une arbalète correspondant au type d’arbalète choisi pour le don Rechargement Rapide, il ne provoque plus d’attaque d’opportunité.", "Le personnage peut recharger les arbalètes à toute vitesse et même tirer à bout portant sans craindre de contre-attaque.", "Maîtrise des arbalètes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-combat-en-aveugle", "Lorsque le personnage porte une attaque au corps à corps, il ignore le pourcentage d’échec dû au camouflage si celui-ci est inférieur à un camouflage total et il traite les adversaires bénéficiant d’un camouflage total comme s’ils ne disposaient que d’un camouflage normal (20% d’échec des attaques au lieu de 50%). En cas d’échec, le personnage a toujours la possibilité de relancer le jet de pourcentage.\r\nSi le personnage parvient à localiser avec précision un attaquant invisible ou caché, ce dernier ne bénéficie d’aucun avantage spécial lorsqu’il attaque le personnage à distance, quelle que soit la distance qui les sépare. En d’autres termes, le personnage ne perd pas son bonus de Dextérité à la CA et l’attaquant ne bénéficie pas du bonus de +2 auquel il aurait normalement droit vu son invisibilité.", "Le personnage débusque toujours tous ses ennemis.", "Maîtrise du combat en aveugle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-tir-à-bout-portant", "Choisissez un type d’armes à distance. Le personnage ne provoque pas d’attaque d’opportunité lorsqu’il tire avec cette arme à distance depuis un espace contrôlé par un ennemi.\r\nNormal. Utiliser une arme à distance dans un espace contrôlé par un ennemi provoque des attaques d’opportunité.", "Le personnage est un expert lorsqu’il s’agit d’utiliser des armes à distance en combat rapproché.", "Maîtrise du tir à bout portant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "poursuite", "Lorsque le personnage utilise le don S’avancer pour suivre un ennemi adjacent, il peut parcourir une distance allant jusqu’à 3 mètres (2 cases). Cela ne l’empêche pas d’effectuer un pas de placement d’1,50 m (1 case) lors de son prochain tour et le déplacement effectué en utilisant le don S’avancer ne réduit en aucune manière la distance qu’il peut parcourir au cours de son prochain tour.", "Le personnage peut se rapprocher des ennemis qui tentent de le fuir sans que cela ne limite ses possibilités de déplacement.", "Poursuite", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "poing-élémentaire", "Lorsque le personnage utilise ce don, il choisit l’un des types d’énergie suivants : acide, électricité, feu ou froid. Une attaque réussie inflige des dégâts normaux plus 1d6 points de dégâts du type choisi. Le personnage doit déclarer qu’il utilise ce don avant le jet d’attaque (une attaque ratée consomme donc une tentative). Le personnage peut effectuer une attaque de Poing élémentaire par jour par tranche de 4 niveaux (voir Spécial), mais jamais plus d’une par round.", "Le personnage peut renforcer ses coups avec de l’énergie élémentaire.", "Poing élémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "perfection-magique", "Le personnage choisit un sort qu’il est capable de lancer. Chaque fois qu’il lance ce sort, il peut appliquer un des dons de métamagie qu’il connaît sans modifier le niveau du sort ni le temps d’incantation tant que le niveau modifié du sort ne dépasse pas le 9<sup>ème</sup> niveau. De plus, si le personnage possède d’autres dons qui lui permettent d’appliquer un bonus numérique fixe à l’une des variables du sort (comme Arme de prédilection (rayon), École renforcée ou Efficacité des sorts accrue par exemple), le bonus est doublé lorsqu’il s’agit du sort choisi pour ce don.", "Le personnage sait lancer un sort en particulier comme personne d’autre.", "Perfection magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "perception-via-la-pierre", "Le personnage gagne la capacité de perception des vibrations sur une portée de 3 mètres (2 cases).", "Le personnage peut sentir les mouvements via la terre et les pierres qui l’entourent.", "Perception via la pierre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "perception-de-la-peur", "Le personnage reçoit un bonus de +4 aux tests de Perception pour détecter les créatures affectées par un état préjudiciable parmi secoué, effrayé et paniqué. Il peut également utiliser sa compétence de Perception pour effectuer les tests de Psychologie contre les cibles possédant l’un de ces états préjudiciables ou tentant de camoufler leur peur d’une manière ou d’une autre.", "Le personnage peut percevoir l’odeur amère de la peur.", "Perception de la peur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "peau-de-fer", "Le personnage gagne un bonus d’armure naturelle de +1 grâce à sa peau particulièrement épaisse.", "La peau du personnage est plus épaisse et plus résistante que celle d’un membre normal de sa race.", "Peau de fer", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "griffes-magiques", "Les armes naturelles du personnage fonctionnent à la fois comme des armes magiques et des armes en argent vis-à-vis de la résistance aux dégâts.", "Le personnage n’a pas besoin d’armes magiques : ces babioles enchantées font pâle figure à côté de sa férocité bestiale.", "Griffes magiques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "patrouille-en-combat", "Par une action complexe, le personnage peut se mettre à patrouiller au cours d’un combat. Concrètement, il augmente le rayon de la zone qu’il contrôle de 1,50 m pour chaque tranche de 5 points dans son BBA. Jusqu’au début de son prochain tour, il peut porter des attaques d’opportunité contre n’importe quel adversaire qui en provoque une au sein de la zone qu’il contrôle. Il peut se déplacer au cours de ces attaques d’opportunité, pour autant que le déplacement total qu’il effectue avant le début de son prochain tour ne dépasse pas sa vitesse de déplacement. Les déplacements du personnage provoquent des attaques d’opportunité normalement.", "Le personnage se déplace sur le champ de bataille pour faire face aux menaces où qu’elles surviennent.", "Patrouille en combat", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pas-léger", "Le personnage peut ignorer les effets des terrains difficiles dans les environnements naturels et fonctionner comme s’il s’agissait d’un terrain normal.", "Le personnage peut agilement se frayer un passage à travers tous les terrains, même les plus dangereux et les plus accidentés.", "Pas léger", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pas-de-côté", "Chaque fois qu’un adversaire rate le personnage avec une attaque au corps à corps, il peut se déplacer de 1,50 m (1 case) par une action immédiate, pour autant qu’il reste dans l’espace contrôlé par l’ennemi en question. Ce déplacement ne provoque pas d’attaque d’opportunité. Si le personnage choisit de faire ce déplacement, il ne peut pas effectuer de pas de placement de 1,50 m au cours de son prochain tour. S’il utilise une action de mouvement pour se déplacer au cours de son prochain tour, il doit soustraire 1,50 m (1 case) à la distance totale qu’il peut parcourir.", "Le personnage peut se repositionner lorsqu’il échappe à une attaque manquée.", "Pas de côté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "parade-de-sorts", "Chaque fois que le personnage parvient à contrer un sort, celui-ci retourne vers celui qui l’a lancé. Cela fonctionne exactement comme le sort renvoi des sorts.", "Le personnage peut renvoyer à ses ennemis les sorts qu’ils lui lancent.", "Parade de sorts", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "par-dessus-et-par-dessous", "Si une créature plus grande que le personnage tente de l’agripper et échoue, le personnage peut effectuer une tentative de croc-en-jambe contre elle par une action immédiate et avec un bonus de +2. Cela ne provoque pas d’attaque d’opportunité.", "Le personnage peut se glisser sous les ennemis qui tentent de l’agripper pour les faire tomber.", "Par-dessus et par-dessous", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "odorat-supérieur", "Le personnage gagne la capacité spéciale d’odorat.", "Le personnage a le nez aussi fin que celui d’un prédateur sauvage.", "Odorat supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mur-de-boucliers", "Chaque fois que le personnage porte un bouclier et est adjacent à un allié qui porte aussi un bouclier et possède ce don, le bonus de CA que le personnage obtient de son bouclier augmente en fonction du type de bouclier utilisé par son allié. Si l’allié porte une targe ou une rondache, le bonus de bouclier du personnage augmente de +1. Si l’allié porte un écu ou un pavois, le bonus de bouclier du personnage augmente de +2. Le personnage conserve ces bonus même si son allié perd son propre bonus de bouclier parce qu’il porte un coup de bouclier. Si l’allié adjacent au personnage utilise un pavois pour gagner un abri total, le personnage en bénéficie également si l’attaque qui le cible passe à travers le bord de case correspondant au bouclier (voir la description du pavois).", "Le personnage peut former un mur défensif avec ceux qui l’entourent.", "Mur de boucliers", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "passer-pour-un-humain", "Lorsque le personnage se déguise en humain, il reçoit un bonus de +10 au test de Déguisement et le fait de se déguiser en une créature d’une autre race ne lui impose aucune pénalité. Dans les zones colonisées par les humains ou à forte population humaine, il peut prendre 10 sur ses jets de Déguisement, ce qui signifie que la plupart des gens pensent qu’il est humain, à moins qu’on ne leur donne une raison d’en douter.", "On prend souvent le personnage pour un humain alors qu’il est d’une race différente.", "Passer pour un humain", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "griffes-coupantes", "Si le personnage touche une créature avec ses deux attaques de griffes au cours de son tour, la seconde griffe inflige 1d6 points de dégâts supplémentaires. Il s’agit de dégâts de précision qui ne sont pas multipliés en cas de coup critique. Le personnage peut utiliser ce don une fois par round.", "Les griffes du personnage infligent des dégâts supérieurs.", "Griffes coupantes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "grande-tenaille", "Chaque fois que le personnage et un allié possédant également ce don prennent un adversaire en tenaille, le bonus de tenaille dont bénéficient les jets d’attaque du personnage passe à +4. De plus, chaque fois qu’il porte un coup critique contre la créature prise en tenaille, cette dernière entraîne une attaque d’opportunité de la part de l’allié du personnage.", "Le personnage est à l’affût de toutes les ouvertures lorsqu’il prend un adversaire en tenaille.", "Grande tenaille", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "garde-du-corps", "Lorsqu’un allié adjacent au personnage est attaqué, le personnage peut utiliser une attaque d’opportunité pour tenter d’améliorer la CA de son allié grâce à une action « aider quelqu’un ». Ce don ne lui permet pas d’utiliser « aider quelqu’un » pour améliorer le jet d’attaque de son allié.", "Les attaques rapides que porte le personnage empêchent ses adversaires d’attaquer les alliés qui se trouvent près de lui.", "Garde du corps", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-déséquilibrant", "Chaque fois que le personnage porte un coup critique avec une attaque au corps à corps, il peut déséquilibrer son adversaire en plus de lui infliger des dégâts normaux. Si le jet de confirmation du coup critique est supérieur ou égal au DMD de la cible, le personnage peut la mettre à terre comme s’il effectuait une manœuvre de croc-en-jambe réussie. Cela ne provoque pas d’attaque d’opportunité.\r\nNormal. Il faut réaliser un test de manœuvre séparé pour mettre un adversaire au sol.", "La puissance des coups critiques du personnage est telle qu’ils peuvent faire tomber ses ennemis.", "Coup déséquilibrant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-désarmant", "Chaque fois que le personnage porte un coup critique avec une attaque au corps à corps, il peut désarmer son adversaire en plus de lui infliger des dégâts normaux. Si le jet de confirmation du coup critique est supérieur ou égal au DMD de la cible, le personnage peut la désarmer comme s’il effectuait une manœuvre de désarmement réussie. Cela ne provoque pas d’attaque d’opportunité.\r\nNormal. Il faut réaliser un test de manœuvre séparé pour désarmer un adversaire.", "Le personnage peut désarmer ses ennemis lorsqu’il porte un coup critique.", "Coup désarmant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-destructeur", "Chaque fois que le personnage porte un coup critique avec une attaque au corps à corps, il peut détruire l’arme de son adversaire en plus d’infliger des dégâts normaux à celui-ci. Si le jet de confirmation du coup critique est supérieur ou égal au DMD de la cible, le personnage peut endommager l’arme de son ennemi comme s’il effectuait une manœuvre de destruction réussie. Cela ne provoque pas d’attaque d’opportunité.\r\nNormal. Il faut réaliser un test de manœuvre séparé pour détruire l’arme d’un adversaire.", "Les coups critiques du personnage peuvent détruire les armes.", "Coup destructeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-de-la-cockatrice", "Par une action complexe, le personnage peut porter une unique attaque à mains nues contre un ennemi ébloui, pris au dépourvu, paralysé, chancelant, étourdi ou inconscient. Si cette attaque résulte en un coup critique, la cible est pétrifiée, à moins qu’elle ne réussisse un jet de Vigueur contre un DD égal à 10 + la moitié du niveau du personnage + son modificateur de Sagesse. Il s’agit d’un effet surnaturel de métamorphose.", "Le personnage peut transformer la chair de sa cible en pierre.", "Coup de la cockatrice", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-de-flèche", "Lorsque le personnage est adjacent à un adversaire et qu’il effectue une attaque à outrance avec un arc (court, long ou composite), il peut choisir d’effectuer une attaque au corps à corps avec une flèche plutôt que de la décocher avec l’arc. Si l’attaque touche (peu importe si elle inflige des dégâts), le personnage repousse la cible en arrière sur 1,50 m (1 case). Il peut ensuite tirer normalement des flèches avec son arc, en visant la même cible ou un autre ennemi à portée. Cette attaque au corps à corps remplace l’attaque supplémentaire donnée par Tir rapide et tous les jets d’attaque que le personnage effectue au cours de ce round (l’attaque de corps à corps et les attaques à distance) subissent une pénalité de -2. Si, après l’attaque initiale, le personnage ne se trouve plus dans la zone contrôlée par aucun ennemi, il peut effectuer les attaques à distance suivantes sans provoquer d’attaques d’opportunité.", "Le personnage sait comment faire de la place pour pouvoir continuer à utiliser son arc.", "Coup de flèche", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-de-bouclier-opportuniste", "Chaque fois que le personnage inflige un coup critique avec une arme de corps à corps, il peut, par une action libre, effectuer une attaque de bouclier contre la même cible en utilisant le même bonus d’attaque.", "Lorsque le personnage inflige un coup puissant avec son arme, il peut en profiter pour enchaîner avec un coup de bouclier.", "Coup de bouclier opportuniste", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-mémorable", "Le personnage doit déclarer qu’il utilise ce don avant de réaliser son jet d’attaque (de sorte qu’un échec compte comme une tentative). En cas de réussite, l’attaque inflige des dégâts normaux et le personnage peut choisir de repousser sa cible de 1,50 m (1 case) ou de tenter de la faire tomber. S’il choisit de la repousser, la cible s’éloigne de 1,50 m (1 case) de lui en ligne droite. Ce mouvement ne provoque pas d’attaque d’opportunité et la cible doit terminer son déplacement dans un endroit sûr suffisamment grand pour l’accueillir. Si le personnage choisit plutôt de tenter de la faire tomber, la cible peut tenter un jet de Vigueur contre un DD égal à 10 + la moitié du niveau du personnage + le modificateur de Sagesse du personnage pour annuler l’effet. Le personnage peut tenter un coup mémorable par jour pour chaque tranche de 4 niveaux qu’il possède (mais voir Spécial), mais jamais plus d’une fois par round.", "Les coups donnés par le personnage sont si puissants qu’il peut les utiliser pour repousser ou faire tomber ses ennemis.", "Coup mémorable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-dans-lombre", "Le personnage peut infliger des dégâts de précision (comme des dégâts d’attaque sournoise) aux cibles qui bénéficient d’un camouflage (mais pas d’un camouflage total).", "Le personnage peut frapper avec précision même lorsqu’il ne peut pas voir clairement ses ennemis.", "Coup dans l'ombre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cosmopolite", "Le personnage peut parler et lire deux langues de plus (de son choix). En plus de cela, il peut choisir deux compétences dépendant de l’Intelligence, de la Sagesse ou du Charisme, qui deviennent des compétences de classe pour lui.", "En vivant dans de grandes villes exotiques, le personnage a rencontré de nombreuses civilisations, cultures et races.", "Cosmopolite", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "connaissances-magiques-étendues", "Le personnage peut ajouter à sa liste de sorts connus un sort issu de la liste des sorts de sa classe. Ce sort vient en plus de ceux qu’il gagne normalement en obtenant un niveau supplémentaire dans sa classe. Au lieu de cela, le personnage peut choisir deux sorts issus de la liste des sorts de sa classe et appartenant à des niveaux strictement inférieurs au niveau de sorts maximum auquel il a accès (pour la classe concernée). Une fois faits, tous ces choix ne peuvent plus être modifiés par la suite.", "Les recherches du personnage lui ont révélé de nouveaux sorts.", "Connaissances magiques étendues", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "connaissance-supérieure-de-la-pierre", "Le personnage reçoit un bonus de +4 aux tests de Perception pour détecter des ouvrages de pierre inhabituels. Ce bonus remplace le bonus normal de Perception donné par la capacité de connaissance de la pierre.", "Rien ne peut tromper les sens du personnage lorsqu’il s’agit d’ouvrages en pierre.", "Connaissance supérieure de la pierre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "concentration-malgré-la-fureur", "Lorsque le personnage manipule une arme à deux mains ou une arme à une main tenue à deux mains et qu’il utilise le don Attaque en puissance, il ne subit pas le malus aux jets d’attaque normalement imposé par ce don lors de la première attaque de chacun de ses tours. La pénalité s’applique normalement aux autres attaques cependant (y compris aux attaques d’opportunité).", "Au milieu du carnage et des coups féroces et sanglants, le personnage reste concentré. Même ses attaques les plus sauvages font mouche.", "Concentration malgré la fureur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combattre-au-delà-de-la-mort", "Une fois par jour, le personnage peut gagner un nombre de points de vie temporaires égal à son modificateur de Constitution. Il peut activer ce don par une action immédiate lorsqu’il est réduit à 0 point de vie ou moins, ce qui peut lui permettre d’éviter de mourir. Ces points de vie temporaires persistent pendant 1 minute. Si les points de vie du personnage descendent sous 0 lorsque ces points de vie temporaires disparaissent, il tombe inconscient et devient mourant (conformément aux règles normales). S’il possède également le trait racial Férocité, il peut l’utiliser après la disparition des points de vie temporaires offerts par ce don.", "Le personnage peut continuer à combattre même lorsqu’il devrait normalement être mort.", "Combattre au-delà de la mort", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-renversante", "Lorsque le personnage charge, il peut tenter de renverser une créature située dans la trajectoire de sa charge par une action libre. Si la tentative de renversement est couronnée de succès, il peut poursuivre sa charge. En cas d’échec, sa charge se termine dans l’espace précédant cette créature.", "Le personnage peut renverser des ennemis lorsqu’il charge.", "Charge renversante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-bousculant", "Chaque fois que le personnage porte un coup critique avec une attaque au corps à corps, il peut repousser son adversaire en plus de lui infliger des dégâts normaux. Si le jet de confirmation du coup critique est supérieur ou égal au DMD de la cible, le personnage peut la repousser comme s’il effectuait une manœuvre de bousculade réussie. Le personnage ne doit pas forcément se déplacer avec la cible en cas de réussite. Cela ne provoque pas d’attaque d’opportunité.\r\nNormal. Il faut réaliser un test de manœuvre séparé pour bousculer un adversaire.", "Les coups critiques du personnage peuvent faire reculer ses ennemis.", "Coup bousculant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pouvoir-de-rage-supplémentaire", "Le personnage gagne un pouvoir de rage supplémentaire. Il doit en remplir les conditions d’accès.", "Le personnage a développé une nouvelle capacité utilisable lorsqu’il est en rage.", "Pouvoir de rage supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-odieux", "Le personnage peut effectuer un coup de grâce sur une cible recroquevillée sur elle-même ou étourdie.", "Le personnage sait comment profiter de l’état d’un ennemi mal en point pour tenter un coup de grâce.", "Coup odieux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-précis", "Chaque fois que le personnage et un allié possédant également ce don prennent en tenaille un même adversaire, le personnage lui inflige 1d6 points de dégâts de précision supplémentaires lors de chaque attaque au corps à corps réussie. Ces dégâts en bonus se cumulent avec les autres sources de dégâts de précision comme les attaques sournoises. Ces dégâts supplémentaires ne sont pas multipliés en cas de coup critique.", "Le personnage sait comment frapper là où ça fait mal, pour autant qu’un de ses alliés distraie sa cible.", "Coup précis", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "expériences-variées", "Le personnage obtient un bonus de +2 à tous les tests de Connaissances et de Profession  et il peut utiliser ces compétences même sans formation.", "Même si le personnage est encore jeune pour sa race, il a déjà accumulé de nombreux savoirs et talents.", "Expériences variées", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "expertise-magique-mineure", "Le personnage choisit un sort de 1<sup>er</sup> niveau qu’il connaît. Il peut lancer ce sort deux fois par jour en tant que pouvoir magique avec un NLS égal à son NLS dans la classe associée à la liste de sorts à laquelle il appartient. Le DD du jet de sauvegarde pour ce pouvoir magique dépend du Charisme du personnage. Les sorts qui requièrent une composante matérielle ou un focalisateur coûteux ne peuvent pas être choisis pour ce don. Le personnage ne peut pas appliquer de dons de métamagie au sort choisi.", "Le personnage peut lancer un sort de 1<sup>er</sup> niveau en tant que pouvoir magique.", "Expertise magique mineure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "expertise-magique-majeure", "Le personnage choisit un sort qu’il connaît et qui appartient au 5<sup>ème</sup> niveau ou à un niveau inférieur. Il peut lancer ce sort deux fois par jour en tant que pouvoir magique avec un NLS égal à son NLS dans la classe associée à la liste de sorts d’où le sort choisi est extrait. Le DD du jet de sauvegarde pour ce pouvoir magique dépend du Charisme du personnage. Les sorts qui requièrent une composante matérielle ou un focalisateur coûteux ne peuvent pas être choisis pour ce don. Le personnage ne peut pas appliquer de dons de métamagie au sort choisi.", "Le personnage peut lancer un sort de bas niveau en tant que pouvoir magique.", "Expertise magique majeure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "entraînement-supérieur", "Le personnage reçoit un bonus de +2 aux tests des tentatives d’entraînement. Ce bonus se cumule avec celui offert par le don Science de l’entraînement. Chaque fois que le personnage entraîne un ennemi derrière lui, le mouvement de ce dernier provoque des attaques d’opportunité de la part de tous les alliés du personnage (mais pas du personnage lui-même).", "Les ennemis que le personnage entraîne avec lui sont déséquilibrés.", "Entraînement supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "démarche-de-laraignée", "Par une action de mouvement, le personnage peut se déplacer de la moitié de la distance de chute ralentie (maximum 15 mètres) sur un mur, au plafond, sur une corde, sur des branches ou même sur l’eau ou n’importe quelle autre surface qui ne pourrait normalement pas supporter son poids. Il doit atteindre une surface solide et horizontale à la fin de son tour, sous peine de tomber.", "La maîtrise physique du personnage lui permet de marcher là où d’autres ne le peuvent pas.", "Démarche de l'araignée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "défenses-coordonnées", "Chaque fois que le personnage est adjacent à un allié qui possède également ce don, il reçoit un bonus de compétence de +2 à son DMD. Ce bonus passe à +4 si la créature qui tente la manœuvre appartient à une catégorie de taille plus grande que celles du personnage et de son allié.", "Le personnage sait comment combattre avec ses alliés pour éviter d’être victime de manœuvres de combat.", "Défenses coordonnées", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-parfait", "Le personnage doit déclarer qu’il utilise ce don avant de faire son jet d’attaque (de sorte qu’un échec compte comme une tentative). Il doit manier l’une des armes suivantes pour porter un coup parfait : bâton, kama, nunchaku, sai ou siangham. Il peut effectuer deux jets d’attaque et choisir le résultat le plus élevé. Si l’un de ces jets indique une possibilité de critique, l’autre dé est utilisé comme jet de confirmation (le joueur peut choisir quel dé utiliser pour l’attaque et pour la confirmation si les deux résultats indiquent une possibilité de critique). Le personnage peut tenter un coup parfait par jour pour chaque tranche de 4 niveaux qu’il possède (mais voir Spécial), mais jamais plus d’une fois par round.", "Lorsque le personnage utilise une arme de moine, ses attaques peuvent être extrêmement précises.", "Coup parfait", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "découverte-supplémentaire", "Le personnage gagne une découverte alchimique de plus dont il doit en remplir les conditions d’accès.", "Le personnage a réalisé une nouvelle découverte alchimique.", "Découverte supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cul-sec", "Une action rapide suffit (au lieu d’une action simple) pour consommer de l’alcool fort et gagner du ki temporaire.", "Le personnage avale rapidement l’alcool pour en tirer du ki.", "Cul sec", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "crocs-acérés", "Le personnage dispose d’une attaque de morsure qui inflige 1d4 points de dégâts plus son modificateur de Force. Il est formé à l’utilisation de cette arme et peut lui appliquer des dons ou des effets fonctionnant sur les attaques naturelles. S’il utilise son attaque de morsure au cours d’une attaque à outrance, elle fonctionne comme une attaque secondaire effectuée à son plein BBA moins 5 et dont les dégâts sont majorés de la moitié de son modificateur de Force.", "La puissante mâchoire ou dentition du personnage est assez dangereuse pour lui permettre d’attaquer en mordant.", "Crocs acérés", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-handicapant", "Chaque fois que le personnage porte un coup critique contre un adversaire, la vitesse de déplacement de celui-ci est divisée par deux pendant 1 minute. Un jet de Vigueur réussi réduit la durée à 1d4 rounds. Le DD de ce jet de sauvegarde est de 10 + le BBA du personnage. Si la cible possède plusieurs modes de mouvement, il doit choisir lequel d’entre eux est affecté. S’il s’agit d’une cible volante, elle doit réussir un test de Vol de DD 10 pour rester en vol et elle voit sa manœuvrabilité réduite d’une catégorie.", "Le personnage peut affecter les capacités de déplacement des cibles qu’il met à mal.", "Critique handicapant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coups-protecteurs", "Lorsque le personnage effectue une attaque à outrance avec une arme à deux mains, il peut choisir de réduire les dégâts de 50% en échange d’un bonus de bouclier de +4 à la CA et au DMD jusqu’au début de son prochain tour. La réduction de dégâts s’applique jusqu’au début de son prochain tour.", "Le personnage peut se protéger en enchaînant plusieurs attaques sauvages.", "Coups protecteurs", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "couple-dopportunistes", "Chaque fois que le personnage est adjacent à un allié qui possède également ce don, il reçoit un bonus de circonstances de +4 aux attaques d’opportunité portées contre les créatures situées dans une zone contrôlée à la fois par le personnage et son allié. De plus, chaque fois qu’un ennemi provoque une attaque d’opportunité de la part de l’allié du personnage, le personnage a également l’occasion de lui porter une attaque d’opportunité (à condition que l’ennemi en question se trouve dans une case contrôlée par le personnage). Le personnage a le droit de porter cette attaque d’opportunité même si, en temps normal, la situation ou l’une des capacités de l’ennemi l’en empêcherait. Cela ne permet toutefois pas au personnage d’effectuer plus qu’une seule attaque d’opportunité en réponse à une action donnée.", "Le personnage sait comment profiter d’un ennemi qui baisse sa garde.", "Couple d'opportunistes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-repositionnant", "Chaque fois que le personnage porte un coup critique avec une attaque au corps à corps, il peut déplacer son adversaire en plus de lui infliger des dégâts normaux. Si le jet de confirmation du coup critique est supérieur ou égal au DMD de la cible, le personnage peut la déplacer comme s’il effectuait une manœuvre de repositionnement réussie. Cela ne provoque pas d’attaque d’opportunité.\r\nNormal. Il faut réaliser un test de manœuvre séparé pour repositionner un adversaire.", "Les coups critiques infligés par le personnage peuvent forcer ses ennemis à se déplacer selon ses désirs.", "Coup repositionnant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cœur-de-vermine", "Les sorts ou capacités spéciales du personnage qui n’affectent normalement que les animaux peuvent cibler les vermines (celles qui pouvaient déjà affecter les vermines continuent de fonctionner). Il peut utiliser sa capacité d’empathie sauvage pour influencer les vermines aussi facilement que s’il s’agissait d’animaux.", "Un lien spécial unit le personnage avec toutes les choses qui rampent, glissent, serpentent et piquent.", "Cœur de vermine", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chanteur-des-roches", "Lorsque le personnage utilise une représentation bardique avec une composante sonore sous terre, la portée ou la zone de la représentation choisie est doublée et elle peut affecter les créatures [sourd|sourdes] si elles possèdent la capacité de perception des vibrations et se trouvent dans la nouvelle portée de la représentation. De plus, le DD des jets de sauvegarde contre les représentations bardiques du personnage augmente de +2 pour les créatures du sous-type Terre et ce, quel que soit l’endroit où cela se produit.", "Les chansons du personnage évoquent les traditions et les terres de son peuple.", "Chanteur des roches", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prestidigitateur-gnome", "En plus des pouvoirs magiques gnomes normaux, le personnage gagne les suivants : 1/jour – manipulation à distance et prestidigitation.", "Les talents magiques du personnage ne se limitent pas aux illusions.", "Prestidigitateur gnome", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "profil-bas", "Le personnage gagne un bonus d’esquive de +1 à la CA contre les attaques à distance. De plus, il n’offre pas d’abri mou aux créatures lorsque les attaques à distance passent à travers sa case.", "Grâce à sa stature réduite, le personnage peut facilement éviter les attaques à distance.", "Profil bas", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "spécialisation-supérieure-aux-boucliers", "Choisissez un des types de boucliers (targe, rondache, écu ou pavois) pour lequel le personnage possède le don Spécialisation au bouclier. Lorsqu’il utilise un bouclier de ce type, il gagne un bonus de +2 à la CA contre les jets de confirmation des critiques (ce bonus se cumule avec celui offert par le don Spécialisation au bouclier). De plus, une fois par jour, le personnage peut annuler un coup critique (les dégâts sont donc déterminés normalement).", "Le personnage maîtrise parfaitement l’utilisation des boucliers et parvient à encore mieux protéger ses organes vitaux.", "Spécialisation supérieure aux boucliers", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "subtilisation-supérieure", "Le personnage reçoit un bonus de +2 aux tests visant à subtiliser un objet à un ennemi. Ce bonus se cumule avec celui offert par le don Science de la subtilisation. S’il réussit à subtiliser un objet à un ennemi pendant un combat, ce dernier ne remarque pas le vol avant la fin de l’affrontement (à moins qu’il ne tente d’utiliser l’objet entretemps).", "Le personnage est doué lorsqu’il s’agit de subtiliser des objets sur ses adversaires au cours d’un combat.", "Subtilisation supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tacticien-de-la-téléportation", "Toute créature utilisant un effet de téléportation pour entrer ou sortir d’une case contrôlée par le personnage provoque une attaque d’opportunité de sa part, même si elle incante sur la défensive ou utilise une capacité surnaturelle.", "Le personnage est toujours prêt à faire face aux ennemis qui utiliseraient la téléportation pour s’approcher de lui ou le fuir.", "Tacticien de la téléportation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tacticien-expérimenté", "Le personnage peut utiliser sa capacité de tacticien pour octroyer à ses alliés un don d’équipe une fois de plus par jour.", "Avec quelques gestes rapides et deux ou trois ordres, le personnage peut diriger ses alliés au combat.", "Tacticien expérimenté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "talent-magique", "Choisissez un sort de niveau 0 appartenant à la liste des ensorceleurs/magiciens. Le personnage peut lancer ce sort trois fois par jour en tant que pouvoir magique avec un NLS égal à son niveau de personnage. Le DD du jet de sauvegarde vaut 10 + le modificateur de Charisme du personnage.", "La magie coule dans les veines du personnage.", "Talent magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "talent-supplémentaire", "Le personnage gagne un talent supplémentaire. Il doit en remplir les conditions d’accès.", "Grâce à son entraînement constant, le personnage est parvenu à maîtriser une nouvelle botte secrète.", "Talent supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-avec-concentration", "Par une action simple, le personnage peut effectuer une attaque avec un arc ou une arbalète et ajouter son modificateur d’Intelligence aux dégâts. Il doit se trouver à au plus 9 mètres (6 cases) de sa cible pour pouvoir lui infliger ces dégâts supplémentaires. Les créatures immunisées contre les coups critiques et les attaques sournoises le sont également contre ces dégâts supplémentaires.", "L’intuition du personnage en matière d’anatomie lui permet de rendre ses tirs plus mortels.", "Tir avec concentration", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-en-retraite", "Une fois par rencontre, lorsque le personnage utilise l’action de retraite, il peut effectuer une unique attaque à distance à n’importe quel moment au cours de son déplacement.\r\nNormal. Le personnage ne peut pas attaquer au cours d’une retraite.", "Le personnage est un tirailleur expert capable de faire s’abattre une pluie de flèches sur ses ennemis tout en avançant ou en battant en retraite.", "Tir en retraite", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-perturbateur", "Si le personnage prépare une action afin de tirer sur un adversaire situé dans un rayon de 9 mètres (6 cases) au cas où celui-ci lancerait un sort et si cette attaque à distance réussit, le DD du test de Concentration de la cible pour mener à bien son incantation augmente de +4.", "Grâce à la précision de ses tirs, le personnage peut mener la vie dure à un adversaire tentant de lancer des sorts.", "Tir perturbateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tirailleur-monté", "Si la monture du personnage se déplace de sa vitesse de déplacement ou moins, il peut effectuer une attaque à outrance.", "Le personnage sait parfaitement attaquer depuis le dos d’une monture avançant à vive allure.", "Tirailleur monté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "toucher-de-la-sérénité", "Le personnage doit déclarer qu’il utilise ce don avant d’effectuer son jet d’attaque (de sorte qu’un échec compte comme une tentative). En cas de réussite, l’attaque n’inflige aucun dégât et n’impose aucun effet ni état préjudiciable, mais la cible ne peut plus lancer de sorts ni attaquer (la restriction porte aussi sur les attaques d’opportunité et les attaques sous forme d’actions immédiates) pendant 1 round, à moins de réussir un jet de Volonté contre un DD égal à 10 + la moitié du niveau du personnage + son modificateur de Sagesse. Le personnage peut tenter d’effectuer un toucher de la sérénité par jour par tranche de 4 niveaux de personnage (mais voir Spécial), mais jamais plus d’une fois par round.", "D’un simple toucher, le personnage peut réduire la dangerosité de n’importe quel ennemi, même le plus violent.", "Toucher de la sérénité", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "traits-supplémentaires", "Le personnage gagne 2 traits supplémentaires (choisis par le joueur). Ces traits doivent appartenir à deux listes différentes, toutes deux distinctes de celles dans lesquelles le personnage a déjà choisi un trait. Il doit remplir toutes les conditions d’accès à ces traits.", "Le personnage possède plus de traits que la normale.", "Traits supplémentaires", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "transmutation-tenace", "Le DD des tests de NLS pour dissiper ou annuler une transmutation lancée par le personnage augmente de +2. De plus, même si le sort est annulé, ses effets persistent pendant 1 round de plus avant d’être dissipés.", "Le personnage maîtrise la magie du changement et, de ce fait, ses transmutations sont plus durables que la moyenne.", "Transmutation tenace", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "travail-en-équipe", "Lorsque le personnage et au moins deux de ses alliés sont adjacents à un adversaire, il peut tenter d’utiliser l’action « aider quelqu’un » par une action de mouvement.", "Lorsque le personnage fait partie d’un groupe qui attaque un ennemi, il peut aider un allié par une feinte rapide.", "Travail en équipe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vigie", "Chaque fois que le personnage est adjacent à un allié possédant ce même don et capable d’agir lors d’un round de surprise, le personnage lui aussi peut agir au cours du round de surprise. Si, sans ce don, le personnage n’avait pas pu agir au cours du round de surprise, son initiative est égale au minimum entre son jet d’initiative et celui de son allié réduit de 1. Si le personnage et son allié avaient pu agir au cours du round de surprise même sans ce don, alors le personnage peut accomplir une action simple et une action de mouvement (ou une action complexe) pendant ce round de surprise.", "Les alliés du personnage l’aident à ne pas se laisser surprendre.", "Vigie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vigueur-en-rage", "Chaque fois que le personnage est en rage, le bonus de moral qui affecte sa Constitution augmente de +2. La rage ne se termine pas automatiquement s’il tombe inconscient : tant qu’il est inconscient, il doit dépenser un round de son quota quotidien de rage chaque round.", "Lorsque le personnage est en rage, il est plein de vigueur.", "Vigueur en rage", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "visage-de-pierre", "Le personnage gagne un bonus de +4 aux tests de Bluff pour mentir ou cacher son opinion ou ses véritables motivations, mais pas pour feinter en combat ni faire passer un message secret. De plus, le DD des tests de Psychologie pour obtenir un pressentiment concernant le personnage est de 25 au lieu de 20.", "Certains cailloux sont plus expressifs que le personnage !", "Visage de pierre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "capture", "Lorsque la créature touche un adversaire à l’aide d’une attaque de griffe ou de morsure, elle peut l’agripper comme si elle possédait la capacité d’étreinte. Si cet adversaire appartient à une catégorie de taille inférieure de trois crans ou plus à celle de la créature, cette dernière peut l’écraser et lui infliger automatiquement des dégâts de griffe ou de morsure chaque round en réussissant un test de manœuvre  offensive. Si l’adversaire est maintenu dans la gueule de la créature, il ne reçoit pas de jet de Réflexes pour résister au souffle de la créature (si elle possède une telle attaque).\r\nLa créature peut lâcher l’adversaire agrippé par une action libre ou le lancer au loin en utilisant une action simple. Dans ce cas, l’adversaire parcourt une distance de 1d6 × 3 m (2 c) et subit 1d6 points de dégât par tranche de 3 m (2 c) dans cette distance. Si la créature est en vol lorsqu’elle lance son adversaire, celui-ci subit soit les dégâts indiqués plus haut, soit les dégâts correspondant à la chute (les plus élevés des deux).", "La créature peut facilement attraper des proies.", "Capture", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaques-multiples", "Les attaques que la créature effectue avec des armes naturelles secondaires ne subissent qu’une pénalité de –2.", "La créature est particulièrement douée pour attaquer avec ses armes naturelles.", "Attaques multiples", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-spéciale-renforcée", "Les effets de ce don s’appliquent à une des attaques spéciales de la créature. Le DD de tous les jets de sauvegarde pour résister à celle-ci augmente de +2.", "Il est particulièrement difficile de résister à l’une des attaques spéciales de la créature.", "Attaque spéciale renforcée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-en-vol", "Lorsqu’elle est en vol, la créature peut effectuer une action de mouvement et autre action simple à n’importe quel moment de son déplacement. Elle ne peut pas réaliser une seconde action de mouvement au cours d’un round où elle utilise Attaque en vol.", "La créature peut se déplacer avant et après une attaque lorsqu’elle est en vol.", "Attaque en vol", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "armure-naturelle-supérieure", "Le bonus d’armure naturelle de la créature augmente de +1.", "La peau de la créature est plus épaisse que la normale.", "Armure naturelle supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-naturelle-supérieure", "Les effets de ce don s’appliquent à une des armes naturelles de la créature (mais pas aux attaques à mains nues). Ses dégâts augmentent d’une catégorie selon la progression suivante (comme si la taille de la créature avait augmenté d’un cran). Les dés de dégâts progressent comme suit : 1d2, 1d3, 1d4, 1d6, 1d8, 2d6, 3d6, 4d6, 6d6, 8d6, 12d6.\r\nSi l’arme ou l’attaque inflige 1d10 points de dégâts, la progression est la suivante : 1d10, 2d8, 3d8, 4d8, 6d8, 8d8, 12d8.", "Les attaques naturelles de la créature infligent de sévères blessures.", "Arme naturelle supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "spécialisation-au-bouclier", "Choisissez un des types de boucliers (targe, rondache, écu ou pavois). Lorsque le personnage utilise ce type de bouclier, il gagne un bonus de +2 à la CA contre les jets de confirmation des coups critiques. De plus, il peut ajouter le bonus de base de son bouclier (y compris le bonus octroyé par Art du bouclier, mais pas le bonus d’altération) à son DMD.", "Le personnage a maîtrisé le maniement d’un type de bouclier.", "Spécialisation au bouclier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "élément-supérieur", "Le DD de tous les jets de sauvegarde contre les sorts qui infligent des dégâts du type choisi et qui sont lancés par le personnage augmente de +1. Ce bonus se cumule avec celui offert par le don Élément renforcé.", "Le personnage choisit un élément auquel il a déjà appliqué le don Élément renforcé. Il est extrêmement difficile de résister aux sorts utilisant ce type d’énergie lorsqu’ils sont lancés par le personnage.", "Élément supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "éclectisme", "Le personnage peut choisir une classe de prédilection de plus et gagner soit +1 point de vie soit +1 point de compétence chaque fois qu’il gagne un niveau dans cette classe. S’il opte pour une classe dans laquelle il possède déjà des niveaux, il peut profiter rétroactivement de ce don.", "Le personnage a un don pour suivre plusieurs vocations à la fois.", "Éclectisme", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "échange-de-place", "Chaque fois que le personnage est adjacent à un allié qui possède également ce don, il peut se déplacer vers la case de cet allié au cours d’un mouvement normal. En même temps, par une action immédiate, l’allié se déplace vers la case où le personnage se trouvait avant de prendre sa place. Le personnage et son allié doivent être tous les deux d’accord pour faire cet échange et capables de se déplacer. L’allié du personnage doit être de la même taille que lui. Le mouvement de l’allié ne provoque pas d’attaque d’opportunité, mais celui du personnage peut-être (les règles normales s’appliquent). Le mouvement de l’allié du personnage ne réduit pas ses capacités de déplacement lors de son prochain tour.", "Le personnage sait comment changer de place avec un allié au cours d’une mêlée chaotique.", "Échange de place", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "âme-dacier", "Le personnage reçoit un bonus racial de +4 aux jets de sauvegarde contre les sorts et les pouvoirs magiques. Cela remplace le bonus normal octroyé par le trait racial Robuste.", "Le personnage est tout particulièrement résistant à la magie.", "Âme d'acier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "à-labri", "Chaque fois que le personnage est adjacent à un allié qui possède également ce don et que lui et cet allié doivent faire un jet de Réflexes contre un sort ou un effet, il peut choisir d’utiliser le résultat de son dé ou du dé de son allié (il applique cependant ses propres modificateurs au résultat choisi). Si le personnage opte pour le résultat de son allié, il se retrouve au sol (ou chancelant pendant son prochain tour s’il est déjà au sol ou ne peut pas être jeté au sol). De plus, le personnage reçoit un bonus d’abri de +2 à la CA contre les attaques à distance tant que son allié porte un bouclier.", "Les alliés du personnage l’aident à éviter certaines attaques.", "À l'abri", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "yeux-de-lynx", "Lorsque le personnage effectue un test de Perception visuelle, il peut ignorer jusqu’à -5 de pénalité de distance, ce qui lui permet de voir avec précision à de plus grandes distances que la plupart des gens.", "Le personnage possède une vue très perçante.", "Yeux de lynx", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-affûtée", "La portée de la vision dans le noir du personnage s’étend à 36 mètres (24 cases).", "Les sens du personnage sont encore plus affûtés lorsqu’il se trouve dans le noir le plus total.", "Vision affûtée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "élément-renforcé", "Choisissez un type d’énergie (acide, électricité, feu, froid). Le personnage ajoute +1 au DD de tous les jets de sauvegarde contre les sorts infligeant des dégâts du type d’énergie choisi.", "Le personnage lance les sorts liés à un élément donné de telle manière qu’il est plus difficile de leur résister.", "Élément renforcé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sous-les-jambes", "Le personnage reçoit un bonus d’esquive de +4 aux tests d’Acrobaties pour se faufiler à côté d’ennemis plus grands que lui sans provoquer d’attaques d’opportunité. De plus, il gagne un bonus d’esquive de +2 à la CA contre les attaques d’opportunité qu’il provoque en sortant ou en se déplaçant à l’intérieur de la zone contrôlée par un ennemi plus grand que lui (ce bonus se cumule avec le bonus de +4 offert par Souplesse du serpent, pour un bonus total de +6).", "Le personnage peut se faufiler sous et autour des ennemis plus grands que lui.", "Sous les jambes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-élémentaire", "Choisissez un type d’énergie parmi acide, électricité, feu ou froid. Le personnage peut transformer les dégâts normaux d’un sort en dégâts de ce type d’énergie ou les diviser de sorte que le sort produise 50 % de dégâts de ce type d’énergie et 50 % de dégâts du type normal. Un sort élémentaire utilise un emplacement de sort d’un niveau de plus que le niveau normal du sort.", "Le personnage peut manipuler la nature élémentaire des sorts.", "Sort élémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-éloigné", "Le personnage peut modifier un sort possédant une portée de type « contact », « courte » ou « moyenne » en augmentant celle-ci d’une ou de plusieurs catégories (selon l’ordre « contact », « courte », « moyenne », « longue »). Un sort éloigné occupe un emplacement de sort d’un niveau de plus que le niveau normal du sort pour chaque augmentation de catégorie. Par exemple, un sort de contact transformé en sort de longue portée utilisera un emplacement de sort de trois niveaux de plus que le sort normal. Si le sort de base nécessite des attaques de contact au corps à corps, la version modifiée par ce don utilise des attaques de contact à distance.\r\nLes sorts dont la portée ne figure pas dans la liste donnée plus haut ne peuvent pas être affectés par ce don.", "Les sorts du personnage vont plus loin que la normale.", "Sort éloigné", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-partage-des-sorts", "Chaque fois que le personnage lance un sort non instantané (mais pas un pouvoir magique) sur lui-même, il peut également affecter une créature liée (comme un compagnon animal, un eidolon, un familier ou une monture spéciale). La créature doit se trouver à au plus 1,50 m (1 case) du personnage lors de l’incantation pour bénéficier du sort. La durée du sort est divisée par deux pour le personnage et la créature liée (par exemple, un sort durant normalement 1 heure ne fera effet que pendant 30 minutes pour le personnage et pour la créature liée).\r\nSi le sort ou effet a une durée autre que « instantanée », il cesse d’affecter la créature liée si celle-ci s’éloigne de plus de 1,50 m du personnage. Si la créature revient par la suite (même avant que la durée du sort n’expire), elle n’est plus affectée à nouveau.\r\nGrâce à ce don, le personnage peut même partager des sorts qui n’affectent normalement pas les créatures du type de la créature liée.\r\nCe don ne s’applique qu’aux compagnons animaux, eidolons, familiers et montures spéciales acquis grâce à une aptitude de classe.", "Le personnage peut partager ses sorts avec ceux qui lui sont liés par magie.", "Science du partage des sorts", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-lancer-ki", "Quand le personnage utilise le don Lancer ki, il peut lancer son adversaire vers n’importe quelle case située dans la zone qu’il contrôle et occupée par une autre créature. Il effectue un test de manœuvre de bousculade avec une pénalité de -4 contre cette créature. En cas de réussite, l’adversaire lancé se retrouve au sol dans la case de la créature visée et cette dernière est repoussée dans une case adjacente, où elle se retrouve également au sol. Si le test échoue, l’adversaire lancé se retrouve au sol dans la case la plus proche du personnage parmi celles qui sont à la fois adjacentes à l’emplacement de la créature visée et situées dans la zone de contrôle du personnage.\r\nSi le personnage lance un adversaire de taille supérieure ou égale à G dans une zone contenant plusieurs créatures, il subit une pénalité supplémentaire de -4 sur son test de manœuvre pour chaque créature visée en plus de la première.", "Le personnage transforme ses ennemis en véritables armes vivantes.", "Science du lancer ki", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-combat-en-aveugle", "Lorsque le personnage attaque au corps au corps, il ignore le pourcentage d’échec dû au camouflage si celui-ci est inférieur à un camouflage total. En cas d’échec, le personnage a toujours la possibilité de relancer le jet de pourcentage.\r\nSi le personnage parvient à localiser avec précision un attaquant invisible ou caché dans un rayon de 9 m (6 cases), ce dernier ne bénéficie d’aucun avantage spécial lorsqu’il attaque le personnage à distance. En d’autres termes, le personnage ne perd pas son bonus de Dextérité à la CA et l’attaquant ne bénéficie pas du bonus de +2 auquel il aurait normalement droit vu son invisibilité.", "Les sens aiguisés du personnage guident sa main lorsqu’il affronte des ennemis invisibles.", "Science du combat en aveugle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-lentraînement", "Le personnage ne provoque pas d’attaque d’opportunité lorsqu’il effectue une manœuvre d’entraînement. De plus, il reçoit un bonus de +2 aux tests des tentatives d’entraînement. Il bénéficie également d’un bonus de +2 à son DMD contre les tentatives d’entraînement effectuées par ses adversaires.", "Le personnage est doué lorsqu’il s’agit d’entraîner ses ennemis avec lui sur le champ de bataille.", "Science de l'entraînement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-subtilisation", "Le personnage ne provoque pas d’attaque d’opportunité lorsqu’il effectue une manœuvre de subtilisation. De plus, il reçoit un bonus de +2 aux tests des tentatives de subtilisation. Il bénéficie également d’un bonus de +2 à son DMD contre les tentatives de subtilisation effectuées par ses adversaires.", "Le personnage est doué lorsqu’il s’agit de prélever des objets sur ses adversaires.", "Science de la subtilisation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-seconde-chance", "Lorsque le personnage relance un jet d’attaque raté en utilisant le don Seconde chance, il peut quand même effectuer le reste de ses attaques pour ce round, mais avec une pénalité de -5 sur chacun des jets d’attaque.", "Le personnage peut tenter sa chance une seconde fois après une attaque ratée, sans devoir sacrifier ses futures attaques.", "Science de la seconde chance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-pas-de-côté", "Même s’il a effectué un pas de côté pour éviter l’attaque d’un adversaire grâce au don Pas de côté, le personnage peut encore réaliser un pas de placement de 1,50 m (1 case) au cours de son prochain tour, ou bien se déplacer de sa vitesse de déplacement en entier s’il effectue une action de mouvement lors de son prochain tour.", "Le personnage sait comment faire un pas de côté pour éviter les attaques au corps à corps de ses adversaires, sans pour autant réduire sa mobilité.", "Science du pas de côté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sale-coup-supérieur", "Le personnage reçoit un bonus de +2 aux tests des tentatives de sale coup. Ce bonus se cumule avec celui offert par le don Science du sale coup. Chaque fois qu’il réussit un sale coup, la pénalité persiste pendant 1d4 rounds plus 1 round par tranche de 5 points de différence entre le résultat de son test et le DMD de son adversaire. De plus, la cible a besoin d’une action simple pour supprimer la pénalité.", "Les sales coups joués par le personnage affaiblissent réellement ses ennemis.", "Sale coup supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "récupération-héroïque", "Une fois par jour, par une action simple, le personnage peut tenter un nouveau jet de sauvegarde contre une condition débilitante ou une affliction qui l’affecte et qui impose un jet de Vigueur. Si ce nouveau jet de sauvegarde échoue, aucun effet supplémentaire n’est appliqué. Dans le cas contraire, il compte comme une réussite pour déterminer si le personnage parvient à se débarrasser de l’affliction (comme un poison ou une maladie). Le personnage ne peut pas utiliser ce don pour combattre les effets instantanés, les effets qui n’autorisent aucun jet de sauvegarde, ni ceux qui imposent autre chose qu’un jet de Vigueur.", "Le personnage peut combattre les effets des conditions débilitantes.", "Récupération héroïque", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "représentation-obsédante", "Les bonus et pénalités découlant des représentations bardiques du personnage persistent pendant 2 rounds après qu’il a cessé de jouer. Les autres conditions (comme la portée ou les conditions spécifiques) doivent encore être remplies pour que l’effet continue. Si le personnage entame une nouvelle représentation bardique pendant cette période, les effets de la représentation précédente cessent immédiatement.", "Les effets des représentations bardiques du personnage se prolongent, même après qu’il a cessé de jouer.", "Représentation obsédante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "repositionnement-supérieur", "Le personnage reçoit un bonus de +2 aux tests visant à repositionner un ennemi. Ce bonus se cumule avec celui offert par le don Science du repositionnement. Chaque fois que le personnage repositionne un ennemi, le mouvement de ce dernier provoque des attaques d’opportunité de la part de tous les alliés du personnage (mais pas du personnage lui-même).", "Lorsque le personnage repositionne des ennemis, ces derniers sont vulnérables aux attaques de ses alliés.", "Repositionnement supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "raillerie", "Le personnage peut démoraliser des adversaires en utilisant la compétence de Bluff au lieu d’Intimidation (voir la description d’Intimidation pour les détails) sans subir de pénalité au test due au fait qu’il est plus petit que sa cible.", "L’impact des remarques acerbes du personnage est inversement proportionnel à sa taille.", "Raillerie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "précision-elfique", "Si le personnage attaque avec un arc long ou un arc court (ou encore un arc composite) et que l’attaque rate à cause d’un camouflage, il peut relancer le pourcentage d’échec (une seule fois) afin de voir s’il est parvenu à toucher sa cible.", "Grâce à sa vision aiguisée, le personnage peut accomplir plus facilement des tirs difficiles.", "Précision elfique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "protection-contre-un-ennemi-juré", "Le personnage choisit un type d’ennemi juré. Il ajoute la moitié de son bonus d’ennemi juré à son DMD et à sa CA (en tant que bonus d’esquive) contre les attaques de ce type d’ennemi.", "Le personnage se sert de sa ruse pour se protéger contre les attaques de ses proies.", "Protection contre un ennemi juré", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "résistance-héroïque", "Une fois par jour, par une action immédiate, le personnage peut retarder l’application d’un état préjudiciable ou d’une affliction (comme paniqué, paralysé, étourdi, etc.), même s’il s’agit d’un état permanent ou instantané. En utilisant ce don, le personnage repousse l’application de la condition débilitante à la fin de son prochain tour, moment auquel celle-ci prend effet normalement. Ce don n’a aucun effet sur les dégâts de points de vie ou les affaiblissements/réductions de caractéristiques.", "Le personnage continue à se battre quand d’autres baisseraient les bras.", "Résistance héroïque", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "proche-de-la-terre", "Le personnage peut utiliser communication avec les animaux en tant que pouvoir magique à volonté, mais seulement pour communiquer avec les animaux enfouisseurs tels que les taupes, les géomyidés et autres animaux similaires. Il reste capable d’utiliser sa capacité gnome de communication avec les animaux une fois par jour pour communiquer avec n’importe quel animal.", "Le personnage sait parler aux animaux enfouisseurs.", "Proche de la terre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-repositionnement", "Le personnage ne provoque pas d’attaque d’opportunité lorsqu’il effectue une manœuvre de repositionnement. De plus, il reçoit un bonus de +2 aux tests des tentatives de repositionnement. Il bénéficie également d’un bonus de +2 à son DMD contre les tentatives de repositionnement effectuées par ses adversaires.", "Le personnage sait comment forcer ses ennemis à se déplacer sur le champ de bataille.", "Science du repositionnement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "seconde-chance", "Lorsque le personnage effectue une attaque à outrance, si sa première attaque échoue, il peut sacrifier toutes les autres attaques qu’il aurait pu effectuer au cours de son tour pour relancer le jet de l’attaque ratée (en utilisant son BBA maximum).", "Les réflexes rapides du personnage lui permettent de tenter sa chance une seconde fois en cas d’attaque ratée.", "Seconde chance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-écœurant", "Le personnage peut modifier un sort afin qu’il écœure et rende fiévreuses les créatures à qui il inflige des dégâts. Lorsque le sort blesse une créature, celle-ci devient fiévreuse pendant un nombre de rounds égal au niveau initial du sort. Si le sort autorise un jet de sauvegarde, l’effet d’écœurement est annulé en cas de réussite. Si le sort n’autorise normalement pas de jet de sauvegarde, la victime bénéficie d’un jet de Vigueur pour échapper à l’écœurement. Si le sort a également pour effet de rendre la cible fiévreuse, la durée de l’effet métamagique est ajoutée à celle du sort. Un sort écœurant occupe un emplacement de sort de deux niveaux de plus que le niveau normal du sort.\r\nLes sorts qui n’infligent pas de dégâts ne peuvent pas être altérés par ce don.", "Les sorts du personnage peuvent rendre les cibles fiévreuses.", "Sort écœurant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-à-rebonds", "Chaque fois qu’un sort à rebonds visant une cible unique n’a aucun effet sur la cible choisie (à cause de sa résistance à la magie ou d’un jet de sauvegarde réussi), le personnage peut le rediriger par une action rapide vers une autre cible visible située à portée. Le sort ainsi redirigé fonctionne comme si la nouvelle cible avait été choisie dès le départ. Si le sort a un quelconque effet sur la cible (y compris un effet réduit à cause d’un jet de sauvegarde réussi), il ne peut pas être redirigé grâce à ce don. Un sort à rebonds occupe un emplacement de sort d’un niveau de plus que le niveau normal du sort.", "Lorsqu’un sort échoue, le personnage peut le diriger vers une nouvelle cible.", "Sort à rebonds", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-tonitruant", "Le personnage peut modifier un sort de sorte qu’il assourdisse les créatures auxquelles il inflige des dégâts. Les créatures blessées par le sort sont assourdies pendant un nombre de rounds égal au niveau initial du sort. Si le sort autorise un jet de sauvegarde, l’effet assourdissant est annulé en cas de réussite. Si le sort n’autorise pas de jet de sauvegarde, la cible bénéficie d’un jet de Vigueur pour annuler l’effet d’assourdissement. Si l’effet normal du sort consiste déjà à assourdir les cibles, la durée de l’effet métamagique s’ajoute à celle du sort. Un sort tonitruant occupe un emplacement de sort de deux niveaux de plus que le niveau normal du sort.\r\nLes sorts qui n’infligent pas de dégâts ne peuvent pas être modifiés par ce don.", "Le personnage peut choisir d’ajouter aux effets de ses sorts de puissants coups de tonnerre ou des cris de terreur qui assourdissent les créatures blessées.", "Sort tonitruant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-sélectif", "Lorsque le personnage lance un sort de zone sélectif, il peut choisir un nombre de cibles situées dans la zone égal au modificateur de la caractéristique qu’il utilise pour déterminer combien de sorts supplémentaires il reçoit chaque jour (pour la classe à laquelle le sort sélectif est associé) : le Charisme pour les bardes, les conjurateurs, les ensorceleurs, les oracles et les paladins ; l’Intelligence pour les magiciens et les sorcières ; la Sagesse pour les druides, les inquisiteurs, les prêtres et les rôdeurs. Les cibles choisies échappent aux effets du sort. Un sort sélectif occupe un emplacement de sort d’un niveau de plus que le niveau normal du sort.\r\nLes sorts qui n’ont pas de zone d’effet ou dont la durée n’est pas instantanée ne peuvent pas être modifiés par ce don.", "Les alliés du personnage n’ont pas à craindre d’être atteints par ses attaques.", "Sort sélectif", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-soutenu", "Le personnage peut faire en sorte qu’un sort de zone instantané persiste jusqu’au début de son prochain tour. Ceux qui se trouvaient déjà dans la zone d’effet au moment de l’incantation ne subissent aucun effet additionnel mais les autres créatures et objets qui y entrent pendant ce laps de temps sont soumis aux effets du sort. Un sort soutenu possédant une manifestation visuelle obscurcit la vision et offre du camouflage (20% d’échec des attaques) au-delà de 1,50 m (1 case) et un camouflage total (50% d’échec des attaques) au-delà de 6 mètres (4 cases).\r\nUn sort soutenu occupe un emplacement de sort d’un niveau supplémentaire que le niveau normal du sort.", "Le sort du personnage s’accroche à la réalité et ne s’éclipse que lentement.", "Sort soutenu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-perturbateur", "Les cibles affectées par un sort perturbateur doivent effectuer des tests de Concentration chaque fois qu’elles lancent un sort ou utilisent un pouvoir magique (le DD du test est égal au DD du jet de sauvegarde contre le sort perturbateur augmenté du niveau du sort qu’elles tentent de lancer). L’effet dure 1 round. Les cibles qui évitent les effets normaux du sort échappent également aux effets du don. Un sort perturbateur utilise un emplacement de sort d’un niveau de plus que le niveau normal du sort.", "Les énergies magiques des sorts du personnage s’accrochent aux ennemis et gênent leurs incantations.", "Sort perturbateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-sale-coup", "Le personnage ne provoque pas d’attaque d’opportunité lorsqu’il effectue une manœuvre de sale coup. De plus, il reçoit un bonus de +2 aux tests des tentatives de sale coup. Il bénéficie également d’un bonus de +2 à son DMD contre les sales coups de ses adversaires.", "Le personnage est doué lorsqu’il s’agit de jouer un sale coup à un adversaire.", "Science du sale coup", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-persistant", "Chaque fois qu’une créature ciblée par un sort persistant ou située dans la zone d’un sort persistant réussit un jet de sauvegarde contre celui-ci, elle doit effectuer un second jet de sauvegarde. Si ce second jet de sauvegarde échoue, la créature subit les pleins effets du sort, comme si elle avait raté son premier jet de sauvegarde. Un sort persistant utilise un emplacement de sort de deux niveaux supplémentaires que le niveau normal du sort.\r\nLes sorts qui n’imposent pas de jet de sauvegarde permettant d’annuler ou d’amoindrir leurs effets ne peuvent pas bénéficier de ce don.", "Le personnage peut modifier un sort pour qu’il soit plus tenace face aux cibles qui résistent à ses effets.", "Sort persistant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-intense", "Un sort intense augmente le nombre maximum de dés de dégâts de 5 niveaux. Le personnage doit posséder un NLS suffisant pour dépasser le maximum s’il veut pouvoir tirer avantage de ce don. Aucune autre variable du sort n’est affectée, et les sorts qui infligent des dégâts ne dépendant pas du NLS restent inchangés. Un sort intense utilise un emplacement de sort d’un niveau de plus que le niveau normal du sort.\r\nPar exemple, Un sort de boule de feu est un sort de magicien de niveau 3. Il inflige un nombre de dégâts correspondant à 1d6 par niveau du magicien dans la limite de 10d6. En le préparant à l'aide du don Sort Intense, il nécessite un emplacement de sort de niveau 4, mais repousse la limite de dégâts possibles à 15d6. Pour en tirer un effet le PJ devra néanmoins toujours être de niveau supérieur à 10 ou disposer d'un autre moyen d'augmenter son NLS. Ainsi un magicien de niveau 11 pourra lancer 11d6 points de dégâts et un magicien de niveau 15, 15d6. Par contre, Un magicien de niveau 20 ne pourra pas en tirer plus de dégâts que 15d6.", "Les sorts du personnage peuvent outrepasser certaines limites.", "Sort intense", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-hébétant", "Le personnage peut modifier un sort pour qu’il hébète les créatures blessées. Les créatures à qui le sort modifié inflige des dégâts deviennent hébétées pendant un nombre de rounds égal au niveau initial du sort. Si le sort autorise un jet de sauvegarde, une réussite annule l’effet d’hébétement. Si le sort n’en autorise pas, la cible peut tenter un jet de Volonté pour annuler l’hébétement. Si le sort en lui-même hébète également la cible, la durée de l’effet métamagique vient s’ajouter à la durée normale du sort. Un sort hébétant utilise un emplacement de sort de trois niveaux de plus que le niveau normal du sort. Les sorts qui n’infligent pas de dégâts ne peuvent pas être modifiés par ce don.", "Les sorts du personnage peuvent hébéter leurs cibles.", "Sort hébétant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-ectoplasmique", "Un sort ectoplasmique agit pleinement sur les créatures éthérées ou intangibles. Un sort ectoplasmique occupe un emplacement de sort d’un niveau de plus que le niveau normal du sort.", "Les sorts du personnage peuvent traverser l’espace qui sépare les dimensions et créer un effet fantôme dans l’éther.", "Sort ectoplasmique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-concentré", "Lorsque le personnage lance un sort qui touche ou cible plus d’une créature, il peut choisir une cible ou une créature dans la zone d’effet du sort. Le DD du jet de sauvegarde de cette créature pour résister au sort est augmenté de +2. Le personnage doit choisir sur quelle cible concentrer le sort avant de lancer celui-ci. Un sort concentré occupe un emplacement de sort d’un niveau de plus que le niveau normal du sort. Seuls les sorts autorisant un jet de sauvegarde permettant de résister à leurs effets ou de les amoindrir peuvent bénéficier de ce don.", "Lorsque le personnage lance un sort qui touche plus d’une créature, il peut faire en sorte qu’un de ses adversaires ait plus de mal à y résister.", "Sort concentré", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sociable", "Par une action de mouvement, le personnage peut donner à toutes les créatures amicales situées dans un rayon de 9 mètres (6 cases) et capables de le voir ou de l’entendre (y compris lui-même) un bonus de +2 aux tests de Diplomatie pendant un nombre de rounds égal à son modificateur de Charisme (minimum 1 round).", "Le personnage trouve toujours un moyen d’aider les autres et de bien s’entendre avec tout le monde.", "Sociable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sens-très-affûtés", "Le personnage reçoit un bonus racial de +4 aux tests de Perception. Ce bonus remplace celui offert par le trait racial Sens aiguisés.", "Les sens du personnage sont exceptionnellement développés, même pour un individu de sa race.", "Sens très affûtés", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-miséricordieux", "Le personnage peut modifier les sorts qui infligent des dégâts de sorte qu’ils infligent des dégâts non létaux. Les sorts qui infligent des dégâts d’un type spécifique (comme le feu) infligent désormais des dégâts non létaux du même type. Un sort miséricordieux utilise un emplacement de même niveau que celui du sort normal.", "Le personnage peut lancer des sorts infligeant des dégâts pour assommer plutôt que tuer ses adversaires.", "Sort miséricordieux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "guérisseur-chanceux", "Le personnage peut dépenser une utilisation de sa chance adaptable pour relancer les dés d’un unique effet de guérison magique (comme un sort de soins ou une canalisation d’énergie). Il se guérit d’un nombre de points de vie égal au montant du meilleur jet, le premier ou le second. Les autres créatures soignées par cet effet ne profitent pas de cet avantage.", "La chance du personnage lui permet d’améliorer l’efficacité de ses soins.", "Guérisseur chanceux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chanteur-de-la-nature", "Lorsque le personnage utilise une représentation bardique avec une composante sonore dans une forêt, la portée ou la zone de la représentation choisie est doublée. De plus, le DD des jets de sauvegarde contre les représentations bardiques du personnage augmente de +2 pour les créatures du type fée et ce, quel que soit l’endroit où cela se produit.", "Les chansons du personnage évoquent les habitudes et les secrets de son peuple.", "Chanteur de la nature", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "carnage-effroyable", "Chaque fois que le personnage réduit un adversaire à 0 point de vie ou moins, il peut effectuer un test d’Intimidation pour démoraliser tous les ennemis dans un rayon de 6 mètres (4 cases) par une action libre. Seuls les ennemis qui peuvent voir le personnage et l’adversaire réduit à 0 point de vie ou moins peuvent être affectés.", "En tuant un adversaire, le personnage peut démoraliser les autres ennemis proches.", "Carnage effroyable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-délixir-sanguin", "Une fois par jour, le personnage peut préparer un élixir sanguin quand il vide son esprit pour renouveler ses emplacements de sort disponibles. À ce moment, il choisit un de ses pouvoirs de lignage qu’il transfère dans une petite potion que n’importe quelle créature peut boire pour bénéficier temporairement de ce pouvoir de lignage.\r\nLe personnage doit consacrer une heure à la préparation de l’élixir. Il a besoin d’huiles et de distillats spéciaux (d’une valeur de 100 po). Quand il prépare l’élixir, il perd accès à son pouvoir de lignage jusqu’à ce qu’il vide de nouveau son esprit pour renouveler ses emplacements de sort.\r\nQuand une créature boit un élixir sanguin, elle peut activer le pouvoir de lignage à n’importe quel moment avant la fin de son prochain tour, comme si elle avait accès à ce pouvoir de lignage. La créature qui boit l’élixir ne bénéficie pas de ses effets si son niveau de personnage n’est pas égal ou supérieur au niveau minimal du pouvoir de lignage. Tout effet qui dépend du niveau utilise le niveau de personnage de la créature qui boit l’élixir sanguin ou du niveau d’ensorceleur du personnage (choisir le plus bas).\r\nLes élixirs sanguins sont extrêmement instables et perdent leurs propriétés 1 jour après leur création.", "Le personnage peut condenser une partie de son pouvoir dans ce puissant élixir. ", "Création d'élixir sanguin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "discernement-ultérieur", "Une fois par jour, par une action libre, le personnage peut repenser à une phrase qu’il a entendue dans la journée et comprendre qu’il s’agit d’un mensonge. Ceci fonctionne comme le sort détection du mensonge mais affecte une phrase et non une créature.\r\nPar exemple, si le roi a dit « Ma fille est dans le donjon du monstre » avant que le personnage ne parte à sa recherche, tant qu’il a affirmé cela dans la journée, le personnage peut utiliser ce don pour savoir si le roi lui a délibérément menti en disant cela.", "En y repensant, le personnage découvre un mensonge dans ce qu’il a cru être la vérité. ", "Discernement ultérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "donner-linitiative", "Au début de chaque rencontre, le personnage peut conserver le bonus d’initiative que lui offre sa Sagesse ou le donner à un allié présent dans son champ de vision. Il doit faire ce choix avant que son allié et lui aient fait leur test d’initiative.", "Le personnage est un maître de l’initiative mais il est aussi capable de la donner à quelqu’un. ", "Donner l'initiative", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "déplacement-mystique", "Le personnage peut se déplacer à sa vitesse maximale, même dans les ronces, les épineux et les zones luxuriantes enchantées ou manipulées par magie pour gêner les mouvements, et même si ces zones devraient enchevêtrer les créatures.", "La végétation enchantée n’entrave pas les déplacements du personnage.", "Déplacement mystique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "détection-de-lexpertise", "Quand le personnage utilise l’un des sorts indiqués dans la liste des conditions requises pour détecter l’alignement d’une créature ou sa magie, il a une chance de détecter aussi son domaine d’incantation de prédilection. Il faut observer la cible du sort de détection pendant 3 rounds.\r\nLa cible fait alors un jet de sauvegarde (Volonté DD 10 +1/2 niveau de lanceur de sorts + modificateur d’Intelligence du personnage). Si elle échoue, le personnage découvre le lignage, les domaines, les maléfices, les écoles ou les mystères dont elle dispose (le cas échéant).\r\nSi la créature réussit son jet de sauvegarde, elle est immunisée contre ce don pendant 24 heures.", "Le personnage détecte la spécialité mystique d’un adversaire. ", "Détection de l'expertise", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "détection-officieuse", "Par une action rapide, le personnage peut concentrer la lucidité offerte par la détection du mal pour augmenter sa conscience de ce qui l’entoure. Il gagne un bonus sacré de +10 aux tests de Perception et de Psychologie pour 1 round.\r\nCeci dépense son utilisation de détection du mal pour 24 heures.", "Le personnage peut utiliser la détection du mal pour des activités plus pragmatiques ou plus ordinaires. ", "Détection officieuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "eidolon-concentré", "Quand le personnage est adjacent à son eidolon, il reçoit un bonus de +4 aux tests de Concentration.", "Le lien entre le personnage et l’eidolon l’aide à se concentrer. ", "Eidolon concentré", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "eidolon-protecteur", "Quand le personnage est adjacent à son eidolon, il peut lui imposer un malus de -1 aux jets d’attaque et aux tests de manœuvre de combat pour gagner un bonus d’esquive de +1 à la CA. Quand le bonus de base à l’attaque de l’eidolon atteint +5 (et par la suite, pour chaque tranche de +5), le malus augmente de -1 et le bonus d’esquive de +1. Le personnage doit utiliser ce don quand son eidolon fait une attaque ou une attaque à outrance avec une arme naturelle ou de corps à corps.\r\nL’effet dure jusqu’au prochain tour de l’eidolon ou jusqu’à ce que le personnage et son eidolon ne soit plus adjacent, selon ce qui se produit en premier.", "Le personnage a entraîné l’eidolon à le protéger. ", "Eidolon protecteur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "eidolon-résilient", "Si le personnage tombe inconscient, s’endort ou se fait tuer, son eidolon reste un nombre de rounds égal à son niveau de conjurateur avant d’être banni. Si le personnage reprend conscience avant la fin de cette période, l’eidolon ne disparaît pas, sinon, il est banni normalement.", "Le lien entre le personnage et l’eidolon est si fort que ce dernier reste avec son maître pendant une courte période après qu’il tombe inconscient ou mort. ", "Eidolon résilient", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "eidolon-vigilant", "Tant que l’eidolon est à portée du personnage, ce dernier gagne un bonus de +4 aux tests de Perception. S’il possède 10 rangs ou plus dans cette compétence, le bonus passe à +8. Ce don ne fonctionne pas si l’eidolon est sans défense ou inconscient.", "L’eidolon du personnage est très observateur et le lien qu’il entretient avec lui augmente la vigilance de son maître. ", "Eidolon vigilant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "empathie-rapide", "L’utilisation de l’empathie sauvage est un pouvoir de classe.", "L’harmonie empathique entre le personnage et la nature lui permet de se lier rapidement à l’esprit des bêtes. ", "Empathie rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "empathie-sauvage-supérieure", "Le personnage gagne un bonus d’intuition de +2 aux tests d’empathie sauvage et peut utiliser ce pouvoir pour faire un test d’Intimidation au lieu de Diplomatie. De plus, il choisit l’un des genres de créatures suivants : élémentaires, fées, lycanthropes, plantes ou vermines.\r\nIl peut influencer des créatures de ce genre à l’aide de son empathie sauvage si elles ont une Intelligence de 1 ou 2 ou aucune intelligence. Une fois que le personnage a choisi le type de créature, il ne peut plus en changer.", "L’empathie sauvage du personnage couvre la totalité du monde naturel. ", "Empathie sauvage supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ensemble", "Quand le personnage donne une représentation, les alliés situés dans les 6 mètres (4 c) qui disposent également de ce don peuvent l’aider à faire son test de Représentation (même une représentation bardique), comme s’ils utilisaient l’action immédiate aider quelqu'un. Les alliés font leur test d’aide à autrui avant que le personnage ne fasse son test de Représentation . Il ne peut pas bénéficier de l’aide de plus de quatre personnages. Les alliés qui apportent leur aide ne sont pas obligés d’utiliser la même compétence de Représentation que le personnage.", "Le personnage peut créer un ensemble d’artistes amateurs compétents pour l’aider à donner sa représentation. ", "Ensemble", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "exploitation-des-connaissances", "Une fois par jour, quand le personnage réussit à identifier les pouvoirs et les faiblesses d’une créature en utilisant le test de Connaissances approprié, il gagne un bonus de +2 aux jets d’attaque et de dégâts contre elle pendant 1 minute.\r\nS’il identifie les pouvoirs et faiblesses de plusieurs créatures, il doit en choisir une seule qui sera la cible de cet effet.", "Le personnage utilise ses connaissances sur les faiblesses d’une créature pour lui porter une série d’assauts implacables. ", "Exploitation des connaissances", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "familier-évolué", "Le personnage choisit une évolution dans la liste des évolutions à 1 point de l’eidolon de l’invocateur et en dote son familier. Ce dernier doit se plier aux limitations de cette évolution. Par exemple, aucun familier ne peut bénéficier de l’évolution monture et seuls les familiers qui possèdent des ailes peuvent choisir l’évolution frappe des ailes. Si le personnage obtient un nouveau familier, l’ancien perd toutes ses évolutions et le personnage peut choisir une nouvelle évolution à 1 point pour son nouveau familier.", "Le familier du personnage diffère des autres de son espèce. ", "Familier évolué", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "foi-absolue", "Le personnage gagne un bonus sacré de +4 aux jets de sauvegarde contre le poison.", "Le personnage est immunisé contre les maladies, comme la plupart des paladins, mais il est aussi très résistant aux poisons. ", "Foi absolue", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "forme-animale-optimale", "Si le personnage est un druide multiclassé, sa forme animale se calcule comme s’il avait quatre niveaux de druide de plus, avec un maximum égal à son niveau de personnage.", "Les pouvoirs de métamorphose du personnage dépassent de loin ce que ses connaissances en magie druidique devraient lui permettre. ", "Forme animale optimale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maléfice-magique", "Le personnage choisit un sort de niveau 1 dans la classe qui lui permet d’utiliser les maléfices majeurs. Il peut apprendre ce sort comme un maléfice et l’utiliser trois fois par jour. C’est un pouvoir magique. Il peut utiliser son niveau dans la classe qui lui accorde le maléfice comme niveau de lanceur de sorts pour le maléfice magique. Ce dernier utilise le DD des maléfices et non celui du sort originel.\r\nSi le sort est un sort de contact et que le personnage rate sa cible, il ne peut plus la viser avec ce maléfice magique pendant 24 heures. Si le sort autorise un jet de sauvegarde pour annuler ses effets ou les réduire et que la cible le réussit, elle ne peut plus être affectée par ce maléfice magique pendant 24 heures, même si elle se trouve dans sa zone d’effet.", "Le personnage peut transformer un sort de niveau 1 en maléfice. ", "Maléfice magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "langue-sauvage", "Quand le personnage utilise la forme animale pour prendre une apparence qui ne lui permet pas de parler (comme celle d’un animal), il peut tout de même s’exprimer normalement dans tous les langages qu’il connaît. Ceci lui permet de lancer des sorts à composante verbale, de prononcer des mots de commande et d’activer des objets à fin d’incantation ou à potentiel magique. Cependant, il ne peut pas lancer un sort qui nécessite des composantes gestuelles à moins de posséder le don Incantation animale, ni lancer des sorts avec des composantes matérielles fusionnées dans sa nouvelle forme.\r\nQuand le personnage utilise forme animale pour prendre la forme d’une bête, il peut utiliser communication avec les animaux pour parler à ceux dont il a pris l’apparence. C’est un pouvoir magique avec un niveau de lanceur de sorts égal au niveau de druide et il peut être utilisé chaque jour pendant un nombre de minutes égal au niveau de druide du personnage. Cette durée n’est pas forcément continue mais doit se découper en segments d’une minute.", "Le personnage parle la langue des hommes et des bêtes. ", "Langue sauvage", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "jugement-renforcé", "Une fois par jour, quand le personnage utilise son pouvoir de jugement, il peut considérer son niveau de classe comme 3 crans plus élevés. S’il a plusieurs jugements actifs à la fois, cet avantage s’applique à tous.", "Une fois par jour, le pouvoir de la foi submerge le personnage et renforce son jugement. ", "Jugement renforcé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "jugement-partial", "Le personnage choisit une race dans la liste des ennemis jurés du rôdeur. Tout bonus sacré ou de malfaisance obtenu grâce à un jugement augmente de 1 pour toutes les attaques portées contre une créature de cette race et pour les attaques qu’elle porte.", "Le personnage porte un jugement particulièrement sévère sur un certain type de créatures. ", "Jugement partial", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "invocateur-de-squelettes", "Le personnage ajoute « squelette humain » à la liste de créatures qu’il peut invoquer avec convocation de monstres I et « squelette humain champion » à celle des créatures qu’il convoque avec convocation de monstres III.\r\nUne fois par jour, quand il lance convocation de monstres, il peut convoquer une version squelettique de l’une des créatures présentes dans la liste associée au sort (on applique l’archétype squelette à la créature).", "Les morts qui marchent répondent à l’appel du personnage. ", "Invocateur de squelettes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "intuition-prophétique", "Le personnage gagne un bonus de +2 aux tests de Psychologie et d’Art de la magie. S’il a 10 rangs ou plus dans une de ces compétences, le bonus passe à +4 pour elle.", "Le personnage est extrêmement sensible à la magie et aux changements qui affectent le comportement des gens. ", "Intuition prophétique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-de-reliques-armes-ou-boucliers", "Quand le personnage crée une arme, une armure ou un bouclier magique, il peut lancer sanctification ou profanation lors du processus de fabrication. Cela augmente le prix de l'objet de 250 po. L’objet devient une relique et peut servir de symbole sacré (ou maudit) pour sa divinité.\r\nSi le personnage lance sanctification ou profanation, la relique compte comme un objet permanent dédié au dieu tant qu’il reste dans la zone d’effet du sort.", "Les créations magiques du personnage sont imprégnées de puissance divine. ", "Création de reliques, armes ou boucliers", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "interférence-divine", "Par une action immédiate, quand un ennemi situé dans les 9 mètres (6 c) touche un allié lors d’une attaque, le personnage peut sacrifier un sort divin préparé ou un emplacement de sort s’il s’agit d’un lanceur de sorts spontanés, pour obliger l’ennemi à refaire son Jet d’attaque. Ce second Jet d’attaque subit un malus égal au niveau du sort sacrifié. Le personnage doit utiliser un sort de niveau 1 ou plus. Que cette seconde attaque soit une réussite ou non, le personnage ne peut plus utiliser ce pouvoir sur la même créature pendant 1 jour.", "Le personnage peut convertir un sort pour interférer avec une attaque ennemie. ", "Interférence divine", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "grâce-ultime", "Le personnage peut dépenser 10 utilisations de son Imposition des mains pour ramener un unique mort à la vie, comme avec un sort de rappel à la vie, avec un niveau de lanceur de sorts égal à son niveau de paladin.\r\nIl doit fournir les composantes du sort ou recevoir 1 niveau négatif qui disparaît automatiquement au bout de 24 heures et ne devient jamais permanent. Il est impossible de s’en débarrasser avant la fin des 24 heures.", "Le personnage peut ramener un mort à la vie grâce à son imposition des mains. ", "Grâce ultime", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "grâce-supérieure", "Quand le personnage utilise son pouvoir d’Imposition des mains sur une cible qui n’est pas victime d’une condition néfaste que l’imposition peut dissiper, elle soigne 1d6 points de dégâts de plus.", "La grâce du personnage possède des pouvoirs curatifs incroyables. ", "Grâce supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "glissade", "Si le personnage possède encore au moins 1 point dans sa réserve de ki, il ne provoque pas d’attaque d’opportunité quand il quitte la première case lors d’un mouvement. Il peut dépenser 1 point de ki pour ne pas en provoquer pendant tout son déplacement.", "Le personnage peut glisser sur la terre ferme comme s’il patinait sur de la glace. ", "Glissade", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-sanglante-ensorcelée", "Une fois par jour, par une action immédiate qui suit la réduction d’une créature à 0 point de vie ou moins suite à l’utilisation d’un sort d’ensorceleur, le personnage récupère l’usage d’un pouvoir de lignage avec un nombre d’utilisations quotidiennes limitées. La créature tuée doit avoir au moins moitié autant de dés de vie que le personnage a de niveaux d’ensorceleur. Le personnage ne peut pas se servir de ce don pour obtenir une utilisation supplémentaire d’un pouvoir de lignage qu’il n’a pas encore utilisé dans la journée.", "Le personnage récupère un pouvoir quand il tue une créature. ", "Frappe sanglante ensorcelée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "forme-puissante", "Quand le personnage est sous forme animale, il est considéré comme étant d’une catégorie de taille supérieure quand il s’agit de calculer son DMD, son BMO, le poids transportable, et toutes les attaques spéciales basées sur la taille qu’il utilise ou qui sont utilisées contre lui (comme étreinte, engloutissement ou piétinement).", "Le druide revêt une forme animale puissante et musculeuse. ", "Forme puissante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "forme-animale-rapide", "Le personnage peut lancer forme animale par une action rapide ou de mouvement mais les formes disponibles se limitent alors à celles accessibles à un druide de deux niveaux de moins que lui quand il utilise une action de mouvement et de quatre niveaux de moins pour l’action rapide.", "Quand le personnage change de forme, il sacrifie la puissance au profit de la vitesse. ", "Forme animale rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "implantation-de-bombe", "Le personnage peut implanter une bombe dans une créature consentante ou sans défense (une créature dépourvue d’Intelligence placée sous son contrôle, comme un zombi, compte comme une créature consentante). Ceci prend 1 heure et consomme une utilisation quotidienne du pouvoir de bombe.\r\nQuand la créature meurt ou se fait détruire, la bombe explose dans la case qu’elle occupait, comme une bombe à retardement placée par le personnage (mais le personnage peut régler la bombe pour qu’elle n’inflige pas la totalité des dégâts de bombe normaux). Celui-ci peut appliquer n’importe quelle découverte de bombe à celle-ci (acide, froid, fumigène, etc.) La bombe explose automatiquement 24 heures après son implantation.\r\nSi le personnage dépense 150 po de réactifs alchimiques par dé de dégâts de la bombe (750 po pour une bombe de 5d6 par exemple), la bombe n’explose pas automatiquement au bout de 24 heures, elle explose seulement si la créature se fait tuer ou si elle est détruite (24 heures après son implantation, la bombe ne compte plus au nombre des utilisations quotidiennes du personnage).\r\nL’implantation d’une bombe est une opération aussi lourde que les soins aux blessures mortelles apportés par Premiers secours . Elle laisse des cicatrices chirurgicales à moins que la créature ne bénéficie de soins magiques ou d’aptitudes comme la régénération ou la guérison accélérée. Il est impossible d’implanter une bombe dans certaines créatures comme les vases, les élémentaires et les créatures intangibles. Pour extraire une bombe, il faut faire un test de Premiers secours comme pour traiter une blessure mortelle puis une dissipation de la magie ou un test de Sabotage pour neutraliser la bombe (DD = 11 + niveau de lanceur de sorts de l’alchimiste).", "Le personnage peut fixer sur une créature une bombe qui explose quand elle meurt ou après 24 heures. ", "Implantation de bombe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "croc-en-jambe-au-bâton", "Le personnage traite les bâtons comme s’ils disposaient de la propriété croc-en-jambe.", "Le personnage peut faire une attaque de croc-en-jambe avec un bâton. ", "Croc-en-jambe au bâton", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-maudit", "Quand le personnage confirme un critique avec un sort ou un pouvoir magique, il peut lancer malédiction ou malédiction majeure sur la cible par une action immédiate. Ceci fonctionne même avec les sorts à distance. Le personnage doit avoir préparé l’un de ces sorts ou être capable d’en lancer un à ce moment-là. Il utilise ce pouvoir pour lancer le sort correspondant.", "Les sorts du personnage portent en eux une malédiction qui se manifeste quand ils font mouche. ", "Critique maudit", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-de-plaie", "Quand le personnage confirme un coup critique avec un sort de contact, de contact à distance ou un pouvoir magique, la victime est atteinte d’une plaie mineure aléatoire.", "Quand le personnage réussit un coup critique avec un sort ou un pouvoir magique, il inflige une plaie magique mineure à la cible. ", "Critique de plaie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aura-dintrépidité", "L’aura de bravoure du personnage s’étend sur 6 mètres (4 c). Les alliés situés à l’intérieur sont immunisés contre les effets de peur.", "L’aura de bravoure du personnage gagne en puissance car ses alliés sont aussi résolus que lui. ", "Aura d'intrépidité", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aura-de-fléau-des-dragons", "Quand le personnage combat des dragons, son aura de bravoure s’étend sur 6 mètres (4 c) de rayon et les alliés qui se trouvent à l’intérieur gagnent un bonus de moral aux jets de sauvegarde contre le souffle du dragon égal au bonus que l’aura de bravoure apporte contre les effets de peur.", "Les créatures qui se trouvent dans l’aura du fléau des dragons bénéficient de la même protection que lui. ", "Aura de fléau des dragons", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attrait-de-la-vie", "Par une action simple, le personnage peut canaliser de l’énergie positive pour fasciner tous les morts-vivants situés dans un rayon de 9 mètres (6 c) pendant un nombre de rounds égal à son modificateur de Charisme (1 au minimum).\r\nLes morts-vivants qui réussissent leur jet de Volonté (DD 10 + 1/2 niveau de prêtre + modificateur de Charisme) ne sont pas affectés.\r\nCe type de canalisation ne soigne ni ne blesse personne.", "La canalisation d’énergie positive du prêtre est d’une saveur irrésistible pour les morts-vivants alentours. ", "Attrait de la vie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-à-aspersion-à-ricochet", "Quand l’arme à aspersion du personnage rate sa cible et que le jet de direction indique qu’elle atterrit dans une case occupée par une créature, il peut faire un Jet d’attaque (avec un malus de -5) comme s’il avait lancé l’arme sur cette créature. Si l’attaque réussit, l’arme touche et la créature reçoit la totalité des dégâts d’aspersion. Les cases adjacentes reçoivent les dégâts d’aspersion habituels.", "Les armes à aspersion du personnage sont particulièrement dangereuses, même quand il rate sa cible. ", "Arme à aspersion à ricochet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arcane-supplémentaire", "Le personnage obtient un arcane de magus supplémentaire. Il doit remplir les conditions requises.", "Le personnage a percé les secrets d’un nouvel arcane de magus. ", "Arcane supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "apprendre-un-piège-de-rôdeur", "Le personnage choisit un piège de rôdeur qu’il peut utiliser chaque jour un nombre de fois égal à son bonus de Sagesse (1 au minimum). Le DD de ce piège est égal à 10 + 1/2 niveau du personnage + bonus de Sagesse et dure 1 jour par tranche de deux niveaux de personnage. Si le personnage n’est pas un rôdeur, ce don lui donne seulement accès aux pièges extraordinaires qui, comme tous les pièges de rôdeur, voient leur DD diminuer de 2.", "Le personnage apprend à créer un piège de rôdeur. ", "Apprendre un piège de rôdeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "balayage-au-bâton", "Quand le personnage manie un bâton à deux mains et fait une attaque à outrance, il peut tenter une manœuvre de croc-en-jambe contre tous les ennemis qui lui sont adjacents.", "Le personnage peut faire une attaque de croc-en-jambe avec un bâton sur tous les ennemis adjacents. ", "Balayage au bâton", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "antagoniste", "Le personnage peut faire des tests de Diplomatie ou d’Intimidation pour pousser les créatures à se montrer hostiles envers lui. Quelle que soit la compétence qu’il utilise, ceci lui demande une action simple qui ne provoque pas d’attaque d’opportunité, avec un DD égal aux dés de vie de la cible + modificateur de Sagesse de la cible + 10. Le personnage ne peut pas faire ce test contre une créature qui ne le comprend pas ou qui a une Intelligence de 3 ou moins.\r\nLe personnage peut faire un test de Psychologie (DD 20) avant, par une action rapide qui lui permet de bénéficier d’un bonus d’intuition égal à son bonus de Charisme aux tests de Diplomatie et d’Intimidation, avant la fin de son prochain tour. Les avantages obtenus dépendent de la compétence utilisée.\r\nC’est un effet mental.\r\n* Diplomatie. Le personnage agace un ennemi. Pendant la minute qui suit, la cible subit un malus de -2 aux jets d’attaque contre toute créature autre que le personnage et a 10% de risque d’échec des sorts quand elle ne vise pas le personnage ou ne l’englobe pas dans la zone d’effet.\r\n* Intimidation. La créature devient folle de rage. À son prochain tour, elle doit tenter de faire une attaque contre le personnage (que ce soit au corps à corps, à distance, avec un sort ciblé ou un sort à zone d'effet). L’effet se termine si la créature est dans l’incapacité de le faire ou si elle risque de se blesser en essayant (si le personnage est de l’autre côté d’un ravin ou d’un mur de feu par exemple). Si la cible ne peut pas attaquer le personnage à son tour, ce dernier peut refaire un test par une action immédiate pour prolonger l’effet de 1 round (mais pas plus). L’effet se termine dès que la créature a fait une attaque contre le personnage.\r\nUne fois que le personnage a utilisé ce don une fois contre une créature, il ne peut plus s’en servir contre elle pendant 1 jour.", "Que le personnage use de remarques mordantes ou de paroles blessantes, il est particulièrement doué pour se faire détester. ", "Antagoniste", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "abondance-de-révélations", "Le personnage choisit une révélation dont le nombre d’utilisations quotidiennes est limité. Il peut l’utiliser une fois de plus par jour.", "Le personnage peut sonder les profondeurs de son mystère pour utiliser ses révélations plus souvent. ", "Abondance de révélations", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "éventration-à-deux-armes", "Si le personnage touche son adversaire avec ses deux armes, il lui inflige 1d10 points de dégâts supplémentaires plus 1,5 fois son modificateur de Force. Le personnage ne peut faire de tels dégâts qu’une fois par round.", "En frappant simultanément avec ses deux armes, le personnage cause de terrifiantes blessures.", "Éventration à deux armes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "éventration-à-deux-armes-(mythique)", "Les dégâts infligés par l’Éventration à deux armes s’élèvent désormais à 2d8 plus deux fois le modificateur de Force du personnage. Il peut dépenser une utilisation de pouvoir mythique pour gagner un bonus sur ces dégâts égal à deux fois son grade.", "Les attaques duales synchronisées du personnage sont encore plus dangereuses et peuvent blesser de manière durable.", "Éventration à deux armes (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "étreinte-de-lombre", "Quand le personnage lance un sort du registre de l’obscurité qui affecte une zone, les créatures dans la zone sont enchevêtrées. Si le sort autorise un jet de sauvegarde, la réussite de ce jet annule l’effet d’enchevêtrement. Si le sort n’autorise normalement aucun jet de sauvegarde, les créatures affectées peuvent effectuer un jet de Réflexes (DD = le DD du sort comme s’il autorisait un jet de sauvegarde) pour annuler l’effet. Si la résistance à la magie permet de résister au sort et si le test associé pour l’ignorer rate, la créature qui en bénéficie n’est pas enchevêtrée.\r\nUne créature demeure enchevêtrée tant qu’elle reste à l’intérieur de la zone du sort et pendant un round après en être sortie.\r\nUne créature qui quitte puis revient dans la zone doit effectuer un nouveau jet de sauvegarde pour éviter l’enchevêtrement. Les créatures qui réussissent leur jet de sauvegarde pour résister à l’enchevêtrement n’ont pas besoin d’effectuer d’autres jets si elles restent à l’intérieur de la zone obscurcie. Le personnage n’est jamais entravé par les effets de ses sorts modifiés par ce don.\r\nUn sort sur lequel est appliqué l’Étreinte de l’ombre utilise un emplacement d’un niveau de plus que le niveau actuel du sort.", "Les sorts d’obscurité du personnage sont concrets et entravent ses ennemis.", "Étreinte de l'ombre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "étrange-héritage-(mythique)", "Le personnage gagne les pouvoirs d’ensorceleur du lignage associé à son Étrange héritage comme si son niveau d’ensorceleur était égal à son niveau de personnage -2. Pour le pouvoir de lignage de niveau 1, appliquez le niveau complet du personnage pour déterminer son effet ; pour l’ensemble des autres pouvoirs de lignage, considérez que le niveau d’ensorceleur est égal au niveau du personnage -2.", "Le sang de votre ancêtre ensorceleur se mêle au pouvoir mythique de votre personnage, ce qui produit des effets importants.", "Étrange héritage (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "érudit", null, "Le personnage est diplômé de l’une des nombreuses universités, écoles et autres institutions d’enseignement de l’éducation supérieure.", "Érudit", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ancre-douloureuse", "Quand un extérieur Mauvais utilise un appel, une convocation ou un effet de téléportation ou tout autre pouvoir qui transporte physiquement une créature d’un plan à un autre (comme clignotement ou passage dans l’éther) alors qu’il se trouve dans l’aura d’ancrage du personnage, il reçoit 4d8 + modificateur de Charisme du personnage points de dégâts. Ces dégâts viennent de la puissance sacrée et ne sont pas soumis à la réduction de dégâts, aux immunités contre les énergies, ni aux résistances.", "Les extérieurs Mauvais reçoivent des dégâts quand ils essayent d’atteindre d’autres plans. ", "Ancre douloureuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maléfice-maudit", "Quand le personnage vise une créature avec un maléfice qui ne peut pas se lancer plus d’une fois par jour sur la même cible et que cette dernière a réussi son jet de sauvegarde contre le maléfice, il peut le lui relancer avant la fin de son prochain tour. En cas d’échec, il ne peut pas relancer ce maléfice sur la cible avant 1 jour.", "Le personnage peut tenter de relancer un maléfice qui n’a pas fonctionné. ", "Maléfice maudit", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bluff-magique", "Si un autre lanceur de sorts essaye de contrer l’incantation, il ajoute +4 au DD d’Art de la magie quand il essaye de déterminer son sort.\r\nComme le personnage a appris à masquer les éléments les plus reconnaissables de ses incantations, il gagne un bonus de +2 aux tests d’Art de la magie pour identifier et contrer le sort d’un adversaire s’il le connaît ou s’il figure dans son grimoire.", "Le personnage connaît les principes du duel magique et, quand il affronte un autre lanceur de sorts, il sait cacher la véritable nature de ses sorts jusqu’au dernier instant. ", "Bluff magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisateur-polyvalent", "Le personnage fait un choix à chaque fois qu’il canalise.\r\nS’il canalise habituellement de l’énergie positive, il peut choisir la négative avec un niveau de prêtre effectif réduit de 2.\r\nS’il canalise habituellement de l’énergie négative, il peut choisir la positive avec un niveau de prêtre effectif réduit de 2.\r\nSi le personnage dispose de ce don, il remplit les conditions requises pour les dons et pouvoirs qui exigent une « canalisation d’énergie positive » ou une « canalisation d’énergie négative » (par exemple, il remplit les conditions du Contrôle des morts-vivants et du Renvoi des morts-vivants).", "Le personnage choisit s’il veut canaliser de l’énergie positive ou négative. ", "Canalisateur polyvalent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-de-plaie-supérieur", "Quand le personnage confirme un coup critique avec un sort de contact, de contact à distance ou un pouvoir magique, la cible est victime d’une plaie magique majeure aléatoire.", "Les coups critiques des sorts et pouvoirs magiques affligent la cible d’une malédiction majeure. ", "Critique de plaie supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "couteau-de-sorcière", "Chaque jour, quand le personnage prépare ses sorts, il peut choisir une dague magique ou de maître et la transformer en couteau de Sorcière qui lui sert de focalisateur supplémentaire pour ses sorts de protecteur. Elle rajoute +1 au DD de ses sorts de protecteur.", "Le personnage renforce la puissance de ses sorts de sorcière en utilisant un couteau cérémoniel lors des incantations.", "Couteau de sorcière", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "convocations-supplémentaires", "Le personnage gagne 1 utilisation quotidienne de convocation de monstres comme pouvoir magique de plus.", "Chaque jour, le personnage peut invoquer des monstres un peu plus souvent. ", "Convocations supplémentaires", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "convocation-supérieure", "À chaque fois que le personnage lance un sort de convocation qui appelle plus d’une créature, il en convoque une de plus.", "Le personnage peut invoquer plus de créatures. ", "Convocation supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "convocation-stellaire", "Les créatures convoquées par le personnage gagnent le don Combat en aveugle, un bonus de 5 aux tests de Perception et de Discrétion dans une faible lumière ou dans l’obscurité et leurs armes naturelles sont assimilées à du fer froid quand il s’agit de vaincre la réduction de dégâts.", "Les créatures que le personnage convoque se glissent dans l’ombre des étoiles.", "Convocation stellaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "convocation-solaire", "Les créatures convoquées par le personnage émettent autant de lumière qu’un sort de lumière. Elles sont immunisées contre les effets d’aveuglement et d’éblouissement et leurs armes naturelles sont considérées comme magiques quand il s’agit de vaincre la réduction de dégâts.", "Les créatures que le personnage convoque brillent du pouvoir du soleil. ", "Convocation solaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bombe-télécommandée", "Le délai maximal des bombes à retardement passe à un nombre de minutes égal au niveau du personnage. S’il a sa bombe à retardement en ligne de mire, il peut la faire exploser en avance sur l’instant prévu en réussissant un test d’Intelligence DD 20.\r\nLe DD augmente de +1 pour chaque tranche de 3 mètres (2 c) qui sépare le personnage de sa bombe.", "Le personnage peut déclencher ses bombes à retardement à grande distance. ", "Bombe télécommandée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "convocation-sacrée", "Quand le personnage utilise convocation de monstres pour appeler des créatures dont l’alignement du sous-type ou des sous-types correspond exactement à son aura, il peut lancer le sort par une action simple et non en 1 round.", "Les sbires du protecteur divin du personnage sont prêts à répondre à son appel. ", "Convocation sacrée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "connaissances-officieuses", "Le personnage choisit un sort de niveau 1, un de niveau 2, un de niveau 3 et un de niveau 4 dans la liste des sorts de barde, de prêtre, d’inquisiteur ou d’oracle. Il ajoute ces sorts à la liste de paladin au niveau approprié. Une fois qu’il les a choisis, il ne peut plus en changer.", "Le personnage a fait des recherches dans des textes interdits et s’est familiarisé avec une magie puissante mais prohibée. ", "Connaissances officieuses", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "concentration-instinctive", "Le personnage n’a pas besoin de faire de test de Concentration quand il est affecté par un effet climatique vigoureux ou violent. Il gagne un bonus de +2 à tous les autres tests de Concentration.", "Le personnage a appris à entrer dans un état second quand il lance des sorts, ce qui lui permet d’ignorer les distractions, les dégâts, les effets météorologiques et même les effets des autres sorts. ", "Concentration instinctive", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "collectionneur-planaire", "À chaque fois que le personnage apprend un extrait de convocation d’alliés naturels, il apprend l’extrait de convocation de monstres équivalent.\r\nS’il apprend plus tard à fabriquer d’autres extraits de convocation d’alliés naturels, il apprendra automatiquement l’extrait de convocation de montres correspondant.", "Le personnage sait comment préserver et reconstituer des monstres extraplanaires ainsi que des animaux normaux. ", "Collectionneur planaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-radieuse", "Quand le personnage réussit une attaque de charge, il peut dépenser toutes les impositions des mains qui lui restent pour infliger des dégâts supplémentaires égaux à 1d6 par imposition dépensée + modificateur de Charisme.\r\nCes dégâts viennent de la puissance sacrée et ne sont pas soumis à la réduction de dégâts, aux immunités contre les énergies, ni aux résistances.", "Quand le personnage charge, il le fait avec toute la puissance de sa foi. ", "Charge radieuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chantesort", "Le personnage peut combiner sa représentation bardique et ses incantations de deux manières.\r\nTout d’abord, il peut dissimuler l’incantation de son sort de barde derrière sa représentation. Par une action rapide, il peut combiner le temps d’incantation du sort avec le test de Représentation. Les observateurs doivent faire un test de Perception ou de Psychologie opposé au test de représentation du personnage pour se rendre compte qu’il lance un sort. Ceci consomme 1 round de représentation bardique, quel que soit le temps d’incantation du sort.\r\nSinon, le personnage peut utiliser 1 round de représentation bardique, par une action de mouvement, pour maintenir un sort de barde qui demande de se concentrer sur la durée. Il peut lancer un autre sort au cours du même round s’il utilise la magie bardique pour maintenir sa concentration. Dans ce cas, il perd sa concentration quand il termine la représentation à laquelle il a intégré le sort.", "Le personnage peut mêler le pouvoir de ses représentations et de ses incantations. ", "Chantesort", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-rapide", "Le personnage peut canaliser l’énergie par une action de mouvement s’il dépense deux utilisations quotidiennes de ce pouvoir.", "L’énergie divine du personnage jaillit à une vitesse étourdissante. ", "Canalisation rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "convocation-lunaire", "Les créatures invoquées émettent autant de luminosité qu’un sort de lumière. Elles sont immunisées contre les effets de confusion et de sommeil, et les armes naturelles sont considérées comme étant en argent quand il s’agit de vaincre la réduction de dégâts.", "Les créatures convoquées par le personnage sont imprégnées de la puissance de la lune. ", "Convocation lunaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chanteur-de-guerre", "Lorsque le personnage utilise une représentation bardique avec une composante sonore sur un champ de bataille (c’est-à-dire n’importe quelle zone où au moins 12 combattants s’affrontent), la portée ou la zone de la représentation choisie est doublée. De plus, le DD des jets de sauvegarde contre les représentations bardiques du personnage augmente de +2 pour les créatures du sous-type orque et ce, quel que soit l’endroit où cela se produit.", "Les mélodies du personnage s’inspirent des chants sauvages et des manières brutales de son peuple et poussent ceux qui les entendent à plus de violence et de sauvagerie.", "Chanteur de guerre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-des-bâtons", "Grâce à différentes positions et techniques, le personnage peut manier un bâton comme une arme à une main. Au début de son tour, il doit décider s’il va le manier à une ou deux mains. Dans le premier cas, son autre main est libre et il est impossible d’utiliser le bâton comme une arme double. Il peut prendre le don Spécialisation martiale pour les bâtons même s’il n’a pas de niveaux de guerrier.", "Le personnage peut manier un bâton comme une arme à une ou deux mains. ", "Maître des bâtons", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-critique-de-plaie", "Quand le personnage applique une plaie à l’aide du don Critique de plaie ou Critique de plaie supérieur, il peut choisir le type de plaie magique qu’il applique au lieu de la déterminer au hasard.", "Le personnage contrôle le type de plaie infligée par ses coups critiques. ", "Maîtrise du critique de plaie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vigilance-instinctive", "Ce don donne un bonus supplémentaire de +1 aux tests de Perception et de Psychologie, et le personnage gagne un bonus de +2 aux jets de sauvegarde  contre les effets de sommeil et de charme.", "Les recherches que le personnage a menées sur les mystères et la nature de la réalité l’ont doté de sens aiguisés. ", "Vigilance instinctive", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "vision-prophétique", "Une fois par jour, le personnage peut entrer dans une transe profonde pour recevoir une vision du futur. Cette transe dure 10 minutes pendant lesquelles il ne peut pas entreprendre une autre action. S’il est interrompu, il doit reprendre à zéro. Quand il sort de sa transe, il sait si une action qui doit se dérouler dans un futur immédiat aura de bons ou de mauvais résultats, comme un sort d’augure avec 70% de chances de succès.", "Les aptitudes prophétiques de l’oracle lui donnent un aperçu du futur. ", "Vision prophétique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "voix-de-la-sibylle", "Le personnage gagne un bonus de +1 à tous les tests de Bluff, de Diplomatie et de Représentation (arts oratoires). S’il possède 10 rangs ou plus dans cette compétence, le bonus passe à +3. Ce don ne fonctionne pas si le personnage n’utilise pas sa voix lors du test de compétence (comme pour le Bluff utilisé pour faire une feinte en combat).", "La voix du personnage est étrangement convaincante. ", "Voix de la sibylle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "écho-magique", "Quand le personnage lance un sort avec un écho magique, il ne disparaît pas entièrement de sa mémoire et il peut le lancer une seconde fois dans la même journée. Aucun effet permettant au personnage de préparer à nouveau ou de relancer un sort n'est utilisable avec Écho magique. Si le personnage prépare ses sorts, il n’a pas besoin de préparer la seconde incantation dans un emplacement de sort. Si c’est un lanceur de sorts spontanés, la seconde incantation ne nécessite pas de dépenser un emplacement de sort utilisable.\r\nUn écho magique utilise un emplacement de sort de trois niveaux de plus que le niveau réel du sort.", "Quand le personnage lance un sort, il libère la majorité de son potentiel magique mais pas l’intégralité. ", "Écho magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "étrange-héritage-supérieur", "Le personnage gagne un nouveau pouvoir issu du lignage qu’il s’est découvert avec le don Étrange héritage. Il gagne un pouvoir de lignage d’ensorceleur de niveau 15 (ou moins) qu’il ne possède pas encore. Quand le personnage utilise ce pouvoir et tous les autres pouvoirs de lignage grâce à ce don, à l’Étrange héritage et à la Science de l’étrange héritage, son niveau d’ensorceleur est égal à son niveau de personnage.", "Le lignage que le personnage s’est découvert atteint des sommets. ", "Étrange héritage supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "étrange-héritage", "Le personnage choisit un lignage d’ensorceleur. Il doit disposer de la compétence de classe que l’ensorceleur obtient au niveau 1 grâce à son lignage (par exemple Premiers secours pour un lignage céleste). Il ne peut pas s’agir d’un lignage que le personnage possède déjà.\r\nLe personnage gagne le pouvoir de lignage de niveau 1 du lignage choisi.\r\nQuand le personnage utilise ce pouvoir, son niveau d’ensorceleur est égal à son niveau de personnage -2, même s’il est ensorceleur.\r\nIl ne gagne jamais les autres pouvoirs du lignage.", "Le personnage descend d’une longue lignée d’ensorceleurs et certains fragments de leur pouvoir coulent dans ses veines. ", "Étrange héritage", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "évolutions-supplémentaires", "La réserve d’évolution de l’eidolon augmente de 1.", "L’eidolon possède des évolutions supplémentaires. ", "Évolutions supplémentaires", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "œil-du-juge", "Quand le personnage utilise son pouvoir de classe de détection d’alignement, il peut passer 3 rounds à étudier une créature située dans les 18 mètres (12 c). Pendant ce temps, il ne peut pas entreprendre d’autre action. Il découvre ensuite l’alignement de la créature.", "Les véritables motivations des créatures n’échappent pas au regard du personnage. ", "Œil du juge", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aide-rapide", "Par une action rapide, le personnage peut tenter d’utiliser l’action « aider quelqu’un » pour octroyer à un allié un bonus de +1 au prochain jet d’attaque ou à sa CA.", "D’un simple mouvement rapide mais inoffensif de son arme, le personnage peut aider un compagnon de combat.", "Aide rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "apparence-enfantine", "Le personnage peut faire 10 sur les tests de Bluff pour convaincre ses interlocuteurs qu’il dit la vérité pour autant que son histoire lui donne le rôle d’un innocent. Il gagne un bonus de +2 aux tests de Déguisement pour se faire passer pour un enfant humain et il ignore les pénalités normalement imposées pour un déguisement en un individu d’une autre race et d’une autre catégorie d’âge dans ce cas-là.", "Le personnage ressemble tellement à un enfant humain que les autres ont tendance à lui faire confiance, peut-être à tort.", "Apparence enfantine", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "appel-du-conjurateur", "Chaque fois que le personnage appelle son eidolon, il peut lui octroyer un bonus d’altération de +2 en Force, Dextérité ou Constitution. Ce bonus persiste pendant 10 minutes après la fin du rituel de conjuration.", "Chaque fois que l’invocateur appelle son eidolon, il est plus puissant pendant un certain temps.", "Appel du conjurateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "art-du-devin", "Le personnage gagne un bonus de +2 aux tests de NLS des divinations pour percer la résistance à la magie ou les effets qui bloquent les divinations (comme un sort d’antidétection). Lorsqu’il utilise un sort de divination nécessitant de la concentration, il reçoit les informations données par le sort avec 1 round d’avance (ainsi, il gagne les informations des 2 premiers rounds après 1 round de concentration, puis les informations du 3e round au bout du 2e round de concentration).", "Grâce à ses puissants instincts magiques, le personnage peut lire les signes, les symboles et les indices mystiques plus facilement et plus rapidement.", "Art du devin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "artisanat-de-groupe", "Le personnage peut aider un autre personnage à fabriquer des objets magiques ou non magiques. Les deux personnages doivent posséder la compétence d’Artisanat adéquate ou le don de création correspondant, mais il suffit que chacune des autres conditions de fabrication soit satisfaite par l’un ou par l’autre. Le personnage offre un bonus de circonstances de +2 à tous les tests d’Artisanat ou d’Art de la magie relatifs à la fabrication d’un objet et, grâce à son aide, la quantité d’objets qu’il est possible de fabriquer en un jour (quantité exprimée en pièces d’or) est doublée.", "Le personnage peut aider efficacement à la fabrication d’objets.", "Artisanat de groupe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assaut-hébétant", "Le personnage peut choisir d’accepter une pénalité de -5 sur tous les jets d’attaque au corps à corps et tous les tests de manœuvres de combat pour hébéter les adversaires touchés par ses attaques au corps à corps pendant 1 round (en plus de leur infliger des dégâts normaux). Un jet de Vigueur réussi permet aux cibles d’annuler l’effet. Le DD de celui-ci est de 10 + le BBA du personnage. Le personnage doit choisir s’il veut utiliser ce don ou pas avant de réaliser le jet d’attaque. Les effets du don persistent jusqu’à son prochain tour.", "Le personnage peut hébéter ses ennemis en attaquant sauvagement.", "Assaut hébétant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assaut-repoussant", "Lorsque le personnage touche une créature de taille inférieure ou égale à la sienne avec une arme à deux mains lors d’une Attaque en puissance, il peut choisir de faire reculer la cible de 1,50 m (en ligne droite par rapport à lui) au lieu de lui infliger les dégâts supplémentaires résultant du don Attaque en puissance. Si l’attaque en question est un coup critique, le personnage peut choisir de faire reculer la cible de 3 mètres (2 cases). Le mouvement de la cible ne provoque pas d’attaques d’opportunité et celle-ci doit terminer son mouvement dans un espace sûr suffisamment grand pour l’accueillir. Le personnage choisit quel effet appliquer après avoir effectué le jet d’attaque, mais avant de lancer les dégâts.", "En portant un coup avec une arme à deux mains, le personnage peut faire reculer une créature de taille similaire à la sienne.", "Assaut repoussant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assaut-sanglant", "Le personnage peut choisir d’accepter une pénalité de -5 à tous ses jets d’attaque au corps à corps et tests de manœuvres de combat pour ajouter 1d4 points de dégâts de saignement aux dégâts normaux de ses attaques des armes au corps à corps. La créature touchée continue de subir des dégâts de saignement chaque round au début de son round. On peut arrêter les dégâts de saignement à l’aide d’un test de Premiers secours contre un DD de 15 ou avec n’importe quel soin magique. Les dégâts de saignement provenant de ce don ne se cumulent pas entre eux. Le personnage doit choisir s’il veut utiliser ce don ou pas avant de réaliser le jet d’attaque. Les effets du don persistent jusqu’au début de son prochain tour (mais le saignement, lui, continue jusqu’à la guérison).", "En sacrifiant un peu de précision, le personnage peut infliger des blessures sanglantes qui guérissent très lentement.", "Assaut sanglant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assaut-étourdissant", "Le personnage peut choisir d’accepter une pénalité de -5 sur tous les jets d’attaque au corps à corps et tous les tests de manœuvres de combat pour étourdir les créatures touchées par ses attaques au corps à corps pendant 1 round. Un jet de Vigueur contre un DD égal à 10 + le BBA du personnage permet d’annuler l’effet. Le personnage doit choisir d’utiliser ce don avant de réaliser le jet d’attaque. Ses effets persistent jusqu’à son prochain tour.", "Malgré leur manque de précision, les puissantes attaques du personnage peuvent étourdir ses ennemis.", "Assaut étourdissant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briseur-dobjets", "Lorsque le personnage attaque un objet inanimé et abandonné (ne faisant pas partie de l’équipement d’une créature), il ignore les 5 premiers points de sa solidité. Il reçoit également un bonus de +5 aux tests de Force pour enfoncer ou briser des portes.", "Le personnage surmonte les obstacles en les détruisant.", "Briseur d'objets", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "boyaux-dacier", "Le personnage gagne un bonus racial de +2 aux jets de sauvegarde contre tout effet infligeant l’état préjudiciable fiévreux ou nauséeux ainsi que contre les poisons ingérés (mais pas les autres). En plus de cela, il reçoit un bonus de +2 aux tests de Survie pour trouver de la nourriture pour lui-même (et seulement lui-même).", "L’estomac du personnage est particulièrement résistant.", "Boyaux dacier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-salvateur", "Chaque fois qu’un allié adjacent au personnage est la cible d’une attaque, le personnage peut, par une action immédiate, lui octroyer un bonus de bouclier à la CA de +2. Le personnage doit tenir une rondache, un écu ou un pavois pour pouvoir utiliser ce don.", "Le personnage parvient à parer des attaques qui pourraient se révéler mortelles pour ses alliés.", "Bouclier salvateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-protecteur", "Chaque fois que le personnage utilise l’action de défense totale avec une rondache, un écu ou un pavois, il peut offrir à un allié adjacent de taille inférieure ou égale à la sienne un bonus d’abri à la CA égal au bonus de bouclier conféré par son bouclier. Ce bonus persiste jusqu’au début de son prochain tour. Le bouclier du personnage ne donne pas de bonus d’abri aux jets de Réflexes cependant.", "Le personnage sait comment utiliser son bouclier pour se protéger et protéger ses alliés.", "Bouclier protecteur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-pour-la-monture", "Le personnage peut ajouter le bonus de base de son bouclier (y compris le bonus offert par Art du bouclier mais pas les bonus d’altération) à la CA de sa monture. De plus, il peut ajouter ce bonus aux tests d’Équitation visant à annuler une attaque portée contre sa monture (grâce au don Combat monté).", "Les manœuvres défensives du personnage permettent de le protéger lui et sa monture.", "Bouclier pour la monture", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-magique", "Par une action immédiate, le personnage peut sacrifier un sort préparé (ou un emplacement de sort inutilisé) de niveau supérieur ou égal à 1 afin d’obtenir un bonus de parade à la CA égal au niveau du sort (ou de l’emplacement) sacrifié pendant 1 round. Les sorts de niveau 0 ne peuvent pas être sacrifiés dans le cadre de ce don.", "Le personnage peut convertir des sorts en protections magiques.", "Bouclier magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ultime-fermeté", "L’aura de fermeté occupe un rayon de 6 mètres (4 c) et ne disparaît pas si le personnage tombe inconscient.", "L’aura de fermeté du paladin ne disparaît pas avec lui. ", "Ultime fermeté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-contre-les-rayons", "Le personnage doit utiliser une rondache, un écu ou un pavois pour tirer parti de ce don. Une fois par round, lorsqu’il devrait normalement être touché par une attaque de contact à distance (qui peut être un rayon ou un autre effet magique), il peut la parer et ainsi ne subir aucun dégât. Le bouclier du personnage, quant à lui, subit les pleins effets du sort ou de l’effet si ceux-ci lui sont applicables.", "Le personnage est capable de parer les rayons grâce à son bouclier.", "Bouclier contre les rayons", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bombes-supplémentaires", "Le personnage peut lancer 2 bombes de plus chaque jour.", "Le personnage peut lancer plus de bombes par jour.", "Bombes supplémentaires", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bien-préparé", "Une fois par jour, lorsque le personnage est confronté à une situation où un objet habituel est nécessaire, il peut effectuer un test d’Escamotage contre un DD de 10 + le coût de l’objet en pièces d’or pour « justement » l’avoir sur lui. Par exemple, il faut atteindre un DD de 12 pour avoir un pied de biche, alors qu’un DD de 20 est nécessaire pour une fiole d’acide. Il doit s’agir d’un objet facilement transportable : si le personnage est à pied et qu’il ne possède qu’un sac à dos, il ne peut pas avoir de grand chaudron en fer. Ce don ne concerne pas les objets magiques ni les objets spécifiques (comme la clef ouvrant une porte en particulier). Si le personnage est privé de son équipement ou de ses possessions, il perd le bénéfice de ce don jusqu’à ce qu’il ait disposé d’au moins un jour pour « acquérir » de nouveaux objets. Le personnage doit payer le prix normal de ces objets.", "Quelle que soit la situation, le personnage semble toujours avoir l’outil ou l’objet qu’il faut.", "Bien préparé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "avance-et-frappe", "Lorsque le personnage utilise le don S’avancer ou Poursuite pour suivre un ennemi, il peut effectuer une unique attaque de corps à corps contre cet ennemi en utilisant son BBA le plus élevé. Cette attaque compte comme une attaque d’opportunité pour le round. Utiliser ce don ne réduit pas le nombre d’actions qu’il est possible d’accomplir à chaque round.", "Lorsqu’un ennemi tente de s’éloigner, le personnage peut le suivre et l’attaquer.", "Avance et frappe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-magique", "Par une action simple, le personnage peut sacrifier un sort préparé (ou un emplacement de sort inutilisé) de niveau supérieur ou égal à 1 et le transformer en un rayon ciblant n’importe quel ennemi situé dans un rayon de 9 mètres (6 cases) et nécessitant une attaque de contact à distance. Cette attaque inflige 2d6 points de dégâts plus 1d6 points de dégâts par niveau du sort (ou de l’emplacement) sacrifié. Les sorts de niveau 0 ne peuvent pas être sacrifiés dans le cadre de ce don.", "Le personnage peut convertir des sorts en attaques magiques.", "Attaque magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-en-groupe", "On considère que le personnage prend en tenaille tout ennemi qui se trouve dans la zone contrôlée par au moins deux de ses alliés et ce, quelle que soit sa position exacte.", "Le personnage sait comment attaquer efficacement en groupe.", "Attaque en groupe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "astuce-déquitation", "Lorsque le personnage porte une armure légère ou aucune armure, il n’a pas besoin de réaliser de test pour toutes les tâches citées dans la description de la compétence d’Équitation dont le DD est inférieur ou égal à 15. Il ne subit pas la pénalité de -5 lorsqu’il monte sans selle. Il peut effectuer un test pour annuler une attaque portée contre sa monture (voir Combat monté) deux fois par round au lieu d’une seule fois.", "Le personnage ne se contente pas de simplement contrôler sa monture au combat ; il a érigé cela en art !", "Astuce d'équitation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-contre-les-projectiles", "Le personnage doit utiliser une rondache, un écu ou un pavois pour tirer parti de ce don. Une fois par round, lorsqu’il devrait normalement être touché par une attaque effectuée à l’aide d’une arme à distance (autre qu’un effet de sort, une attaque naturelle ou une attaque par un projectile de grande taille), il peut la parer et ainsi ne subir aucun dégât, comme s’il possédait le don Parade de projectiles. Cela ne fonctionne pas si le personnage est pris au dépourvu ou surpris par l’attaque.", "Le personnage sait comment parer les attaques à distance à l’aide de son bouclier.", "Bouclier contre les projectiles", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tueur-prolongé", "Le personnage ajoute son bonus de Sagesse au nombre de rounds quotidiens pendant lesquels il peut utiliser son pouvoir de tueur.", "La dévotion du personnage est sans limite et sa colère refuse de s’éteindre. ", "Tueur prolongé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tours-de-magie-ou-oraisons-supplémentaires", "Le personnage ajoute deux tours de magie ou oraisons à la liste de ceux qu’il connaît.", "Le personnage est un maître des sorts mineurs. ", "Tours de magie ou oraisons supplémentaires", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "théurgie", "Le personnage peut augmenter la puissance de ses sorts divins avec de l’énergie profane et améliorer ses sorts profanes avec l’énergie divine.\r\nQuand il lance un sort divin, il peut sacrifier un emplacement de sort profane ou un sort profane préparé du même niveau que ce sort ou plus, par une action rapide. Le niveau de lanceur du sorts divin augmente de +1.\r\nQuand le personnage lance un sort profane, il peut sacrifier un emplacement de sort divin ou un sort divin préparé de même niveau ou de niveau supérieur, par une action rapide. La moitié des dégâts du sort profane deviennent de type sacré (comme si le personnage canalisait de l’énergie positive) ou impie (comme s’il canalisait de l’énergie négative).", "Le personnage peut mêler le pouvoir de la magie divine et profane. ", "Théurgie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "récompense-de-grâce", "À chaque fois que le personnage fait une Imposition des mains, il gagne un bonus sacré de +1 à tous les jets d’attaque pendant 1 round.", "Quand le personnage fait une imposition des mains, il est parcouru de vagues d’énergie divine qui lui accordent une grâce. ", "Récompense de grâce", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "regard-pénétrant", "Quand le personnage fait un test de Psychologie opposé à un test de Bluff, il peut lancer deux fois les dés et choisir le meilleur résultat.", "Lorsque le personnage interagit avec autrui, il remarque des choses que les autres ne voient pas. Il est difficile de lui cacher quoi que ce soit. ", "Regard pénétrant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "regard-intimidant", "Une fois par jour, par une action libre, quand le personnage fait un test d’Intimidation, il peut lancer deux fois les dés et choisir le meilleur.", "Dans le regard du personnage, il y a quelque chose qui effraye les gens. ", "Regard intimidant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "refuser-la-mort", "Tant que le personnage a encore 1 point de ki dans sa réserve, s’il rate un test de Constitution pour se stabiliser, il ne perd pas 1 point de vie. S’il réussit le test, il peut dépenser 1 point de ki pour guérir de 1d6 points de vie. S’il fait un 20 naturel au test de stabilisation, il peut dépenser ce point pour soigner 2d6 points de vie.", "Le ki du personnage est si puissant qu’il refuse la mort. ", "Refuser la mort", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prêtre-guerrier", "Le personnage gagne un bonus de +1 aux tests d’initiative et de +2 aux tests de Concentration pour lancer un sort ou utiliser un pouvoir magique quand il incante sur la défensive ou quand il est agrippé.", "La religion du personnage est comme une arme et un bouclier. ", "Prêtre guerrier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prodige", "Le personnage choisit deux compétences d’Artisanat , de Profession ou de Représentation , avec n’importe quelle combinaison (deux compétences d’Artisanat , une d’Artisanat et une de Représentation , etc.) Il reçoit un bonus de +2 aux tests liés. S’il possède 10 rangs ou plus dans l’une d’elles, le bonus concerné passe à +4.", "Le personnage est particulièrement doué pour les arts, les professions et l’acquisition de connaissances. ", "Prodige", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "récompense-de-vie", "À chaque fois que le personnage utilise son imposition pour soigner une cible autre que lui, il guérit d’un nombre de points de vie égal à son bonus de Charisme.\r\nCe don n’a aucun effet si le personnage utilise l’imposition pour blesser des morts-vivants.", "Quand le personnage utilise l’imposition des mains, il guérit également. ", "Récompense de vie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "piège-de-rôdeur-évolué", "Le personnage ajoute +1 au DD de tous les tests de Perception et de Sabotage destinés à trouver ou désamorcer des pièges avec son pouvoir de piège de classe.\r\nIl ajoute +1 à tous ses jets de sauvegarde contre les effets des pièges de rôdeur posés avec son pouvoir de classe de Piège.", "Il est très difficile de remarquer et d’éviter les pièges de rôdeur du personnage. ", "Piège de rôdeur évolué", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "oracle-du-péché", "Le personnage bénéficie de détection du mal. Il peut utiliser ce pouvoir ou détection des morts-vivants mais pas les deux à la fois.", "Contrairement à d’autres adeptes du serment contre les morts-vivants, l’obsession du personnage pour les morts-vivants n’obscurcit pas ses capacités à différencier le bien du mal. ", "Oracle du péché", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "métamagie-spontanée", "Le personnage choisit un unique sort qu’il est capable de lancer spontanément. Quand il lui applique un don de métamagie, il peut le lancer avec un temps d’incantation normal, sans ralentissement.", "Le personnage peut combiner un sort connu et un don de métamagie. ", "Métamagie spontanée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "musique-flamboyante", "Quand le personnage lance un sort de barde qui inflige des dégâts, il peut remplacer ces dégâts habituels par des dégâts de feu ou répartir les dégâts de manière à ce que la moitié soit du type normal et l’autre de feu.\r\nSi le personnage lance un sort de convocation de monstres en tant que sort de barde, il peut donner une apparence flamboyante aux créatures convoquées, ce qui leur donne une résistance au feu de 5 et ajoute 1 point de dégâts de feu à leurs attaques naturelles. La créature émet une faible lumière dans un rayon de 1,50 mètre (1 c).\r\nCet aspect du don reste sans effet sur des créatures de sous-type feu.\r\nQuand le personnage utilise ce don, le sort affecté devient du registre du feu.", "Les capacités du personnage en matière de musique de barde et de contrôle du feu se sont mêlées en une étrange fusion des deux magies. ", "Musique flamboyante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mur-de-boucliers-canalisé", "Par une action rapide, le personnage peut dépenser une utilisation de sa canalisation d’énergie pour se doter d’un bonus de parade de +2 quand il utilise un bouclier. Ce bonus dure une minute par niveau de prêtre ou niveau effectif de prêtre. Tant que le personnage bénéficie de ce bonus, ses alliés équipés de boucliers et adjacents à lui bénéficient aussi de ce bonus de parade de +2.", "Le personnage utilise sa canalisation d’énergie pour améliorer les capacités protectrices de son bouclier et des alliés adjacents. ", "Mur de boucliers canalisé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mot-de-guérison", "Le personnage peut utiliser l’Imposition des mains pour guérir une créature située à 9 mètres (6 c), par une action simple qui ne provoque pas d’attaque d’opportunité. Il doit être en mesure de parler et doit avoir une main libre. La cible guérit de la moitié de ce qu’elle devrait si le personnage la touchait mais bénéficie de ses grâces comme à l’accoutumée.", "Le personnage utilise la même énergie que celle de l’imposition des mains pour soigner ses alliés de loin. ", "Mot de guérison", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "meurs-pour-ton-maître", "Si le familier tumeur est attaché au personnage et que ce dernier doit tomber à 0 point de vie ou moins à cause de dégâts reçus en combat (de la part d’une arme ou d’une autre forme de coup, pas à cause d’un sort ou d’un pouvoir spécial), le familier se jette sur la trajectoire de l’attaque par une action immédiate. S’il réussit un jet de Réflexes (DD = dégâts infligés) il reçoit tous les dégâts de l’attaque, s’il le rate il en prend la moitié et le personnage reçoit le reste.\r\nPour se servir de ce pouvoir, le familier doit être conscient de l’attaque et capable de réagir. Il ne peut le faire qu’une fois par jour. S’il ne dispose pas de son bonus de Dextérité à la CA, il ne peut pas s’en servir. Comme ce pouvoir ne revient pas à faire un jet de Réflexes pour éviter la moitié des dégâts, l’esquive surnaturelle ne s’applique pas au jet de sauvegarde.", "Le familier tumeur du personnage est prêt à tout pour lui sauver la vie. ", "Meurs pour ton maître", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "piège-de-rôdeur-supplémentaire", "Le personnage peut utiliser ses pièges de rôdeur deux fois de plus par jour.", "Le personnage peut utiliser ses pièges de rôdeurs plus souvent. ", "Piège de rôdeur supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-des-morts-vivants", "Quand le personnage lance animation des morts ou utilise le don Contrôle des morts-vivants, on considère qu’il possède quatre niveaux de plus qu’en réalité quand il détermine le nombre de dés de vie à animer. La durée du contrôle mineur des morts-vivants est doublée.", "Le personnage peut rassembler de vastes armées de morts-vivants sous sa bannière. ", "Maître des morts-vivants", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "répartition-des-maléfices-majeurs", "Quand le personnage utilise un de ses maléfices majeurs (pas des grands) qui vise une unique créature, il peut en choisir une seconde située à moins de 9 mètres (6 c) de la première. Elle sera aussi affectée par le maléfice.", "Le personnage peut diviser les effets d’un maléfice qui cible une créature et en affecter une autre située dans son champ de vision. ", "Répartition des maléfices majeurs", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "réserve-magique-supplémentaire", "La réserve magique du personnage augmente de 2", "Le personnage a appris à tirer plus de puissance de sa réserve magique. ", "Réserve magique supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "synergie-des-sens", "Quand le personnage partage ses sens avec son eidolon, il gagne un bonus de compétence de +4 aux tests de Perception tant que dure le pouvoir de sens liés.", "Quand le personnage et son eidolon partagent leurs sens, leurs esprits combinés les dotent de pouvoirs d’observation exceptionnels. ", "Synergie des sens", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "spécialisation-magique", "Le personnage choisit un sort d’une école pour laquelle il dispose du don École renforcée . Il considère qu’il possède deux niveaux de lanceur de sorts de plus quand il s’agit de déterminer les effets variables du sort.\r\nÀ chaque fois que le personnage gagne un niveau pair dans la classe de lanceur de sorts dans laquelle il a choisi son sort, il peut en choisir un nouveau pour remplacer celui qu’il avait anciennement choisi pour le don. Ce sort devient son nouveau sort de spécialité.", "Le personnage choisit un sort qu’il lance avec plus de puissance que la normale. ", "Spécialisation magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "spécialisation-magique-supérieure", "Si le personnage sacrifie un sort préparé de même niveau que son sort de spécialité ou de niveau supérieur, il peut lancer spontanément son sort de spécialité. Ce dernier agit à son niveau normal, quel que soit le niveau du sort sacrifié.\r\nLe personnage peut lui appliquer un don de métamagie en augmentant le niveau d’emplacement du sort et le temps d’incantation, comme un prêtre qui lance spontanément un sort de soins ou de blessure avec un don de métamagie.", "Le personnage peut sacrifier un sort préparé afin de lancer spontanément son sort de spécialisation.", "Spécialisation magique supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-éblouissant", "Les effets d’électricité, de feu ou de lumière du sort affecté génèrent un flamboiement qui éblouit les créatures blessées. Si une créature reçoit des dégâts de feu ou d’électricité de la part d’un sort éblouissant, elle est éblouie pendant un nombre de round égal au niveau réel du sort. Le sort éblouissant affecte uniquement les sorts du registre du feu, de la lumière ou de l’électricité.\r\nCe sort utilise un emplacement de sort d’un niveau de plus que le niveau réel du sort.", "Quand le personnage affecte une créature avec un sort du registre du feu, de la lumière ou de l’électricité, il l’éblouit. ", "Sort éblouissant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-transperçant", "Quand le personnage lance un sort transperçant contre une cible douée de résistance à la magie, il la considère comme réduite de 5 points par rapport à la réalité.\r\nUn sort transperçant utilise un emplacement de sort d’un niveau de plus que le niveau réel du sort.", "Les études du personnage lui ont permis de développer une méthode pour vaincre la résistance à la magie. ", "Sort transperçant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-thrénodique", "Ce don fonctionne uniquement sur les sorts mentaux. Il affecte les morts-vivants (même s’ils sont dépourvus d’Intelligence) comme s’ils n’étaient pas immunisés contre les effets mentaux mais il n’a aucun effet sur les créatures vivantes.\r\nUn sort thrénodique utilise un emplacement de sort de deux niveaux de plus que le niveau réel du sort.", "Le personnage peut convertir une magie mentale en puissance nécromantique capable de contrôler les morts-vivants. ", "Sort thrénodique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "répartition-des-maléfices", "Quand le personnage utilise un de ses maléfices (pas un majeur ni un grand) qui vise une unique créature, il peut en choisir une seconde située à moins de 9 mètres (6 c) de la première. Elle sera aussi affectée par le maléfice.", "Le personnage peut diviser les effets d’un maléfice qui cible une créature et en affecter une autre située dans son champ de vision. ", "Répartition des maléfices", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-thanatopique", "Un sort thanatopique franchit les défenses et les immunités qui protègent contre les effets de mort, l’énergie négative et les absorptions d’énergie, qui affectent alors la cible comme si sa barrière protectrice n’existait pas.\r\nPar exemple, le personnage peut lancer un baiser du vampire ou une énergie négative thanatopique sur une cible à l’abri d’une protection contre la mort. La cible souffre des effets normaux du sort. Les jets de sauvegarde et la Résistance à la magie (le cas échéant) s’appliquent toujours.\r\nLes morts-vivants sont sensibles aux sorts modifiés par ce don car il harmonise l’énergie négative de manière à ce qu’elle les blesse. Un sort thanatopique qui devrait tuer une créature vivante (en lui donnant autant de niveaux négatifs qu’elle a de DV par exemple) détruit un mort-vivant (même si les fantômes, les liches, les vampires et autres se reforment comme d’habitude). Les morts-vivants affectés par un sort thanatopique qui impose des niveaux négatifs réussissent automatiquement leur jet de sauvegarde pour s’en débarrasser au bout de 24 heures.\r\nUn sort thanatopique utilise un emplacement de sort de deux niveaux de plus que le niveau réel du sort.", "Les sorts du personnage franchissent les sceaux contre l’énergie négative et affectent même les morts-vivants. ", "Sort thanatopique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-percutant", "Une vague d’énergie perturbatrice accompagne les dégâts sonores du sort et s’abat sur les créatures qu’il affecte.\r\nLe sort percutant affecte les créatures victimes d’un sort du registre du son et leur inflige un malus de -2 aux jets d’attaque, de sauvegarde, de compétence et de caractéristique pendant un nombre de rounds égal au niveau réel du sort.\r\nLe sort percutant affecte seulement les sorts du registre du son.\r\nUn sort percutant utilise un emplacement de sort de deux niveaux de plus que le niveau réel du sort.", "Le personnage désoriente les créatures qu’il affecte avec un sort du registre du son. ", "Sort percutant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-gelé", "Le givre des sorts de froid du personnage s’accroche à la cible et la gêne pendant une courte période.\r\nLa créature qui reçoit des dégâts de froid de la part d’un sort gelé est enchevêtrée pendant un nombre de rounds égal au niveau originel du sort.\r\nCe don affecte uniquement les sorts du registre du froid.\r\nUn sort gelé utilise un emplacement de sort d’un niveau de plus que le niveau réel du sort.", "Les créatures touchées par les sorts du registre du froid du personnage sont enchevêtrées. ", "Sort gelé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-brûlant", "Les effets de feu ou d’acide du sort affecté adhèrent à la créature et lui infligent des dégâts accrus au prochain round. Quand une créature subit des dégâts de feu ou d’acide de la part du sort affecté, elle subit aussi 2 x niveau réel du sort points de dégâts au début de son prochain tour. Ce sont des dégâts de feu ou d’acide, comme indiqué par le registre du sort. Si un sort brûlant est à la fois de feu et d’acide, c’est au lanceur de sort de choisir le type de dégâts infligés par l’effet de sort brûlant.\r\nUn sort brûlant utilise un emplacement de sort de deux niveaux de plus que le niveau réel du sort.", "Le personnage inflige des dégâts supplémentaires quand il affecte une créature avec un sort du registre du feu ou de l’acide. ", "Sort brûlant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "se-relever-avec-le-ki", "Tant que le personnage a au moins 1 point de ki dans sa réserve, il peut se relever par une action rapide qui provoque des attaques d’opportunité.\r\nIl peut dépenser 1 point de cette réserve pour se relever par une action rapide qui ne déclenche pas d’attaque d’opportunité.", "Si un adversaire fait tomber le personnage, il se relève instantanément. ", "Se relever avec le ki", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-létrange-héritage", "Le personnage gagne le pouvoir de niveau 3 ou 9 du lignage qu’il a choisi avec le don Étrange héritage, au choix. Quand le personnage utilise ce pouvoir, son niveau d’ensorceleur est égal à son niveau de personnage -2, même s’il est ensorceleur.\r\nIl ne gagne jamais les autres pouvoirs du lignage.", "La puissance du lignage que le personnage s’est découvert continue de croître. ", "Science de l'étrange héritage", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-connaissance-des-monstres", "Le personnage gagne un bonus sacré égal à 1/2 niveau des classes qui donnent le pouvoir connaissance des monstres à tous les tests de compétence qui visent à identifier les pouvoirs et les faiblesses des créatures.", "Le personnage est obsédé par les pouvoirs et les faiblesses des monstres.", "Science de la connaissance des monstres", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sort-renversant", "L’impact des sorts de force du personnage est si violent qu’il renverse les créatures.\r\nSi la cible reçoit des dégâts, rate son jet de sauvegarde ou se fait déplacer par le sort de force, le personnage doit faire un test de croc-en-jambe contre elle, en utilisant son niveau de lanceur de sorts plus son bonus de caractéristique de lanceur de sorts (la Sagesse pour les prêtres, l’Intelligence pour les magiciens, etc.) Ceci ne provoque pas d’attaque d’opportunité. Si le personnage rate son test, la cible ne peut pas essayer de faire un croc-en-jambe en retour sur le personnage ni sur l’effet de force.\r\nUn sort renversant s’applique uniquement aux sorts du registre de la force.\r\nIl utilise un emplacement de sort d’un niveau de plus que le niveau réel du sort.", "Les sorts du registre de la force renversent les créatures qu’ils affectent. ", "Sort renversant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combat-à-plusieurs-armes", "Lorsque la créature attaque avec plusieurs armes, les malus sont réduits de –2 pour les attaques effectuées avec la main directrice et de –6 pour les autres.\r\nNormal. Sans ce don, la créature subit un malus de –6 à toutes les attaques effectuées avec sa main directrice et de –10 à toutes les attaques effectuées avec les autres mains (elle ne possède qu’une seule main directrice). Voir Combat à deux armes.", "La créature sait utiliser ses nombreux bras pour attaquer avec plusieurs armes.", "Combat à plusieurs armes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "guérison-athée", null, "Le personnage maîtrise une série de techniques spécialisées complexes qui lui permettent d’ignorer la douleur en se concentrant sur sa foi en lui-même et non en une religion.", "Guérison athée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "guide-divin-(mythique)", "Lorsque le personnage attaque avec l’arme de prédilection de sa divinité, il peut remplacer son modificateur de Force ou de Dextérité aux jets de dégâts par son modificateur de Sagesse.", "Les attaques effectuées avec l’arme de prédilection du dieu que vénère le personnage sont divinement inspirées.", "Guide divin (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ruée-du-jann", "Quand le personnage utilise l’École du jann et qu’il saute, on considère qu’il bénéficie tout le temps d’une course d’élan. De plus, s’il saute lors d’une charge et qu’il fait une attaque à mains nues, s’il touche, il lance deux fois les dés de dégâts et les additionne avant d’ajouter les modificateurs (comme la Force) ou les dés de dégâts supplémentaires (comme les dégâts de précision ou les dés de dégâts des propriétés d’une arme). On ne multiplie pas les dés de dégâts supplémentaires en cas de coup critique.", "Quand le personnage saute pour attaquer, ses coups ressemblent à des éclairs tombés du ciel. ", "Ruée du jann", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "réduction-agressive", "Quand un adversaire adjacent au personnage ne parvient pas à franchir sa RD avec une attaque au corps à corps, le personnage peut dépenser une action immédiate pour tenter une bousculade contre lui. S’il réussit, il ne se déplace pas avec l’adversaire.", "La réduction de dégâts du personnage retourne parfois les coups ennemis contre l’assaillant. ", "Réduction agressive", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "réparations-de-fortune", "Si le personnage a des rangs en Artisanat dans une compétence associée à un objet brisé, il peut le réparer sans payer de matériaux bruts et sans malus au test d’Artisanat s’il utilise des outils improvisés. En un jour de travail, l’objet récupère 1 point de vie et un quart de ses points de vie originaux. De plus, si l’objet est brisé parce qu’il s’agit d’une arme à feu qui a fait long feu, d’un engin de siège qui a eu une avarie ou encore d’une arme fragile (ou avec une propriété similaire), il peut faire un test d’Artisanat avec le DD de création de l’objet. S’il réussit son test, l’objet n’est plus brisé.", "Le personnage peut remettre une arme ou une armure brisée en état sans outils d’artisan. ", "Réparations de fortune", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "résilience-renforcée", "Le personnage peut, par une action immédiate, doubler sa RD contre une unique attaque (avec un maximum de RD 20). Le type de RD ne change pas. Si l’attaque contre laquelle le personnage se protège ne le touche pas, la RD accrue persiste jusqu’à ce qu’une attaque le touche ou jusqu’au début de son prochain tour, selon ce qui se produit en premier. Il est fatigué dès le début de son prochain tour. Il ne peut pas utiliser ce don quand il est fatigué.", "Le personnage peut considérablement augmenter sa réduction de dégâts au prix d’une grande fatigue. ", "Résilience renforcée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sage-harmonique", "Quand le personnage se trouve dans une structure artificielle, il peut dépenser une action libre pour faire un test de Connaissances (ingénierie) DD 15 au début de sa représentation bardique. S’il réussit, il a droit à l’un des avantages suivants.\r\n* Harmonisation. Le personnage profite de la réverbération acoustique pour augmenter le DD de ses effets de représentation bardique de +1.\r\n* Réverbération. Quand le personnage termine sa représentation, l’effet se poursuit 1 round de plus, quelle que soit la raison pour laquelle il a cessé la représentation. Cela ne lui permet pas d’avoir plusieurs représentations bardiques actives à la fois.", "Le personnage a si bien saisi le fonctionnement acoustique des structures artificielles qu’il en profite pour améliorer ses représentations bardiques. ", "Sage harmonique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sagesse-de-la-mante", "Quand le personnage détermine les effets applicables avec le Coup étourdissant du moine, il compte la moitié de ses autres niveaux de classe comme des niveaux de moine. Il peut débarrasser une cible des effets d’un Coup étourdissant avec une action simple et une attaque réussie au corps à corps. Tant qu’il utilise l’École de la mante, il gagne un bonus de +2 aux jets d’attaque à mains nues consacrées au Coup étourdissant.", "Le personnage connaît si bien les points vitaux qu’il porte ses coups handicapants avec une précision redoutable. ", "Sagesse de la mante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "saisir-sa-chance", "Quand un allié qui possède aussi ce don confirme un coup critique contre un adversaire que le personnage menace également, ce dernier a droit à une attaque d’opportunité.", "Le personnage et ses alliés sont prêts à bondir dès que l’un d’eux assène un coup puissant. ", "Saisir sa chance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "sale-coup-rapide", "Le personnage peut, à son tour, faire une manœuvre offensive de sale coup au lieu d’une attaque au corps à corps. Cette manœuvre doit remplacer l’attaque avec le meilleur bonus.", "Le personnage peut faire un sale coup et attaquer avant que son adversaire ne réagisse. ", "Sale coup rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "savant-dimensionnel", "Quand le personnage utilise le don Derviche dimensionnel, il compte comme une personne prenant un adversaire en tenaille pour toutes les cases d’où il lance une attaque lors du tour. Cette prise en tenaille commence à l’instant où il débute son attaque et se termine au début de son prochain tour. Grâce à ce don, il peut donc prendre un ou des adversaires en tenaille avec lui-même et avec plusieurs alliés.", "Le personnage entre et sort de la réalité à une telle vitesse qu’on ne peut pas savoir où il se trouve à un moment donné. ", "Savant dimensionnel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-feinte-à-deux-armes", "Quand le personnage se bat au corps à corps avec deux armes, il peut renoncer à la première attaque de sa main directrice pour faire une feinte avec un test de Bluff. S’il réussit, son adversaire perd son bonus de Dextérité à la CA jusqu’à la fin du tour du personnage.", "Le personnage se sert de son arme principale pour déséquilibrer son adversaire, ce qui lui permet de franchir ses défenses avec la seconde. ", "Science de la feinte à deux armes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-frappe-ciblée", "Le personnage reçoit un bonus de +2 à son jet d’attaque lorsqu’il entreprend une frappe ciblée. Quand le\r\npersonnage choisit une action simple ou complexe qui lui\r\npermet de multiples attaques, il peut remplacer une attaque\r\nsimple par une frappe ciblée. Le personnage ne peut tenter\r\nqu’une frappe ciblée par round.", "Le personnage est doué pour placer ses coups où il le souhaite.", "Science de la frappe ciblée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-frappe-dévastatrice", "Quand le personnage utilise la Frappe décisive, la Science de la frappe décisive ou la Frappe décisive supérieure, il gagne un bonus égal au bonus de dégâts de la Frappe dévastatrice au jet de confirmation de coup critique.", "La fureur et la puissance que le personnage met dans ses coups suffit à tuer des êtres fragiles sur le champ.", "Science de la frappe dévastatrice", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-lacération-furieuse", "Quand le personnage réussit à éventrer un adversaire, il lui inflige 1d6 points de dégâts de plus. On ne multiplie pas ces dégâts en cas de coup critique.", "Le personnage a affûté ses griffes au point de devenir une véritable tornade de dévastation sanglante. ", "Science de la lacération furieuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-la-maîtrise-du-fouet", "Quand le personnage manie un fouet, il menace la zone correspondant à son allonge naturelle plus 1,50 m (1 c). Il peut se servir de son fouet pour attraper un objet abandonné de taille P ou TP à portée et l’attirer dans sa case. Pour cela, il doit toucher une CA de 10 avec une attaque de contact à distance. De plus, il peut se servir du fouet pour s’accrocher à un objet. Il utilise alors 1,50 m (1 c) de fouet comme un grappin, ce qui lui permet d’utiliser le reste de sa longueur comme une corde. Il peut relâcher l’objet par une action libre mais il ne peut pas s’en servir pour attaquer tant qu’il tient quelque chose.", "Le personnage enchevêtre ses ennemis dans les anneaux de son fouet. ", "Science de la maîtrise du fouet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-de-lenchaînement-final", "Le personnage peut utiliser Enchaînement final aussi souvent qu’il veut au cours du même round.", "Le personnage peut terrasser de nombreux adversaires d’un seul coup.", "Science de l'enchaînement final", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-combat-dos-à-dos", "Quand le personnage est adjacent à un allié qui est pris en tenaille mais possède aussi ce don, il peut dépenser une action rapide pour bénéficier d’un bonus de +2 à la CA contre tous ceux qui prennent l’allié en tenaille, jusqu’au début de son prochain tour.", "Après un long entraînement, le personnage et un allié sont devenus particulièrement doués dans l’art de se battre côte à côte.", "Science du combat dos à dos", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-critique-dempalement", "Quand le personnage utilise Critique d’empalement contre un adversaire et qu’il tient encore son arme, la victime doit faire un test de lutte contre lui pour la retirer. Si le personnage a lâché son arme, la victime doit dépenser une action simple pour l’enlever.\r\nTant que la victime ne s’est pas débarrassée de l’arme, sa vitesse est réduite de moitié, quel que soit son mode de déplacement, et sa manœuvrabilité éventuelle diminue d’un cran.\r\nQuand l’arme sort, le personnage peut renoncer aux dégâts du Critique d’empalement pour infliger des dégâts de saignement une fois par round, pour un montant égal au résultat des dés de dégâts de l’arme. Ces dégâts se produisent au début du tour de la victime.", "Le personnage peut empaler sa cible, ce qui entrave ses mouvements et la fait saigner. ", "Science du critique d'empalement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-soudain-supérieur", "Quand le personnage fait une attaque d’opportunité avec une arme à distance et qu’il touche, il gagne un bonus de +2 au jet de dégâts et un bonus de +2 au jet de confirmation de coup critique (pour cette attaque seulement). Ce bonus passe à +4 quand le BBA du personnage passe à +16 et à +6 quand le BBA passe à +20.", "Le personnage profite de toutes les failles dans la défense de son adversaire, en toute impunité et de très loin. ", "Tir soudain supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-optimisé", "Quand le personnage utilise une fronde ou un bâton de jet halfelin, il réduit le malus des jets d’attaque dû à la distance de 2. Le bonus de dégâts du Tir à bout portant s’applique dans le premier facteur de portée de la fronde (15 m ou 10 c) ou du bâton de jet (24 m ou 16 c).", "Le personnage peut faire tournoyer sa fronde de manière à optimiser son efficacité. ", "Tir optimisé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-en-plein-saut", "Le personnage gagne un bonus de +2 aux tests d’Acrobaties pour sauter. Par une action complexe, il peut se déplacer à sa vitesse et faire une attaque à l’arme à feu avec son meilleur bonus et ce, pour chaque arme chargée qu’il tient. Il attaque à n’importe quel stade de son déplacement et, s’il manie deux armes, il peut attaquer à divers moments du déplacement. Il tombe à la fin du mouvement. Cet exploit coûte 1 point d’audace.", "Le personnage bondit dans les airs alors que ses armes retentissent. ", "Tir en plein saut", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-en-enfilade", "Le personnage reçoit un bonus de +2 aux jets d’attaque à distance contre un adversaire pris en tenaille par un ou plusieurs alliés qui disposent aussi de ce don.", "Les attaques à distance du personnage bénéficient de la prise en tenaille. ", "Tir en enfilade", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tempête-du-jann", "Quand le personnage utilise l’École du jann, qu’il fait une attaque à mains nues et touche, il gagne un bonus de +4 aux tests de bousculade ou de croc-en-jambe contre son adversaire, tant qu’il fait ce test juste après l’attaque et avant la fin de son tour. Cette manœuvre ne provoque pas d’attaque d’opportunité de la part de sa cible.", "La volée de coups du personnage déséquilibre souvent ses adversaires. ", "Tempête du jann", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tactique-de-laile-cassée", "Quand le personnage fait une attaque de corps à corps et touche son adversaire, il peut utiliser une action libre pour donner à ce dernier un bonus de +2 aux jets d’attaque et de dégâts contre sa personne jusqu’à la fin de son prochain tour ou jusqu’à ce que l’adversaire l’attaque, selon ce qui se produit en premier. Si l’adversaire attaque avec ce bonus, il provoque des attaques d’opportunité de la part de tous les alliés qui possèdent ce don.", "Le personnage feint une faiblesse et offre une cible tentante qui distraie les ennemis. ", "Tactique de l'aile cassée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ruse-au-filet", "Au lieu d’utiliser son filet pour attaquer, le personnage peut s’en servir pour faire un sale tour et aveugler un adversaire. Si un adversaire est enchevêtré dans son filet, il peut lui faire un croc-en-jambe s’il se trouve dans la limite imposée par l’allonge du filet ou que le personnage contrôle la corde qui lui est reliée. Le personnage gagne un bonus de +2 aux tests de manœuvres offensives attirer et repositionner à l’aide du filet.", "Le personnage est devenu particulièrement doué pour gêner les adversaires à l’aide de son filet. ", "Ruse au filet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "synergie-de-la-discrétion", "Quand le personnage voit un ou plusieurs alliés qui disposent de ce don et qu’ils font tous des tests de Discrétion, ils prennent tous le meilleur jet et lui ajoutent leur propre modificateur de Discrétion.", "Quand le personnage travaille en étroite collaboration avec un allié, ils se déplacent comme des ombres jumelles.", "Synergie de la discrétion", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "subtilisation-rapide", "Le personnage peut, à son tour, faire une manœuvre offensive de subtilisation au lieu de faire une attaque de corps à corps. Cette manœuvre doit remplacer l’attaque avec le meilleur bonus.", "Le personnage est particulièrement doué pour débarrasser ses adversaires de leurs possessions, même en plein combat. ", "Subtilisation rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "strangulation", "Quand le personnage réussit à maintenir une créature agrippée et décide de lui infliger des dégâts, il peut dépenser une action rapide pour lui appliquer aussi ses dégâts d’attaque sournoise.", "Donner la mort par asphyxie, c’est une seconde nature pour le personnage. ", "Strangulation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "souvenir-destructeur", "Quand le personnage réussit à briser le sort d’un adversaire grâce à une attaque au corps à corps, il peut se servir de suite du rappel de sort pour récupérer un sort de magus lancé au préalable. Ce pouvoir fonctionne comme s’il avait dépensé le nombre de points de réserve magique requis (sans dépasser le niveau de sort maximal qu’il peut lancer).", "Le personnage peut détruire un sort ennemi pour alimenter sa propre puissance magique. ", "Souvenir destructeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-tir-soudain", "Le personnage peut utiliser Tir soudain à 3 m (2 c).", "Le personnage sait tirer parti des faiblesses de son adversaire, même de loin et sans vous exposer. ", "Science du tir soudain", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-partenaire-de-feinte", "Quand un allié qui dispose de ce don réussit une feinte contre un adversaire, ce dernier provoque une attaque d’opportunité de la part du personnage.", "Le personnage connaît si bien les astuces et les techniques de ses compagnons qu’il tire le meilleur parti de leurs feintes. ", "Science du partenaire de feinte", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "science-du-développement-de-la-résistance", "Le personnage double la RD qu’il obtient avec Développement de la résistance, avec un maximum de 10/–.", "Le personnage supporte les coups et les rend même à ses agresseurs. ", "Science du développement de la résistance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "synergie-de-dissipation", "Quand le personnage réussit à dissiper un effet magique continu sur un ennemi, ce dernier subit un malus de -2 aux jets de sauvegarde contre les sorts du personnage jusqu’à la fin du prochain tour du personnage.", "Quand le personnage dépouille un ennemi de ses défenses magiques, il le rend particulièrement vulnérable et le malheureux a bien du mal à résister à ses sorts. ", "Synergie de dissipation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tir-soudain", "Quand le personnage manie une arme à distance pour laquelle il possède le don Arme de prédilection, il menace aussi les cases qui se trouvent à 1,50 m (1 c) de lui. Il peut faire des attaques d’opportunité avec cette arme et il n’en provoque pas quand il fait une attaque à distance en tant qu’attaque d’opportunité.", "Quand le personnage manie une arme à distance, il profite de la moindre ouverture dans les défenses de son adversaire. ", "Tir soudain", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rugissement-du-dragon", "Le personnage a droit à un Coup étourdissant de plus par jour. Quand il utilise l’École du dragon, il peut, par une action simple, dépenser deux tentatives de Coup étourdissant pour pousser un rugissement tonitruant dans un cône de 4,50 m (3 c). Les créatures situées dans le cône subissent les dégâts de l’attaque à mains nues du personnage et sont secouées pendant 1d4 rounds. Jet de Volonté, (DD 10 + 1/2 niveau de personnage + modificateur de Sagesse) 1/2 dégâts et pas secoué.", "L’esprit du dragon se concentre dans le personnage avant d’en sortir dans un puissant rugissement. ", "Rugissement du dragon", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "riposte-de-la-grue", "Le personnage subit un malus de -1 seulement aux jets d’attaque quand il se bat sur la défensive. Lorsqu'il se bat sur la défensive et qu'il utilise Aile de la grue pour obtenir un bonus d'esquive contre une attaque, cette attaque provoque une attaque d’opportunité de sa part si elle échoue. D'autre part, lorsque le personnage dévie une attaque grâce à l'utilisation d'Aile de la grue en défense totale, il peut effectuer une attaque d’opportunité contre cet adversaire (même s'il ne devrait normalement pas pouvoir faire d'attaque d'opportunité en défense totale).", "Le personnage profite de ses aptitudes défensives pour lancer de puissantes contre-attaques. ", "Riposte de la grue", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-du-refus-de-mourir-(alternatif)", "Quand ses points de blessure atteignent son seuil de blessure, le personnage ne reçoit pas le point de blessure normalement infligé lorsqu’une action est entreprise dans cet état.\r\n(((Ce don est une version modifiée du don Maître du refus de mourir pour utilisation avec les règles alternatives «&nbsp;Blessures et vitalité&nbsp;» de l’Art de la Guerre)))", "Même lorsqu’il souffre de blessures graves, le personnage peut ignorer celles-ci et poursuivre ses assauts implacables.", "Maître du refus de mourir (alternatif)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-du-refus-de-mourir", "Quand le personnage est à 0 point de vie ou moins, il ne perd pas de point de vie quand il fait une action.\r\n(((Ce don possède une variante Maître du refus de mourir (alternatif) pour utilisation avec les règles alternatives «&nbsp;Blessures et vitalité&nbsp;» de l’Art de la Guerre)))", "Même si le personnage est grièvement blessé, il peut ignorer les dégâts et continuer son assaut.", "Maître du refus de mourir", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-ingénieur-de-siège", "Si le personnage est chef d’équipe d’un engin de siège, son équipe utilise des actions de mouvement pour charger une arme de siège. Quand il vise avec une telle arme, son équipe et lui ont seulement besoin d’une action de mouvement et non d’une action complexe.", "Le personnage est particulièrement rapide quand il s’agit de charger une arme à feu de siège et il vise avec précision. ", "Maître ingénieur de siège", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-des-armes-de-spectacle", "Le personnage considère toutes les armes qu’il sait manier comme étant des armes de spectacle.", "Le personnage manie toutes les armes avec panache. ", "Maîtrise des armes de spectacle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-fouet-supérieure", "Le personnage manie son fouet si rapidement qu’il ne risque pas de le lâcher s’il rate une tentative de désarmement ou de croc-en-jambe.\r\nDe plus, il peut s’en servir pour faire une lutte. La lutte suit alors les règles habituelles, avec les modifications suivantes :\r\n* Allonge. Quand le personnage réussit un test de lutte et qu’il veut déplacer cette lutte, il n’est pas obligé d’attirer son adversaire à lui, il doit le garder à portée de son fouet mais se tenir lui-même hors de portée de cet adversaire.\r\nSi la différence entre les deux allonges est inférieure à 0 (si une créature de taille M tente de lutter avec une créature de taille Gig par l’intermédiaire d’un fouet), le personnage ne peut pas utiliser son fouet pour lutter. Si le personnage doit attirer une créature vers lui pour poursuivre une lutte au fouet, il provoque une attaque d’opportunité de la part de cette créature, à moins qu’il ne dispose du don Science de la lutte.\r\n* Attacher. Quand le personnage est adjacent à son adversaire, il peut tenter de l’attacher avec son fouet. Si l’adversaire est agrippé et non immobilisé, le personnage subit un malus de -5 seulement aux tests de manœuvres offensives au lieu du -10 habituel.\r\n* Attaque. Le personnage ne peut pas attaquer avec son fouet s’il s’en sert déjà pour lutter contre une créature.\r\n* Dégâts. Quand le personnage inflige des dégâts à un ennemi agrippé, il s’agit des dégâts du fouet et non des dégâts à mains nues.\r\n* Mains libres. Quand le personnage utilise son fouet pour faire une lutte, il ne reçoit aucun malus dû à l’absence de mains libres.", "Le personnage exécute les manœuvres offensives au fouet avec une facilité déconcertante. ", "Maîtrise du fouet supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maîtrise-du-fouet", "Le personnage ne provoque plus d’attaque d’opportunité quand il se bat avec un fouet. Il peut s’en servir pour infliger des dégâts létaux s’il le désire. Il inflige des dégâts en dépit du bonus d’armure ou d’armure naturelle de l’adversaire.", "Le personnage maîtrise si bien cette arme qu’il ne provoque pas d’attaques d’opportunité de la part de ses ennemis. ", "Maîtrise du fouet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mort-venue-du-ciel", "Quand le personnage charge un adversaire alors qu’il se trouve en hauteur par rapport à lui ou qu’il vole, il gagne un bonus de +5 aux jets d’attaque au lieu du bonus lié à la charge et au terrain surélevé.", "Le personnage se sert de la gravité pour ajouter des dégâts de force supplémentaires à ses charges. ", "Mort venue du ciel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "mouvements-du-singe", "Quand le personnage utilise l’École du singe, il ajoute son bonus de Sagesse à ses tests d’Escalade. Il grimpe et rampe à la moitié de sa vitesse de base, il peut faire un pas de 1,50 m (1 c) en sautant, en rampant ou en grimpant et il conserve son bonus de Dextérité à la CA quand il grimpe. De plus, tant qu’il utilise cette école et qu’il fait deux attaques à mains nues ou plus contre un adversaire au cours du même tour, il peut dépenser une action rapide pour faire un pas de 1,50 m (1 c), même s’il a déjà bougé lors de ce round.", "Le personnage s’agite autour de ses ennemis. Il se déplace et frappe de manière erratique. ", "Mouvements du singe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "médecin-de-guerre", "Quand le personnage utilise Premiers secours pour apporter une aide d’urgence, soigner les blessures dues à une chausse-trappe ou guérir un empoisonnement chez un allié qui possède aussi ce don, il ne provoque pas d’attaque d’opportunité et peut faire 10 au test. Contrairement aux autres dons de travail en équipe, les alliés paralysés, étourdis, inconscients ou incapables d’agir sont pris en compte pour faire fonctionner le don.", "Le personnage sait à quel point il est important de soigner les blessures quand le combat fait rage et administre les premiers soins à ses alliés à une telle vitesse que personne n’est laissé pour compte.", "Médecin de guerre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "optimisation-de-la-frappe-magique", "Quand le personnage arrive à utiliser la frappe magique lors d’une attaque au corps à corps contre un adversaire privé de son bonus de Dextérité à la CA, il peut dépenser 3 points de réserve magique pour optimiser le sort associé, comme s’il utilisait le don de métamagie Quintessence des sorts.", "Le personnage blesse grièvement les ennemis qu’il prend au dépourvu. ", "Optimisation de la frappe magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "parade-de-la-panthère", "Quand le personnage utilise l’École de la panthère, ses ripostes à mains nues s’effectuent avant les attaques qui les ont déclenchées. Si le personnage inflige des dégâts avec une riposte, son adversaire subit un malus de -2 aux jets d’attaque et de dégâts de l’attaque d’opportunité qui a déclenché la riposte.", "Les attaques vicieuses du personnage gênent les ennemis qui tentent de l’attaquer quand il se déplace. ", "Parade de la panthère", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "partenaire-de-feinte", "Quand un allié du personnage (qui possède aussi ce don) réussit à faire une feinte, son adversaire perd aussi son bonus de Dextérité à la CA contre la prochaine attaque du personnage, à condition qu’elle se produise avant la fin du prochain tour de l’allié.", "Le personnage a juste besoin d’une petite diversion pour franchir les défenses de son adversaire. ", "Partenaire de feinte", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pied-marin", "Le personnage gagne un bonus de +2 aux tests d’Acrobaties, Escalade et Natation.", "Le personnage se déplace sur les navires avec l’instinct d’un vrai marin. ", "Pied marin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "pistolier-amateur", "Le personnage obtient une petite quantité d’audace et peut faire un unique exploit de pistolier de niveau 1. Chaque matin, il gagne 1 point d’audace mais, au fil de la journée, ce nombre peut augmenter jusqu’à un maximum égal à son modificateur de Sagesse (1 au minimum). Il peut récupérer de l’audace en suivant les mêmes règles que le pistolier. Il peut dépenser cette audace pour accomplir l’exploit de niveau 1 choisi avec ce don ou tout autre exploit dont il est capable grâce à d’autres dons ou grâce à des objets magiques.", "Même si le personnage n’est pas un pistolier, il a de l’audace et peut s’en servir. ", "Pistolier amateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "piétinement-vicieux", "Quand un adversaire tombe à côté du personnage, il provoque une attaque d’opportunité à mains nues.", "Le personnage profite d’un avantage momentané pour donner un violent coup de pied à un ennemi à terre. ", "Piétinement vicieux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "poing-de-cauchemar", "Quand le personnage se bat dans une zone d’obscurité magique, il gagne un bonus de +2 aux jets de dégâts à mains nues ou un bonus de +4 contre les adversaires secoués, effrayés ou paniqués. Il gagne aussi un bonus de moral de +2 aux tests d’Acrobaties et d’Intimidation.", "Le personnage devient encore plus dangereux quand il est entouré d’une obscurité magique. ", "Poing de cauchemar", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "poing-de-dissipation", "Si le personnage a préparé dissipation de la magie ou s’il peut le lancer spontanément, il peut dépenser une action rapide pour le lancer de façon ciblée sur un adversaire qu’il vient de toucher à mains nues.", "En se concentrant sur la magie et les sorts d’annulation, le personnage peut arracher les défenses magiques d’un ennemi à mains nues. ", "Poing de dissipation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "retour-en-force", "Quand le personnage a droit de refaire un test de caractéristique, de compétence ou un jet de sauvegarde, il gagne un bonus de circonstances de +2 au second jet.", "Le personnage apprend vite de ses erreurs. ", "Retour en force", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "responsable-du-siège", "Quand le personnage dirige l’équipe qui assemble une arme de siège, il donne un bonus de +4 à tous les membres de l’équipe au test d’assemblage ou de déplacement. Le temps d’assemblage est réduit de moitié.", "Sous la houlette du personnage, il faut beaucoup moins longtemps pour assembler et déplacer un engin de siège. ", "Responsable du siège", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "représentation-théâtrale", "Quand le personnage dépense une action rapide pour faire un test de représentation, il dégage une aura qui émerveille les gens. Il gagne un bonus de +2 au test et un autre bonus de +2 aux jets d’attaque et aux tests de manœuvre offensive jusqu’à la fin de son prochain tour.", "Le personnage est un talentueux combattant, c’est aussi évident pour ses ennemis que pour les spectateurs. ", "Représentation théâtrale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "reprends-toi", "Quand le personnage est adjacent à un ou plusieurs alliés qui disposent aussi de ce don, il gagne un bonus de +1 aux jets de sauvegarde pour chaque allié présent (+4 au maximum).", "Le personnage soutient ses alliés et les aide à se débarrasser des effets gênants. ", "Reprends-toi", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "repositionnement-rapide", "Le personnage peut, à son tour, faire une manœuvre offensive de repositionnement au lieu de faire une attaque de corps à corps. Cette manœuvre doit remplacer l’attaque avec le meilleur bonus.", "L’adversaire du personnage se comporte comme un partenaire de danse involontaire et le suit alors qu’il mène le combat. ", "Repositionnement rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "renvoi-de-linsaisissable", "Quand le personnage évite des dégâts grâce au pouvoir cible insaisissable, il peut dépenser une action immédiate et un point de ki pour renvoyer cette attaque à son adversaire ou un autre ennemi adjacent à eux deux. Cette attaque utilise le même jet d’attaque que l’originale mais vise la cible choisie par le personnage.", "Le personnage peut renvoyer une attaque contre son assaillant ou un autre ennemi adjacent. ", "Renvoi de l'insaisissable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "roulade", "Si le personnage est victime d’un croc-en-jambe, il peut dépenser une action immédiate pour se déplacer de 1,50 m (1 c) sans provoquer d’attaque d’opportunité. Ce n’est pas un pas de placement de 1,50 m (1 c). Il tombe à terre après ce déplacement.", "Le personnage a appris à rouler loin de son adversaire quand il est victime d’un croc-en-jambe. ", "Roulade", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "renversement-de-lenfant-de-la-terre", "Le personnage peut renverser une créature de sous-type géant de taille TG au maximum. Tant qu’il utilise l’École de l’enfant de la terre, il ajoute son bonus de Sagesse aux tests de manœuvres offensives pour faire tomber une créature de sous-type géant. Il ajoute ce même bonus aux jets d’attaque pour confirmer un coup critique contre ces créatures.", "Le personnage maîtrise si bien le mélange entre équilibre et élan qu’il parvient à faire tomber les géants à mains nues. ", "Renversement de l'enfant de la terre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "rechargement-rapide", "Le temps de rechargement de l’arme choisie passe à une action libre (pour une arbalète de poing ou légère), à une action de mouvement (pour une arbalète lourde ou une arme à feu à une main) ou à une action simple (pour une arme à feu à deux mains). Le chargement de l’arbalète ou de l’arme à feu provoque toujours une attaque d’opportunité. Si le personnage applique ce don à une arbalète de poing ou légère et qu’il fait une attaque à outrance, il peut tirer autant de fois qu’il a d’attaques, comme avec un arc.\r\nNormal. Le personnage qui ne possède pas ce don a besoin d’une action de mouvement pour recharger une arbalète de poing ou légère, d’une action simple pour une arme à feu à une main et d’une action complexe pour une arbalète lourde ou une arme à feu à deux mains.", "Le personnage choisit une arbalète (de poing, légère ou lourde) ou un type d’arme à feu à une ou deux mains. Il doit savoir manier cette arme et peut la recharger plus rapidement. ", "Rechargement rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "prêt-de-canalisation", "Le personnage peut utiliser une action simple et une utilisation quotidienne de la canalisation d’énergie pour créer une réserve d’énergie positive chez un allié. Elle contient le même nombre et le même type de dés de canalisation que ce que le personnage utilise normalement. Elle persiste 1 minute. L’allié qui dispose de cette réserve peut dépenser une action immédiate pour lancer les dés de la réserve et récupérer un nombre de points de vie égal au résultat. Si l’allié qui dispose de la réserve tombe à moins de 0 point de vie, elle s’active automatiquement et le soigne sans qu’il ait besoin de faire une action.", "Le personnage peut imprégner un tiers de son énergie guérisseuse afin qu’il l’utilise au meilleur moment. ", "Prêt de canalisation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "précision-sournoise", "Quand le personnage réussit une seconde attaque sournoise sur un même adversaire au cours du même tour, il peut dépenser une action rapide pour lui appliquer les effets d’un don de critique de sa connaissance.", "Le personnage connaît les points faibles de ses ennemis et les exploite. ", "Précision sournoise", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "première-volée", "Quand le personnage inflige des dégâts avec une arme à distance, il gagne un bonus de circonstances de +4 au prochain jet d’attaque au corps à corps contre le même adversaire. Cette attaque doit se produire avant la fin de son tour suivant.", "Le personnage désoriente ses adversaires avec un assaut à distance et les rend plus vulnérables à ses attaques au corps à corps. ", "Première volée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "posture-de-lefrit", "Le personnage a droit à une utilisation quotidienne de plus du Poing élémentaire. Quand il utilise l’École de l’efrit, il gagne une résistance au feu égale à son bonus de base à l’attaque ou à son niveau de moine (choisir le plus élevé). Il perd cette résistance quand il perd son bonus de Dextérité à la CA Les créatures qui subissent des dégâts de feu à cause du Poing élémentaire doivent réussir un jet de Réflexes (DD 10 + 1/2 niveau de personnage + modificateur de Sagesse) pour ne pas prendre feu.", "Le personnage fait appel aux esprits brûlants de la flamme vivante. Ils lui permettent de manipuler le feu pour se protéger et immoler ses adversaires. ", "Posture de l'efrit", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "poing-du-croisé", "Quand le personnage se bat à mains nues et qu’il touche une créature susceptible d’être blessée par l’imposition des mains ou le contact corrupteur, il peut dépenser une action rapide et une utilisation de ce pouvoir pour infliger les dégâts associés, comme avec une attaque de corps à corps ordinaire. En cas de coup critique, ces dégâts supplémentaires ne sont pas multipliés.", "Le personnage déverse de l’énergie divine sur l’ennemi qu’il frappe. ", "Poing du croisé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "renaissance-canalisée", "Par une action complexe qui provoque des attaques d’opportunité, le personnage peut dépenser trois utilisations de la canalisation pour ramener une créature morte à la vie, comme avec un sort de souffle de vie.", "Le personnage peut dépenser une grande quantité de son pouvoir de canalisation pour inverser la mort. ", "Renaissance canalisée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tireur-couché", "Tant que le personnage est à terre, il gagne un bonus de +1 aux jets d’attaque quand il utilise une arbalète ou une arme à feu pour laquelle il dispose du don Arme de prédilection. De plus, quand il utilise une arbalète pour tirer en utilisant la compétence Discrétion, il subit seulement un malus de -10 au test et non de -20.", "Quand le personnage est couché, il profite du sol pour améliorer sa visée avec une arbalète ou une arme à feu. ", "Tireur couché", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tireur-à-la-fronde-couché", "Le personnage peut utiliser une fronde pour faire des attaques à distance même s’il est couché au sol.", "Le personnage sait tirer à la fronde de biais, ce qui lui permet de lancer des pierres même s’il est à terre. ", "Tireur à la fronde couché", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tireur-à-larme-de-siège", "Le personnage ne subit pas de malus de taille quand il vise avec une arme de siège directe plus grande que lui. S’il rate sa cible avec une arme de siège indirecte, il dévie d’une case par facteur de portée.", "Le personnage n’a aucun mal à viser avec une arme de siège d’une taille disproportionnée. ", "Tireur à l'arme de siège", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-tigre", "Quand le personnage utilise l’École du tigre, il gagne un bonus de +2 au DMD contre les manœuvres de bousculade, de renversement et de croc-en-jambe. Il peut aussi infliger des dégâts tranchants avec les attaques à mains nues. Quand il réussit un coup critique avec une attaque à mains nues tranchante, son adversaire subit 1d4 points de dégâts de saignement au début de ses deux prochains tours.", "Le style de combat à mains nues du personnage prend pour modèle la force et la férocité du tigre.", "École du tigre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "épée-et-pistolet", "Quand le personnage utilise le Combat à deux armes avec une arme de mêlée et une arbalète ou une arme à feu, il ne provoque pas d’attaques d’opportunité de la part des adversaires qu’il menace de son arme de mêlée quand il utilise l’arme à feu ou l’arbalète.", "Le personnage mêle sans mal le combat au corps à corps et à distance.", "Épée et pistolet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "équipe-de-pickpockets", "Quand un allié qui possède aussi ce don réussit un test de Bluff pour faire une feinte, le personnage peut dépenser une action immédiate pour faire un test d’Escamotage et voler sa bourse, avec un bonus de +4 au test. Il doit être adjacent à la cible.", "Le personnage distrait une cible grâce à son bavardage amical pendant que son partenaire la dépouille.", "Équipe de pickpockets", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "étourdir-limmobile", "Quand le personnage immobilise un adversaire, il peut utiliser une action rapide pour faire une tentative de Coup étourdissant contre lui.", "Le personnage met temporairement hors de combat un adversaire immobilisé.", "Étourdir limmobile", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "étreinte-de-la-tortue-alligator", "Quand le personnage utilise l’École de la tortue alligator, le bonus de bouclier que l’école lui donne à la CA s’applique aussi au DMD et à la CA au contact. Quand un adversaire le rate lors d’une attaque au corps à corps, il peut dépenser une action immédiate pour tenter une manœuvre de lutte contre lui avec un malus de -2.", "Le style de combat du personnage lui permet de transformer les attaques de ses adversaires en opportunités.", "Étreinte de la tortue alligator", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "étreinte-fatale", "Le personnage gagne constriction et étreinte.\r\nSes attaques de constriction infligent autant de dégâts que ses coups à mains nues ou son attaque primaire avec des armes naturelles. De plus, il peut entamer une lutte contre des adversaires de sa taille ou moins avant de faire une constriction.", "Les anneaux du personnage sont particulièrement dangereux et lui permettent de faire une constriction contre des adversaires de sa taille ou plus petits.", "Étreinte fatale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "étreinte-étouffante", "Quand le personnage agrippe un ennemi d’une catégorie de taille de plus que lui au maximum, il peut tenter une manœuvre offensive de lutte avec un malus de -5. S’il réussit, il immobilise son adversaire et l’étouffe. Il maintient l’étreinte tant qu’il parvient à poursuivre la lutte.\r\nUne créature victime d’une étreinte étouffante ne peut plus parler ni respirer, elle est donc incapable de lancer un sort à composante verbale. Elle doit retenir son souffle sinon, elle suffoque. Les créatures qui ne respirent pas, qui sont immunisées contre le saignement ou encore contre les coups critiques sont aussi immunisées contre l’Étreinte étouffante.\r\nSi la lutte se termine, l’étreinte aussi.", "Quand le personnage agrippe un adversaire, il peut bloquer l’arrivée d’air et le flux sanguin chez sa victime.", "Étreinte étouffante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "éventration-de-limmobile", "Quand le personnage immobilise un adversaire et réussit une manœuvre de lutte pour lui infliger des dégâts à mains nues ou avec une arme à une main, l’adversaire reçoit des dégâts de saignement égaux à ses dés de dégâts à mains nues ou avec son arme. Une créature immunisée contre les coups critiques l’est aussi contre les effets de ce don.", "Quand le personnage blesse un adversaire immobilisé, il déchire ses chairs.", "Éventration de l'immobile", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "absorption-rageuse", "Lorsqu’il est en rage sanguine, qu’il réussit un jet de sauvegarde contre un sort profane provoquant des dégâts qui le cible ou l’inclut dans sa zone d’effet, et qu’il ne reçoit aucun dégât de ce sort, le personnage peut absorber une partie de son énergie magique pour alimenter sa rage sanguine. Il regagne 1 round de rage sanguine par tranche de 2 niveaux du sort contre lequel il a réussi son jet de sauvegarde.\r\nIl ne peut pas utiliser ce don pour regagner plus de rounds de rage sanguine que son nombre maximal de rounds par jour.", "Le personnage peut absorber l’énergie magique hostile pour réalimenter sa rage sanguine continue.", "Absorption rageuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "acolyte-de-la-nature", null, "Le personnage s’est entraîné à canaliser son énergie magique de façon à ne pas endommager le monde naturel qui l’entoure.", "Acolyte de la Nature", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "acrobate-des-corniches", "Le personnage peut se déplacer à sa vitesse maximale quand il utilise Acrobaties pour garder l’équilibre sur une surface étroite. De plus, il bénéficie d’un bonus de +4 aux tests d’Escalade pour se rattraper ou rattraper une autre créature en cas de chute. Il gagne aussi un bonus de +4 aux jets de sauvegarde contre les effets susceptibles de le faire tomber (comme un tremblement de terre). Ce bonus ne s’applique pas au DMD contre les tentatives de bousculade ou de croc-en-jambe.", "Le personnage se déplace sur les corniches les plus étroites avec l’agilité d’une chèvre de montagne.", "Acrobate des corniches", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "acrobate-rusé", "Quand il utilise Acrobaties pour traverser la zone d’un adversaire ou l’espace qu’il menace sans provoquer d’attaque d’opportunité de la part de cet adversaire, le personnage gagne un bonus de circonstances de +2 à son prochain jet d’attaque au corps à corps contre cet adversaire, à partir du moment où il porte son attaque avant le début de son tour suivant.", "Les prouesses acrobatiques du personnage distraient ses ennemis.", "Acrobate rusé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "adepte-de-la-discipline", "Le personnage gagne un bonus de +1 aux tests de niveau de lanceur de sorts et de concentration quand il lance un sort issu de sa discipline psychique. Il gagne aussi un bonus d’intuition de +1 aux jets de sauvegarde contre les sorts de sa discipline.", "Le personnage a étudié très sérieusement la magie mentale, ce qui l’aide à maîtriser sa discipline.", "Adepte de la discipline", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "adepte-du-chakra", "La réserve de ki de feu-serpent passe à 4 points. Quand le personnage maintient ses chakras par une action rapide sans en ouvrir de nouveaux, il peut faire un jet de Vigueur ou de Volonté au lieu des deux.\r\nDe plus, quand le personnage arrête de dépenser du ki pour maintenir ses chakras, l’énergie kundalini s’attarde dans son corps. Lors du premier round où le personnage cesse de dépenser du ki pour maintenir ses chakras, le plus élevé se ferme et le personnage peut dépenser une action rapide pour utiliser l’un des pouvoirs associés aux chakras encore ouverts. Au round suivant, tous ses chakras se ferment et le personnage doit dépenser un point de ki et une action rapide pour recommencer à partir de son chakra racine.", "Le corps et l’âme du personnage se sont accoutumés à l’énergie kundalini.", "Adepte du chakra", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "adroit-et-rapide", "Le personnage gagne un bonus racial de +2 en Acrobaties et en Escalade.", "Le personnage est rapide et prudent.", "Adroit et rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "affinité-nécromantique", null, "Le personnage a été si longtemps exposé aux énergies nécromantiques qu’il leur résiste en partie. ", "Affinité nécromantique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "agitation-du-sang", "Lorsqu’il canalise l’énergie, plutôt que de créer l’effet normal, l’orque peut donner aux autres orques un bonus à leurs dégâts d’arme et à leurs jets de confirmation de critique jusqu’à son prochain tour. Ce bonus est égal au nombre de dés que son énergie canalisée soigne ou inflige normalement. Sa canalisation agit normalement sur les autres créatures de la zone.", "L’orque peut déchaîner une vague d’énergie faisant basculer ses semblables dans la frénésie.", "Agitation du sang", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "amélioration-des-créatures-convoquées", "Toutes les créatures que le personnage convoque grâce à un sort de convocation bénéficient d’un bonus d’altération de +4 en Force et en Constitution pendant la durée du sort qui a permis de les convoquer.", "Les créatures invoquées par le personnage sont plus puissantes et plus résistantes.", "Amélioration des créatures convoquées", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "amélioration-des-créatures-convoquées-(mythique)", "Une créature que le personnage convoque par le biais d’un sort d’invocation est considérée comme mythique dans le cadre de ses interactions avec d’autres créatures mythiques. Elle ne gagne aucune aptitude ou pouvoir mythique, mais elle peut être affectée par des sorts et des aptitudes mythiques en considérant qu’elle est une créature mythique de grade 1. De plus, si la créature convoquée possède une réduction des dégâts, celle-ci change et devient RD/épique.", "Les sorts d’invocation lancés par le personnage ne convoquent plus uniquement des animaux ordinaires, mais également des créatures mythiques.", "Amélioration des créatures convoquées (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "allié-naturel-spontané", "Le personnage peut choisir de « perdre » un sort préparé afin de lancer n’importe quel sort de convocation d’alliés naturels de niveau égal ou inférieur plutôt que de lancer un sort de soins ou de blessure comme il le ferait normalement.", "Plutôt que de lancer un sort de soins, le personnage peut lancer convocation d’allié naturel.", "Allié naturel spontané", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "alchimie-inspirée", "Le personnage peut recréer un extrait qu’il a consommé au cours de la dernière heure. Pour cela, il doit prendre 10 minutes et dépenser un nombre d’utilisations d’inspiration égal au niveau de la formule de l’extrait. Quand il recrée un extrait de cette manière, celui-ci n’est pas décompté du nombre d’extraits qu’il peut préparer chaque jour.", "Avec un peu d’inspiration et d’huile de coude, le personnage peut recréer un extrait consommé au besoin.", "Alchimie inspirée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aisance", "Quand le personnage se déplace, il peut franchir 1,5 m (1 case) de terrain difficile par round, comme s’il se trouvait sur un terrain ordinaire. Ce don lui permet donc de faire un pas de placement de 1,5 m (1 case) sur un terrain difficile.", "Le personnage franchit facilement les obstacles.", "Aisance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aisance-(mythique)", "Pour chaque tranche de deux grades, le personnage peut se déplacer sur 1,50 mètre de terrain difficile par round comme si c’était un terrain normal (1,50 mètre au minimum). Cet effet se cumule avec ceux fournis par Aisance et Déplacement acrobatique.", "Le personnage franchit les obstacles avec grâce et aisance et se déplace comme s’il n’y en avait pas.", "Aisance (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-singe", "Le personnage ajoute sa Sagesse aux tests d’Acrobaties . Quand il utilise l’École du singe, il ne subit pas de malus aux jets d’attaque ni à la CA quand il est à terre.\r\nDe plus, quand il est en position allongée, il peut ramper et se relever sans provoquer d’attaque d’opportunité. S’il réussit un test d’Acrobaties DD 20, il peut se relever par une action rapide.", "Le personnage a un style de combat à mains nues agile et imprévisible, plein de roulades et de petits bonds.", "École du singe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ailes-rallongées", "La vitesse de vol du strix passe à 18 mètres (moyenne). Le strix ignore le test de Vol qu’impose le trait ailes tailladées pour prendre son envol.", "Le strix renforce ses ailes estropiées.", "Ailes rallongées", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ailes-d’ange", "L’aasimar gagne une paire d’ailes aux plumes étincelantes qui lui accordent une vitesse de vol de 9 mètres (manoeuvrabilité moyenne) s’il porte une armure légère ou non-encombrante, de 6 mètres (manoeuvrabilité médiocre) s’il porte une armure ou une charge intermédiaire ou lourde. Il considère Vol comme une compétence de classe.", "Des ailes à plumes poussent dans le dos du personnage.", "Ailes d’ange", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ailes-de-vent", "Le bonus du sylphe à ses jets de sauvegarde contre les effets du registre air ou électricité et contre les effets infligeant des dégâts d’électricité passe à +4. En outre, le sylphe gagne une vitesse de vol surnaturelle égale à sa vitesse de base (bonne manœuvrabilité). Il ne peut voler avec cette capacité que s’il porte une armure légère ou pas d’armure.", "Les vents portent le sylphe, l’emmenant où il veut aller.", "Ailes de vent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ailes-de-tengu", "Une fois par jour, le tengu peut se faire pousser une paire d’ailes de corbeau noir géant lui accordant une vitesse de vol de 9 mètres avec une manoeuvrabilité moyenne. En outre, ce pouvoir magique fonctionne comme Forme bestiale I (bien que le tengu ne bénéficie d’aucun des autres avantages du sort) avec un niveau de lanceur de sorts égal à son niveau.", "Le tengu se fait pousser des ailes qui lui permettent de voler.", "Ailes de tengu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ailes-dange", "L’aasimar gagne une paire d’ailes aux plumes étincelantes qui lui accordent une vitesse de vol de 9 mètres (manoeuvrabilité moyenne) s’il porte une armure légère ou non-encombrante, de 6 mètres (manoeuvrabilité médiocre) s’il porte une armure ou une charge intermédiaire ou lourde. Il considère Vol comme une compétence de classe.", "Des ailes à plumes poussent dans le dos du personnage.", "Ailes d'ange", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aide-exceptionnelle", "Quand vous utilisez l'action aider autrui pour donner un bonus au test de compétence d'un allié, le bonus passe à +4 au lieu de +2.", "Vous êtes toujours prêt à donner un coup de pouce à votre allié", "Aide exceptionnelle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "agression-de-lombre", null, "Le personnage peut puiser dans les énergies du plan de l’Ombre pour solidifier momentanément l’un de ses sorts d’illusion (chimère).", "Agression de l'ombre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ailes-métalliques", "L’aasimar gagne deux attaques d’aile. Ce sont des attaques naturelles secondaires infligeant 1d4 points de dégâts tranchants (ou 1d3 si l’aasimar est de taille P).", "Les ailes de l’aasimar sont faites d’un métal scintillant.", "Ailes métalliques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-shaitan", "Le personnage gagne une utilisation quotidienne du Poing élémentaire de plus. Quand il utilise l’École du shaitan et le Poing élémentaire pour infliger des dégâts d’acide, il gagne un bonus au jet de dégâts d’acide égal à son modificateur de Sagesse. De plus, s’il rate son attaque de Poing élémentaire alors qu’il l’utilise pour infliger des dégâts d’acide, il fait tout de même 1d6 points de dégâts d’acide à la cible.", "Le personnage frappe avec les forces caustiques issues des profondeurs de la terre.", "École du shaitan", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-serpent", "Le personnage gagne un bonus de +2 aux tests de Psychologie et peut infliger des dégâts perforants avec ses attaques à mains nues. Quand il utilise l’École du serpent et qu’un adversaire l’attaque en mêlée ou à distance, il peut dépenser une action immédiate pour faire un test de Psychologie et substituer le résultat du test à sa CA ou sa CA au contact contre cette attaque. Pour cela, il doit être conscient de l’attaque et ne pas être pris au dépourvu.", "Le personnage observe les mouvements de son ennemi avant de traverser ses défenses.", "École du serpent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-sanglier", "Le personnage peut infliger des dégâts contondants ou tranchants avec ses attaques à mains nues. Il change de type de dégâts par une action libre. Quand il utilise cette école, il peut, une fois par round, déchiqueter son adversaire en le touchant avec deux attaques à mains nues ou plus. L’attaque inflige alors 2d6 points de dommages de plus.", "Le personnage a des dents et des ongles si acérés qu’ils déchirent les chairs de ses adversaires.", "École du sanglier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "voie-du-kirin", "Quand le personnage fait un test de Connaissances pour identifier une créature, même quand il utilise École du kirin, il peut faire 10, même s’il ne devrait pas en être capable à cause du stress ou d’une distraction. Quand il utilise École du kirin contre une créature qu’il a identifiée grâce à ce don, si elle termine son tour dans une case qu’il menace, il peut dépenser une utilisation d’attaque d’opportunité pour se déplacer sur une distance égale à 1,50 m (1 c) x modificateur d’Intelligence (1 au minimum). Il doit terminer le déplacement dans une case que menace la créature. Ce mouvement ne provoque pas d’attaque d’opportunité.", "Le personnage profite de sa connaissance de l’ennemi pour lui opposer une défense sans faille. ", "Voie du kirin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "une-fronde-pour-fléau", "Le personnage peut faire une attaque au corps à corps avec une fronde chargée d’une bille. Il utilise les statistiques normales de la fronde mais la considère comme un fléau. Le personnage ne dépense pas de munitions quand il utilise sa fronde en mêlée mais si la bille est magique ou de maître, elle perd cette propriété après le premier coup qui touche.", "Le personnage peut utiliser une fronde et sa bille pour frapper un adversaire proche. ", "Une fronde pour fléau", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tueur-supplémentaire", "Chaque jour, le personnage peut utiliser le pouvoir tueur pendant 3 rounds de plus.", "Le personnage peut utiliser ce pouvoir plus souvent. ", "Tueur supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tueur-miséricordieux", "Quand le personnage manie une arme qu’il a dotée de la propriété tueur, il peut utiliser une action rapide pour passer des dégâts létaux aux dégâts non létaux. Quand il inflige ce type de dégâts grâce à la propriété de l’arme, il ne subit pas de malus au jet d’attaque.", "Le personnage peut utiliser le pouvoir tueur pour infliger des dégâts non létaux. ", "Tueur miséricordieux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tueur-menaçant", "Le personnage peut utiliser le pouvoir de classe tueur pour imprégner son arme de la propriété menaçante au lieu de tueur. Il peut passer d’une propriété à l’autre par une action rapide. En dehors de cela, ce don fonctionne comme le pouvoir de classe tueur.", "Le personnage est encore plus dangereux quand il se bat de concert avec ses alliés contre un seul adversaire. ", "Tueur menaçant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tueur-magique", "Quand le pouvoir tueur affecte une créature, le DD des sorts du personnage augmente de +2 pour elle.", "Tant que la propriété tueur est active, les créatures qu’elle affecte ont plus de mal à résister aux sorts du personnage.", "Tueur magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "voix-discordante", "Quand le personnage utilise la représentation bardique pour créer un effet magique ou surnaturel, les alliés qui se trouvent à moins de 9 m (6 c) de lui infligent 1d6 points de dégâts sonores de plus lors d’une attaque réussie avec une arme. Ces dégâts se cumulent avec d’éventuels dégâts d’énergie dus à l’arme. Les armes à projectiles transmettent les dégâts supplémentaires à leurs munitions mais seulement si la cible se trouve à moins de 9 m (6 c).", "En chantant une note précise, le personnage fait courir des vibrations discordantes dans les armes de ses alliés. ", "Voix discordante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tueur-intimidant", "Quand le personnage utilise Démonstration alors que son arme profite de son pouvoir tueur, il gagne un bonus de +2 au test d’Intimidation autorisé par le don contre toutes les créatures affectées par tueur. Elles sont secouées tant que la propriété est active contre une créature de leur type.", "L’arme tueuse du personnage terrifie ses ennemis. ", "Tueur intimidant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tourment-de-la-mante", "Le personnage peut utiliser le Coup étourdissant une fois de plus par jour. Quand il utilise cette école, il peut faire une attaque qui coûte deux utilisations du Coup étourdissant. S’il touche, son adversaire doit réussir un jet de sauvegarde contre le Coup ou se retrouver chancelant et ébloui par la douleur jusqu’au début du prochain tour du personnage. Il est alors fatigué.", "Le personnage connaît les mystères de l’anatomie, ce qui lui permet de provoquer des douleurs handicapantes d’un simple contact. ", "Tourment de la mante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tourbillon-du-djinn", "Quand le personnage utilise l’École du djinn, il peut, par une action simple, dépenser deux Poings élémentaires pour s’entourer d’un tourbillon d’air électrifié. Les créatures adjacentes subissent les dégâts de ses coups à mains nues en plus des dégâts d’électricité du poing élémentaire et elles sont sourdes pendant 1d4 rounds.\r\nUn Jet de Vigueur (DD 10 +1/2 niveau du personnage + modificateur de Sagesse) permet de ne subir que 1/2 dégâts et annule la surdité.", "Le personnage s’entoure de la puissance des tempêtes. Il tourbillonne comme un cyclone avant de lancer une violente décharge électrique. ", "Tourbillon du djinn", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tour-de-passe-passe", "Quand le personnage réussit un test d’Acrobaties pour traverser la case d’un adversaire, il peut dépenser une action rapide pour faire un test de Bluff contre lui et faire une feinte.", "Le personnage a une chance de faire une feinte quand il parvient à se glisser derrière un adversaire. ", "Tour de passe-passe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tonnerres-jumeaux", "Une fois par round, quand le personnage manie une arme contondante dans chaque main et affronte une créature de sous-type géant, s’il touche sa cible avec son arme principale puis avec son arme secondaire, il lance deux fois les dés de la deuxième attaque et additionne les résultats avant d’ajouter ses bonus. On ne multiplie pas les dés de dégâts supplémentaires en cas de coup critique.", "Quand le personnage affronte des géants, ses puissants coups se combinent avec les talents qu’il a hérités des générations précédentes pour égaliser les chances. ", "Tonnerres jumeaux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tisseur-de-cauchemar", "Le personnage peut passer une action complexe à lancer ténèbres et faire un test d’Intimidation pour démoraliser tous les adversaires qui se trouvent dans la zone d’effet.", "Le personnage peut profiter de sa capacité à créer des ténèbres magiques pour terrifier ses ennemis. ", "Tisseur de cauchemar", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "tirs-groupés", "Quand le personnage fait une attaque à outrance pour tirer plusieurs projectiles sur un même adversaire, il comptabilise la totalité des dégâts avant d’appliquer la RD.", "Le personnage prend son temps pour viser soigneusement. Tous ses coups frappent le même point.", "Tirs groupés", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "traqueur-au-clair-de-lune", "Quand le personnage est camouflé aux yeux de son adversaire, il bénéficie d’un bonus de +2 aux jets d’attaque et de dégâts vis-à-vis de ce dernier.", "Le personnage sait comment utiliser les ombres pour dissimuler ses attaques. ", "Traqueur au clair de lune", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-du-combat-de-spectacle", "Le personnage peut faire un test de combat de spectacle par une action libre. Il sait manier toutes les armes de spectacle.", "Le personnage est passé maître des techniques et des armes utilisées dans l’arène et sur scène. ", "Maître du combat de spectacle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "zélote-du-refus-de-mourir", "Quand une créature lance le dé pour confirmer un coup critique contre le personnage, il doit lancer le dé deux fois et conserver le pire résultat.", "Seules les blessures les plus graves parviennent encore à arrêter le personnage.", "Zélote du refus de mourir", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "échange-trompeur", "Si le personnage arrive à duper son adversaire avec une feinte, au lieu de le priver de son bonus de Dextérité à la CA lors de sa prochaine attaque, il peut le pousser à attraper un objet à une main. L’adversaire doit posséder des appendices capables de tenir l’objet que tend le personnage et cet appendice doit être libre.", "Le personnage peut pousser son adversaire à attraper un objet qu’il lui tend, même en plein combat.", "Échange trompeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-marid", "Chaque jour, le personnage peut utiliser le Poing élémentaire une fois de plus. Quand il utilise l’École du marid avec le Poing élémentaire pour infliger des dégâts de froid, il gagne un bonus aux jets de dégâts de froid égal à son modificateur de Sagesse et l’allonge de ses attaques à mains nues augmente de 1,50 m (1 c).", "Le personnage invoque des vrilles d’eau glacée pour frapper ses ennemis de loin.", "École du marid", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-kirin", "Quand le personnage utilise cette école, il peut passer une action rapide à faire un test de Connaissances pour identifier une unique créature (DD 15 + FP de la créature).\r\nS’il réussit, tant qu’il utilise cette école, il gagne un bonus de +2 aux Jets de sauvegarde contre les attaques de cette créature ainsi qu’un bonus d’esquive de +2 à la CA contre ses attaques d’opportunité. Ces bonus persistent tant que le personnage utilise cette école. S’il rompt le combat avec la créature et le reprend plus tard, il peut refaire le test.", "Les connaissances et la grâce du personnage lui permettent d’exploiter les faiblesses de ses ennemis.", "École du kirin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-jann", "Quand le personnage utilise cette école, il subit un malus à la CA de seulement -1 quand il charge. De plus, s’il est pris en tenaille, ses adversaires ont un bonus de +1 seulement aux jets d’attaque.", "Le personnage est très difficile à toucher à cause de ses techniques de combat tourbillonnantes.", "École du jann", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-dragon", "Quand le personnage utilise cette école, il gagne un bonus de +2 aux Jets de sauvegarde contre les effets de sommeil, de paralysie et d’étourdissement. Il ignore le terrain difficile quand il charge, court ou bat en retraite. Il peut aussi charger à travers une case occupée par un allié. De plus, il peut ajouter 1,5 fois son bonus de Force à la première attaque à mains nues du round.", "Le personnage fait appel à l’esprit du dragon pour améliorer sa résilience, sa mobilité et sa férocité grâce à la bénédiction de ces êtres d’exception.", "École du dragon", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-du-djinn", "Le personnage peut utiliser Poing élémentaire une fois de plus par jour. Quand il utilise cette école, il doit choisir des dégâts de foudre pour le Poing élémentaire et gagne un bonus égal à son modificateur de Sagesse aux jets de dégâts d’électricité. De plus, tant qu’il utilise cette école et qu’il n’a pas utilisé tous ses Poings élémentaires, il gagne un bonus d’esquive de +2 à la CA contre les attaques d’opportunité. Il perd ce bonus s’il perd son bonus de Dextérité à la CA.", "Le personnage se déplace comme le vent, les mains entourées d’une aura de foudre.", "École du djinn", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-de-lenfant-de-la-terre", "Quand le personnage utilise cette école, son bonus d’esquive à la CA dû à l’entraînement défensif passe à +6. De plus, quand il se bat contre des créatures de sous-type Géant, il ajoute son bonus de Sagesse aux jets de dégâts à mains nues.", "Le personnage a suivi un entraînement qui en fait une cible dangereuse et insaisissable pour les géants.", "École de l'enfant de la terre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "à-terre-à-cheval", "Quand le personnage réussit un test d’Acrobaties pour sauter grâce à son pouvoir de bond du lancier, il peut remonter en selle par une action rapide.", "L’expérience équestre du personnage lui permet de mettre pied à terre et de remonter en selle à toute allure.", "À terre à cheval", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-de-lefrit", "Le personnage a droit à une utilisation quotidienne de plus du Poing élémentaire. Quand il utilise l’École de l’efrit et le Poing élémentaire pour infliger des dégâts de feu, il gagne un bonus égal à son modificateur de Sagesse aux jets de dégâts de feu. De plus, si le Poing élémentaire rate sa cible alors qu’il devait infliger des dégâts de feu, la cible reçoit tout de même 1d6 points de dégâts de feu.", "Le personnage maîtrise si bien la puissance imprévisible des flammes qu’il peut asséner des coups enflammés qui brûlent ses ennemis, même s’il ne parvient pas à les toucher.", "École de l'efrit", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-de-la-panthère", "Quand le personnage utilise l’École de la panthère et qu’un adversaire fait une attaque d’opportunité contre lui parce qu’il se déplace dans une case menacée, il peut dépenser une action rapide pour riposter à mains nues. Son attaque se produit après l’attaque d’opportunité.", "Le personnage riposte quand un ennemi l’attaque alors qu’il se déplace.", "École de la panthère", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-de-la-mante", "Le personnage peut utiliser le Coup étourdissant une fois de plus par jour. Quand il utilise cette école, il gagne un bonus de +2 au DD des effets du Coup.", "Le personnage vise les points vitaux avec précision et handicape ses ennemis.", "École de la mante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-de-la-grue", "Le personnage subit seulement un malus de -2 aux jets d’attaque quand il se bat sur la défensive. Quand il utilise cette école et se bat sur la défensive ou qu’il utilise la défense totale, il gagne un bonus d’esquive supplémentaire de +1 à la CA.", "Le personnage utilise des techniques de combat à mains nues qui mêlent l’assurance à une défense gracieuse.", "École de la grue", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "éclats-darme", "Quand le personnage utilise une arme de corps à corps ou de jet fragile ou similaire et qu’il touche un adversaire, il peut briser son arme pour infliger 1d4 points de saignement à l’ennemi.", "Le personnage sait tirer profit de la fragilité de ses armes et laisse des fragments dans les blessures qu’il inflige.", "Éclats d'arme", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "éclat-du-singe", "Quand le personnage utilise l’École du singe, s’il réussit un Coup étourdissant, il inflige les effets normaux et peut, en plus, dépenser une action libre pour entrer dans une case adjacente à la sienne qui fait partie de celle(s) occupée(s) par son adversaire. Ce déplacement ne provoque pas d’attaque d’opportunité. Tant que le personnage se trouve dans la case de son adversaire, il gagne un bonus d’esquive de +4 à la CA et un bonus de +4 aux jets d’attaque au corps à corps contre lui. Si rien ne l’en empêche, l’adversaire peut s’éloigner du personnage mais il provoque alors une attaque d’opportunité de la part de ce dernier, même si le déplacement choisi devrait normalement l’en protéger.", "Le personnage profite de ses capacités d’acrobate et saisit toutes les opportunités de causer des ravages.", "Éclat du singe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "échappatoire", "Si un allié qui possède ce don traverse la case du personnage ou une case adjacente, il ne provoque pas d’attaques d’opportunité.", "Le personnage est entraîné à surveiller les arrières de ses alliés et à les couvrir lors d’une retraite tactique.", "Échappatoire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "école-de-la-tortue-alligator", "Quand le personnage utilise l’École de la tortue alligator et qu’il a au moins une main libre, il gagne un bonus de bouclier de +1 à la CA.", "Le style de combat à mains nues du personnage est si rapide qu’il lui permet de dévier les coups.", "École de la tortue alligator", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-des-écoles-de-combat", "Le personnage peut changer d’école par une action libre. Il choisit une école en début de combat et commence l’affrontement avec, même lors du round de surprise.", "Le personnage peut changer d’école de combat et les combiner pour améliorer leurs effets. ", "Maître des écoles de combat", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-des-tonnerres-jumeaux", "Quand le personnage inflige des dégâts supplémentaires grâce au don Tonnerres jumeaux, son adversaire est secoué pendant 1 round. L’adversaire doit aussi faire un jet de Vigueur (DD 10 + 1/2 niveau + modificateur de Force). S’il échoue, il est chancelant pendant 1 round. Si le personnage utilise ce don pour faire chanceler un adversaire déjà chancelant, il l’hébète à la place. De même, il étourdit un adversaire déjà hébété.", "Le personnage peut soumettre un puissant géant grâce à ses frappes jumelles tonitruantes.", "Maître des tonnerres jumeaux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-des-chevaux", "Le personnage utilise son niveau total pour déterminer son niveau de druide effectif et connaître les pouvoirs et aptitudes de sa monture.", "Le personnage mélange des techniques de monte issues de plusieurs traditions pour obtenir une technique de combat montée sans faille. ", "Maître des chevaux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combat-chorégraphié", "Quand le personnage fait une attaque avec une arme pour laquelle il dispose du don Arme de prédilection, il peut faire des dégâts non létaux sans malus au jet d’attaque.", "Le personnage est un maître des combats chorégraphiés non létaux.", "Combat chorégraphié", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combattant-de-spectacle", "Le personnage peut faire un test de combat de spectacle lors de n’importe quel combat. Quand il ne s’agit pas d’un combat de spectacle, il choisit un seul don. Il gagne automatiquement tous les bonus qu’accorde le don avant de faire le test de spectacle avec un DD 20. S’il réussit, il bénéficie de la totalité des effets du don choisi.", "Le personnage considère tous les combats comme un spectacle et fait preuve de panache et d’un art de la mise en scène consommé.", "Combattant de spectacle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "conducteur-de-talent", "Le personnage gagne un bonus de +4 aux tests de conduite pour le type de véhicule choisi.", "Le personnage choisit un type de véhicule (aérien, aquatique ou terrestre) et améliore ses capacités de conducteur.", "Conducteur de talent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "conducteur-expérimenté", "Le personnage peut accélérer, ralentir ou tourner par une action de mouvement au lieu d’une action simple . De plus, quand il arrête le véhicule, il enlève 3 m (2 c) au jet qui détermine la distance parcourue avant l’arrêt.", "Le personnage choisit un type de véhicule. Quand il le conduit, il fait preuve d’une maîtrise hors du commun. Il le manie et l’arrête avec une précision surnaturelle.", "Conducteur expérimenté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-de-dégagement", "Quand le personnage utilise Feinte de dégagement ou Dégagement flamboyant, il peut faire une unique attaque contre un adversaire qui a cru à sa feinte. Pour cette attaque, cet adversaire est privé de son bonus de Dextérité à la CA.", "Le personnage lance une dernière attaque avant de battre rapidement en retraite.", "Coup de dégagement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-de-grâce-de-lenragé", "Le personnage gagne 1 round de rage quotidien supplémentaire à chaque fois qu’il fait tomber un adversaire à -1 point de vie ou moins alors qu’il est enragé. Si l’attaque était, en plus, un coup critique, il gagne un autre round de plus. Il perd ces rounds de rage supplémentaires quand il se repose pour renouveler son total de rounds quotidiens.", "Chaque coup de grâce ravive la vitalité du personnage et alimente sa rage.", "Coup de grâce de l’enragé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-déloyal", "Quand le personnage réussit un test de Diplomatie pour modifier l’attitude d’une créature, il peut dégainer et faire une unique attaque de corps à corps contre elle, par une action immédiate. S’il est parvenu à rendre sa cible amicale ou serviable, l’attaque la prend au dépourvu. Si elle survit, elle subit un malus de -2 aux tests d’initiative pour tout le combat. La créature devient hostile après l’attaque.", "Le personnage peut charmer les gens pour qu’ils baissent leur garde et qu’il puisse les attaquer plus facilement.", "Coup déloyal", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-déstabilisant", "Si le personnage utilise une action d’attaque pour faire une unique attaque au corps à corps avec son meilleur bonus de base à l’attaque avec Attaque en puissance et qu’il touche son ennemi, il peut dépenser une action rapide pour faire une manœuvre de croc-en-jambe contre lui.", "Le personnage met toutes ses forces dans un coup dévastateur et essaye d’écraser son adversaire au sol.", "Coup déstabilisant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-final-mortel", "Quand le personnage touche un adversaire avec une attaque au corps à corps et le réduit à -1 point de vie ou moins, il peut l’obliger à faire un jet de Vigueur (DD15 + dégâts de l’attaque). S’il échoue, il meurt.", "Les adversaires du personnage ne font pas que tomber sous ses coups, ils en meurent sur-le-champ.", "Coup final mortel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-final-sanglant", "Quand le personnage utilise cette attaque, il doit manier une arme pour laquelle il dispose du don Arme de prédilection et fait une attaque unique, avec son meilleur bonus de base. S’il fait tomber sa cible en dessous de 0 point de vie, il peut dépenser une action rapide pour faire un test d’Intimidation et démoraliser tous les adversaires qui se trouvent dans les 9 m (6 c) et qui ont vu l’attaque.", "Le personnage puise dans ses réserves de sauvagerie pour faire preuve de créativité et tuer ses adversaires de manière aussi sanglante que terrifiante, afin d’intimider les survivants.", "Coup final sanglant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-final-violent", "Quand le personnage est enragé et qu’il utilise la Frappe décisive, il n’est pas obligé de lancer les dés de dégâts, il peut infliger le maximum de dégâts possible mais dans ce cas, sa rage se termine et il est fatigué (même si, normalement, il ne devrait pas).", "Pour écraser son ennemi, le personnage concentre toute sa rage dans un puissant coup.", "Coup final violent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-écrasant", "Le personnage peut faire une tentative de Coup étourdissant par une action complexe. S’il réussit, au lieu d’étourdir sa cible, il réduit sa CA d’un montant égal à son modificateur de Sagesse pendant 1 minute. Ce malus ne se cumule pas avec les autres malus dus au Coup écrasant.", "Les coups du personnage sont tellement concentrés qu’ils traversent les défenses de l’ennemi.", "Coup écrasant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-de-dissipation", "Si le personnage a préparé dissipation de la magie ou s’il peut le lancer spontanément, il peut dépenser une action rapide pour le lancer de façon ciblée sur un adversaire contre lequel il a réussi un coup critique.", "Les coups du personnage s’attaquent à la forme physique et magique de ses ennemis.", "Critique de dissipation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-dempalement", "Quand le personnage réussit un coup critique avec l’arme perforante de corps à corps choisie, il peut empaler son ennemi sur son arme. Tant qu’il est empalé, à chaque début de tour, il subit des dégâts égaux aux dés de dégâts de l’arme plus tout dé de dégâts supplémentaire dus à ses propriétés spéciales. Le personnage peut retirer son arme par une action immédiate . Si l’adversaire se met hors de portée, il doit utiliser une action libre pour lâcher son arme ou la retirer du corps ennemi. L’adversaire peut également dépenser une action de mouvement pour retirer l’arme. Quand l’arme sort du corps de l’ennemi, celui-ci reçoit les mêmes dégâts que s’il venait de commencer son tour empalé sur elle. Tant que l’adversaire est empalé sur son arme, le personnage ne peut pas s’en servir pour attaquer et il est obligé de la tenir fermement.", "Les coups critiques du personnage transpercent ses adversaires.", "Critique d'empalement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "croc-du-serpent", "Quand le personnage utilise l’École du serpent et qu’un adversaire le rate, il peut faire une attaque à mains nues contre lui, comme attaque d’opportunité. S’il touche, il peut dépenser une action immédiate pour faire une seconde attaque à mains nues contre lui.", "Le personnage frappe les adversaires qui baissent leur garde.", "Croc du serpent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-darmes-à-feu", "Si le personnage a accès à une trousse d’armurier, il peut créer ou restaurer des armes à feu, fabriquer des balles et mélanger de la poudre noire pour toutes les armes à feu.\r\nPour cela, il n’a pas besoin de faire de test d’Artisanat.\r\n* Fabriquer une arme à feu. Le personnage fabrique toutes les armes à feu rudimentaires pour la moitié de leur prix habituel en matériaux bruts. Si le MJ est d’accord, il peut également fabriquer des armes à feu évoluées pour la moitié de leur prix habituel en matériaux bruts. Cette méthode de fabrication demande une journée de travail par tranche de 1.000 po du prix de l’arme (1 jour au minimum).\r\n* Fabriquer des munitions. Le personnage fabrique des balles, des plombs et de la poudre noire pour 10% (en composantes brutes) du prix habituel. S’il possède au moins 1 rang en Artisanat (alchimie), il peut fabriquer une cartouche alchimique pour un coût en composantes brutes égal à la moitié de son prix. Si le MJ est d’accord, il peut fabriquer des cartouches alchimiques métalliques pour un coût en composantes brutes égal à la moitié de leur prix. Cette méthode de fabrication demande une journée de travail par tranche de 1 000 po de munition (1 jour au minimum).\r\n* Restaurer une arme à feu brisée. Chaque jour, le personnage peut utiliser ce don pour consacrer une heure à la réparation d’une arme à feu brisée. Il peut profiter d’une période de repos pour réparer l’arme.", "Le personnage sait réparer et restaurer les armes à feu.", "Création d'armes à feu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "danse-de-la-moquerie", "Quand le personnage utilise une action rapide pour faire un test de combat de spectacle, il peut se déplacer de 1,50 m (1 c) avant de faire le test, sans provoquer d’attaque d’opportunité, ou se déplacer à sa vitesse en provoquant une attaque d’opportunité. Il ne peut pas terminer ce déplacement dans une case où il menace un ennemi. S’il se déplace d’au moins 1,50 m (1 c), il gagne un bonus de +2 au test de combat de spectacle.", "Le personnage exécute une petite danse pour se moquer de son adversaire et divertir la foule.", "Danse de la moquerie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "démonstration-féroce", "Quand le personnage utilise une action rapide pour faire un test de combat de spectacle, il gagne un bonus de +2 au test et un bonus de +1d6 aux jets de dégâts jusqu’à la fin de son prochain tour. Ce ne sont pas des dégâts de précision.", "Le personnage pousse un rugissement triomphal qui ravive sa férocité.", "Démonstration féroce", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "démonstration-du-héros", "Quand le personnage consacre une action rapide à un test de combat de spectacle, il présente une arme pour laquelle il dispose d’Arme de prédilection lors d’une courte démonstration triomphale. Il gagne un bonus de +2 au test de combat de spectacle et il peut faire un test d’Intimidation pour démoraliser les adversaires qui assistent à la démonstration et se trouvent à moins de 9 m (6 c).", "Dans un geste théâtral, le personnage brandit ses armes devant la foule, ce qui émerveille les spectateurs et démoralise les ennemis.", "Démonstration du héros", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "démonstration-de-maître", "Le personnage choisit deux dons de spectacle qu’il maîtrise. Quand il fait un test de combat de spectacle, il profite des avantages de ces deux dons mais il gagne seulement un bonus de +2 au test de combat de spectacle.", "Le personnage donne une représentation spéciale pour sa victoire et met la foule en transe.", "Démonstration de maître", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "déluge-du-croisé", "Le personnage peut utiliser l’arme de prédilection de son dieu comme si c’était une arme de moine.", "Le personnage sait intégrer l’arme de prédilection de son dieu dans sa pratique des arts martiaux.", "Déluge du croisé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "déluge-des-tonnerres-jumeaux", "Le personnage peut faire un croc-en-jambe à une créature de sous-type géant de taille TG au maximum. Il gagne un bonus de +2 aux jets de dégâts contre les créatures de sous-type géant. De plus, à chaque fois qu’il touche une créature de ce type avec son arme principale puis secondaire, il inflige les dégâts supplémentaires de Tonnerres jumeaux.", "Les frappes jumelles du personnage sont encore plus dangereuses pour les géants.", "Déluge des tonnerres jumeaux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dégagement-flamboyant", "Le personnage dépense une action simple pour faire un test de Bluff contre tous les adversaires qui le menacent actuellement. S’il réussit contre au moins l’un d’eux, il peut se déplacer à sa vitesse sans provoquer d’attaque d’opportunité de la part des ennemis qui ont cru à la feinte.", "Le personnage distrait ses adversaires pour battre rapidement en retraite.", "Dégagement flamboyant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coincer", "Quand un adversaire que menace le personnage fait un pas de 1,50 m (1 c) ou utilise l’action de repli, il provoque une attaque d’opportunité de la part du personnage. Si l’attaque touche, elle n’inflige pas de dégâts mais empêche la cible de faire l’action de mouvement qui lui a permis le pas de 1,50 m (1 c) ou le repli. La cible ne peut donc pas se déplacer.", "Le personnage n’a aucun mal à empêcher les ennemis de fuir.", "Coincer", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "déclenchement-manuel", "Quand le personnage déclenche manuellement un piège contre un adversaire, le piège reçoit un bonus de circonstances de +2 aux jets d’attaque ou au DD du jet de sauvegarde.", "L’instinct du personnage lui permet d’attendre le moment idéal pour déclencher un piège.", "Déclenchement manuel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dur-à-cuire-(alternatif)", null, "Le personnage tient bon et ne s’arrête pas, même quand ses points de blessure sont en dessous de son seuil de blessure.", "Dur à cuire (alternatif)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "double-tueur", "Le personnage applique la capacité tueur à la seconde arme qu’il tient. Tant que ce pouvoir est actif, au début de chaque tour, il utilise une action libre pour déterminer si la propriété s’applique à une arme ou l’autre ou les deux. À chaque fois qu’il l’applique aux deux, il dépense 2 rounds de tueur.", "Le personnage accorde la propriété tueur à deux armes.", "Double tueur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "double-croc-en-jambe", "Quand le personnage fait une manœuvre de croc-en-jambe contre un ennemi menacé par un allié qui possède aussi ce don, il lance deux fois le dé et conserve le meilleur résultat.", "Le personnage sait comment travailler en équipe pour faire un croc-en-jambe à un adversaire.", "Double croc-en-jambe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dos-à-dos", "Quand le personnage est pris en tenaille et adjacent à un allié qui possède ce don, il reçoit un bonus de circonstances de +2 à la CA contre les attaques venant des adversaires qui forment la tenaille.", "L’allié du personnage lui prête ses yeux et inversement.", "Dos à dos", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dissipation-destructrice", "Quand le personnage réussit un test de dissipation ciblée sur un adversaire, ce dernier doit faire un jet de Vigueur (avec un DD égal à celui du sort utilisé pour dissiper la magie). S’il échoue, il est étourdi jusqu’au début du prochain tour du personnage, s’il réussit, il est seulement fiévreux jusqu’au début du prochain tour du personnage.", "Quand le personnage dissipe les défenses magiques ennemies, elles s’effondrent avec des effets débilitants.", "Dissipation destructrice", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "derviche-dimensionnel", "Le personnage peut faire une attaque à outrance et activer pas chassé ou porte dimensionnelle par une action rapide. Il se téléporte alors à une distance maximale de deux fois sa vitesse (dans la limite autorisée par le pouvoir ou le sort). Il peut parcourir cette distance en plusieurs tronçons : un avant sa première attaque, plusieurs entre les attaques suivantes et un après la dernière. Ces tronçons doivent faire 1,50 m (1 c) au minimum.", "Le personnage se téléporte d’une simple pensée et massacre ses ennemis alors qu’il entre et sort de la réalité.", "Derviche dimensionnel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "déchiquetage-du-sanglier", "Le personnage peut faire un test d’Intimidation pour démoraliser son adversaire par une action de mouvement. Quand il utilise l’École du sanglier et qu’il blesse sa cible, cette dernière subit 1d6 points de dégâts de saignement une fois par round, au début de son tour. Ces dégâts persistent même si le personnage change ensuite d’école.", "Les blessures que le personnage inflige avec ses mains nues saignent et le motivent.", "Déchiquetage du sanglier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cible-dopportunité", "Quand un allié qui possède ce don fait une attaque à distance et touche un adversaire situé à moins de 9 m (6 c) du personnage, ce dernier peut utiliser une action immédiate pour faire une unique attaque à distance contre la cible. Pour cela, il doit tenir son arme à distance en main, elle doit être chargée et il doit être prêt à tirer.", "Le personnage et ses alliés bombardent leurs adversaires de projectiles.", "Cible d'opportunité", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "châtiment-canalisé-supérieur", "À son tour et avant de faire sa première attaque, le personnage utilise une action rapide pour dépenser une utilisation quotidienne de canalisation. Les dés de canalisation forment une réserve dans laquelle il peut puiser pour infliger des dégâts supplémentaires aux créatures susceptibles d’être blessées par l’énergie (c’est-à-dire les morts-vivants pour l’énergie positive et les créatures vivantes pour l’énergie négative). Avant chaque attaque, le personnage décide du nombre de dés issus de la réserve qu’il consacre aux dégâts supplémentaires. Comme d’habitude, la cible a droit à un jet de Volonté, 1/2 dégâts. On ne multiplie pas ces dégâts supplémentaires en cas de coup critique. Si le personnage rate sa cible, les dés restent dans sa réserve mais tout dé qui n’est pas utilisé avant la fin du tour est perdu.", "Le personnage imprègne son arme de la puissance de son dieu et la décharge en frappant ses ennemis.", "Châtiment canalisé supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "choc-de-lenragé", "Quand le personnage est enragé et qu’il fait une manœuvre de bousculade, il peut, par une action rapide, dépenser 1 round de rage supplémentaire pour ajouter son bonus de Constitution au test de manœuvre offensive.\r\nDe plus, s’il pousse l’adversaire dans une case occupée par une autre créature ou par un objet solide, l’adversaire et la créature ou l’objet subissent des dégâts contondants égaux aux modificateurs de Force + de Constitution du personnage.", "Le personnage utilise sa rage pour faire entrer deux ennemis en collision.", "Choc de l'enragé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "balancement-du-serpent", "Le personnage gagne un bonus de +4 au DMD contre les manœuvres de croc-en-jambe et aux tests d’Acrobaties et aux Jets de sauvegarde pour éviter de tomber.\r\nTant qu’il utilise cette école, quand il menace de faire un coup critique à mains nues, il peut faire un test de Psychologie pour le confirmer, au lieu d’un jet d’attaque. Quand il réussit un coup critique à mains nues, il peut dépenser une action immédiate pour faire un pas de 1,50 m (1 c), même s’il a déjà bougé pendant le round.", "Les oscillations sinueuses du personnage empêchent ses adversaires d’anticiper ses attaques.", "Balancement du serpent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "audace-supplémentaire", "Chaque matin, le personnage dispose de 2 points d’audace de plus et son maximum augmente aussi de 2.\r\nNormal. Si le personnage est un pistolier, chaque matin, il dispose d’une quantité d’audace égale à son modificateur de Sagesse, qui représente aussi le maximum d’audace qu’il peut accumuler. S’il dispose du don Pistolier amateur, il dispose d’un point d’audace supplémentaire au matin et son maximum est égal à son modificateur de Sagesse.", "Le personnage a plus d’audace que les pistoliers ordinaires.", "Audace supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attirer-rapidement", "Le personnage peut, à son tour, faire une manœuvre offensive pour attirer son ennemi au lieu de faire une attaque de corps à corps. Cette manœuvre doit remplacer l’attaque avec le meilleur bonus.", "Le personnage peut traîner son ennemi à lui et lui asséner un coup.", "Attirer rapidement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaquer-en-meute", "Quand le personnage est adjacent à un allié qui possède aussi ce don, la première fois qu’il attaque un adversaire au corps à corps, il peut dépenser une action immédiate pour faire un pas de 1,50 m (1 c), même s’il a déjà bougé pendant le round.", "Le personnage est particulièrement doué pour encercler ses ennemis.", "Attaquer en meute", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assurer-sa-prise", "Le personnage lance deux fois le dé quand il grimpe ou quand il fait un jet de Réflexes pour éviter de tomber. Il conserve le meilleur résultat.", "Grâce à ses réflexes et son talent de grimpeur, le personnage ne risque pas de tomber.", "Assurer sa prise", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assommant", "Le personnage ne subit pas de malus quand il utilise une arme contondante pour infliger des dégâts non létaux.\r\nNormal. Le personnage subit un malus de -4 aux jets d’attaque quand il utilise une arme létale pour infliger des dégâts non létaux. Il ne peut pas utiliser une arme létale pour infliger des dégâts non létaux lors d’une attaque sournoise.", "Le personnage peut assommer ses adversaires avec n’importe quel objet contondant.", "Assommant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bond-du-tigre", "Quand le personnage utilise l’École du tigre, il peut appliquer le malus de l’Attaque en puissance à sa CA au lieu de ses jets d’attaque. De plus, une fois par round, il peut, par une action rapide, se rapprocher d’une cible touchée à mains nues à la moitié de sa vitesse ou d’une cible contre laquelle il a réussi une manœuvre offensive lors de ce tour ou du précédent.", "Les coups du personnage sont aussi précis que puissants et lui permettent de poursuivre ses adversaires à une vitesse stupéfiante mais ils le laissent à découvert.", "Bond du tigre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assaut-dimensionnel", "Le personnage peut utiliser pas chassé ou lancer porte dimensionnelle lors d’une charge spéciale, ce qui lui permet de se téléporter pour doubler sa vitesse actuelle (sur une distance maximale délimitée par le sort ou le pouvoir de classe) et de faire une attaque de charge normale.", "Le personnage s’est entraîné à intégrer les déplacements magiques dans ses tactiques de combat.", "Assaut dimensionnel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "artiste-du-ko", "Quand le personnage fait une attaque non létale à mains nues et inflige des dégâts d’attaque sournoise à un adversaire qui a perdu son bonus de Dextérité à la CA, il gagne un bonus de +1 au jet de dégâts pour chaque dé d’attaque sournoise qu’il lance.", "Le personnage peut mettre des coups de poing dévastateurs.", "Artiste du KO", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-jetable", "Quand le personnage utilise une arme de corps à corps ou de jet dotée de la propriété brisée, et qu’il menace de faire un coup critique contre un adversaire, il peut briser son arme pour le confirmer automatiquement.", "Le personnage ignore les défauts de son équipement et frappe de toutes ses forces, même si cela endommage son arme.", "Arme jetable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "anonyme", "Lors de ses interactions sociales, le personnage compte souvent sur la surprise et les instants de distraction. Il gagne un bonus de +2 aux tests de Bluff et peut dépenser 1 point d’audace pour gagner un bonus de +10 aux tests de Déguisement pendant 10 minutes par niveau de pistolier (10 minutes au minimum). Cet exploit ne change pas son apparence, il lui permet juste de dissimuler son identité autrement.", "Le personnage n’a pas besoin d’un déguisement élaboré pour dissimuler son identité.", "Anonyme", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aile-de-la-grue", "Quand le personnage se bat sur la défensive et qu’il a au moins une main libre, il gagne un bonus d'esquive à la CA de +4 uniquement contre les attaques de mélées. Si une attaque de mélée vous rate de 4 ou moins vous perdez ce bonus d'esquive jusqu'au début de votre prochain tour.\r\nS’il se bat en [Pathfinder-RPG.défense totale|défense totale] il peut dévier une attaque de corps à corps qui devrait le toucher. Une attaque ainsi déviée ne lui inflige aucun dégât et n'a aucun autre effet (traitez-la comme une attaque ratée). L'utilisateur de ce don ne dépense pas d’action mais il doit être conscient de l’attaque et ne pas être [Pathfinder-RPG.pris au dépourvu|pris au dépourvu].", "Le personnage se déplace avec la vivacité et la finesse d’un chasseur aviaire. Ses larges parades et ses mouvements gracieux lui permettent de dévier sans mal les attaques de mêlée.", "Aile de la grue", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "agilité-dimensionnelle", "Le personnage peut effectuer toutes les actions qui lui restent après avoir lancé porte dimensionnelle ou fait un pas chassé.\r\nIl gagne aussi un bonus de +4 aux tests de Concentration quand il lance un sort de téléportation.", "La téléportation ne perturbe plus le personnage.", "Agilité dimensionnelle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "adepte-du-filet", "Le personnage considère le filet comme une arme de corps à corps à une main avec une allonge de 3 m (2 c). De plus, il ne subit aucun malus au jet d’attaque quand il utilise un filet déplié. Il peut le plier par une action complexe ou deux actions simples.", "Le personnage sait se servir du filet comme d’une arme de corps à corps.", "Adepte du filet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assaut-de-gnome-hanté", "Le personnage gagne une utilisation supplémentaire de la magie gnome. Elle est indépendante de ses pouvoirs magiques de gnome. Il peut la dépenser quand il veut se servir d’un pouvoir magique pour lequel il ne lui reste aucune utilisation quotidienne.\r\nDe plus, quand il se trouve sous l’effet de l’aspect de fée hantée, il peut décharger le sort par une action libre , une fois qu’il a touché un adversaire avec une charge ou un coup critique. L’ennemi est alors secoué pour un round.", "Le personnage décharge son inquiétante illusion alors qu’il frappe et terrifie son ennemi.", "Assaut de gnome hanté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "développement-de-la-résistance", "Quand le personnage se met en défense totale, qu’il se bat sur la défensive ou qu’il utilise Expertise du combat, il peut échanger son bonus d’esquive à la CA contre une RD d’un montant équivalent (avec un maximum de RD 5/–) jusqu’au début de son prochain tour. Cette RD se cumule avec celle issue d’un pouvoir de classe, comme celle du barbare, mais pas avec une RD issue d’une autre source. Si le personnage est privé de son bonus de Dextérité à la CA, il ne bénéficie pas de cette RD.", "Le personnage adopte une posture défensive qui lui permet d’absorber les coups et de les renvoyer.", "Développement de la résistance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-vivant", "Quand le personnage lutte contre une créature adjacente, il peut, par une action immédiate , faire un test de lutte contre elle pour bénéficier d’un abri contre une attaque. S’il réussit et que l’attaque le rate, elle vise la créature utilisée comme abri (avec le même jet d’attaque). Le personnage ne peut pas utiliser ce don contre une créature qui l’agrippe et l’abri disparaît une fois que l’attaque associée se termine.", "D’une manœuvre sournoise, le personnage pousse l’ennemi agrippé sur la trajectoire d’une attaque.", "Bouclier vivant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bousculade-tourbillonnante", "Quand le personnage réussit une manœuvre de croc-en-jambe contre un adversaire de sa taille ou plus petit, il peut dépenser une action rapide pour tenter une bousculade contre lui. S’il réussit, il peut le pousser dans une case inoccupée qu’il menace puis le pousser (sur le nombre de segments de 1,50 m (1 c) que le test autorise). La cible tombe ensuite au sol. Si le personnage rate son test de bousculade, il peut utiliser le don Lancer ki.\r\nS’il possède le don Science du Lancer ki, il peut pousser sa cible dans une case occupée par une deuxième cible quand il réussit son test de bousculade. Cet effet fonctionne comme si le personnage avait utilisé le Lancer ki pour envoyer son adversaire dans cette case.", "Le personnage fait tournoyer ses adversaires avant de les projeter au loin.", "Bousculade tourbillonnante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chasseur-métamorphe", "Les niveaux de druide du personnage se cumulent avec ses niveaux de rôdeur pour déterminer quand choisir son nouvel ennemi juré. Ils se cumulent aussi pour connaître le nombre d’utilisations quotidiennes de la forme animale, avec un maximum de huit fois par jour.", "Le personnage se sert à la fois des connaissances qu’il possède sur ses adversaires et de ses aptitudes de métamorphe.", "Chasseur métamorphe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-coordonnée", "Si un allié qui dispose de ce don charge une créature que le personnage peut atteindre avec sa vitesse, il peut la charger aussi, par une action immédiate , à condition qu’il puisse obéir à toutes les règles habituelles de la charge.", "Le personnage est particulièrement doué pour conduire ses alliés au cœur de la mêlée.", "Charge coordonnée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "champion-expérimenté", "Quand le personnage utilise le châtiment du mal, il peut, par une action rapide effectuée au début de son tour, renoncer au bonus de dégâts et ajouter la moitié de ce même bonus aux tests de manœuvres offensives contre la cible du châtiment. Le châtiment du mal agit normalement dès le début du prochain tour du personnage.", "Le personnage peut modifier son châtiment et canaliser la puissance de son dieu pour obtenir l’inspiration divine et augmenter ses performances lors de manœuvres offensives.", "Champion expérimenté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cercle-du-meurtrier", "Quand le personnage passe une action rapide à faire un test de combat de spectacle après un coup critique ou une manœuvre offensive et qu’il est adjacent à la cible du critique ou de la manœuvre, il peut se placer dans n’importe quelle case adjacente à la cible, sans provoquer d’ attaque d’opportunité. Pour cela, le chemin doit être dégagé jusqu’à la case et le personnage doit pouvoir l’atteindre par une action de mouvement. S’il termine ce mouvement dans une case autre que celle de départ, il gagne un bonus de +2 au test de combat de spectacle.", "Une fois que le personnage a lardé son adversaire de coups, il décrit des cercles autour de lui, comme un animal prêt à porter le coup de grâce.", "Cercle du meurtrier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cavalerie-en-formation", "Le personnage et sa monture peuvent empiéter sur l’espace occupé par les montures de cavaliers disposant de ce don, mais il ne peut pas y avoir plus de deux montures par case. De plus, le personnage peut charger à travers une case qui contient un allié monté s’il dispose aussi de ce don. En revanche, la case de départ doit être inoccupée ou obéir aux limites imposées par le don.", "Le personnage peut chevaucher en formation serrée avec ses alliés montés, sans que cela diminue leur efficacité sur le champ de bataille.", "Cavalerie en formation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "caresse-de-lefrit", "Quand le personnage utilise l’École de l’efrit, il peut, par une action simple , dépenser deux utilisations du Poing élémentaire pour lancer un jet de flammes dans un cône de 4,50 m. Les créatures situées dans ce cône reçoivent les dégâts de l’attaque à mains nues plus les dégâts de feu du Poing élémentaire et prennent feu. Réflexes (DD 10 + 1/2 niveau de personnage + modificateur de Sagesse) 1/2 dégâts et annule la combustion.", "Le personnage connaît les secrets des vents brûlants et du soleil flamboyant, ce qui lui permet de rassembler les flammes au creux de sa main avant de les projeter en avant.", "Caresse de l’efrit", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bousculade-rapide", "Le personnage peut, à son tour, faire une unique manœuvre offensive de bousculade au lieu d’une de ses attaques de corps à corps. Il doit choisir celle qui possède le meilleur bonus.", "Le personnage se jette sur ses adversaires avant de les attaquer.", "Bousculade rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "carapace-de-la-tortue", "Quand le personnage utilise l’École de la tortue alligator, le bonus de bouclier que l’école accorde à la CA passe à +2 et ses ennemis subissent un malus de -4 aux jets de confirmation de coup critique.", "La main que le personnage utilise pour se défendre semble magique tellement elle est prompte à détourner les coups.", "Carapace de la tortue", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "brute-enragée", "Quand le personnage est enragé et utilise Attaque en puissance, il peut dépenser 3 rounds de rage supplémentaires, par une action rapide, pour ajouter son bonus de Constitution aux jets de dégâts des attaques au corps à corps ou des armes de jet effectuées à son tour. S’il utilise une arme à deux mains, il ajoute 1,5 fois son bonus de Constitution. On ne multiplie pas ces dégâts supplémentaires en cas de coup critique.", "Le personnage utilise sa rage pour frapper ses adversaires avec plus de force.", "Brute enragée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briser-les-os", "Quand le personnage réussit un Coup étourdissant contre un adversaire agrippé, sans défense ou étourdi, il peut renoncer aux effets supplémentaires du Coup étourdissant pour lui infliger 1d6 points d’affaiblissement de Force ou Dextérité.", "Quand un adversaire est incapable de se défendre correctement contre les frappes précises du personnage, ce dernier brise ses os et déchire ses tissus de ses mains nues.", "Briser les os", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briser-le-cou", "Si le personnage lutte contre un adversaire de sa taille ou plus petit ou l’immobilise, il peut faire une tentative de Coup étourdissant avec un malus de -5 au jet d’attaque après avoir initié la lutte ou l’avoir maintenue. S’il réussit, il tord le cou de son adversaire et lui inflige 2d6 points d’affaiblissement de Force ou de Dextérité. Si la valeur de caractéristique de la cible tombe à 0, les points restants sont déduits de sa Constitution.\r\nUne créature immunisée contre les coups critiques ou qui ne possède pas une tête et un cou visibles est immunisée contre les effets de ce don.", "Le personnage rompt le cou de son ennemi d’un mouvement rapide.", "Briser le cou", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briser-la-tenaille", "Quand le personnage touche un adversaire adjacent avec une attaque de corps à corps, ce dernier perd ses bonus de prise en tenaille jusqu’au prochain tour du personnage. Il ne peut pas prendre le personnage en tenaille ni lui faire d’attaque sournoise mais il peut toujours servir à ses alliés à prendre le personnage en tenaille.", "Le personnage n’a aucun mal à affronter plusieurs adversaires.", "Briser la tenaille", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briser-la-mâchoire", "Quand le personnage réussit un Coup étourdissant contre un adversaire agrippé, sans défense ou étourdi, il peut renoncer aux effets du Coup étourdissant pour estropier son adversaire au niveau de la mâchoire. Il inflige alors les dégâts habituels de l’attaque à mains nues et 1d4 points de saignement. La cible est dans l’incapacité d’attaquer, de parler de façon intelligible et d’employer des sorts à composante verbale tant qu’elle n’a pas mis fin aux dégâts de saignement. Une créature immunisée contre les coups critiques ou qui ne possède pas de bouche visible est immunisée contre ce don.", "Le personnage donne un puissant coup à la mâchoire qui brise dents et os.", "Briser la mâchoire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briser-la-garde", "Quand le personnage manie deux armes et qu’il en utilise une pour désarmer un adversaire, il peut dépenser une action rapide pour l’attaquer avec la seconde arme.", "Le personnage peut utiliser une de ses deux armes pour distraire la défense adverse tout en attaquant avec l’autre.", "Briser la garde", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-agressive", "Quand le personnage utilise la canalisation d’énergie pour infliger des dégâts, il utilise ses niveaux d’inquisiteur comme niveaux de prêtre pour déterminer le nombre de dés de dégâts et le DD du jet de sauvegarde.", "Le personnage montre tant de zèle à chasser les ennemis de sa religion que sa canalisation s’en voit renforcée, tant qu’il l’utilise pour blesser.", "Canalisation agressive", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "appel-spirituel", "Quand le personnage invoque son fantôme, il peut lui donner un bonus d’altération de +2 à la Force, à la Dextérité ou au Charisme. Une fois le rituel d’invocation terminé, ce bonus dure 10 minutes.", "Quand le personnage appelle son fantôme, ce dernier est plus puissant pendant une brève période.", "Appel spirituel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "efficacité-du-tueur", "Le personnage choisit un de ses ennemis jurés quand il obtient ce don. La zone de critique possible de son arme double quand il affronte ce type d’adversaire. Cet effet ne se cumule pas avec d’autres effets qui augmentent la zone de critique possible.", "Le personnage affronte ses ennemis jurés avec une telle efficacité que toutes les armes qu’il emploie contre eux semblent plus dangereuses.", "Efficacité du tueur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "enchaînement-final", "Si le personnage fait une attaque au corps à corps et que sa cible tombe à 0 point de vie ou moins à cause d’elle, il peut faire une autre attaque de corps à corps avec son meilleur bonus de base à l’attaque contre un adversaire situé à portée. Ce don permet seulement une attaque supplémentaire par round.", "Quand le personnage terrasse un adversaire, sa lame poursuit son mouvement et atteint une autre cible.", "Enchaînement final", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "griffe-de-la-panthère", "Quand le personnage utilise l’École de la panthère, il peut dépenser une action libre au lieu d’une action rapide pour riposter à mains nues. À chaque tour, il a droit à un nombre de ripostes à mains nues égal à son modificateur de Sagesse.", "Le personnage lance une série de coups rapides contre les adversaires qui tentent de l’attaquer lors d’un déplacement. ", "Griffe de la panthère", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "griffes-du-tigre", "Quand le personnage utilise l’École du tigre et qu’il a les deux mains libres, il peut utiliser l’attaque à outrance pour faire une unique attaque à mains nues, à deux mains. Le personnage utilise son meilleur BBA, lance les dés séparément pour chaque main de l’attaque à mains nues et multiplie les deux s’il réussit un coup critique. S’il utilise cette attaque avec l’Attaque en puissance, il ajoute la moitié de son bonus de Force à l’un des deux jets de dégâts. S’il touche, il peut tenter une bousculade avec un bonus de +2 au test. Cette bousculade ne provoque pas d’attaque d’opportunité de la part de son adversaire mais le personnage ne peut pas se déplacer avec lui.", "Le personnage peut renoncer à faire plusieurs attaques pour en faire une seule aux conséquences dévastatrices. ", "Griffes du tigre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "guide-divin", "Quand le personnage manie l’arme de prédilection de son dieu, il peut utiliser son modificateur de Sagesse au lieu de son modificateur de Force ou de Dextérité aux jets d’attaque.", "Le dieu du personnage bénit chaque coup qu’il porte avec son [[Arme de prédilection]]. ", "Guide divin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "guérison-vertueuse", "Si le personnage lance un sort de soins quand il a un jugement actif, sa cible récupère 1 point de vie de plus grâce au sort et 1 autre point de vie de plus par tranche de trois niveaux d’inquisiteur.", "Les sorts de guérison du personnage sont plus puissants quand il a un jugement actif. ", "Guérison vertueuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "horreur-de-létreinte-fatale", "Une créature qui reçoit des dégâts de constriction est secouée jusqu’au début du prochain tour du personnage.", "La constriction du personnage devient plus puissante et plus dangereuse. ", "Horreur de l'étreinte fatale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "héritage-monastique", "Quand le personnage détermine son niveau de moine effectif pour calculer ses dégâts à mains nues, il ajoute à ses niveaux de moine la moitié de ses niveaux dans d’autres classes. Ce don ne permet pas de bénéficier des niveaux autres que moine pour déterminer les autres pouvoirs de cette classe.", "L’entraînement rigoureux que le personnage a suivi pour le combat à mains nues l’aide à optimiser son entraînement dans d’autres domaines. ", "Héritage monastique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "impact-du-tir-critique", "Quand le personnage réussit un coup critique avec une attaque à distance, il inflige les dégâts habituels et, si le jet de confirmation dépasse le DMD de l’adversaire, il peut soit le repousser comme avec une bousculade, soit le faire tomber comme avec un croc-en-jambe. S’il choisit la bousculade, il ne peut pas se déplacer avec son adversaire. Cette manoeuvre ne provoque pas d’attaque d’opportunité.", "Le personnage fait une série d’attaques à distance qui fait tomber ses adversaires à genoux ou les oblige à se déplacer.", "Impact du tir critique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ingénieur-de-siège", "Le personnage sait manier toutes les armes de siège. Quand il dirige une équipe, l’engin de siège ne court aucun risque d’avarie en cas de 1 naturel.", "Le personnage sait utiliser toutes les armes de siège. ", "Ingénieur de siège", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "initié-du-refus-de-mourir", "Le personnage n’est pas chancelant quand il utilise le don Dur à cuire mais, s’il fait une action de mouvement et une action simple ou une action complexe alors qu’il se trouve à 0 point de vie ou moins, il subit 1 point de dégâts. De plus, tant qu’il utilise Dur à cuire, il gagne un bonus de +2 aux jets d’attaque au corps à corps et aux jets de dégâts.\r\n(((Ce don possède une variante Initié du refus de mourir (alternatif) pour utilisation avec les règles alternatives «&nbsp;Blessures et vitalité&nbsp;» de l’Art de la Guerre)))", "L’imminence de la mort rend le personnage fou furieux. ", "Initié du refus de mourir", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "jugement-instantané", "Le personnage peut dépenser une action immédiate pour prononcer un jugement ou changer un jugement actif.", "Même les condamnations les plus hâtives du personnage sont efficaces.", "Jugement instantané", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "jugement-partagé", "Le personnage prononce un uniquejugement et fait profiter un allié adjacent de ses effets au lieu d’en prononcer un second.\r\nDe même, s’il possède le pouvoir troisième jugement, il peut en prononcer un seul et en faire bénéficier deux alliés adjacents au lieu d’utiliser le deuxième et le troisième.\r\nUne fois que l’allié a reçu les effets du jugement, il n’est pas obligé de rester adjacent au personnage pour continuer d’en profiter. Le personnage peut dépenser une action libre pour supprimer l’effet sur un allié ou deux. Si les bonus du jugement sont en suspens pour le personnage, ils le sont aussi pour tous ses alliés qui les récupèrent tous en même temps que lui.", "Le personnage fait profiter un allié de son jugement. ", "Jugement partagé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ko-sur-limmobile", "Quand le personnage immobilise un adversaire et réussit une manœuvre de lutte pour lui infliger des dégâts non létaux à mains nues ou avec une arme légère ou à une main, il inflige des dégâts doublés. Une créature immunisée contre les coups critiques l’est aussi contre les effets de ce don.", "Le personnage a plus de facilités à frapper un ennemi qu’il immobilise. ", "KO sur l'immobile", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "la-mort-ou-la-gloire", "Si le personnage affronte une créature d’une catégorie de taille de plus que lui ou plus, il peut faire une unique attaque au corps à corps, par une action complexe. Il gagne un bonus de +4 au jet d’attaque, de dégâts et de confirmation des coups critiques. Ce bonus augmente de +1 à BBA +11, +16 et +20 (pour un maximum de +7 quand il atteint un BBA +20). Une fois l’attaque résolue, l’adversaire attaqué peut dépenser une action immédiate pour faire une unique attaque de corps à corps contre le personnage, avec les mêmes bonus.", "Même quand le personnage affronte un ennemi plus grand que lui, il n’a pas peur de prendre des risques pour terminer le combat. ", "La mort ou la gloire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lacération-furieuse-supérieure", "Quand le personnage éventre un adversaire, il lui inflige 1d6 points de dégâts de saignement, en plus des autres effets de l’éventration.", "Les griffes du personnage sont d’une efficacité terrifiante quand elles s’enfoncent dans la chair d’un adversaire. ", "Lacération furieuse supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lacération-furieuse", "Pour infliger des dégâts d’éventration, le personnage doit seulement réussir la moitié des attaques habituellement requises. Par exemple, un troll qui dispose de ce don éventre son adversaire s’il touche avec une seule attaque de griffe alors qu’un girallon doit toucher une cible avec deux attaques de griffe. Cette éventration fonctionne seulement une fois par round.", "Les attaques naturelles du personnage lui permettent de démembrer son adversaire sans mal. ", "Lacération furieuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lancer-lors-dune-charge", "Le personnage peut utiliser les règles de la charge pour faire une attaque avec une arme de jet. Il applique tous les paramètres de la charge mais il a juste besoin de se rapprocher de son adversaire et doit terminer son mouvement à moins de 9 m (6 c) de lui. Il peut alors effectuer une unique attaque avec une arme de jet, avec un bonus de +2 au jet d’attaque et un malus de -2 la CA jusqu’au début de son prochain tour.", "Le personnage sait profiter de l’élan de la charge pour améliorer ses attaques avec les armes de jet.", "Lancer lors d'une charge", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lancer-à-deux-mains", "Quand le personnage utilise ses deux mains pour lancer une arme à une ou deux mains, il gagne un bonus aux jets de dégâts égal à 1,5 fois son bonus de Force. Il lui faut juste une action simple pour lancer une arme, n’importe laquelle, avec ses deux mains. S’il possède le don Arme en main, il peut lancer une arme à deux mains au même rythme d’attaque qu’une arme à une main.", "Le personnage lance ses armes à deux mains, avec un maximum de force, parfois en les faisant tournoyer pour leur faire fendre l’air à une vitesse étonnante. ", "Lancer à deux mains", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-de-létreinte-fatale", "Le personnage double le nombre de dés de dégâts de l’attaque spéciale constriction.", "Peu de gens peuvent survivre à l’horrible étreinte broyeuse du personnage. ", "Maître de l'étreinte fatale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-de-la-traque-au-clair-de-lune", "Quand le personnage est camouflé aux yeux de son adversaire, ce dernier a 10% de risques de le rater de plus. S’il le rate à cause de son camouflage, le personnage peut utiliser une action immédiate pour se déplacer de 1,50 m (1 c), sans provoquer d’attaque d’opportunité. Ce déplacement ne compte pas comme un pas de placement de 1,50 m (1 c).", "Le personnage laisse ses ennemis s’acharner contre des ombres tandis qu’il se glisse discrètement dans les ténèbres. ", "Maître de la traque au clair de lune", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "maître-de-la-matraque", "Quand le personnage utilise une arme contondante pour infliger des dégâts d’attaque sournoise non létaux à un adversaire pris au dépourvu, il lance deux fois les dés d’attaque sournoise. Le total représente les dégâts d’attaque sournoise non létaux de l’attaque.", "Le personnage assomme ses adversaires grâce à une attaque surprise bien placée. ", "Maître de la matraque", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "marteler-la-faille", "Quand le personnage fait une attaque à outrance, chaque coup qui touche le même adversaire inflige un montant de dégâts supplémentaires égal au nombre de coups que le personnage a déjà porté à cette cible au cours de ce tour. On multiplie ces dégâts en cas de coup critique.", "Le personnage s’acharne à frapper au même endroit pour augmenter ses dégâts. ", "Marteler la faille", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "marqué-pour-punir", "Le personnage peut, par une action simple, dépenser 3 rounds du pouvoir tueur pour faire une attaque de corps à corps avec l’arme tueuse. S’il touche, sa cible ne reçoit pas de dégâts mais elle est marquée jusqu’au début du prochain tour du personnage et, tant que la marque persiste, les armes des alliés du personnage fonctionnent comme si elles possédaient la même propriété de tueur que celle qui a servi à marquer l’ennemi.", "Le personnage marque son ennemi avec son arme tueuse et le rend ainsi plus vulnérable aux attaques de ses alliés. ", "Marqué pour punir", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "manœuvres-dimensionnelles", "Quand le personnage utilise le don Derviche dimensionnel, il gagne un bonus de +4 aux tests de manœuvre offensive de bousculade, de désarmement, de repositionnement et de croc-en-jambe contre ses adversaires.", "Le personnage se déplace si vite qu’il est bien difficile d’éviter ses manœuvres offensives. ", "Manœuvres dimensionnelles", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "gnome-hanté", "Le personnage ajoute Aspect de fée hantée à sa liste de pouvoirs magiques de gnome. Il peut l’utiliser deux fois par jour.", "Le personnage utilise sa magie gnome pour prendre une étrange apparence illusoire. ", "Gnome hanté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "manœuvrer-le-filet", "Quand le personnage se bat au corps à corps, il peut se servir de son filet pour faire un croc-en-jambe à un adversaire ou le désarmer au lieu de l’enchevêtrer. Il gagne un bonus de +2 aux tests de désarmement quand il utilise ainsi son arme. De plus, si l’adversaire est enchevêtré dans le filet, il peut le traîner ou le repositionner tant que l’adversaire se trouve dans la limite imposée par l’allonge du filet ou tant que le personnage contrôle la corde qui lui est reliée.", "Le personnage use de larges mouvements et sa force brute pour handicaper ses adversaires à l’aide de son filet.", "Manœuvrer le filet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lutteur-rapide", "Quand le personnage utilise Lutte supérieure pour maintenir une lutte par une action de mouvement, il peut ensuite dépenser une action rapide pour faire un test de lutte.", "Le personnage entame la lutte avec célérité. ", "Lutteur rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "linceul-de-gnome-hanté", "Le personnage gagne une nouvelle utilisation indépendante de pouvoir de gnome, comme celle d’Assaut de gnome hanté. De plus, tant qu’il est sous l’effet de l’aspect de fée hantée, il bénéficie d’un camouflage (20% de chances de rater) contre un adversaire jusqu’à ce que ce dernier parvienne à lui infliger des dégâts.", "Les illusions du personnage s’étendent au point que les gens ont du mal à déterminer où il se trouve. ", "Linceul de gnome hanté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lien-de-lenfant-de-la-terre", "Le personnage peut renverser une créature de sous-type géant, quelle que soit sa taille. Tant qu’il utilise l’École de l’enfant de la terre, quand une créature de sous-type géant est à terre, qu’elle se relève et provoque une attaque d’opportunité de sa part, il peut faire une attaque à mains nues et, si elle touche, déclarer qu’il s’agit d’une tentative de Coup étourdissant. Le personnage gagne aussi un bonus de +4 au DD du Coup étourdissant quand il l’utilise ainsi.", "Même les immenses géants craignent la technique du personnage.", "Lien de l'enfant de la terre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lanceur-à-distance", "Le malus de distance des jets d’attaque à distance des armes de jet est réduit de 2.", "Le personnage est plus précis que les autres quand il s’agit d’utiliser des armes de jet de loin.", "Lanceur à distance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lanceur-à-courte-distance", "Le personnage choisit un type d’arme de jet. Il ne provoque pas d’attaque d’opportunité quand il fait une attaque à distance avec. S’il est alchimiste, il peut appliquer ce don aux bombes. Dans ce cas, il ne provoque pas d’attaque d’opportunité quand il prend ses composantes, crée la bombe et la lance.", "Le personnage est assez agile pour éviter les attaques au corps à corps tout en utilisant une arme de jet ou une bombe. ", "Lanceur à courte distance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "lanceur-enragé", "Quand le personnage est enragé, il lance des armes à deux mains par une action simple et double le facteur de portée de toutes les armes qu’il lance. S’il possède également le don Arme en main, il dispose de toutes ses attaques quand il lance des armes à deux mains. De plus, il peut ramasser un objet abandonné, placé à sa portée et susceptible de servir d’arme improvisée, lors de l’action d’attaque qui permet de le lancer.", "L’adversaire a bien du mal à éviter les armes et les objets que le personnage lance furieusement. ", "Lanceur enragé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "manœuvre-de-désorientation", "Si le personnage réussit à utiliser Acrobaties pour traverser la case d’un adversaire, il gagne un bonus de circonstances de +2 aux jets d’attaque contre lui jusqu’au début de son prochain tour. S’il choisit de lui faire un croc-en-jambe, il gagne un bonus de circonstances de +4 au test de manœuvre offensive. Ce bonus dure également jusqu’au début de son prochain tour.", "Les déplacements erratiques du personnage déroutent ses adversaires. ", "Manœuvre de désorientation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "férocité-du-sanglier", "Le personnage ajoute « perforants » aux types de dégâts qu’il peut infliger avec des attaques à mains nues. De plus, il gagne un bonus de +2 aux tests d’Intimidation pour démoraliser ses adversaires. Quand il utilise l’École du sanglier, dès qu’il déchire les chairs de son ennemi, il peut dépenser une action libre pour faire un test d’Intimidation qui vise à démoraliser sa victime.", "Les attaques à mains nues du personnage labourent les chairs et terrifient ses victimes. ", "Férocité du sanglier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "férocité-du-dragon", "Quand le personnage utilise l’École du dragon, il gagne un bonus égal à la moitié de son bonus de Force aux jets de dégâts à mains nues, pour un total du double de son bonus de Force lors de sa première attaque à mains nues et 1,5 fois son bonus de Force sur ses autres attaques. Quand il réussit un coup critique ou un Coup étourdissant alors qu’il utilise cette école, son adversaire est secoué pendant un nombre de rounds égal à 1d4 + bonus de Force.", "Le personnage attaque avec la puissance d’un dragon, au point que ses coups terrifient ses ennemis. ", "Férocité du dragon", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fuite-renversante", "Quand le personnage met fin à une lutte grâce à un test de manœuvre offensive ou d’Évasion, il peut dépenser une action rapide pour tenter de faire un croc-en-jambe à l’ennemi.", "Le personnage use de contorsions fluides et profite de diverses manœuvres pour faire levier et jeter son assaillant au sol après avoir échappé à la lutte. ", "Fuite renversante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fausse-ouverture", "Le personnage choisit une arme de jet ou à projectile. Quand il fait une attaque à distance avec elle, il peut provoquer volontairement une attaque d’opportunité de la part d’un ou plusieurs adversaires qui le menacent. Il gagne un bonus d’esquive de +4 à la CA contre eux. Si l’une de ces attaques rate le personnage, son auteur perd son bonus de Dextérité à la CA contre le personnage jusqu’à la fin du tour de ce dernier.", "Quand le personnage fait une attaque à distance alors qu’il est menacé, il peut pousser son adversaire à croire qu’il lui laisse une ouverture. ", "Fausse ouverture", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "exploit-signé", "Le personnage choisit un exploit de sa connaissance qui nécessite une dépense d’audace. Il peut l’accomplir pour 1 point d’audace de moins (0 au minimum). Si cela fait passer la dépense à 0, il peut faire cet exploit en faisant uniquement l’action requise tant qu’il possède encore au moins 1 point d’audace.", "Le personnage est connu pour privilégier un exploit donné pour lequel il est particulièrement doué.", "Exploit signé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "exploit-du-tireur-habile", "Tant que le personnage a au moins 1 point d’audace, il ne provoque pas d’attaque d’opportunité quand il tire ou quand il recharge son arme.", "Le personnage garde un œil sur ce qui se passe alentour, même quand il s’occupe de son arme, ce qui lui permet d’éviter les attaques quand il tire ou recharge une arme à feu.", "Exploit du tireur habile", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "exploit-du-tir-par-ricochet", "Le personnage peut tirer sur un mur ou une surface solide pour faire un ricochet. Dans ce cas, c’est la case qui se trouve juste devant le mur ou l’élément solide qui détermine la ligne de mire jusqu’à la cible. On considère cette case comme le nouveau point de départ de l’attaque et c’est elle que l’on utilise pour déterminer l’effet de l’abri.\r\nLa case du personnage sert à déterminer le camouflage. Le personnage peut accomplir cet exploit tant qu’il lui reste 1 point d’audace. Il peut dépenser 1 point d’audace pour ignorer les effets de tous les abris et de tous les camouflages.\r\nIl doit dépenser ce point avant de faire son jet d’attaque.", "Le personnage fait ricocher un tir d’arme à feu contre un mur pour toucher sa cible.", "Exploit du tir par ricochet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "exploit-de-la-cachette-secrète", "En plein combat, le personnage peut dépenser 1 point d’audace pour retrouver une balle et une dose de poudre noire ou une cartouche alchimique cachées sur sa personne. Il en avait oublié l’existence jusqu’à présent. Si ces munitions sont de type normal, le personnage n’a pas besoin de les payer, en revanche, si elles sont d’un type spécial, il doit retirer le nombre de pièces d’or requis de sa feuille de personnage. Il est impossible de réduire le coût en audace de ce don avec Exploit signé, audace absolue ou un effet similaire réduisant le nombre de points d’audace nécessaires à l’utilisation d’un don.\r\nLe personnage gagne également un bonus de +4 aux tests d’Escamotage lors d’un jeu de hasard.", "Le personnage est si doué pour cacher de petits paquets de munitions pour arme à feu et de poudre noire sur sa personne qu’il lui arrive de se surprendre lui-même quand il les trouve.", "Exploit de la cachette secrète", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esquive-acrobatique", "Quand le personnage réussit à éviter des dégâts grâce à l’esquive surnaturelle, il peut se déplacer de la moitié de sa vitesse, par une action immédiate . Ce mouvement provoque les attaques d’opportunité habituelles.", "Le personnage utilise son habileté à éviter les dégâts pour se repositionner en plein combat.", "Esquive acrobatique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "feinte-de-dégagement", "Le personnage utilise le Bluff pour faire une feinte contre un adversaire, par une action simple. S’il réussit, il ne prive pas son ennemi de son bonus de Dextérité à la CA, en revanche, il peut quitter sa case et s’éloigner à sa vitesse de cet adversaire sans provoquer d’attaque d’opportunité de sa part.", "Le personnage peut faire une feinte pour rompre le combat. ", "Feinte de dégagement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esprit-du-marid", "Chaque jour, le personnage peut utiliser le Poing élémentaire une fois de plus. Quand il utilise l’École du marid, il gagne une résistance au froid égale à son BBA ou à son niveau de moine plus le BBA obtenu grâce à d’autres classes éventuelles, selon ce qui donne le résultat le plus élevé. Il perd cette résistance quand il ne bénéficie plus de son bonus de Dextérité à la CA. Les créatures qui reçoivent des dégâts de froid à cause du Poing élémentaire doivent réussir un jet de Vigueur (DD 10 +1/2 niveau du personnage + modificateur de Sagesse). Si elles échouent, elles sont enchevêtrées dans la glace pendant 1d4 rounds. La glace possède un nombre de points de vie égal à trois fois le BBA du personnage ou trois fois son niveau de moine (prendre le plus haut) et un DD Enfoncer de 15 + BBA ou niveau de moine (prendre le plus haut). Si quelqu’un détruit la glace ou la brise, la victime n’est plus enchevêtrée.", "Le personnage manipule le froid pour se protéger et geler ses ennemis.", "Esprit du marid", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "entraîner-au-sol", "Quand un adversaire arrive à faire tomber le personnage, ce dernier peut lui faire un croc-en-jambe par une action immédiate.", "Quand le personnage tombe à terre, il entraîne son adversaire avec lui.", "Entraîner au sol", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "entraînement-défensif-aux-armes", "Le personnage choisit un groupe d’armes issu de la liste d’entraînement aux armes du guerrier (à l’exception des armes naturelles). Il gagne un bonus d’esquive de +2 à la CA quand il se fait attaquer avec cette arme. S’il a aussi le pouvoir entraînement aux armes pour le même groupe, le bonus d’esquive passe à +3.", "Le personnage sait comment se défendre au mieux contre une certaine classe d’armes.", "Entraînement défensif aux armes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "entraînement-aux-armures-renforcé", "Si un adversaire fait un coup critique contre le personnage, ce dernier peut le transformer en coup normal mais son armure ou son bouclier est alors brisé (c’est à lui de choisir).", "Le personnage sait comment reporter le plus gros des coups sur son armure.", "Entraînement aux armures renforcé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "entraînement-au-combat-féroce", "Le personnage choisit une de ses armes naturelles. Quand il l’utilise, il peut appliquer les effets des dons qui mentionnent Science du Combat à mains nues dans leurs conditions requises, ainsi que les effets qui améliorent les coups à mains nues.", "Le personnage a été formé dans un style d’arts martiaux qui se repose sur les armes naturelles issues des pouvoirs de sa classe ou de sa race.", "Entraînement au combat féroce", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "entrave-à-distance", "Quand le personnage réussit un Lancer ki contre un adversaire, il peut utiliser une action rapide pour tenter une manœuvre de lutte contre lui.\r\nNormal. La lutte est une action simple .", "Le personnage peut frapper son ennemi et profiter du coup pour entamer une lutte.", "Entrave à distance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ennemi-des-métamorphes", "Quand le personnage touche une créature, elle a du mal à utiliser ou maintenir un effet de métamorphose jusqu’à la fin du prochain tour du personnage. Si elle veut se servir d’un tel effet, elle doit réussir un test de Concentration (DD 15 + deux fois le niveau de l’effet). Si le personnage inflige des dégâts à un adversaire métamorphosé, ce dernier doit réussir un jet de Volonté, (DD 10 + 1/2 niveau du personnage + modificateur de Sagesse) ou reprendre sa forme normale.\r\nSi le personnage réussit un coup critique contre un tel adversaire, la cible n’a pas droit au jet de sauvegarde.", "Le personnage maîtrise la magie du changement de forme, ce qui lui permet d’empêcher les autres d’en user.", "Ennemi des métamorphes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esprit-du-djinn", "Le personnage peut utiliser Poing élémentaire une fois de plus par jour. Quand il utilise l’École du djinn, il gagne une résistance à l’électricité égale à son niveau de moine ou à son BBA (choisir le plus élevé). Il perd cette résistance quand il ne bénéficie plus de son bonus de Dextérité à la CA. Les créatures qui subissent des dégâts à cause du Poing élémentaire doivent réussir un jet de Vigueur (DD 10 +1/2 niveau du personnage + modificateur de Sagesse) ou devenir sourdes pendant 1d4 rounds. Celles qui subissent des dégâts à cause du Tourbillon du djinn sont sourdes même si elles réussissent leur jet de sauvegarde.", "Le personnage fait appel à l’esprit des tempêtes pour manipuler la foudre. Elle le protège et frappe ses ennemis.", "Esprit du djinn", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "empoisonneur-précis", "Quand le personnage utilise Frappe de la vipère, il peut empoisonner deux fléchettes qu’il utilise pour attaquer ses adversaires au corps à corps. (Il dégaine les fléchettes par une action libre.) Tant qu’il les tient en main, il peut utiliser une action simple pour attaquer avec l’une d’elles ou une action complexe pour attaquer avec les deux.\r\nCe sont des attaques de contact au corps à corps qui infligent 1d2 points des dégâts plus les bonus habituels des attaques à mains nues du personnage et permettent d’appliquer le poison. Sinon, le personnage peut lancer les fléchettes comme des shurikens et faire un jet d’attaque à distance contre la CA de la cible.", "Le personnage utilise habilement des aiguilles enduites de poison pour une efficacité maximale.", "Empoisonneur précis", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "feinte-du-traqueur-au-clair-de-lune", "Une fois par round, quand le personnage est camouflé aux yeux d’un adversaire, il peut utiliser une action rapide pour faire une feinte avec un test de Bluff.", "Le personnage se tapit dans les ombres et frappe si rapidement que son adversaire a à peine le temps de réagir.", "Feinte du traqueur au clair de lune", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "filet-et-trident", "Le personnage considère le filet comme une arme à distance à une main, ce qui lui permet de tenir une arme légère ou à une main dans son autre main sans l’empêcher d’attaquer à distance avec le filet. Quand il utilise la seconde arme pour attaquer un adversaire enchevêtré, il gagne un bonus de +2 aux tests de dégâts et aux jets d’attaque pour confirmer un coup critique.", "Le personnage est doué pour manier les armes légères, ce qui lui permet d’en utiliser une en même temps que son filet.", "Filet et trident", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "froid-du-marid", "Quand le personnage utilise l'École du marid, il peut dépenser deux utilisations du Poing élémentaire pour envoyer un jet d’eau glacée sur une ligne de 9 m (6 c), par une action simple. Les créatures situées dans la ligne d’effet subissent les dégâts de l’attaque à mains nues et les dégâts de froid du Poing élémentaire et se retrouvent coincées par la glace, comme avec le don Esprit du marid. Réflexes (DD 10 + 1/2 niveau de personnage + son modificateur de Sagesse) 1/2 dégâts et empêche l’enchevêtrement.", "Le personnage invoque un torrent d’eau qui frappe ses ennemis et les gèle jusqu’à la moelle. ", "Froid du marid", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-révélatrice", "Le personnage choisit une révélation qui affecte un adversaire seulement au moment de prendre ce don. S’il réussit une attaque à mains nues contre un ennemi, il lui inflige les dégâts habituels et peut, en plus, utiliser une action rapide pour lui attribuer les effets de la révélation choisie, sans provoquer d’attaque d’opportunité.", "Les coups que le personnage porte à mains nues donnent une révélation à ses ennemis. ", "Frappe révélatrice", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-ondoyante", "Si le personnage dégaine une arme de corps à corps pour attaquer un ennemi à portée au premier round de combat, il peut dépenser une action rapide pour faire un test de Bluff contre cet ennemi.", "Le personnage garde une attitude sereine jusqu’à ce qu’il tire son arme et frappe d’un mouvement fluide. ", "Frappe ondoyante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-maléfique", "Quand le personnage obtient ce don, il choisit un maléfice qui n’affecte pas plus d’un ennemi. S’il réussit une attaque à mains nues contre un adversaire, il lui inflige les dégâts habituels et peut, en plus, utiliser une action rapide pour lancer le maléfice sur lui, sans provoquer d’attaque d’opportunité.", "Le personnage psalmodie et maudit son ennemi et lui lance un maléfice lors d’une attaque à mains nues. ", "Frappe maléfique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-ensorcelée", "Le personnage choisit un pouvoir de lignage d’ensorceleur qui affecte un seul adversaire quand il obtient ce don. S’il réussit une attaque à mains nues contre un adversaire, il inflige les dégâts habituels du coup et peut dépenser une action rapide pour appliquer les effets du pouvoir de lignage choisi, sans provoquer d’attaque d’opportunité.", "Le pouvoir qui coule dans les veines du personnage transparaît aussi dans ses attaques à mains nues. ", "Frappe ensorcelée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-dévastatrice", "Quand le personnage utilise Frappe décisive, Science de la Frappe décisive ou Frappe décisive supérieure, il gagne un bonus de +2 sur chaque dé de dégâts supplémentaire (avec un maximum de +6). Ces dégâts supplémentaires sont multipliés en cas de coup critique.", "Le personnage rassemble toute sa force et sa résolution dans un coup que son ennemi ne peut pas ignorer. ", "Frappe dévastatrice", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "feinte-à-deux-armes", "Quand le personnage fait une attaque de corps à corps avec Combat à deux armes, il peut échanger l’attaque de son arme principale pour faire une feinte avec un test de Bluff.", "Le personnage utilise une arme pour distraire son adversaire tandis qu’il franchit ses défenses avec l’autre. ", "Feinte à deux armes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-du-kirin", "Le personnage gagne un bonus d’intuition de +2 aux tests de Connaissances destinés à identifier une créature, y compris avec École du kirin. Quand il utilise l’École du kirin contre une créature qu’il a identifiée grâce à ce don, s’il la touche à distance ou au corps à corps, il peut, par une action rapide, ajouter deux fois son modificateur d’Intelligence à ses dégâts (2 au minimum).", "Le personnage a lu les textes de la voie parfaite et sait comment identifier les faiblesses de ses ennemis. ", "Frappe du kirin", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-de-la-vipère", "Par une action rapide, le personnage peut enduire deux parties de son corps (qu’il utilise pour porter des attaques à mains nues) d’un poison qui agit par contact ou par blessure. Il doit se protéger contre toute exposition à la toxine s’il utilise ainsi un poison de contact.", "Le personnage peut appliquer rapidement du poison sur ses mains gantées, sur des pieds chaussés ou sur une autre partie protégée de son corps pour porter des attaques empoisonnées à mains nues. ", "Frappe de la vipère", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-de-domaine", "Quand le personnage prend ce don, il choisit un de ses pouvoirs de domaine (qui n’affecte pas plus d’un adversaire). S’il réussit une attaque à mains nues contre un ennemi, il peut dépenser une action rapide pour lui infliger les effets du pouvoir choisi en plus des dégâts normaux. Ceci ne provoque pas d’attaque d’opportunité.", "Le personnage déchaîne un pouvoir de domaine sur un ennemi lors d’une attaque à mains nues. ", "Frappe de domaine", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-de-cauchemar", "Quand un adversaire est auréolé de lueur féerique lancées par le personnage et non issues d’un objet à potentiel magique ou à fin d’incantation, le DD pour résister aux Coups étourdissants du personnage augmente de +2 pour cet adversaire. Si le personnage réussit une tentative de Coup étourdissant et que l’adversaire affecté par les lueurs rate son jet de sauvegarde, il est secoué pendant 1d2 rounds plus 1 round pour chaque tranche de 5 en deçà du DD.", "Les lueurs féeriques du personnage illuminent ses adversaires et soulignent aussi leurs faiblesses. ", "Frappe de cauchemar", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-ciblée-supérieure", "Lorsque le personnage attaque, il peut choisir de remplacer son attaque par une frappe ciblée. Le personnage peut faire plusieurs frappes ciblées en un seul round. Chaque frappe ciblée supplémentaire après la première au cours d’un même round ajoute un malus de -5. En outre, les frappes ciblées ôtant plus de la moitié des points de vie de leur cible (minimum 40) sont considérées comme des coups incapacitants.", "Le personnage peut entreprendre plusieurs frappes ciblées quand les autres ne peuvent en placer qu’une dans le meilleur des cas.", "Frappe ciblée supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "forme-animale-planaire", "Quand le personnage utilise forme animale pour prendre la forme d’un animal, il peut dépenser une utilisation supplémentaire de ce pouvoir pour lui ajouter l’archétype fiélon ou céleste. (Les druides Bons sont obligés de prendre la forme céleste et les Mauvais la forme fiélone.) La forme céleste d’un animal donne un bonus de +2 aux jets d’attaque pour confirmer un coup critique sur une créature Mauvaise alors que la forme fiélone donne le même bonus contre les créatures Bonnes.", "Le personnage peut donner des caractéristiques planaires à sa forme animale. ", "Forme animale planaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fioritures-rhétoriques", "Quand le personnage utilise la Diplomatie pour faire une requête ou changer l’attitude d’une créature, il peut donner le change verbalement. Il fait alors un test de Bluff contre la créature. S’il réussit, il gagne un bonus de +4 au prochain test de Diplomatie à son encontre pendant la minute qui suit. S’il échoue de 5 ou plus, il subit un malus de -2 à ce même test.\r\nIl peut aussi utiliser ce don pour refaire un unique test de Diplomatie raté. Il subit alors un malus de -4 au test de Bluff contre une créature. S’il réussit tout de même, au lieu de bénéficier des avantages habituels du don, il peut refaire le dernier test de Diplomatie raté contre cette créature, à condition qu’il ne remonte pas à plus d’une minute.", "Le personnage change rapidement de sujet et emploie une rhétorique confuse pour cacher ses véritables intentions.", "Fioritures rhétoriques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-de-lécole", "Quand le personnage obtient ce don, il choisit un pouvoir d’école qui affecte un seul adversaire. S’il réussit une attaque à mains nues contre un ennemi, il lui inflige les dégâts habituels et peut, en plus, utiliser une action rapide pour lui attribuer les effets du pouvoir d’école choisi, sans provoquer d’attaque d’opportunité.", "Le personnage concentre les secrets de son école de magie dans un coup à mains nues.", "Frappe de l'école", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "apprentissage-rapide", "Quand le personnage gagne un niveau dans une classe de prédilection, il gagne +1 pv et +1 point de compétence au lieu de devoir choisir l’un ou l’autre. Il peut choisir une récompense de classe alternative à la place de l’un ou de l’autre.", "La progression du personnage est de plus en plus diversifiée.", "Apprentissage rapide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aptitude-de-voie-supplémentaire-(mythique)", "Le personnage choisit une aptitude mythique pour laquelle il remplit toutes les conditions, dans la liste des aptitudes de sa voie ou de la voie universelle. Il gagne cette nouvelle aptitude de voie.", "Le personnage peut faire usage d’aptitudes mythiques autres que celles qu’il maîtrise déjà.", "Aptitude de voie supplémentaire (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aptitude-magique-(mythique)", "Le bonus aux tests d’Art de la magie et d’Utilisation d'objets magiques conféré par Aptitude magique augmente de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il a obtenu un 20 naturel à l’un de ces tests. Le personnage doit décider s’il utilise cette aptitude avant de lancer le dé.", "Le personnage possède une compréhension des principes et des rouages de la magie qui dépasse de loin le cadre de son apprentissage classique.", "Aptitude magique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dispense-de-composantes-(mythique)", "Le personnage peut lancer n’importe quel sort de son répertoire sans disposer des composantes matérielles dont le prix est inférieur ou égal à 10 po par grade. Si le personnage dépense une utilisation de pouvoir mythique lorsqu’il lance un sort qui nécessite une composante matérielle dont le prix est égal ou inférieur à 50 po par grade, il peut le lancer sans disposer de cette composante. En dépensant deux utilisations de pouvoir mythique, il peut lancer un sort sans avoir besoin de fournir une composante matérielle dont le prix est égal ou inférieur à 100 po par grade.", "Le personnage peut lancer ses sorts, même les plus puissants, sans utiliser aucune composante matérielle.", "Dispense de composantes (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dispense-de-composantes-matérielles", "Le personnage peut lancer des sorts nécessitant une composante matérielle de 1 po ou moins sans utiliser cette composante. L’incantation provoque tout de même des attaques d’opportunité. Le personnage doit fournir les composantes matérielles dont le prix est supérieur à 1 po.", "Le personnage peut lancer de nombreux sorts sans utiliser de composantes matérielles mineures.", "Dispense de composantes matérielles", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "doigts-de-fée-(mythique)", "Les bonus aux tests de Sabotage et Escamotage conférés par Doigts de fée sont augmentés de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il a obtenu un 20 naturel à l’un de ces tests. Le personnage doit décider s’il utilise cette aptitude avant de lancer le dé.", "Le personnage fait preuve d’une agilité manuelle exceptionnelle.", "Doigts de fée (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "doigts-de-fée", "Le personnage obtient un bonus de +2 sur tous ses tests d’Escamotage et de Sabotage. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence augmente de +4.", "Le personnage est d’une dextérité hors du commun.", "Doigts de fée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dompteur-de-feu", "Le gobelin gagne un bonus de +2 à ses jets de sauvegarde contre les sorts du registre feu. En outre, ses cicatrices le distinguent comme un talentueux dompteur de feu, lui accordant un bonus de circonstances de +2 à ses jets de Diplomatie et d’Intimidation lorsqu’il traite avec d’autres gobelins.", "Le gobelin sait comment s’occuper du feu, même magique.", "Dompteur de feu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "don-de-lesprit", "Au début de la journée, le personnage peut décider de communier avec un esprit de chaman. (Même si le chaman aurait besoin d’une heure de préparation, cela ne demande pas de temps au personnage.) Pendant les prochaines 24 heures, cet esprit de chaman améliore le compagnon animal ou le familier du personnage, comme le permettrait son pouvoir esprit animal.", "Le personnage communie avec un esprit de son choix, qui accorde un bienfait à son compagnon animal ou à son familier.", "Don de l'esprit", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "don-pour-les-critiques-(mythique)", "Le personnage confirme automatiquement ses coups critiques contre des adversaires non-mythiques. De plus, lorsqu’il obtient un critique potentiel contre une créature portant une armure dotée de la propriété spéciale défense ou d’un effet similaire, celle-ci doit lancer deux dés et conserver le pire lorsqu’il s’agit de déterminer l’annulation du coup critique.", "Les coups du personnage touchent de manière infaillible les points vitaux de ses cibles.", "Don pour les critiques (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "don-pour-les-critiques", "Le personnage reçoit un bonus de circonstances de +4 aux jets d’attaque destinés à confirmer un coup critique.", "Le personnage est très doué pour semer la souffrance.", "Don pour les critiques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "double-frappe", "Le personnage ajoute son bonus de Force aux jets de dégâts de son arme secondaire.", "Quand le personnage se bat à deux armes, il manie son arme secondaire avec une puissance accrue.", "Double frappe", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "duelliste-à-la-falcata", null, "Le personnage s’est entraîné dans une école d’élite où se pratique habilement le rondelero, l’art du combat à la falcata et au bouclier.", "Duelliste à la falcata", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dur-comme-le-fer", null, "Les effets accordant une armure naturelle fonctionnent encore mieux sur le personnage.", "Dur comme le fer", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dur-à-cuire", "Lorsque les points de vie du personnage tombent en dessous de 0 mais qu’il n’est pas mort, il se stabilise automatiquement. Il n’a pas besoin de faire de test de Constitution à chaque round pour ne pas perdre de points de vie supplémentaires. Il peut choisir d’être hors de combat plutôt que mourant. Le joueur doit prendre cette décision dès que son personnage atteint un total de points de vie négatif (même si cela arrive en dehors de son tour de jeu). Si le personnage ne choisit pas cela, il tombe aussitôt inconscient.\r\nLorsqu’il utilise ce don, le personnage est chancelant. Effectuer une action de mouvement n’aggrave pas ses blessures, mais s’il entreprend une action simple (ou toute autre action fatigante, y compris certaines actions libres comme lancer un sort à incantation rapide), le personnage subit 1 point de dégâts aussitôt après avoir accompli son action. Le personnage meurt immédiatement si son total de points de vie négatif atteint ou dépasse sa valeur de Constitution.", "Le personnage est particulièrement difficile à tuer. Ses blessures se stabilisent automatiquement quand il est grièvement blessé mais, en plus, même aux portes de la mort, il reste conscient et peut continuer de bouger.", "Dur à cuire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "débiter-les-orques", "Ce don fonctionne comme Enchaîner les gobelins, mais les attaques supplémentaires sont valables contre des adversaires de la taille du personnage ou moins. Les attaques supplémentaires portées contre les humanoïdes (orques) bénéficient d’un bonus de circonstances de +2 aux jets d’attaque.", "Le personnage débite férocement ses adversaires, surtout les orques.", "Débiter les orques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "défense-à-deux-armes-(mythique)", "Lorsqu’il utilise Défense à deux armes, le personnage ajoute au bonus de bouclier conféré par ce don le bonus d’altération d’arme le plus élevé (entre ses deux armes).", "Les mouvements fluides et gracieux du personnage en matière d’attaque et de défense le rendent difficile à tuer.", "Défense à deux armes (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "défense-à-deux-armes", "Quand le personnage manie une arme double ou deux armes (sans compter les armes naturelles et les attaques à mains nues), il gagne un bonus de bouclier de +1 à la CA.\r\nQuand il se bat sur la défensive ou qu’il se met en défense totale, ce bonus de bouclier passe à +2.", "Le personnage sait se défendre avec deux armes.", "Défense à deux armes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "démarche-de-laraignée-(drow)", "Le drow peut lancer le pouvoir magique pattes d’araignée une fois par jour et utilise son niveau pour définir son niveau de lanceur de sorts. De plus, il gagne un bonus de +4 à ses jets de sauvegarde contre l’attaque spéciale de toile des araignées et les effets de toile d’araignée et autres sorts similaires (comme nuage de toile).", "Le drow s’aventure là où seuls les arachnides osent aller.", "Démarche de l'araignée (drow)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "démonstration-(mythique)", "En manipulant une arme pour laquelle il possède le don Arme de prédilection, le personnage peut utiliser le don Démonstration par une action simple en subissant un malus de -5 au test d’Intimidation, par une action de mouvement avec un malus de -10, ou par une action rapide avec un malus de -20. Le personnage peut dépenser une utilisation de pouvoir mythique pour effrayer au lieu de simplement secouer les créatures affectées par la Démonstration.", "Le personnage dissuade rapidement ses adversaires par son impressionnante maîtrise martiale.", "Démonstration (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "enchantement-dual", "Quand il utilise son pacte divin ou son arme sacrée\r\npour améliorer une arme, le personnage peut améliorer\r\ndeux armes ou les deux extrémités d’une arme double. Toutes\r\nles armes affectées doivent être tenues en main ou maniées,\r\nde quelque manière que ce soit. Tout bonus d’altération que le\r\npersonnage rajoute à l’une de ses deux armes se rajoute automatiquement\r\nà la seconde mais les propriétés spéciales d’arme\r\ndoivent être appliquées séparément. Par exemple, si le personnage\r\npeut apporter une amélioration équivalant à un bonus de\r\n+2, il peut doter ses deux armes d’un bonus supplémentaire de +1\r\net appliquer la propriété spéciale d’arme acérée à l’une des deux.", "Le personnage peut améliorer deux armes, ou les deux extrémités d’une arme double, en une seule action.", "Enchantement dual", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "empathie-rapide-(mythique)", "Le personnage peut utiliser empathie sauvage par une action rapide.", "Le personnage peut calmer les animaux sauvages en faisant preuve d’une rapidité presque surnaturelle.", "Empathie rapide (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "efficacité-des-sorts-accrue", "Le personnage bénéficie d’un bonus de +2 à son test de niveau de lanceur de sorts (1d20 + niveau de lanceur de sorts) lorsqu’il s’agit de franchir la résistance à la magie d’une cible.", "Les sorts du personnage ont plus de facilité à franchir les défenses de ses ennemis.", "Efficacité des sorts accrue", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "efficacité-des-sorts-accrue-supérieure", "Le personnage obtient un bonus supplémentaire de +2 sur les tests de niveau de lanceur de sorts (1d20 + niveau de lanceur de sorts) effectués pour franchir la résistance à la magie d’une créature. Ce bonus se cumule avec celui d’Efficacité des sorts accrue.", "Les sorts du personnage franchissent bien plus facilement la résistance aux sorts de ses ennemis que ceux de la plupart des gens.", "Efficacité des sorts accrue supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "efficacité-des-sorts-accrue-(mythique)", "Le personnage ajoute la moitié de son grade aux tests de niveau de lanceur de sorts effectués pour ignorer la résistance à la magie. S’il possède Efficacité des sorts accrue supérieure, il ajoute à la place son grade complet.", "Le pouvoir mythique du personnage perce les défenses de ses ennemis.", "Efficacité des sorts accrue (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "détection-de-l’expertise-(mythique)", "Le personnage gagne immédiatement les avantages conférés par Détection de l’expertise (sans avoir besoin d’observer une créature pendant trois rounds) lorsqu’il utilise les moyens divinatoires indiqués dans la description de ce don. Les créatures non-mythiques ne peuvent pas effectuer de jet de Volonté pour résister à cet effet.", "Le personnage peut intuitivement sentir les aptitudes magiques d’une créature d’un simple regard.", "Détection de l’expertise (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "diseuse-de-bonne-aventure", null, "Le personnage a été élevé dans un pays ancré dans les traditions et les superstitions et il est devenu très doué pour communiquer avec le monde des esprits.", "Diseuse de bonne aventure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "désarçonner", "Quand le personnage charge un adversaire alors qu’il est monté et manie une lance d’arçon, l’attaque se déroule comme à l’accoutumée. En revanche, si l’attaque touche, le personnage peut immédiatement tenter une bousculade gratuite en plus d’infliger les dégâts habituels. S’il réussit, sa cible tombe à terre dans une case adjacente à sa monture et opposée au personnage.", "Le personnage est doué pour désarçonner ses adversaires.", "Désarçonner", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "désarmement-à-distance", "le personnage peut tenter un  désarmement à distance par une action complexe avec n’importe quelle arme à distance et une pénalité de -2 au jet de  manœuvre de combat. Ajoutez le modificateur de dextérité du personnage à la place de son modificateur de force à sa BMO et ajoutez-y aussi des pénalités de distance doublées. Si la cible est à plus de 9m (6c), ajoutez une pénalité supplémentaire de -2. Si la tentative de désarmement est un succès, la cible subit aussi des dégâts équivalents à une attaque à distance réussie avec cette arme. Le personnage ne peut pas être désarmé en échouant cette tentative de désarmement.", "Un tir bien placé permet de désarmer votre adversaire.", "Désarmement à distance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "désarmement-supérieur", "Le personnage reçoit un bonus de +2 aux tests de désarmement. Ce bonus s’ajoute à celui de la Science du désarmement. Quand le personnage parvient à désarmer son adversaire, il envoie son arme à 4,50 m de son propriétaire, dans une direction aléatoire.", "Le personnage désarme ses ennemis et envoie leurs armes voltiger au loin.", "Désarmement supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "désarmement-contrôlé", "A chaque fois que le personnage réussit une manœuvre de combat afin de désarmer son adversaire, l’arme atterrit jusqu’à une distance maximale de 4.5m (3c) de son porteur, dans la direction que le personnage choisit. Il peut aussi choisir d’effectuer une attaque à distance avec cette arme contre un autre adversaire par une action immédiate, tant que l’arme désarmée est une arme légère ou une arme à une main. S’il ne s’agit pas d’une arme de jet, le personnage subit une pénalité de -4 sur le jet d'attaque. Il subit également une pénalité de -4 sur le jet d’attaque s’il n’a pas au moins une main libre.", "L’arme que le personnage retire de la main de l’un de ses adversaires s’envole en direction d’un autre ennemi.", "Désarmement contrôlé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "déplacement-acrobatique", ". Quand le personnage se déplace, il peut franchir 4,50 m (3 cases) par round sur un terrain difficile comme s’il s’agissait d’un terrain ordinaire. Les effets de ce don s’ajoutent à ceux accordés par Aisance (don qui permet au personnage de se déplacer normalement sur 6 m (4 cases) par round en terrain difficile).", "Le personnage peut facilement franchir les obstacles.", "Déplacement acrobatique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "démonstration", "Quand le personnage manie son Arme de prédilection, il peut faire une démonstration impressionnante de ses talents lors d’une action complexe. Il fait un test d’Intimidation pour démoraliser tous les ennemis qui se trouvent dans un rayon de neuf mètres et qui le regardent.", "L’habileté avec laquelle le personnage manie son arme de prédilection terrifie ses adversaires.", "Démonstration", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "démonstration-décourageante", "Quand le personnage réussit à utiliser Démonstration\r\ncontre des adversaires secoués, effrayés ou paniqués, leur terreur\r\nest accrue d’un niveau. Une créature déjà paniquée qui se retrouve\r\ndémoralisée par ce don se recroqueville sur elle-même. Une fois\r\naffectée par ce don, la créature ne peut plus l’être à nouveau (que\r\nce soit par le personnage ou quelqu’un d’autre) pendant 24 heures.", "Par la démonstration de ses prouesses, le personnage démoralise les autres.", "Démonstration décourageante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "désarçonner-(mythique)", "Lorsque le personnage parvient à bousculer un adversaire et à le désarçonner grâce au don Désarçonner, son adversaire subit 1d6 points de dégâts de chute par tranche de 2 grades du personnage. Les objets ou les aptitudes qui réduisent les dégâts de chute, tel qu’un test d’Acrobaties réussi, peuvent réduire les dégâts infligés par l’impact.", "Le personnage désarçonne ses adversaires grâce à sa force impressionnante.", "Désarçonner (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "discret", "Le personnage obtient un bonus de +2 sur tous ses tests de Discrétion et d’Évasion. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence augmente de +4.", "Le personnage a un don pour éviter d’attirer l’attention et échapper à ses liens.", "Discret", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "discret-(mythique)", "Le bonus aux tests d’Évasion et de Discrétion conféré par le don Discret augmente de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il obtient un 20 naturel lorsqu’il effectue l’un de ces tests. Le personnage doit décider s’il utilise cette aptitude avant de lancer le dé.", "La grâce et la fluidité motrice du personnage sont incomparables.", "Discret (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "discipliné", null, "Le personnage est très au fait des traditions militaires de son pays natal et elles lui inspirent un courage hors du commun.", "Discipliné", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "croc-en-jambe-au-bâton-(mythique)", "Le personnage ajoute la moitié de son grade à son DMD contre les manœuvres de croc-en-jambe et aux tests de manoeuvre de croc-en-jambe effectués avec un bâton. Lorsque le personnage manipule un bâton et qu’un adversaire dans sa zone de menace tente en vain de lui faire un croc-en-jambe, cet adversaire provoque une attaque d’opportunité de la part du personnage. Le personnage doit utiliser son bâton lorsqu’il effectue cette attaque d’opportunité.", "Le personnage manipule son précieux bâton aussi bien pour attaquer que pour se défendre.", "Croc-en-jambe au bâton (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critiques-divers", "Une fois par jour, le personnage peut passer une heure à répéter des mouvements pour gagner un don de critique dont il remplit les conditions. Il gagne les avantages de ce don de critique jusqu’à ce qu’il s’entraîne à effectuer d’autres mouvements pour en obtenir un autre.", "Grâce à son esprit ouvert et à son entraînement martial, le personnage diversifie ses possibilités de coup critique.", "Critiques divers", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-étourdissant", "Quand le personnage inflige un coup critique, son adversaire est étourdi pendant 1d4 rounds. Un jet de Vigueur réussi permet à l’adversaire d’être seulement chancelant pendant 1d4 rounds. Le DD du jet de Vigueur est égal à 10 + bonus de base à l’attaque. Les effets de ce don ne se cumulent pas. Les coups supplémentaires prolongent sa durée.", "Les coups critiques du personnage étourdissent ses adversaires.", "Critique étourdissant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-épuisant", "Quand le personnage inflige un coup critique à un adversaire, ce dernier est épuisé. Ce don n’a aucun effet sur une cible déjà épuisée.", "Les critiques du personnage épuisent son adversaire.", "Critique épuisant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-sanglant", ". Quand le personnage réussit un coup critique avec une arme tranchante ou perforante, son adversaire subit 2d6 points de dégâts de saignement par round, à son tour de jeu, en plus des dégâts causés par le coup critique. On peut arrêter l’hémorragie à l’aide d’un test de Premiers secours DD 15 ou grâce à un soin magique. Les effets de ce don se cumulent.", "Les coups critiques du personnage font saigner abondamment ses adversaires.", "Critique sanglant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-sanglant-(mythique)", "Les dégâts de saignement infligés par Critique sanglant augmentent d’un nombre égal au grade du personnage. Pour stopper l’hémorragie, il faut réussir un test de Premiers secours contre un DD égal à 15 + ½ de son grade. N’importe quel soin magique peut normalement mettre fin aux dégâts de saignement. Le personnage peut dépenser une utilisation de pouvoir mythique pour infliger 1 point de saignement en Constitution, en plus des dégâts de saignement infligeant une perte de points de vie par le biais de ce don. Cet effet augmente de 1 point pour chaque utilisation de pouvoir mythique dépensée en plus de la première.", "Les coups les plus décisifs du personnage déversent des torrents d’hémoglobine.", "Critique sanglant (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "croc-en-jambe-supérieur", "Le personnage reçoit un bonus de +2 aux tests destinés à faire un croc-en-jambe à un adversaire. Ce bonus s’ajoute à celui de la Science du croc-en-jambe. Quand le personnage réussit à faire un croc-en-jambe à son adversaire, ce dernier provoque des attaques d’opportunité.", "Le personnage attaque gratuitement les adversaires qu’il a fait tomber.", "Croc-en-jambe supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-ralentissant", "Quand le personnage inflige un coup critique, son adversaire est chancelant pendant 1d4+1 rounds. Un jet de Vigueur réussi permet de réduire cette durée à 1 round. Le DD du jet de Vigueur est égal à 10 + bonus de base à l’attaque. Les effets de ce don ne se cumulent pas. Les coups supplémentaires prolongent la durée.", "Les coups critiques du personnage ralentissent ses adversaires.", "Critique ralentissant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-fatigant", "Quand le personnage inflige un coup critique à un adversaire, ce dernier est fatigué. Ce don n’a aucun effet sur une cible déjà fatiguée ou épuisée.", "Les critiques du personnage fatiguent son adversaire.", "Critique fatigant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-aveuglant", "Quand le personnage inflige un coup critique, son adversaire est définitivement aveuglé. Un jet de Vigueur réussi permet de limiter les effets à la condition ébloui pendant 1d4 rounds. Le DD du jet de sauvegarde est égal à 10 + bonus de base à l’attaque du personnage. Ce don n’a aucun effet sur les créatures qui ne dépendent pas de leur vue ou sur celles qui possèdent plus de deux yeux (bien que, si le MJ est d’accord, le personnage puisse aveugler la créature en répétant les critiques).\r\nOn peut soigner une créature aveugle à l’aide d’un sort de guérison suprême, guérison de la cécité ou d’un pouvoir similaire.", "Les coups critiques du personnage aveuglent ses adversaires.", "Critique aveuglant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-assourdissant", "Quand le personnage réussit un coup critique, sa victime devient définitivement sourde. Un jet de Vigueur réussi permet d’être assourdi pendant seulement un round. Le DD du jet de Vigueur est égal à 10 + bonus de base à l’attaque du personnage. Ce don n’a aucun effet sur une créature déjà sourde. On peut soigner la surdité par un sort de guérison suprême, régénération, guérison de la surdité ou par un pouvoir similaire.", "Les coups critiques du personnage assourdissent ses adversaires.", "Critique assourdissant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cri-de-guerre", "Un nombre de fois par jour égal à son modificateur\r\nde Charisme, le personnage peut pousser un cri de guerre par\r\nune action rapide. Ce faisant, les alliés qui se trouvent dans un\r\nrayon de 9 mètres et qui peuvent l’entendre gagnent un bonus\r\nde moral de +1 aux jets d’attaque et un bonus de moral de +4\r\naux jets de sauvegarde contre la terreur. Cet effet dure 1 minute.\r\nSi un allié qui est sous les effets de ce don rate un jet de sauvegarde,\r\nil peut décider de mettre un terme à l’effet du cri de\r\nguerre sur lui-même pour relancer le jet raté. L’allié doit conserver\r\nle résultat du second jet, même s’il est pire. Chaque allié ne\r\npeut utiliser cet effet qu’une seule fois par utilisation de ce don.", "Le cri du personnage encourage ses alliés, les enjoignant à se battre.", "Cri de guerre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "crame-!-crame-!-crame-!", "Le gobelin inflige 1d4 points de dégâts supplémentaires lorsqu’il attaque avec du feu issu d’une source alchimique ou non-magique (comme des torches alchimiques ou non-magiques) et gagne un bonus de compétence de +4 à ses jets de Réflexes pour éviter de prendre feu ou pour éteindre les flammes lorsqu’il a pris feu. Les dégâts supplémentaires causés avec ce don ne s’appliquent pas aux attaques magiques (comme les bombes de l’alchimiste) ou aux dégâts d’aspersion.", "La passion du gobelin pour le feu et les incendies atteint un tout autre niveau.", "Crame ! Crame ! Crame !", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "couteau-de-sorcière-(mythique)", "Le personnage peut utiliser son couteau de sorcière comme un focaliseur supplémentaire pour tous ses sorts de sorcière et pas seulement ses sorts de protecteur, en augmentant leur DD de 1. Ce bonus se cumule avec celui appliqué aux sorts de protecteur et conféré par Couteau de sorcière, ce qui octroie un bonus total de +2 au DD des sorts de protecteur. De plus, la main qui tient le couteau de sorcière est toujours considérée comme étant libre lorsqu’il s’agit de déterminer s’il peut lancer des sorts et effectuer des attaques de contact.", "Le couteau de sorcière du personnage est autant un prolongement de son corps que de son pouvoir.", "Couteau de sorcière (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "critique-fébrile", "Quand le personnage réussit un coup critique, ses adversaires sont fiévreux pendant une minute. Les effets de ce don ne se cumulent pas mais des coups supplémentaires rallongent la durée de l’effet.", "Les critiques du personnage rendent ses adversaires fiévreux.", "Critique fébrile", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "enchaînement-(mythique)", "Lorsqu’il utilise Enchaînement ou Succession d’enchaînements, l’une des attaques du personnage peut cibler un adversaire situé à portée, mais pas forcément adjacent à celui qu’il a attaqué. Il peut dépenser une utilisation de pouvoir mythique quand il utilise Enchaînement ou Succession d’enchaînements pour continuer à effectuer des attaques contre les adversaires à portée, quelle que soit leur position, tant qu’il parvient à les toucher. Il ne peut utiliser cette aptitude pour attaquer un adversaire plus d’une fois par round.", "Le personnage enchaîne les coups sur tout adversaire à portée.", "Enchaînement (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "crocenjambe-à-distance", "Le personnage peut tenter un croc-en-jambe à distance par une action complexe avec n’importe quelle arme à distance et une pénalité de -2 au jet de manœuvre de combat. Ajoutez le modificateur de dextérité du personnage à la place de son  modificateur de force à sa BMO et ajoutez-y aussi des pénalités de distance doublées. Si lacible est à plus de 9m (6c), ajoutez une pénalité supplémentaire de -2. Si la tentative de croc-en-jambe est un succès, la cible subit aussi des dégâts équivalents à une attaque à distance réussie avec cette arme. Le personnage ne peut pas être renversé en échouant cette tentative de croc-en-jambe.", "Un tir dans la jambe fait tomber votre adversaire au sol.", "Croc-en-jambe à distance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-darmes-et-armures-magiques", "Le personnage peut créer n’importe quel type de bouclier, d’arme ou d’armure magique. Le processus de fabrication prend un jour par tranche de 1000 po du prix de base des modifications à apporter à l’objet. Le personnage doit acheter les matières premières nécessaires à la moitié de leur prix de base (Voir la page sur la création d'objets magiques). L’arme, l'armure ou le bouclier devant être enchanté doit obligatoirement être un objet de maître (son prix n’est pas inclus dans le total précédent).\r\nLe personnage peut également réparer des armes, des armures ou des boucliers cassés s’il remplit les conditions de création de ce type d’objet. La procédure à suivre reste la même, mais tous les chiffres (prix et durée) sont réduits de moitié.", "Le personnage sait fabriquer des armures, des boucliers et des armes magiques.", "Création d'armes et armures magiques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "deux-potions-d’un-coup-(mythique)", "Par une action de mouvement, le personnage peut prendre en main deux potions ou autres breuvages, ordinaires ou magiques, rangés dans une bourse, une bandoulière ou un contenant similaire (mais pas dans un sac à dos). Par une action simple, il peut boire deux potions ou autres breuvages. Il doit avoir les deux mains libres pour pouvoir utiliser ce don.", "Le personnage peut descendre en un clin d’oeil plusieurs breuvages d’affilée.", "Deux potions d’un coup (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "destruction-darme-supérieure", "Le personnage reçoit un bonus de +2 aux tests destinés à détruire un objet. Ce bonus s’ajoute à celui de la Science de la destruction. Quand le personnage tente de détruire une arme, un bouclier ou une armure, il applique tous les dégâts en excès au propriétaire de l’objet. Si le personnage décide de laisser l’objet avec 1 point de vie, son propriétaire ne subit pas le moindre dégât.", "Les coups dévastateurs du personnage traversent aussi bien les armes et les armures que leur propriétaire. Ces frappes terrifiantes brisent aussi bien les objets que la personne qui se trouve derrière.", "Destruction d'arme supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "destin-intrépide", "Le personnage gagne un bonus de +1 aux jets de sauvegarde contre les effets du registre des émotions. Ce bonus se cumule avec celui des dons Curiosité intrépide et Assurance intimidante.\r\nDe plus, une fois par jour, le personnage peut refaire un jet de sauvegarde ou d’attaque s’il a obtenu un 1 naturel. Si ce deuxième jet se traduit par une attaque ou une sauvegarde réussie, il peut faire un test d’Intimidation, par une action libre, pour démoraliser la cible de son attaque ou la créature qui l’a obligé à faire le jet de sauvegarde, tant que cette créature ou cette cible se trouve à moins de 9 m (6 c) de lui et qu’elle le voit et l’entend.\r\nCet effet ne s’applique pas aux jets de sauvegarde contre les pièges et autres objets.\r\nIl ne se cumule pas avec d’autres effets qui permettent de refaire un jet de sauvegarde ou un jet d’attaque. Le personnage ne peut relancer le dé qu’une seule fois.", "Le personnage est incroyablement doué pour éviter les catastrophes.", "Destin intrépide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "destin-glorieux", "Le personnage peut utiliser l’aptitude montée en puissance (voir page 12) une fois par jour (en ajoutant 1d6). Si le personnage est ou devient mythique, il peut utiliser sa montée en puissance une fois de plus par jour.", "Le personnage est promis à un destin glorieux qui dépasse l’entendement des aventuriers ordinaires et des simples mortels.", "Destin glorieux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "dents-acérées", "L’homme-rat gagne une attaque de morsure. Il s’agit d’une attaque naturelle principale qui inflige 1d3 points de dégâts.", "L’homme-rat est pourvu de dents particulièrement solides et acérées.", "Dents acérées", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "danseur-à-la-baguette", null, "Le personnage a été formé selon la tradition des mages de cour qui mêlent la danse et l’utilisation des baguettes magiques.", "Danseur à la baguette", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-danneaux-magiques", "Le personnage peut créer des anneaux magiques. Le processus de fabrication prend un jour par tranche de 1 000 po du prix de base. Le personnage doit acheter les matières premières nécessaires pour une valeur totale de la moitié du prix de base.\r\nLe personnage peut également réparer un anneau cassé s’il remplit les conditions de création de celui-ci. La procédure à suivre est la même, mais la réparation nécessite moitié moins de temps et de composantes matérielles.", "Le personnage sait fabriquer des anneaux magiques.", "Création d'anneaux magiques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "danse-du-derviche", null, "Le personnage a appris à transformer sa rapidité en puissance, même avec une lame pesante.", "Danse du derviche", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "curiosité-intrépide", "Le personnage gagne un bonus de +1 aux jets de sauvegarde contre les effets du registre des émotions. De plus, à chaque fois qu’il commence un round sous l’emprise d’un effet de terreur, il a droit à un nouveau jet de sauvegarde au début de son tour pour diminuer la sévérité de l’effet (de paniqué à effrayé, d’effrayé à secoué et de secoué à rien).", "Le personnage désire tant découvrir le monde qu’il en oublie toute prudence.", "Curiosité intrépide", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-dobjets-merveilleux", "Le personnage peut fabriquer une grande variété d’objets merveilleux. Le processus de fabrication prend un jour par tranche de 1000 po du prix de base de l’objet. Le personnage doit acheter les matières premières nécessaires à la moitié de leur prix de base (Voir la page sur la création d'objets magiques).\r\nLe personnage peut réparer un objet s’il est capable de le fabriquer. La procédure à suivre reste la même, mais le temps passé et le coût en matières premières sont réduits de moitié.", "Le personnage sait fabriquer des objets merveilleux, un type d’objets magiques.", "Création d'objets merveilleux", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-de-tatouages-magiques", null, "Le personnage sait créer des tatouages magiques.", "Création de tatouages magiques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-de-sceptres-magiques", "Le personnage peut fabriquer un sceptre magique. Le processus de fabrication prend un jour par tranche de 1000 po du prix de base de l’objet. Le personnage doit acheter les matières premières nécessaires à la moitié de leur prix de base (Voir la page sur la création d'objets magiques).", "Le personnage sait fabriquer des sceptres.", "Création de sceptres magiques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-de-bâtons-magiques", "Le personnage peut fabriquer un bâton magique. Le processus de fabrication prend un jour par tranche de 1000 po du prix de base de l’objet. Le personnage doit acheter les matières premières nécessaires (à la moitié de leur prix de base). À sa création, le bâton possède dix charges  (Voir la page sur la création d'objets magiques).", "Le personnage sait fabriquer des bâtons magiques.", "Création de bâtons magiques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-de-baguettes-magiques", "Le personnage peut fabriquer une baguette magique pour n’importe quel sort de niveau 4 ou moins qu’il connaît. Le processus de fabrication prend un jour par tranche de 1000 po du prix de base de l’objet. Le personnage doit acheter les matières premières nécessaires à la moitié de leur prix de base. À sa création, la baguette possède cinquante charges (Voir la page sur la création d'objets magiques).", "Le personnage sait fabriquer des baguettes magiques.", "Création de baguettes magiques", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "danse-du-boxeur", "Chaque fois que le personnage touche un adversaire avec une attaque à mains nues alors qu’il utilise École du boxeur, il peut se déplacer de 1,50 mètre sans provoquer d’attaque d’opportunité, tant qu’il se déplace dans une case adjacente à l’adversaire touché. S’il utilise ce don, il ne peut pas faire de pas de placement lors de son tour suivant.", "Le personnage a appris à plier et à mouvoir son corps pour éviter les attaques et se repositionner avantageusement.", "Danse du boxeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "course", "Lorsqu’il court, le personnage couvre une distance égale à cinq fois sa vitesse de déplacement normale (à condition qu’il ne porte aucune armure, une armure légère ou intermédiaire et qu’il transporte une charge intermédiaire ou moindre) ou quatre fois sa vitesse de déplacement (s’il porte une armure lourde ou s’il transporte une charge lourde). Lorsqu’il exécute un saut avec élan (voir la compétence Acrobaties), il bénéficie d’un bonus de +4 au test d’Acrobaties. Enfin, le personnage conserve son bonus de Dextérité à la CA quand il court.", "Le personnage a le pied léger.", "Course", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "enchaînement-surprise", "Quand le personnage utilise Enchaînement ou Succession d’enchaînements, le second adversaire qu’il attaque à son tour est privé de son bonus de Dextérité contre lui.", "Quand le personnage frappe un adversaire, il prend ses alliés par surprise.", "Enchaînement surprise", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "endurance-(mythique)", "Le bonus aux tests et aux jets de sauvegarde conférés par Endurance augmente de la moitié du grade du personnage. Il subit la moitié des dégâts lorsqu’il rate un test effectué pour éviter les dégâts non-létaux infligés par une marche forcée, la faim ou la soif, les environnements chauds ou froids, ou l’asphyxie. Le personnage peut dormir en armure lourde sans être fatigué le lendemain.", "L’endurance du personnage dépasse véritablement les limites physiques naturelles.", "Endurance (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "force-intimidante", "Le personnage ajoute son modificateur de Force en plus de son modificateur de Charisme lors de ses tests d’Intimidation.", "La force physique du personnage intimide les autres.", "Force intimidante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "forme-puissante-(mythique)", "Le personnage peut appliquer l’archétype simple de créature géante aux formes animales qu’il adopte. Cet avantage remplace celui conféré par Forme puissante. Par une action libre, le personnage peut dépenser une utilisation de pouvoir mythique pour combiner l’avantage de ce don avec celui de Forme puissante non-mythique pendant un nombre de rounds égal à son grade.", "Les formes que le personnage adopte avec forme animale sont bien plus impressionnantes que celles de ses équivalents naturels.", "Forme puissante (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "forte-montée-en-puissance-(mythique)", "Chaque fois qu’il utilise son aptitude de montée en puissance, le personnage ajoute 1 point au résultat de son dé.", "Les montées en puissance du personnage sont plus intenses.", "Forte montée en puissance (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fourberie-(mythique)", "Le bonus aux tests de Bluff et Déguisement conféré par Fourberie est augmenté de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il a obtenu un 20 naturel à l’un de ces tests. Le personnage doit décider s’il utilise cette aptitude avant de lancer le dé.", "L’impact des fourberies orchestrées par le personnage est incomparable.", "Fourberie (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fourberie", "Le personnage obtient un bonus de +2 sur tous ses tests de Bluff et de Déguisement. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence augmente de +4.", "Le personnage est particulièrement doué pour tromper les autres, que ce soit à l’aide d’un discours ou d’un habile déguisement.", "Fourberie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-de-titan-(mythique)", "Les coups à mains nues du personnage infligent des dégâts équivalents à ceux infligés par un individu plus grand d’une catégorie de taille. Pour chaque catégorie de taille de différence entre le personnage et la cible (si celle-ci est plus grande que lui), il gagne également un bonus de +1 aux tests de manœuvre offensive suivants : bousculade, entraînement, lutte, renversement, destruction et croc-en-jambe, et au DD du don Coup étourdissant.", "Les poings du personnage peuvent venir à bout des adversaires les plus titanesques.", "Frappe du titan (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-du-bouclier-(mythique)", "Lorsque le personnage utilise Frappe du bouclier pour effectuer une bousculade et que son adversaire se retrouve à terre à cause d’un obstacle situé sur sa trajectoire, il subit également les dégâts de la collision. Le montant de dégâts infligés est égal à 1d6 points par tranche de deux grades du personnage. Par une action immédiate, le personnage peut dépenser une utilisation de pouvoir mythique pour ajouter son grade aux tests de manoeuvre offensive lorsqu’il effectue une bousculade par le biais d’une Frappe du bouclier.", "Les frappes réalisées par le personnage avec son bouclier sont comme de puissants coups de butoir.", "Frappe du bouclier (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-du-bouclier", "Tout adversaire touché par le coup de bouclier du personnage est aussi victime d’une bousculade gratuite qui se substitue au jet d’attaque de la manœuvre offensive du personnage . Cette bousculade ne provoque pas d’attaque d’opportunité. Les adversaires qui ne peuvent pas reculer en raison d’un mur ou d’un autre obstacle tombent à terre après avoir parcouru la plus grande distance possible. Le personnage peut décider de se déplacer en même temps que son adversaire s’il a encore droit à un pas de placement de 1,50 m ou s’il peut utiliser une action pour se déplacer pendant son tour.", "Utilisé correctement, le bouclier du personnage lui permet de projeter ses adversaires dans les airs.", "Frappe du bouclier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-du-fléau-des-anges", null, "Le personnage canalise la puissance de son protecteur démoniaque à travers sa lame pour châtier les vertueux.", "Frappe du fléau des anges", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-décisive-(mythique)", "Lorsque le personnage utilise Frappe décisive, Science de la frappe décisive ou Frappe décisive supérieure, multipliez le bonus de Force, le bonus magique et les autres bonus normalement multipliés lors d’un coup critique par le nombre de dés de dégâts d’arme lancés grâce à ce don. Les dégâts supplémentaires qui ne sont normalement pas multipliés lors d’un coup critique ne le sont pas plus avec ce don.", "Les frappes réalisées par le personnage avec son bouclier sont comme de puissants coups de butoir.", "Frappe décisive (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-décisive-supérieure", "Quand le personnage recourt à cette action, il porte une unique attaque avec son bonus de base à l’attaque le plus élevé. Cette attaque inflige alors des dégâts supplémentaires : le personnage lance quatre fois les dés de dégâts et additionne leurs résultats avant d'ajouter les bonus découlant de sa Force ou des propriétés des armes (comme une arme de feu), les dégâts de précision et les autres bonus de dégâts. Ces dés de dégâts supplémentaires ne sont pas multipliés en cas de critique mais sont ajoutés au total.", "Le personnage peut faire une attaque qui inflige des dégâts incroyables.", "Frappe décisive supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-décisive", "Quand le personnage effectue une action d'attaque, il porte une unique attaque avec son bonus de base à l’attaque le plus élevé. Cette attaque inflige alors des dégâts supplémentaires : le personnage lance deux fois les dés de dégâts et additionne leurs résultats avant d'ajouter les bonus découlant de sa Force ou des propriétés des armes (comme une arme de feu), les dégâts de précision et les autres bonus de dégâts. Le dé de dégâts supplémentaire n'est pas multiplié en cas de critique mais est ajouté au total.", "Le personnage peut faire une attaque qui inflige des dégâts incroyables.", "Frappe décisive", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-inspirée", "Quand le personnage blesse une créature en utilisant le pouvoir de classe combat étudié, il peut dépenser une utilisation d’inspiration pour relancer un dé d’inspiration et augmenter les dégâts du résultat obtenu.", "Le personnage peut concentrer son talent naturel aussi bien sur l’efficacité de ses armes que sur ses compétences.", "Frappe inspirée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-magique-(mythique)", "Chaque fois qu’il utilise Frappe magique pour améliorer ses armes, l’effet dure une minute au lieu d’un round. S’il dépense une utilisation de pouvoir mythique quand il utilise Frappe magique, il peut également ajouter sur ses armes une propriété spéciale d’arme magique ayant un modificateur au prix de base de +1. Ce modificateur au prix de base s’élève à +2 au grade 4, +3 au grade 7 et +4 au grade 10.", "Grâce à ces incroyables talents en matière de magie profane, le personnage peut infliger des coups dévastateurs imprégnés d’énergie magique.", "Frappe magique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-magique", "Par une action rapide, le personnage peut imprégner ses armes d’une partie de son pouvoir. Pendant un round, ses armes infligent +1 point de dégâts et sont considérées comme magiques quand il s’agit de vaincre la réduction de dégâts. Ce bonus augmente de +1 pour chaque tranche de cinq niveaux de lanceur de sorts, avec un maximum de +5 au niveau 20.", "Le personnage puise dans ses pouvoirs magiques pour améliorer ses armes.", "Frappe magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-mortelle-(mythique)", "Lorsqu’il effectue une attaque de Frappe mortelle, le personnage peut dépenser une utilisation de pouvoir mythique pour tuer instantanément une créature vivante non-mythique. Un jet de Vigueur réussi (DD 10 + le bonus de base à l’attaque(BBA) + le grade du personnage) annule cet effet, même si la Frappe mortelle inflige toujours le double de dégâts et les points de saignement en Constitution. Les créatures artificielles, les vases et les créatures immunisées aux coups critiques ou aux dégâts de précision ne peuvent être tuées à l’aide de ce don.", "Le personnage peut tuer d’un unique coup.", "Frappe mortelle (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-mortelle", "Par une action simple, le personnage porte une unique attaque avec son Arme de prédilection supérieure contre un adversaire étourdi ou pris au dépourvu. S’il le touche, il lui inflige le double des dégâts habituels et la cible subit un saignement de 1 point de Constitution. Les dégâts supplémentaires et le saignement ne sont pas multipliés en cas de critique.", "Le personnage peut mettre un terme brutal aux souffrances de ses adversaires, d’un unique coup bien placé.", "Frappe mortelle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "grâce-supplémentaire", "Le personnage choisit une grâce supplémentaire. Quand il utilise imposition des mains, la créature qu’il soigne bénéficie aussi des effets de cette grâce.", "Quand le personnage utilise imposition des mains, il ajoute une grâce supplémentaire.", "Grâce supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "grâce-du-chat", "L’homme-félin gagne un bonus de +2 au DMD contre les manoeuvres offensives de bousculade, de lutte, de renversement, de repositionnement et de croc-en-jambe.", "La grâce naturelle de l’homme-félin lui permet de se sortir des situations les plus difficiles.", "Grâce du chat", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "grimpeur-bien-accroché", "Lors d’une action d’escalade et par une action de mouvement, le personnage peut se tenir contre une surface à l’aide de ses jambes uniquement en réussissant un test de compétence en escalade (DD = DD de la surface à escalader). Si le jet est réussi, il peut ensuite effectuer des attaques avec une arme à distance portée à deux mains et recharger ses armes à distances jusqu’à ce qu’il reprenne son escalade. Le MJ peut décider que certaines surfaces ne peuvent pas bénéficier de ce don.", "Utilisant les prises disponibles et la force brute de ses muscles, le personnage peut tenir contre une paroi, une corde ou une échelle avec ses jambes uniquement afin de libérer ses deux mains.", "Grimpeur bien accroché", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "grande-haine", "Le personnage gagne un bonus supplémentaire de +1 aux jets d’attaque des armes de corps à corps et de jet contre les cibles de sa haine.", "La haine du personnage brûle incroyablement fort.", "Grande haine", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "grand-veneur", "Si le personnage possède le pouvoir compagnon animal, il choisit quel type d’animal affecter parmi les suivants : oiseaux, chiens, petits félins ou chevaux. S’il possède le pouvoir pacte divin (monture) ou monture, le don affecte toujours les chevaux.\r\nLe personnage gagne un bonus de +2 aux tests de Dressage et de Connaissances (nature) quand ils concernent les créatures du type choisi. De plus, il est considéré comme étant d’un niveau de plus qu’en réalité quand il détermine les aptitudes de son compagnon animal ou de sa monture, tant qu’il est du type choisi.", "Le personnage est un grand dresseur de chevaux, de chiens, de faucons ou de félins.", "Grand veneur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "gouts-variés", "Le dhampir choisit un [type humanoïde|sous-type humanoïde] ou le type humanoïde monstrueux. Il peut utiliser son don Buveur de sang sur les créatures de ce type ou sous-type.", "Le dhampir développe un goût pour le sang plus étendu que celui des autres dhampirs.", "Gouts variés", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "force-intimidante-(mythique)", "Le personnage gagne un bonus aux tests d’Intimidation égal à son grade contre les créatures non-mythiques, ou la moitié de son grade contre les créatures mythiques. Si le personnage possède le don Persuasif (mythique), il peut dépenser une utilisation de pouvoir mythique pour considérer que le jet de dé donne un 20 naturel. Le personnage doit décider s’il utilise cette aptitude avant d’effectuer le jet.", "La stature mythique du personnage est plutôt perturbante.", "Force intimidante (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fureur-de-la-méduse", "Quand le personnage effectue une attaque à outrance et porte au moins une attaque à mains nues, il peut faire deux attaques à mains nues supplémentaires avec son bonus de base à l’attaque maximal. Ces attaques en bonus ne peuvent être menées que contre une cible chancelante, étourdie, hébétée, inconsciente, paralysée ou prise au dépourvu.", "Le personnage profite de la confusion de son adversaire pour lui porter une multitude de coups.", "Fureur de la méduse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frisson-de-la-mort", "Quand le personnage est enragé et que ses attaques font tomber un ennemi en dessous de 0 point de vie ou le tuent, il récupère 1 round de rage. En revanche, il peut utiliser ce don uniquement si son ennemi a au moins autant de DV que lui. Il ne peut bénéficier de ce don qu’une fois par round.", "Les meurtres du personnage alimentent sa rage.", "Frisson de la mort", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fraternité-animale", "Le personnage obtient un bonus de +2 sur tous ses tests de Dressage et d’Équitation. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence augmente de +4.", "Le personnage est doué avec les animaux et les montures.", "Fraternité animale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fraternité-animale-(mythique)", "Le bonus aux tests de Dressage et d’Équitation conféré par Fraternité animale augmente de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour parler avec les animaux comme s’il utilisait communication avec les animaux. Le niveau de lanceur de sorts de cet effet est égal au double de son grade.", "L’affinité qu’a développée le personnage envers les animaux sauvages est si importante qu’il peut leur parler.", "Fraternité animale (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-puissante", "Les attaques du personnage utilisant une des armes choisies pour Arme de prédilection ignorent jusqu’à 5 points de réduction de dégâts. Ce don ne s'applique pas aux RD non typées (telles que RD 10/-).", "Les attaques du personnage traversent la défense de certaines créatures. Le personnage doit choisir une arme pour laquelle il dispose déjà du don Arme de prédilection.", "Frappe puissante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-puissante-supérieure", "Les attaques du personnage utilisant une des armes choisies pour Arme de prédilection ignorent jusqu’à 10 points de réduction de dégâts. Cette réduction est diminuée de 5 contre les RD non typées (telles que RD 10/-).", "Les attaques du personnage pénètrent les défenses de la plupart des adversaires. Le personnage doit choisir une arme pour laquelle il dispose déjà du don Frappe puissante.", "Frappe puissante supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "frappe-puissante-(mythique)", "Lorsqu’il utilise Frappe puissante ou Frappe puissante supérieure, le personnage peut ignorer un point supplémentaire de réduction des dégâts pour chaque tranche de trois grades possédés. De plus, les effets s’appliquent aux réductions de dégâts dénuées de type (telle que RD 10/- par exemple).", "Le personnage sait exactement comme frapper pour ignorer les méthodes défensives les plus sûres et les armures les plus lourdes.", "Frappe puissante (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fureur-de-la-méduse-(mythique)", "Le personnage peut remplacer les deux attaques à mains nues supplémentaires conférées par Fureur de la méduse par une unique attaque à mains nues effectuée avec son bonus de base à l’attaque le plus élevé. S’il parvient à toucher son adversaire, celui-ci doit réussir un jet de Vigueur (DD 10 + ½ du niveau du personnage + modificateur de Sagesse du personnage) pour ne pas être chancelant pendant un round. Le personnage peut dépenser une utilisation de pouvoir mythique lorsqu’il fait chanceler une cible avec Fureur de la méduse pour prolonger la durée de l’état chancelant d’un nombre de rounds égal à la moitié de son grade.", "Le déluge d’attaques à mains nues du personnage ne forme plus qu’un unique coup qui force la cible à chanceler.", "Fureur de la méduse (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "flexibilité-innée", null, "La magie innée du personnage est flexible.", "Flexibilité innée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "flamme-intérieure", "Le bonus de l’ifrit à ses jets de sauvegarde contre les attaques de feu et les sorts du registre feu ou lumière passe à +4. Lorsqu’il utilise Armes ardentes, les armes affectées infligent 1d6 points de dégâts de feu supplémentaires au lieu de 1, et lorsqu’il agrippe un adversaire, l’ifrit inflige ces dégâts à l’adversaire qu’il tient lors de son tour.", "Le corps de l’ifrit génère tellement de chaleur que son simple contact brûle ses ennemis.", "Flamme intérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fiélon-sanguinaire", "Quand le personnage est enragé et confirme un coup critique avec une arme de corps à corps ou que quelqu’un confirme un coup critique contre lui (avec une arme de corps à corps, un sort ou une arme à distance), il récupère un round de rage (dans la limite de son maximum quotidien). Il profite de cet avantage une fois par round seulement.", "Les blessures béantes font chanter le sang du personnage, qu’elles s’ouvrent sur son corps ou sur celui de ses ennemis.", "Fiélon sanguinaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "extension-de-pouvoir-magique", "Ce don s’applique à un des pouvoirs magiques de la créature, qui doit vérifier les conditions décrites ci-dessous. Trois fois par jour (ou moins souvent si la créature est normalement limitée à une ou deux utilisations quotidiennes du pouvoir), le pouvoir choisi peut être utilisé avec des effets étendus.\r\nLorsque les effets d’un pouvoir sont étendus, toutes ses quantités numériques variables sont augmentées de 50%. Les jets de sauvegarde et les tests opposés ne sont pas concernés. Les pouvoirs magiques qui ne comportent pas de quantités aléatoires ne sont pas affectés.\r\nLe pouvoir magique choisi doit imiter un sort dont le niveau est inférieur ou égal à la moitié du NLS de la créature (arrondi vers le bas) moins 2. Le tableau inclus ci-dessous présente les restrictions imposées par cette condition.", "Un des pouvoirs magiques de la créature est particulièrement puissant et efficace.", "Extension de pouvoir magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "extension-de-portée", "Le personnage peut modifier un sort de courte, moyenne ou longue portée afin de doubler celle-ci. Une fois la portée étendue, une courte portée devient alors égale à 15 m + 1,5 m/niveau, une portée moyenne à 60 m + 6 m/niveau et une longue portée à 240 m + 24 m/niveau. Un sort à extension de portée nécessite un emplacement de sort d’un niveau de plus que son niveau réel.\r\nLes sorts dont la portée ne s’exprime pas par une mesure physique ou qui n’est ni courte, ni moyenne, ni longue, ne peuvent être affectés par ce don.", "Le personnage augmente la portée de ses sorts.", "Extension de portée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "extension-de-durée", "Un sort à extension de durée dure deux fois plus longtemps qu’indiqué dans sa description. Les sorts permanents, instantanés et ceux dont la durée dépend de la concentration du personnage ne sont pas concernés. Un sort à extension de durée nécessite un emplacement de sort d’un niveau de plus que son niveau réel.", "Les sorts du personnage durent deux fois plus longtemps.", "Extension de durée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "expertise-du-combat", "Le personnage peut choisir de subir un malus de -1 aux jets d’attaque au corps à corps et aux manœuvres offensives en échange d’un bonus d’esquive de +1 à la CA. Quand son bonus de base à l’attaque atteint +4, et, par la suite, pour chaque tranche de bonus de +4, le malus augmente de -1 et le bonus d’esquive de +1. Le personnage ne peut choisir d'utiliser ce don qu'au moment où il entreprend une action d'attaque ou une attaque à outrance , avec une arme de mêlée. Les effets de ce don se poursuivent jusqu’au prochain tour du personnage.", "Le personnage améliore sa défense au détriment de sa précision.", "Expertise du combat", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "expertise-du-combat-(mythique)", "Lorsqu’il utilise Expertise du combat, le personnage gagne un bonus d’esquive supplémentaire de +2 à sa classe d’armure. Il peut dépenser une utilisation de pouvoir mythique pour annuler pendant une minute les malus aux jets d’attaque au corps à corps et aux tests de manœuvre offensive imposés par l’Expertise du combat.", "Le personnage évite tous les coups avec talent et défiance.", "Expertise du combat (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esquive", "Le personnage bénéficie d’un bonus d’esquive de +1 à la CA (et donc au DMD). Il perd automatiquement ce bonus s’il se trouve dans une situation où il perd son bonus de Dextérité à la CA.", "L’entraînement et les réflexes du personnage lui permettent de réagir rapidement quand il s’agit d’éviter une attaque.", "Esquive", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "extension-de-zone-deffet", "Le personnage peut modifier un sort dont la zone d’effet est une émanation, une étendue, une ligne ou un rayonnement afin d’augmenter la surface couverte. Toutes les données numériques déterminant la surface de la zone d’effet sont doublées. Un sort à extension de zone d’effet nécessite un emplacement de sort de trois niveaux de plus que son niveau réel.\r\nLes sorts dont la zone d’effet n’est pas d’un des quatre types cités ne peuvent être affectés par ce don.", "Les sorts lancés par le personnage affectent une zone plus étendue.", "Extension de zone d'effet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esquive-(mythique)", "Le bonus à la CA conféré par Esquive augmente de +1. Par une action immédiate, le personnage peut dépenser une utilisation de pouvoir mythique pour bénéficier d’un bonus d’esquive supplémentaire de +10 à la CA contre une attaque.", "Lorsqu’il reste attentif, le personnage devient pratiquement impossible à toucher au combat.", "Esquive (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esprit-humain", "Le personnage reçoit un rang de compétence supplémentaire. Il en obtient un autre dès qu’il gagne un nouveau dé de vie, mais il ne peut pas gagner plus de quatre rangs de compétence de cette manière.", "La passion et le désir d’amélioration des ancêtres humains du personnage bouillonnent dans ses veines.", "Esprit humain", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esprit-elfique", "Le personnage possède le trait racial des elfes magie elfique, qui lui donne un bonus racial de +2 aux tests de niveau de lanceur de sorts pour vaincre la résistance à la magie. De plus, il reçoit un bonus racial de +2 aux tests d’Art de la magie pour identifier les objets magiques. Le personnage peut choisir un autre trait, que les elfes peuvent échanger contre magie elfique, à la place de celui-ci.", "Le personnage est de sang mêlé, mais il est plus proche de ses ancêtres elfiques et leur magie coule librement dans ses veines.", "Esprit elfique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esprit-des-étendues-sauvages", "Quand le personnage se trouve dans un environnement choisi pour le don Harmonie sauvage, il gagne un bonus de +4 aux tests de Perception qui lui permettent d’agir pendant le round de surprise. S’il agit pendant ce round de surprise, il gagne perception aveugle dans un rayon de 9 mètres (6 c) pendant le round de surprise. S’il se trouve dans une zone qui compte comme plusieurs environnements, les bonus ne se cumulent pas.", "Le lien mystique qui unit le personnage à la nature se renforce un peu plus.", "Esprit des étendues sauvages", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "entraînement-guerrier-elfique", "Le personnage est formé au maniement des armes traditionnelles elfiques (arc long, arc long composite, épée longue, rapière, arc court, arc court composite et toutes les armes qui contiennent l’adjectif « elfique » dans leur nom). Il reçoit un bonus de +2 au DMD contre les manoeuvres de désarmement et de destruction qui visent ces armes. De plus, s’il manie l’une des armes de corps à corps de la liste, il a droit à une attaque d’opportunité de plus par round (ce bonus se cumule avec Attaques réflexes).", "Le personnage a suivi un entraînement spécial qui lui permet de manier toutes sortes d’armes traditionnelles elfiques.", "Entraînement guerrier elfique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "enquêteur-amateur", "Comme l’enquêteur, le personnage a la possibilité\r\nd’améliorer ses tests de Connaissances, de Linguistique et Art de la magie. Il gagne une réserve d’inspiration égale à son modificateur\r\nd’Intelligence. Il peut dépenser une utilisation d’inspiration\r\npar une action libre pour ajouter 1d6 au résultat d’un test de\r\nConnaissances, de Linguistique ou d’Art de la magie à partir du moment\r\noù il est formé à la compétence (même s’il décide de faire 10\r\nou 20 sur ce test). Il fait son choix après avoir fait son jet mais avant\r\nd’en connaître le résultat. Il ne peut utiliser inspiration qu’une seule\r\nfois par test de compétence. Sa réserve d’inspiration se réinitialise\r\nchaque jour, généralement après une bonne nuit de sommeil.", "Le savoir du personnage est plus que de la simple intelligence : il est inspiré.", "Enquêteur amateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "endurance", "Le personnage bénéficie d’un bonus de +4 à chacun des tests et jets de sauvegarde suivants :\r\n* test de Natation pour résister à des dégâts temporaires dus à la fatigue,\r\n* tests de Constitution pour continuer à courir,\r\n* tests de Constitution pour éviter les dégâts non-létaux infligés par une marche forcée,\r\n* tests de Constitution pour retenir sa respiration,\r\n* tests de Constitution pour éviter les dégâts non-létaux infligés par la famine ou la soif,\r\n* jets de Vigueur pour éviter les dégâts non-létaux infligés par les climats chauds ou froids et\r\n* jets de Vigueur pour résister aux dégâts infligés par l’asphyxie.\r\nLe personnage peut également dormir en armure légère ou intermédiaire sans être fatigué le lendemain.", "Le personnage résiste particulièrement bien aux efforts prolongés et aux conditions difficiles.", "Endurance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "esprit-libre", null, "Le personnage croit si fort à la liberté que cela le protège contre les entraves physiques et mentales.", "Esprit libre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "enchaînement", "Par une action simple, le personnage porte une unique attaque avec la totalité de son bonus d’attaque à un adversaire situé à sa portée. S’il le touche, il lui inflige les dégâts habituels et peut faire une attaque supplémentaire (toujours avec son bonus d’attaque maximal) contre un adversaire adjacent au premier ennemi et situé à portée du personnage. Ce don ne permet qu’une attaque supplémentaire par round. Quand le personnage utilise ce don, il subit un malus de -2 à la CA jusqu’à son prochain tour.", "Le personnage peut frapper deux adversaires adjacents d’un seul mouvement.", "Enchaînement", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "extension-deffet", "Toutes les variables numériques et aléatoires d’un sort bénéficiant d'une extension d’effet augmentent de 50%, y compris les bonus à ces jets de dés.\r\nLes jets de sauvegarde et les jets opposés ne sont pas affectés, ni les sorts sans variable numérique aléatoire. Un sort à extension d’effet nécessite un emplacement de sort de deux niveaux de plus que son niveau réel.", "Le personnage augmente la puissance de ses sorts qui infligent donc plus de dégâts.", "Extension d'effet", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "familier-supérieur-(mythique)", "Pour chaque tranche de trois grades du personnage, son familier gagne un bonus de +2 à une valeur de caractéristique de son choix. Ce bonus se cumule avec les autres. Le familier ajoute le grade du personnage à son bonus d’armure naturelle et à sa résistance à la magie (le cas échéant).", "Le familier du personnage bénéficie de la résistance et des intuitions phénoménales de son maître.", "Familier supérieur (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fioritures-rhétoriques-(mythique)", "Lorsqu’il réussit à utiliser Fioritures rhétoriques contre une créature non-mythique, le personnage ajoute son grade en bonus au test de Diplomatie. Si la cible est une créature mythique, le personnage bénéficie à la place d’un bonus égal à la moitié de son grade. Le personnage peut dépenser une utilisation de pouvoir mythique pour relancer un test de Bluff raté. Il doit conserver le résultat du second jet, même s’il est pire.", "Le personnage sait suprêmement bien tourner ses phrases et faire preuve de finesse pour influencer autrui.", "Fioritures rhétoriques (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fils-de-lombre", "Le drow choisit l’un des sorts suivants : dissipation de la magie, faveur divine ou suggestion. Il peut l’utiliser une fois par jour comme un pouvoir magique. Son niveau de lanceur de sorts est égal à son niveau.", "À mesure que le drow s’élève et prend de l’ascendant parmi son peuple, de nouveaux pouvoirs magiques lui sont accessibles.", "Fils de l'ombre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fils-de-lombre-supérieur", "Le drow peut utiliser les pouvoirs magiques dissipation de la magie, faveur divine et suggestion une fois par jour. Son niveau de lanceur de sorts est égal à son niveau.", "Le drow maîtrise la noble magie drow.", "Fils de l'ombre supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "feu-nourri", "Quand le personnage fait une attaque à outrance avec un arc, il peut tirer deux flèches lors de sa première attaque. S’il touche, les deux flèches atteignent leur cible. Les dégâts de précision (comme les attaques sournoises) et les coups critiques s’appliquent à une seule flèche. Les bonus aux dégâts des arcs composites qui offrent un bonus de Force s’appliquent aux deux flèches, comme les autres bonus de dégâts (ennemi juré pour les rôdeurs par exemple). La réduction de dégâts et les résistances s’appliquent séparément à chaque flèche.", "Le personnage peut tirer plusieurs flèches sur une cible unique.", "Feu nourri", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "feu-nourri-(mythique)", "Lorsqu’il effectue une attaque à outrance avec un arc en utilisant Feu nourri, le personnage peut tirer deux flèches à chaque fois lors de sa première et de sa deuxième attaque au lieu de deux flèches uniquement lors de la première attaque.", "Le personnage peut tirer sans problème un barrage de flèches sur sa cible.", "Feu nourri (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "festin-sanglant", "Si le dhampir utilise son don Buveur de sang pour absorber 4 points de Constitution ou plus d’une créature vivante, il gagne un bonus de +2 à ses jets de dégâts et un bonus de +1 à ses tests de compétences basées sur la Force. Ce bonus dure un nombre de rounds égal à la moitié de ses dés de vie.", "La consommation de sang accorde une force surhumaine au dhampir.", "Festin sanglant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fabuleuses-chimères-(mythique)", "Les créatures non-mythiques ne peuvent identifier à l’aide d’un test d’Art de la magie (y compris avec vision magique, détection de la magie ou des effets similaires) la nature illusoire des sorts d’illusion lancés par le personnage. Lorsque des créatures mythiques tentent de repérer la nature illusoire des sorts d’illusion du personnage, ajoutez son grade au DD des tests d’Art de la magie. Les créatures mythiques informées par leurs alliés que les illusions du personnage ne sont pas réelles gagnent un bonus de +2 aux jets de Volonté pour tenter de les réfuter. Les créatures non-mythiques ne gagnent aucun bonus lorsqu’elles tentent de réfuter de cette manière.", "Les illusions créées par le personnage sont incroyablement réalistes.", "Fabuleuses chimères (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fente", ". Le personnage peut augmenter la portée de ses attaques de corps à corps d’un mètre cinquante jusqu’à la fin de son tour en acceptant un malus de -2 à la CA jusqu’au prochain tour. Il doit décider s’il veut utiliser ce don ou non avant la moindre attaque.", "Le personnage frappe des adversaires qui devraient être hors de portée.", "Fente", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "feinte-supérieure", "Quand le personnage feinte pour faire perdre le bonus de Dextérité à la CA de son adversaire, ce dernier le perd pour la prochaine attaque du personnage qui l’a feintée mais, en plus, ne le récupère pas avant le début du prochain tour du personnage.", "Le personnage est doué pour amplifier les réactions de ses adversaires à ses attaques.", "Feinte supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "feinte-brûlante", "L’ifrit gagne un bonus de +2 à ses tests de feinte lorsqu’il manie une arme infligeant des dégâts de feu. Chaque fois qu’il réussit une feinte sur une créature alors qu’il utilise ce genre d’armes, il peut infliger ses dégâts de feu à l’ennemi.", "Les ennemis de l’ifrit reculent devant la chaleur de ses armes, lui donnant ainsi l’opportunité de passer leurs défenses.", "Feinte brûlante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "faux-focalisateur", null, "Le personnage peut utiliser un focalisateur divin pour lancer des sorts profanes.", "Faux focalisateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fausse-incantation", null, "Le personnage peut tromper ceux qui l’observent en leur faisant croire qu’il lance un sort alors qu’il utilise un objet magique.", "Fausse incantation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fauconnerie-ancestrale", null, "Le personnage connaît les secrets de la fauconnerie développée par une famille célèbre.", "Fauconnerie ancestrale", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fantôme-dombre", "Le fetchelin gagne la capacité à utiliser traversée des ombres une fois de plus par jour.", "Le fetchelin peut se déplacer plus souvent entre le plan de l’Ombre et le plan Matériel.", "Fantôme d'ombre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "fente-(mythique)", "Chaque fois que le personnage utilise Fente et touche une créature à l’aide d’une attaque au corps à corps, il ne subit plus le malus de -2 à la CA contre cette créature. Il peut dépenser une utilisation de pouvoir mythique lorsqu’il utilise Fente pour annuler le malus de -2 à sa CA quel que soit le résultat de son attaque et pour gagner un bonus de +2 aux jets d’attaque d’opportunité effectués pendant toute la durée de Fente.", "La longue portée des frappes réalisées par le personnage lui permet de renforcer ses défenses contre les adversaires qu’il parvient à toucher.", "Fente (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "guérillero", "Quand une créature prise au dépourvue ignore que le personnage est présent et que ce dernier réussit une manoeuvre de lutte contre elle, il peut tenter une seconde manoeuvre de lutte pour immobiliser la créature par une action libre. S’il possède le don Étreinte étouffante, il peut à la place utiliser ce second test de manoeuvre offensive pour se servir du don.", "Le personnage peut maîtriser un adversaire sans méfiance en un éclair.", "Guérillero", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "course-(mythique)", "Lorsqu’il court, le personnage se déplace d’une distance équivalente à sept fois sa vitesse de déplacement normale s’il porte une armure légère, intermédiaire ou aucune armure et s’il ne transporte pas une charge lourde. Il se déplace d’une distance équivalente à six fois sa vitesse de déplacement s’il porte une armure lourde ou transporte une charge lourde. Lorsqu’il saute après avoir couru il gagne un bonus supplémentaire égal à son grade + 4 au test d’Acrobaties. Le personnage peut courir normalement pendant un nombre de rounds égal à sa valeur de Constitution plus son grade, mais une fois cette période écoulée, il doit réussir ses tests de Constitution pour poursuivre sa course.", "La vitesse et l’endurance du personnage lorsqu’il court sont surnaturelles.", "Course (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-étourdissant-(mythique)", "Le DD du Coup étourdissant augmente de la moitié du grade du personnage et celui-ci peut utiliser ce don plusieurs fois au cours d’un même round. Par une action libre, il peut dépenser une utilisation de pouvoir mythique pour utiliser Coup étourdissant sans dépenser une utilisation quotidienne du don.", "Les coups de poing du personnage étourdissent ses adversaires avec une régularité et une efficacité renforcées.", "Coup étourdissant (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-puissante-supérieure", "Les attaques que le personnage porte avec l’arme choisie ignorent jusqu’à 10 points de réduction de dégâts. Cette somme est réduite à 5 points pour la réduction de dégâts sans type particulier (comme RD 10/—).", "Les attaques du personnage pénètrent les défenses de la plupart des adversaires. Le personnage choisit une attaque pour laquelle il possède le don Attaque puissante.", "Attaque puissante supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-éclair-(mythique)", "Lorsqu’il utilise Attaque éclair, le personnage n’a plus besoin de parcourir au moins 3 mètres avant d’effectuer la première attaque. S’il dépense une utilisation de pouvoir mythique lorsqu’il entame une Attaque éclair, le déplacement qu’il effectue pendant l’Attaque éclair ne provoque pas d’attaque d’opportunité.", "Le personnage traverse le champ de bataille à grande vitesse, tel une tornade indistincte de lames et d’acier.", "Attaque éclair (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-éclair", "En une action complexe, le personnage peut se déplacer à sa vitesse de base et faire une attaque de corps à corps sans provoquer d’attaque d’opportunité de la part de sa cible. Il peut se déplacer avant et après l’attaque mais il doit parcourir au moins trois mètres avant d’attaquer et la distance parcourue au total ne doit pas dépasser sa vitesse de base. Le personnage ne peut pas utiliser ce don pour attaquer un ennemi qui se trouve sur une case adjacente à la sienne au début du tour.", "Le personnage peut s’approcher d’un adversaire, le frapper et battre en retraite avant que ce dernier n’ait le temps de réagir.", "Attaque éclair", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaques-réflexes-(mythique)", "Le personnage peut réaliser un certain nombre d’attaques d’opportunité supplémentaires chaque round. Par une action rapide, il peut dépenser une utilisation de pouvoir mythique pour effectuer jusqu’au début de son prochain tour des attaques d’opportunité contre des adversaires qu’il a déjà attaqués au cours du round si ceux-ci provoquent des attaques d’opportunité de sa part en se déplaçant.", "Le personnage exécute un coup vicieux dès qu’il repère une faille dans les défenses de ses adversaires.", "Attaques réflexes (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaques-réflexes", "À chaque round, le personnage a droit à un nombre d’attaques d’opportunité supplémentaires égal à son bonus de Dextérité. Le personnage peut faire une attaque d’opportunité même s’il est pris au dépourvu.\r\nNormal. Un personnage ne possédant pas ce don n’a droit qu’à une attaque d’opportunité par round et il ne peut pas le faire s’il est pris au dépourvu.", "Le personnage peut faire des attaques d’opportunité supplémentaires.", "Attaques réflexes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "augmentation-dintensité", "Ce don permet d’amplifier l’intensité du sort choisi en augmentant son niveau effectif (d’un ou plusieurs niveaux, sans toutefois dépasser le 9e). Contrairement aux autres dons de métamagie, il augmente réellement le niveau de sort modifié. Toutes les propriétés du sort qui dépendent de son niveau (comme le DD de son jet de sauvegarde et sa capacité à traverser un globe d'invulnérabilité partielle) sont calculées en fonction de son nouveau niveau, choisi par le personnage. Un sort à intensité augmentée nécessite un emplacement de sort égal à son nouveau niveau effectif.", "Le personnage lance ses sorts comme s’ils étaient d’un niveau supérieur.", "Augmentation d'intensité", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aura-ardente", "Lorsqu’il utilise Armes ardentes lors de son tour, par une action libre, l’ifrit peut créer une aura de chaleur durant 1 round. Cette aura inflige 1d6 points de dégâts de feu à toute créature qui commence son tour adjacente à l’ifrit.", "Un brasier fait rage à l’intérieur de l’ifrit et génère une chaleur intense qui irradie de son corps.", "Aura ardente", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "autonome-(mythique)", "Le bonus aux tests de Premiers secours et de Survie conféré par Aptitude magique augmente de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il obtient un 20 naturel lorsqu’il effectue l’un de ces tests. Le personnage doit décider s’il utilise cette aptitude avant de lancer le dé.", "Le personnage est naturellement indépendant et il est passé maître dans l’art de subvenir seul à ses besoins.", "Autonome (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "autonome", "Le personnage obtient un bonus de +2 sur tous ses tests de Premiers secours et de Survie. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence augmente de +4.", "Le personnage sait se débrouiller dans la nature et soigner ses plaies.", "Autonome", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "battu-par-les-vents", null, "Le personnage a passé sa vie à résister aux coups de vent et aux tempêtes, ainsi son corps a appris à supporter les éléments.", "Battu par les vents", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bec-de-sang", "Le tengu augmente les dégâts de son attaque de bec à 1d6. En outre, lorsqu’il confirme un coup critique avec une attaque de bec, il inflige également 1 point de saignement.", "Les attaques de saignement du tengu sont sanglantes et dangereuses.", "Bec de sang", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bienfait-du-croyant", "Le personnage choisit un domaine qu’accorde sa divinité. Une fois par jour, il peut utiliser tous les pouvoirs de domaine de niveau 1 accordés aux prêtres de ce domaine. Le personnage ne gagne accès qu’à cette capacité, pas aux pouvoirs de domaine de niveau supérieur, ni aux sorts de domaine ou aux compétences de classe supplémentaires que le domaine peut accorder, ni à aucun des dons supplémentaires indiqués dans la liste des pouvoirs accordés. Le niveau de prêtre effectif du personnage au regard de ce pouvoir est de 1.", "Sa divinité récompense le personnage pour sa loyauté.", "Bienfait du croyant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "blessant", "Quand le personnage réussit à démoraliser un adversaire à portée d’allonge avec un test d’Intimidation, il peut effectuer une unique attaque au corps à corps contre cette créature par une action libre. S’il n’arrive pas à blesser la cible, cette dernière est immédiatement débarrassée de la condition secoué acquise lors de la démoralisation.", "Le personnage sait comment ajouter les blessures aux insultes.", "Blessant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "boire-c’est-vivre-(mythique)", "Le personnage ne subit plus aucun malus infligé par la consommation d’alcool non-magique. Il peut consommer une boisson alcoolisée par une action rapide au lieu d’une action de mouvement. Chaque fois qu’il consomme une telle boisson, il gagne l’un des avantages de panacée universelle (Art de la magie). Il ne peut pas bénéficier de plusieurs effets de ce sort à la fois. S’il dépense une utilisation de pouvoir mythique tout en buvant un breuvage alcoolisé, le personnage peut récupérer deux points de caractéristique, annuler un affaiblissement temporaire sur une valeur de caractéristique ou supprimer l’un des états préjudiciables suivants : confus, nauséeux, épuisé, fatigué, effrayé, nauséeux, empoisonné, secoué, fiévreux, malade ou chancelant.", "Les boissons fortes alimentent le feu mythique qui brûle à l’intérieur du personnage.", "Boire, c’est vivre (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bond-griffu", "Lorsqu’il charge, l’homme-félin peut effectuer une attaque à outrance avec ses griffes.", "L’homme-félin peut charger et porter une attaque avec ses pattes.", "Bond griffu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "boucherie-impitoyable", "Par une action simple qui ne provoque pas d’attaque d’opportunité, le personnage peut tenter de porter un coup de grâce à un adversaire recroquevillé, sans défense ou étourdi qu’il a désigné comme sa cible étudiée.", "Du fait de son talent pour le massacre, le personnage abat les adversaires sans défense avant que ceux-ci ne puissent récupérer.", "Boucherie impitoyable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-contre-les-projectiles-(mythique)", "Lorsqu’il utilise Bouclier contre les projectiles, le personnage peut, chaque round, parer un nombre d’attaques à distance supplémentaires égal à la moitié de son grade. Il peut dépenser une utilisation de pouvoir mythique par une action immédiate pour parer un unique rayon produit par un sort ou un effet sous forme de rayon qui le prend pour cible.", "La réactivité stupéfiante du personnage lui permet de parer plusieurs attaques à distance, même les rayons magiques.", "Bouclier contre les projectiles (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bénédiction-de-guerre", "Le personnage choisit deux bénédictions de prêtre combattant lorsqu’il prend ce don. Chacune d’elles doit être liée à un domaine que sa divinité lui accorde ou à l’un des deux domaines qui représentent ses pouvoirs et penchants spirituels.\r\nDeux fois par jour, le personnage peut en appeler à la bénédiction mineure de l’une ou l’autre des bénédictions choisies. Ce pouvoir fonctionne autrement comme le pouvoir de classe du prêtre combattant bénédictions. Le niveau effectif de prêtre combattant du personnage est égal au plus haut niveau qu’il possède dans la classe possédant le pouvoir de classe mystère ou domaine.", "Le personnage lance un appel si fervent à sa divinité qu’elle lui accorde une bénédiction temporaire.", "Bénédiction de guerre", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bénédiction-canalisée", "Quand le personnage canalise l’énergie pour soigner, il peut, à la place, octroyer une bénédiction de prêtre combattant à une seule créature consentante (y compris à lui-même) qui se trouve dans la zone et qui aurait été soignée par son énergie canalisée. La bénédiction doit nécessiter une action simple et affecter une ou plusieurs créatures. Si la bénédiction affecte normalement plusieurs cibles, le personnage n’en affecte qu’une seule. La cible reçoit la bénédiction à la place des soins et bénéficie de tous les autres effets de l’énergie canalisée. (Cette application n’est pas décomptée du nombre d’utilisations de bénédictions quotidiennes du personnage.)", "L’énergie canalisée du personnage peut octroyer une bénédiction de prêtre combattant.", "Bénédiction canalisée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bénédiction-accélérée", "Le personnage choisit l’une de ses bénédictions, dont l’utilisation nécessite normalement une action simple. Il peut dépenser deux de ses utilisations quotidiennes de bénédictions pour octroyer cette bénédiction (qu’il s’agisse d’un effet mineur ou majeur) par une action rapide.", "Le personnage peut octroyer ses bénédictions plus rapidement.", "Bénédiction accélérée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bébé-féerique", null, "Le personnage a été trouvé dans la nature quand il était petit et porte la marque du Monde féérique.", "Bébé féerique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "buveur-de-sang", "Le dhampir choisit un sous-type humanoïde, comme « gobelinoïde » (ce sous-type ne peut pas être « dhampir ») : il a développé un certain goût pour le sang des créatures de ce sous-type. Lorsqu’il boit du sang frais d’une telle créature, il gagne 5 points de vie temporaires supplémentaires et un bonus de +1 à ses tests et jets de sauvegarde basés sur la Constitution. Les effets durent 1 heure. S’il se nourrit ainsi plusieurs fois, il continue de gagner des points de vie jusqu’à un maximum de 5 points de vie temporaires par tranche de 3 dés de vie qu’il a, mais le bonus de +1 à ses tests et jets de sauvegarde basés sur la Constitution n’est pas cumulable.\r\nNormalement, il peut uniquement boire le sang d’un adversaire sans défense, agrippé, paralysé, immobilisé en lutte, inconscient ou soumis d’une manière similaire. S’il a une attaque de morsure, le dhampir peut automatiquement boire du sang comme partie de son attaque ; sinon, il doit d’abord entailler la cible en lui infligeant 1 point de dégâts avec une arme tranchante ou perforante (mais il peut se nourrir d’une créature ayant des blessures graves ou des dégâts de saignement sans avoir besoin de l’entailler au préalable). Une fois qu’il a entaillé sa cible, le dhampir peut s’abreuver à sa blessure par une action simple. La consommation de sang inflige un affaiblissement temporaire de 2 points de Constitution à la créature dont il se nourrit. Le sang doit provenir d’une créature vivante du sous-type humanoïde précisé. Il ne peut pas venir d’une créature morte ou invoquée.\r\nSe nourrir d’une créature intelligente non-consentante est un acte maléfique.", "La consommation de sang revigore le dhampir.", "Buveur de sang", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "brute-résiliente", "Une fois par jour, quand une créature confirme un coup critique contre le personnage, il peut considérer la moitié des dégâts comme des dégâts non létaux. Il ne peut pas utiliser ce don s’il est immunisé contre les dégâts létaux. Quand le BBA du personnage atteint +10, il peut utiliser ce don une fois de plus par jour.", "Le personnage supporte des coups qui en tueraient d’autres.", "Brute résiliente", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-magique-(mythique)", "Quand il utilise Attaque magique, le personnage peut diminuer de moitié les dés de dégâts pour accomplir en plus une manœuvre offensive de croc-en-jambe ou de bousculade contre la cible de l’attaque. Il gagne un bonus à ce test de manœuvre offensive égal à la moitié du niveau du sort sacrifié pour lancer Attaque magique et peut remplacer son modificateur de Force par son modificateur d’Intelligence ou de Charisme pour déterminer le bonus dont il bénéficie pour effectuer cette manœuvre offensive.", "Le personnage peut manipuler l’énergie magique brute pour produire davantage que des dégâts.", "Attaque magique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briseur-de-sorts", "Les lanceurs de sorts adverses qui se trouvent dans une case contrôlée par le personnage et qui ratent un sort lancé sur la défensive provoquent une attaque d’opportunité de la part du personnage.", "Le personnage frappe les lanceurs de sorts ennemis quand ils ratent un sort lancé sur la défensive alors qu’ils se trouvent dans une case contrôlée par le personnage.", "Briseur de sorts", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briser-les-défenses", "Si, au cours d’un round, le personnage frappe un adversaire effrayé, paniqué ou secoué, ce dernier est considéré comme pris au dépourvu jusqu’à la fin du prochain tour du personnage. Cela comprend toute attaque supplémentaire que le personnage peut porter au cours du même round.", "Le talent dont le personnage fait montre au combat empêche ses adversaires de se protéger si leurs défenses sont déjà ébranlées.", "Briser les défenses", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "briser-les-défenses-(mythique)", "Un adversaire affecté par Briser les défenses est pris au dépourvu vis-à-vis de toutes les attaques qui le prennent pour cible et non plus seulement vis-à-vis de celles du personnage. Si une créature non-mythique située à 9 mètres ou moins du personnage lance un sort ou utilise un pouvoir magique, le personnage peut dépenser une utilisation de pouvoir mythique pour effectuer une attaque à distance (considérée comme une attaque d’opportunité) contre elle, même si cette créature ne provoquerait normalement pas d’attaque d’opportunité. Le personnage doit avoir une arme à distance en main ou avoir une main de libre et le don non-mythique Arme en main pour pouvoir utiliser cette aptitude. Le personnage peut utiliser cette aptitude contre une créature mythique en dépensant deux utilisations de pouvoir mythique.", "Les attaques impressionnantes du personnage laissent ses adversaires pantois et perplexes, incapables de l’attaquer ou de se défendre.", "Briser les défenses (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bretteur-amateur", "Le personnage gagne une certaine dose de panache ainsi que la capacité à accomplir un seul exploit de bretteur de niveau 1. Choisissez un exploit de niveau 1 du pouvoir de classe de bretteur exploits : une fois choisi, cet exploit ne peut plus être changé.\r\nChaque matin, le personnage gagne 1 point de panache. Au cours de la journée, il peut gagner d’autres points, sans dépasser un maximum égal à son modificateur de Charisme (1 au minimum). Il récupère des points de panache comme le bretteur qui utilise le pouvoir de panache. Il les dépense pour utiliser l’exploit de bretteur de niveau 1 qu’il a choisi en même temps que ce don ou dont il bénéficie grâce à d’autres dons ou des objets magiques.", "Bien qu’il ne soit pas bretteur, le personnage a du panache et peut s’en servir.", "Bretteur amateur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bousculade-supérieure", "Le personnage reçoit un bonus de +2 aux tests de bousculade. Ce bonus s’ajoute à celui de la Science de la bousculade. Quand le personnage bouscule un adversaire, il permet à tous ses alliés de faire une attaque d’opportunité sur sa victime (mais lui ne peut pas).", "Le personnage charge ses ennemis pour les déséquilibrer.", "Bousculade supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-salvateur-(mythique)", "Le bonus de bouclier que le personnage confère à un allié avec le don Bouclier salvateur s’élève désormais à +3. S’il utilise Bouclier salvateur pour annuler avec succès une attaque effectuée contre un allié adjacent, le personnage peut immédiatement effectuer une attaque d’opportunité contre l’agresseur si celui-ci est à portée d’attaque au corps à corps.", "Le personnage peut riposter quand il protège un compagnon.", "Bouclier salvateur (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bouclier-magique-(mythique)", "Le personnage ajoute la moitié de son grade au bonus de parade à sa CA lorsqu’il utilise Bouclier magique. Il peut dépenser une utilisation de pouvoir mythique pour prolonger la durée du don d’un nombre de rounds égal à son grade.", "Le personnage façonne ses sorts pour en faire des barrières profanes persistantes et solides.", "Bouclier magique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "brisesort", "Par une [action simple]], le personnage peut tenter de dissiper un effet de sort actif comme s’il possédait le pouvoir de rage destruction de sort. Le personnage peut utiliser ce don une fois par jour puis une fois de plus pour chaque tranche de 5 points de BBA au-dessus de +10.", "Les puissants coups du personnage brisent la magie de l’ennemi.", "Brisesort", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-en-rotation", "Lors d’une attaque à outrance, le personnage peut sacrifier ses attaques \"normales\" pour porter une attaque de corps à corps avec son bonus de base maximal à l’attaque contre chacun des adversaires à sa portée. Il doit faire un jet d'attaque distinct pour chaque ennemi.\r\nUn personnage qui effectue une attaque en rotation sacrifie aussi toutes les attaques supplémentaires dont il bénéficie \"normalement\", quelle que soit leur origine.", "Le personnage frappe tous les ennemis qui se trouvent à portée.", "Attaque en rotation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-en-puissance", "Le personnage peut appliquer un malus de -1 à ses jets d'attaque de corps à corps et à ses tests de manœuvre offensive pour bénéficier d’un bonus de +2 aux jets de dégâts. Ces bonus aux dégâts augmentent de 50% si le personnage utilise une arme à deux mains ou une arme à une main qu’il manie à deux mains ou encore une arme naturelle principale qui ajoute 1,5 x le modificateur de Force aux jets de dégâts. Ce bonus aux dégâts est réduit de moitié (50%) si le personnage attaque avec son arme secondaire ou avec une attaque naturelle secondaire. Quand son bonus de base à l’attaque atteint +4, et, par la suite, pour chaque tranche de +4 points de bonus, le malus augmente de -1 et le bonus aux jets de dégâts de +2. Le personnage doit choisir s'il veut utiliser ce don avant de faire un jet d'attaque, et ses effets durent jusqu'à son prochain tour de jeu. Le bonus de dégâts ne s’applique pas aux attaques de contact ou aux effets qui n’infligent pas de points de dégâts.", "Le personnage est capable de réaliser des attaques de mêlée mortelles en sacrifiant de la précision contre de la puissance.", "Attaque en puissance", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-en-puissance-(mythique)", "Lorsqu’il utilise Attaque en puissance, le personnage gagne un bonus de +3 au lieu de +2 aux jets de dégâts infligés au corps à corps. Quand son bonus de base à l’attaque (BBA) est au moins égal à +4 et tous les 4 points par la suite, le montant de dégâts supplémentaires augmente de +3 au lieu de +2. De plus, les dégâts supplémentaires conférés par ce don sont doublés en cas de coup critique, avant d’être multipliés par le multiplicateur de critique de l’arme. Le personnage peut dépenser une utilisation de pouvoir mythique lorsqu’il active Attaque en puissance pour ignorer les malus aux jets d’attaque au corps à corps et aux tests de manœuvre offensive pendant une minute.", "Les attaques du personnage sont véritablement dévastatrices.", "Attaque en puissance (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "art-du-bouclier-(mythique)", "Le personnage ajoute le bonus de bouclier et le bonus d’altération du bouclier à sa CA au contact. Par une action immédiate, il peut dépenser une utilisation de pouvoir mythique pour ajouter le bonus de bouclier et le bonus d’altération du bouclier à un jet de Vigueur ou de Réflexes juste avant de lancer le dé.", "Le personnage est passé maître dans l’art de manier le bouclier pour se protéger.", "Art du bouclier (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arpenteur-des-ombres", "Le fetchelin peut dépenser une utilisation de son pouvoir magique traversée des ombres pour utiliser celui de porte dimensionnelle. Sa position de départ et sa position d’arrivée pour cette capacité doivent être dans une zone de faible luminosité ou de ténèbres.", "Le fetchelin est en mesure de mieux percer, et plus souvent, le voile entre le plan de l’Ombre et le plan Matériel.", "Arpenteur des ombres", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "armure-résiliente", "Lorsque son armure ou son bouclier est sous les effets de son pouvoir de pacte divin ou d’armure sacrée, le personnage gagne une réduction de dégâts égale au bonus d’altération de son armure (ce qui inclut le bonus de son pacte divin ou de son armure sacrée) contre la première attaque qui le touche à chaque round.\r\nLes armes en adamantium passent cette réduction de dégâts mais toute attaque portée contre le personnage avec une telle arme n’est pas décomptée de la durée de l’effet.", "Quand le personnage augmente son armure ou son bouclier, il gagne une certaine réduction de dégâts.", "Armure résiliente", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "armure-de-la-fosse", "Le tieffelin gagne un bonus d’armure naturelle de +2.", "Les traits fiélons du tieffelin prennent la forme d’une peau écailleuse qui le protège.", "Armure de la Fosse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "armes-ardentes", "L’ifrit gagne un bonus de +2 à ses jets de sauvegarde contre les attaques de feu et les sorts du registre feu ou lumière. Par une action rapide, il peut rendre deux armes métalliques non-magiques brûlantes pendant 1 round, infligeant 1 point de dégâts de feu supplémentaire sur une attaque réussie. Ceci ne se cumule pas avec les autres effets qui s’ajoutent aux dégâts de feu de l’arme, comme la propriété spéciale de feu.", "Un feu élémentaire brûle dans le corps de l’ifrit, faisant bouillir son sang et le rendant résistant aux flammes.", "Armes ardentes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-en-main", "Le personnage peut dégainer une arme à l’aide d’une action libre au lieu d’une action de mouvement. Il peut aussi dégainer une arme cachée (voir la compétence Escamotage) par une action de mouvement.\r\nGrâce à ce don, un personnage qui combat avec des armes de jet bénéficie de la totalité de ses attaques (ces armes fonctionnent alors plus ou moins comme un arc).\r\nLe personnage ne peut pas utiliser ce don pour sortir rapidement un objet alchimique, une potion, un parchemin ou une baguette.", "Le personnage peut tirer son arme bien plus vite que la plupart des gens.", "Arme en main", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "art-du-bouclier-supérieur", "Augmentez de +1 le bonus de CA accordé par n’importe quel bouclier utilisé par le personnage. Ce bonus se cumule avec le bonus octroyé par le don Art du bouclier.", "Le personnage est un expert lorsqu’il s’agit de parer les coups à l’aide de son bouclier.", "Art du bouclier supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-en-main-(mythique)", "Le personnage peut utiliser Arme en main pour dégainer des objets de n’importe quel type et pas seulement des armes, à condition qu’ils soient rangés ou dissimulés sur sa personne. Par une action de mouvement, il peut dépenser une utilisation de pouvoir mythique pour sortir jusqu’à deux objets dissimulés. Il doit avoir les deux mains libres pour pouvoir agir de la sorte.", "Le personnage peut dégainer n’importe quel objet et pas seulement des armes.", "Arme en main (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-de-prédilection-supérieure", "Le personnage bénéficie d’un bonus de +1 à tous ses jets d’attaque lorsqu’il utilise l’arme choisie. Ce bonus se cumule avec tous les autres bonus aux jets d'attaque, dont celui du don Arme de prédilection.", "Le personnage choisit une arme pour laquelle il possède le don Arme de prédilection. Il peut aussi choisir l’attaque à mains nues ou la lutte. Il passe maître dans le maniement de cette arme.", "Arme de prédilection supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-de-prédilection-gnome", "Le personnage gagne un bonus de +1 aux jets d’attaque avec les armes gnomes (toutes les armes qui comportent l’adjectif « gnome » dans leur nom).", "Le personnage s’est tellement entraîné à manier les armes traditionnelles gnomes qu’elles lui donnent un avantage.", "Arme de prédilection gnome", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-de-prédilection-(mythique)", "Le personnage double les bonus aux jets d’attaque conférés par Arme de prédilection et Arme de prédilection supérieure. Par une action rapide, le personnage peut dépenser une utilisation de pouvoir mythique pour gagner un bonus aux jets d’attaque avec l’arme sélectionnée égal à la moitié de son grade, jusqu’à la fin de son tour.", "Le personnage sait parfaitement manipuler une arme particulière.", "Arme de prédilection (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-de-lélu", "Par une action rapide, le personnage peut faire appel à sa divinité afin qu’elle guide une attaque qu’il porte avec l’arme de prédilection de celle-ci. Au cours du même round, lors de sa prochaine attaque avec cette arme, cette dernière compte comme une arme magique quand il s’agit de déterminer si elle passe la réduction de dégâts ou touche une créature intangible. Si son attaque est ratée à cause d’un camouflage, le personnage peut refaire son jet de chance de rater une fois afin de savoir s’il touche ou non.", "L’influence de sa divinité guide l’arme de prédilection du personnage.", "Arme de l'élu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-de-lélu-supérieure", "Quand le personnage utilise l’arme de prédilection de son dieu pour porter une seule attaque pendant son action, il lance deux dés d’attaque et garde le résultat le plus élevé. Il n’a pas besoin d’utiliser son don Arme de l’Élu pour profiter des avantages de ce don.", "Quand le personnage se bat avec l’arme de prédilection de son dieu, celui-ci guide sa main.", "Arme de l'élu supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aptitude-magique", "Le personnage gagne un bonus de +2 aux tests d’Art de la magie et d’Utilisation d’objets magiques. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence augmente à +4.", "Le personnage est doué pour lancer des sorts ou utiliser des objets magiques.", "Aptitude magique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "arme-de-prédilection", "Le personnage bénéficie d’un bonus de +1 à tous ses jets d’attaque lorsqu’il utilise cette arme.", "Le personnage choisit une arme. Il peut aussi choisir l’attaque à mains nues ou la lutte (ou les rayons s’il lance des sorts).", "Arme de prédilection", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "bénédiction-du-destructeur", "Lorsque le personnage est en rage et qu’il réussit une tentative de destruction, il récupère un round de rage. Si la tentative de destruction casse effectivement l’objet, le personnage récupère 1 point de vie.", "Casser des objets augmente votre puissance.", "Bénédiction du destructeur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "art-du-bouclier", "Augmente le bonus de CA du bouclier de +1.", "Le personnage est très doué pour détourner les coups à l’aide de son bouclier.", "Art du bouclier", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "artiste-du-ko-(mythique)", "Lorsque le personnage effectue un coup à mains nues pour infliger des dégâts non-létaux et des dégâts d’attaque sournoise à un adversaire, il remplace les d6 par des d8 lorsqu’il détermine les dégâts infligés par l’attaque sournoise.", "Les coups de poings assommants du personnage sont véritablement dévastateurs.", "Artiste du KO (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-en-finesse", "Lorsqu’il utilise une arme légère, une chaine cloutée, un fouet, une lame elfique incurvée ou une rapière (adaptée à une créature de sa catégorie de taille), le personnage peut choisir d’appliquer son bonus de Dextérité à ses jets d’attaque plutôt que son bonus de Force. Si le personnage utilise un bouclier, le malus d’armure aux tests imposé par celui-ci s’applique aux jets d’attaque.", "Le personnage privilégie l’agilité plutôt que la force brute quand il s’agit de se battre.", "Attaque en finesse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-en-finesse-(mythique)", "Lorsqu’il utilise Attaque en finesse, le personnage peut également utiliser son modificateur de Dextérité à la place de celui de Force aux jets de dégâts. S’il porte un bouclier, son malus d’armure aux tests ne s’applique ni aux jets d’attaque ni aux jets de dégâts.", "Le personnage est un expert dans la manipulation des armes qui exploitent l’agilité de leurs utilisateurs.", "Attaque en finesse (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-au-galop", "Lorsqu’un personnage lance une charge à cheval (ou toute autre monture), il peut se déplacer et attaquer comme lors d’une charge \"normale\", puis se déplacer de nouveau en poursuivant son mouvement (en ligne droite). Son mouvement total au cours du round ne peut pas dépasser le double de sa vitesse de déplacement monté. Le mouvement du personnage et de sa monture ne provoque pas d’attaque d’opportunité de la part de l’adversaire qu’ils chargent.", "Lors d’une charge montée, le personnage peut se déplacer, frapper son adversaire et se déplacer à nouveau.", "Attaque au galop", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "attaque-au-galop-(mythique)", "Lorsque le personnage parvient à frapper un adversaire lors d’une Attaque au galop, il peut continuer à effectuer des attaques contre des cibles successives. Il effectue une attaque supplémentaire par tranche de trois grades, tout en étant limité par le nombre d’attaques maximum qu’il peut effectuer lors d’une attaque à outrance. Les attaques successives utilisent le bonus le moins élevé pour les attaques supplémentaires, comme lorsque le personnage effectue une attaque à outrance. Le personnage doit parcourir au moins 3 mètres entre chaque attaque.", "Le personnage peut attaquer plusieurs fois alors qu’il traverse les rangs ennemis en monture.", "Attaque au galop (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "athlétisme", "Le personnage obtient un bonus de +2 sur tous ses tests d’Escalade et de Natation. Si le personnage a 10 rangs ou plus dans l’une de ces compétences, le bonus ajouté à cette compétence augmente de +4.", "Le personnage possède un potentiel athlétique inné.", "Athlétisme", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "athlétisme-(mythique)", "Le bonus aux tests de Natation et d’Escalade conféré par Athlétisme augmente de +2. De plus, le personnage peut dépenser une utilisation de pouvoir mythique pour considérer qu’il a obtenu un 20 naturel à l’un de ces tests. Le personnage doit décider s’il utilise cette aptitude avant de lancer le dé.", "Le personnage est bien supérieur aux autres en matière de prouesses physiques.", "Athlétisme (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "artisan-mythique-(mythique)", "Le personnage peut créer des objets magiques mythiques s’il possède le don de création d’objets approprié. De plus, il gagne un bonus de +5 aux tests de compétence effectués lors de la création d’objets magiques non-mythiques.", "Le personnage peut créer des objets magiques mythiques.", "Artisan mythique (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assurance-intimidante", "Le personnage gagne un bonus de +1 aux jets de sauvegarde contre les effets du registre des émotions. Ce bonus se cumule avec celui de la Curiosité intrépide. Quand le personnage confirme un coup critique, il peut dépenser une action libre pour faire un test d’Intimidation afin de démoraliser une créature qu’il menace. S’il possède le don Démonstration, il peut faire des tests d’Intimidation pour démoraliser toutes les créatures qu’il menace. Il gagne un bonus de +2 au test si son arme a une zone de critique possible de ×3 et un bonus de +4 si elle a un modificateur de ×4.", "Le personnage est absolument persuadé qu’il va réussir dans la vie.", "Assurance intimidante", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assaut-élémentaire-différentiel", "Le suli utilise sa capacité d’assaut élémentaire par segments de 1 round, jusqu’à un nombre de rounds par jour maximum égal à son niveau. Ces rounds ne doivent pas nécessairement être consécutifs. L’activation de cette capacité demande une action rapide ; sa désactivation est une action libre.", "Le suli peut activer et désactiver sa capacité d’assaut élémentaire plusieurs fois par jour.", "Assaut élémentaire différentiel", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aspersion-contrôlée", "Lorsque vous touchez directement une créature avec une arme à impact qui inflige normalement des dégâts d’aspersion, vous pouvez choisir de ne pas infliger ces dégâts. Si vous faites ce choix, les dégâts effectués à la cible direct de votre attaque augmentent de 50%. Ces dégâts sont multipliés en cas de coup critique.\r\nCe don ne peut pas être utilisé avec les bombes de l’alchimiste.", "Source : RTT", "Aspersion contrôlée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aspect-draconique", "Les écailles du kobold ont la couleur et certaines des résistances d’un type de dragon chromatique. Il choisit l’un des types de dragons chromatiques suivants : noir (acide), bleu (électricité), vert (acide), rouge (feu) ou blanc (froid). Ses écailles ont la couleur de ce dragon et il gagne une résistance de 5 au type d’énergie correspondant.", "Le kobold possède certaines qualités de ces ancêtres draconiques.", "Aspect draconique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aspect-bestial", "La nature bestiale du personnage se manifeste d’une des manières suivantes. Le joueur choisit le type de manifestation lorsqu’il acquiert ce don et ne peut plus changer d’avis par la suite.\r\n:Bond du prédateur (Ext). Lorsque le personnage saute, on considère qu’il s’agit d’un saut avec élan, même s’il n’a pas couru sur 3 mètres (2 cases) avant celui-ci.\r\n:Griffes bestiales (Ext). Le personnage possède deux mains griffues. Il s’agit d’attaques primaires de griffes qui infligent 1d4 points de dégâts (1d3 si le personnage est de Taille P).\r\n:Instinct sauvage (Ext). Le personnage gagne un bonus de +2 aux tests d’initiative et un bonus de +2 aux tests de Survie.\r\n:Vision de nuit (Ext). Si la race du personnage possède une vision normale, il gagne la capacité de vision nocturne. Si la race du personnage possède déjà la vision nocturne, il gagne la capacité de vision dans le noir avec une portée de 9 mètres (6 cases). Si la race du personnage possède déjà la capacité de vision dans le noir, la portée de celle-ci augmente de 9 mètres (6 cases).", "Suite à un effet magique ou à une malédiction touchant la famille du personnage, celui-ci possède certaines caractéristiques qui tiennent plus de la bête que de l’humain.", "Aspect bestial", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "aspect-bestial-(mythique)", "La nature bestiale du personnage se manifeste d’une des façons suivantes. Le personnage doit choisir la manifestation lorsqu’il sélectionne ce don. Ce choix est définitif.\r\nBond mythique (Ext). Le personnage peut réaliser un saut avec course d’élan sans avoir besoin de courir sur une distance de 3 mètres au préalable. De plus, il gagne un bonus de +5 aux tests d’Acrobaties pour déterminer la réussite de ce saut et la distance parcourue. Si le personnage dispose déjà de la manifestation d’Aspect bestial bond du prédateur, le bonus aux tests d’Acrobaties s’élève à +10.\r\nGriffes mythiques (Ext). Des griffes impressionnantes apparaissent sur les deux mains du personnage. Ces griffes sont des attaques principales qui infligent 1d4 points de dégâts (1d3 si le personnage est de taille P). Si le personnage dispose déjà de griffes, les dégâts qu’elles infligent utilisent désormais le type de dé directement supérieur (les d4 deviennent des d6, les d3 des d4, et ainsi de suite). Le personnage peut dépenser une utilisation de pouvoir mythique lorsqu’il touche avec ses deux griffes au cours d’un même round pour réaliser une éventration et infliger des dégâts supplémentaires égaux aux dégâts d’une griffe plus 1,5 fois son bonus de Force.\r\nInstinct mythique (Ext). Le personnage gagne un bonus de +2 aux tests d’initiative et de Survie. Ces bonus se cumulent avec ceux conférés par la manifestation d’Aspect bestial instinct sauvage.\r\nSens mythiques (Ext). Si le personnage dispose d’une vision normale, il gagne vision dans le noir jusqu’à 9 mètres. S’il dispose de la vision nocturne, il gagne vision dans le noir jusqu’à 18 mètres. S’il dispose déjà de vision dans le noir, la portée de celle-ci augmente de 9 mètres plus 3 mètres par grade.", "La fureur sauvage dont fait preuve le personnage lui fait bouillir les sangs, ce qui lui confère des capacités et des pouvoirs bestiaux.", "Aspect bestial (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "ascendance-aquatique", "L’ondin gagne la capacité spéciale amphibie. Sa vitesse de nage augmente de +3 mètres.", "L’ondin privilégie son ascendance extérieure et est plus adapté à la vie dans l’eau.", "Ascendance aquatique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "assaut-élémentaire-supplémentaire", "L’assaut élémentaire du Suli  dure 2 rounds par jour supplémentaire.", "Le [[Suli (race)|Suli]] a libéré un plus grand pouvoir élémentaire.", "Assaut élémentaire supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-étourdissant", "Le personnage doit annoncer qu’il utilise ce don avant de faire son jet d’attaque (la tentative échoue donc si le personnage rate son jet). L’attaque inflige les dégâts habituels. De plus, le Coup étourdissant oblige tout adversaire frappé par l’attaque à mains nues du personnage à faire un jet de Vigueur (DD 10 + 1/2 niveau du personnage + modificateur de Sagesse). S'il rate ce jet de sauvegarde, il est étourdi pendant 1 round (jusqu’au début du prochain tour du personnage). Un personnage étourdi ne peut pas entreprendre d’action, perd son bonus de Dextérité à la CA et subit un malus de -2 à la CA. Le personnage peut porter un Coup étourdissant par jour par tranche de quatre niveaux mais pas plus d’une fois par round. Le personnage ne peut pas étourdir les créatures artificielles, les vases, les plantes, les morts-vivants, les créatures intangibles et les créatures immunisées contre les coups critiques.", "Le personnage sait où frapper pour étourdir son adversaire.", "Coup étourdissant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cachette-commune", "Lorsqu’il se trouve sur la même case qu’un allié consentant qui possède aussi ce don, l’homme-rat peut récupérer une arme sur son allié par une action libre ou dégainer une arme ou tout autre objet caché par une action de mouvement.\r\nIl peut également récupérer un objet qu’un allié volontaire situé sur la même case que lui tient en main par une action libre. Dégainer ou récupérer un objet (mais pas une arme) de cette manière provoque une attaque d’opportunité.", "L’homme-rat est particulièrement doué pour emprunter à ses alliés, même en plein milieu d’un combat.", "Cachette commune", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canal-dénergie", "Par une action rapide, le personnage peut dépenser une utilisation de canalisation d’énergie pour donner un bonus aux jets de dégâts de ses attaques armées égal à deux fois le nombre de dés qu’il lance pour sa canalisation d’énergie. Ces dégâts supplémentaires sont du type d’énergie déterminé par le domaine ou la bénédiction du personnage : acide (Terre), électricité (Air), feu (Feu) ou froid (Eau). Si le personnage a plus d’un seul de ces domaines ou bénédictions, il doit choisir l’un de ces types de dégâts quand il utilise son pouvoir. Cet effet dure le temps des trois prochaines attaques armées du personnage ou jusqu’à la fin du combat (ce qui arrive en premier).", "Le personnage distille son énergie canalisée dans son arme pour lui donner plus de force.", "Canal d'énergie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cogneur-tyrannique", "Quand le personnage touche sa cible avec une attaque de l’École du cogneur, il peut faire un test de manoeuvre offensive de repositionnement ou de croc-en-jambe par une action libre.", "Le coup de poing que porte le personnage est susceptible de faire trébucher la cible ou de la déplacer.", "Cogneur tyrannique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "collectionneur", "Une fois par jour, quand il est confronté à une situation nécessitant l’utilisation d’un objet ordinaire spécifique, l’homme-rat se trouve avoir cet objet sur lui. L’objet ne doit pas valoir plus de 25 po + 5 po par niveau et l’homme-rat doit s’acquitter de ce prix quand il « trouve » l’objet (en d’autres termes, l’argent qu’il pensait avoir sur lui était, en réalité, l’objet attendu). L’objet doit être facilement transportable (si, par exemple, l’homme-rat est à pied et n’a qu’un sac à dos, il ne peut pas « trouver » un grand chaudron en fer). Il ne peut pas trouver d’objets magiques avec ce don, ni posséder d’objets spécifiques tels que la clé d’une porte donnée. S’il a été dépouillé de son équipement ou de ses possessions, il perd les avantages de ce don jusqu’à ce qu’il ait passé au moins une journée à refaire le plein de nouveaux objets.", "L’homme-rat accumule toutes sortes de choses qu’il perd et remplace fréquemment.", "Collectionneur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combat-en-aveugle-(mythique)", "Par une action rapide, le personnage peut dépenser une utilisation de pouvoir mythique pour ignorer tous risques d’échec provoqués par un camouflage normal ou total pendant un nombre de rounds égal à son grade.", "Aucune créature n’échappe aux sens surnaturels du personnage.", "Combat en aveugle (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combat-en-aveugle", "Lors d’un combat au corps à corps, à chaque fois que le personnage rate son adversaire à cause de son camouflage, il peut à nouveau jeter 1d100 afin de savoir s’il touche.\r\nUn adversaire invisible ne bénéficie d’aucun avantage offensif contre le personnage. Autrement dit, ce dernier ne perd pas son bonus de Dextérité à la CA et son adversaire n’a pas droit au bonus habituel de +2 accordé aux créatures invisibles. Un ennemi invisible conserve cependant ses avantages pour les attaques à distance.\r\nNormal. Les modificateurs de jet d’attaque liés aux assaillants invisibles s’appliquent au personnage, et il perd son bonus de Dextérité à la CA face à eux. La réduction de vitesse s’applique aussi dans les ténèbres et en cas de mauvaise visibilité.", "Le personnage est très doué pour attaquer des adversaires qu’il distingue mal.", "Combat en aveugle", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combat-monté", "Lorsque sa monture est touchée (dans la limite d’une fois par round), le personnage peut tenter d’annuler le coup en réussissant un test d’Équitation. Il s’agit là d’une réaction, pas d’une action. Le coup est annulé si le test de compétence du personnage est supérieur au jet d’attaque de l’adversaire.", "Le personnage sait diriger sa monture au milieu des combats.", "Combat monté", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combat-à-deux-armes-(mythique)", "Par une action immédiate, le personnage peut dépenser une utilisation de pouvoir mythique pour annuler les malus aux jets d’attaques occasionnés par le combat à deux armes pendant un nombre de rounds égal à son grade.", "Grâce à ses frappes adroites, le personnage gagne un avantage certain sur ses adversaires, en plus de les blesser purement et simplement.", "Combat à deux armes (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combat-à-deux-armes-supérieur", "Le personnage a droit à une troisième attaque avec son arme secondaire mais avec un malus de -10.", "Le personnage est incroyablement doué pour se battre avec deux armes à la fois.", "Combat à deux armes supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combat-à-deux-armes", null, "Le personnage sait se battre avec une arme dans chaque main. À chaque round, il peut faire une attaque supplémentaire avec son arme secondaire.", "Combat à deux armes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combattant-désespéré", null, "Le personnage a vécu tant de combats contre de nombreux adversaires qui avaient déjà terrassé ses alliés, ses amis ou sa famille qu’il excelle dans le combat en solitaire. ", "Combattant désespéré", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "combattant-prudent", "Quand le personnage se bat sur la défensive ou utilise la défense totale, son bonus d’esquive à la CA augmente de 2.", "Le personnage se préoccupe plus de survie que de victoire.", "Combattant prudent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "comme-le-vent", "Si le personnage parcourt plus de 1,5 m pendant son tour, il gagne un camouflage de 20% contre les attaques à distance pendant un round.", "Les ennemis du personnage ont bien du mal à le situer à cause de ses mouvements erratiques.", "Comme le vent", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "comme-léclair", "Si le personnage consacre deux actions à un déplacement ou s’il bat en retraite, il gagne un bonus de camouflage de 50% pendant un round.", "Le personnage se déplace à une telle vitesse que ses adversaires peinent à le toucher.", "Comme l'éclair", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "compagnon-mythique", "Le personnage est considéré comme une créature mythique lorsqu’il s’agit de déterminer si les sorts et les effets mythiques l’affectent. S’il devient mythique par la suite, il gagne un bonus de +1 à tous ses jets de sauvegarde effectués contre les effets et les sorts mythiques.", "Même si le personnage n’a rien de mythique, il joue un rôle déterminant dans le monde mythique supérieur.", "Compagnon mythique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "compagnon-évolué", "Choisissez une évolution à 1 point parmi celles accessibles aux eidolons des conjurateurs. Le compagnon animal du personnage gagne cette évolution et doit se conformer à toutes les limitations que celle-ci impose. Par exemple, seul un compagnon animal de la taille et de la forme de base appropriées peut prendre l’évolution monture. Si le personnage gagne un nouveau compagnon animal, l’ancienperd cette évolution et le personnage peut choisir une nouvelle évolution à 1 point pour son nouveau compagnon animal.", "Le compagnon animal du personnage possède des pouvoirs qui le rendent différent des autres de son espèce.", "Compagnon évolué", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "compétence-de-marque", "Le personnage choisit une compétence et bénéficie des avantages indiqués dans la description de l'extension des compétences pour cette compétence au rang 5. Il bénéficie d’avantages supplémentaires au fur et à mesure qu’il investit des rangs dans la compétence choisie. S’il possède 10 rangs ou plus dans cette compétence, il gagne immédiatement les avantages correspondants.\r\nS’il choisit Artisanat, Connaissances, Représentation ou Profession, le personnage gagne les avantages indiqués uniquement pour une catégorie spécifique, tel que Artisanat (arcs) par exemple.\r\nCe don peut être sélectionné une fois seulement mais l’avantage qu’il confère se cumule avec l’aptitude de spécialité du roublard et le talent de maître-roublard spécialités multiples.", "Le personnage fait montre d’un talent légendaire lorsqu’il utilise une compétence précise et peut la mettre à profit pour accomplir des exploits hors d’atteinte d’autrui. ", "Compétence de marque", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "concentration-malgré-la-fureur-(mythique)", "Lorsqu’il utilise Concentration malgré la fureur, le personnage ne subit pas le malus infligé par Attaque en puissance aux jets d’attaque effectués pour réaliser des attaques d’opportunité. Par une action libre, le personnage peut dépenser une utilisation de pouvoir mythique pour annuler le malus d’Attaque en puissance à l’ensemble des attaques au corps à corps effectuées pendant le round où il utilise ce don.", "Les attaques du personnage forment un déluge de coups effrénés, précis et puissants.", "Concentration malgré la fureur (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "concentration-rageuse", null, "Quand le personnage est en pleine fureur, même la douleur ne peut briser sa concentration.", "Concentration rageuse", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-ébranlant", "Quand le personnage attaque un ennemi avec une Frappe décisive, il peut décider de subir un malus de –2 au jet d’attaque pour que cette attaque soit aussi une frappe déstabilisante. Il doit faire ce choix avant de faire son jet d’attaque.\r\nSi ce dernier se solde par une réussite, la cible doit faire un jet de Vigueur DD = 10 + 1/2 niveau du personnage + modificateur de Force du personnage. Si elle échoue, elle est chancelante jusqu’au début du prochain tour du personnage. Une fois qu’elle s’est remise des effets du Coup ébranlant, elle est immunisée contre lui pendant 24 heures.\r\nLes créatures artificielles, les créatures immunisées contre les coups critiques, les créatures intangibles, les plantes, les morts-vivants et les vases sont immunisés contre cet effet.\r\n{s:Desambi|Ce don est dénommé coup déstabilisant dans le codex monstrueux. Cependant, cette dénomination désigne déjà un autre don existant}", "Le personnage sait où frapper pour faire chanceler son adversaire.", "Coup ébranlant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-odieux-(mythique)", "Le personnage peut exécuter un coup de grâce sur des adversaires étourdis ou chancelants. De plus, lorsqu’une créature effectue un jet de Vigueur pour survivre à une tentative de coup de grâce réalisée par le personnage, elle subit un malus égal au grade du personnage.", "Le personnage frappe avec une précision mortelle tous les adversaires à terre, chancelants ou étourdis.", "Coup odieux (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-désespéré", "Une fois par jour, le personnage peut faire une unique attaque de corps à corps alors qu’il est en défense totale. Il gagne aussi un bonus de +4 aux confirmations de coup critique quand il se bat sur la défensive ou qu’il fait une attaque d’opportunité à l’aide de ce don.", "Le personnage porte ses coups les plus puissants dans les situations désespérées.", "Coup désespéré", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-de-focalisation", "Un allié qui a lui aussi ce don peut infliger des dégâts au hobgobelin afin de briser l’effet mental auquel il est sujet, lui donnant ainsi droit à un jet de sauvegarde. L’allié doit infliger au moins 5 points de dégâts au hobgobelin au moyen d’une attaque, d’un sort ou d’une autre capacité. Le hobgobelin refait alors un jet de sauvegarde, avec un bonus de +1 par tranche de 5 points de dégâts supplémentaires causés par l’attaque. Si le hobgobelin réussit son jet, l’effet mental prend fin. Seuls les dégâts réellement infligés comptent pour ce don : les dégâts non létaux et les dégâts réduits ou éliminés par une réduction de dégâts, des résistances, etc. n’entrent pas en considération.", "Le hobgobelin et ses alliés unissent leurs forces pour chasser les effets mentaux.", "Coup de focalisation", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coup-bouleversant", "Coup bouleversant force un adversaire blessé par l’attaque à mains nues du personnage à faire un jet de Vigueur (DD = 10 + 1/2 niveau du personnage + modificateur de Sagesse du personnage) en plus de subir les dégâts normaux. Le personnage doit déclarer l’utilisation de ce don avant de faire son jet d’attaque : ainsi, sa tentative est perdue sur un jet d’attaque raté.\r\nSi la victime rate ce jet de sauvegarde, elle est confuse pendant 1d4 rounds.\r\nLe personnage peut utiliser Coup bouleversant une fois par jour par tranche de 4 niveaux de personnage qu’il possède mais ne peut pas l’utiliser plus d’une fois par round.\r\nLes créatures artificielles, les créatures intangibles, les créatures dépourvues d’intelligence, les plantes, les morts-vivants et les créatures immunisées contre les coups critiques ne peuvent pas être affectés par ce pouvoir.", "Le personnage sait où frapper pour plonger temporairement son adversaire dans un état de stupeur confuse.", "Coup bouleversant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "convocations-féroces", "Les créatures que le personnage invoque gagnent le pouvoir universel de monstre férocité.", "Les créatures que le personnage convoque sont aussi féroces que lui.", "Convocations féroces", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coeur-vaillant", "Lorsqu’il maintient un chant de rage et qu’il rate un jet de sauvegarde contre un effet de terreur, le personnage peut dépenser un round de représentation pour relancer son jet par une action immédiate. Il doit garder le second résultat, même s’il est inférieur au premier.", "Le personnage combat sa peur en chantant la mélodie qui résonne en son coeur.", "Coeur vaillant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "contrôle-des-morts-vivants", "Par une action simple, le personnage peut canaliser l’énergie négative pour réduire en esclavage des morts-vivants situés dans un rayon de neuf mètres. Les morts-vivants ont droit à un jet de Volonté pour annuler l’effet. Le DD de ce jet est égal à 10 + 1/2 niveau de lanceur de sorts du personnage + modificateur de Charisme. Les morts-vivants qui ratent leur jet de sauvegarde tombent sous le contrôle du personnage et font de leur mieux pour lui obéir, comme s’ils étaient sous l’effet d’un contrôle des morts-vivants. Les morts-vivants doués d’Intelligence ont droit à un nouveau jet de sauvegarde par jour. Le personnage peut contrôler n’importe quel nombre de morts-vivants tant que le total de leurs DV ne dépasse pas son niveau de prêtre. Si le personnage choisit de canaliser l’énergie dans ce but, elle n’a pas d’autre effet (elle ne soigne ni ne blesse les créatures proches). Si un mort-vivant est sous le contrôle d’une autre créature, le personnage doit faire un test de Charisme opposé à celui de celle-ci quand les ordres entrent en conflit.", "Le personnage peut recourir à d’immondes pouvoirs nécromantiques pour prendre le contrôle de créatures mortes-vivantes et en faire ses serviteurs.", "Contrôle des morts-vivants", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "contre-droite", "Une fois par round, lorsque le personnage a les deux mains libres et se bat à mains nues tandis que son adversaire rate une attaque au corps à corps contre lui, cet adversaire provoque une attaque d’opportunité de la part du personnage. Ce dernier doit porter son attaque d’opportunité à mains nues.", "Avec ses réflexes habiles, le personnage tire profit des erreurs de ses adversaires.", "Contre-droite", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "contemplateur-des-nuages", "Le sylphe voit à travers le brouillard, la brume et les nuages sans malus, ignorant tous les bonus d’abri ou de camouflage de ces effets. Si l’effet est magique, ce don triple la distance à laquelle le sylphe peut voir sans malus au lieu d'ignorer les bonus d'abri et de camouflage.", "La connaissance qu’a le sylphe de son héritage élémentaire lui offre une clarté de vue que peu d’humains possèdent.", "Contemplateur des nuages", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "contact-magique-bondissant", "Le personnage peut augmenter l’allonge des attaques de contact au corps à corps de ses sorts de 1,50 mètre jusqu’à la fin de son tour en recevant un malus de -2 à la CA jusqu’à son tour suivant. Il doit décider de l’utilisation de ce pouvoir avant de tenter la moindre attaque lors de son tour.", "Le personnage peut étendre son allonge pour toucher des ennemis qui seraient normalement trop loin de lui.", "Contact magique bondissant", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "conseil-de-combat", "Le personnage peut offrir au moment opportun un conseil utile à un allié engagé dans un affrontement par une action de mouvement. Le personnage doit désigner un ennemi ; son allié gagne un bonus d'aptitude de +2 sur son prochain jet d'attaque contre cet ennemi. Pour que cet allié puisse bénéficier de ce don, le personnage doit clairement le voir ainsi que l’ennemi désigné, et cet allié doit pouvoir l’entendre.", "Des mots succincts bien choisis peuvent aider un allié, même en plein affrontement.", "Conseil de combat", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "connaissances-magiques-mythiques-(mythique)", "Le personnage peut apprendre un nombre de sorts mythiques égal à son grade et peut dépenser du pouvoir mythique lorsqu’il les lance pour augmenter leurs effets. Pour sélectionner un sort mythique, le personnage doit être capable de lancer sa version non-mythique ou l’avoir dans son répertoire de sorts connus. Chaque fois qu’il gagne un nouveau grade, il peut choisir un sort mythique supplémentaire.", "Le personnage a appris comment développer la puissance de ses sorts et les associer à son pouvoir mythique.", "Connaissances magiques mythiques (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "connaissance-supérieure-de-la-pierre-(mythique)", "Le personnage gagne le pouvoir magique de pierres commères utilisable une fois par jour avec un niveau de lanceur de sorts égal à deux fois son grade.", "Si le personnage écoute attentivement, il peut entendre la pierre lui murmurer ses secrets.", "Connaissance supérieure de la pierre (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "contrôle-des-morts-vivants-(mythique)", "Lorsque le personnage contrôle des morts-vivants, le DD des jets de Volonté augmente de la moitié de son grade et les morts-vivants intelligents ne bénéficient d’aucune sauvegarde supplémentaire au-delà de la première pour y résister. Toutes les tentatives effectuées par des créatures non-mythiques pour reprendre le contrôle imposé par le personnage échouent automatiquement et le personnage gagne un bonus de +4 aux tests de Charisme effectués pour empêcher les créatures mythiques de s’emparer du contrôle de ses morts-vivants.", "Le contrôle du personnage sur les morts-vivants est pratiquement absolu.", "Contrôle des morts-vivants (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "coeur-de-feu", "Le gobelin gagne une résistance au feu de 5. Lorsqu’il lance des sorts du registre du feu ou lance des bombes d’alchimiste infligeant des dégâts de feu, il traite son niveau de lanceur de sorts ou son niveau d’alchimiste comme supérieur d’un niveau.", "Le gobelin maîtrise le feu magique et l’art de l’alchimie.", "Coeur de feu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "châtiment-canalisé", "Avant de porter une attaque au corps à corps, le personnage peut choisir de dépenser une utilisation de sa capacité de canalisation d’énergie, par une action rapide. Si le personnage canalise de l’énergie positive et qu’il frappe un mort-vivant, ce dernier encaisse un nombre de points de dégâts supplémentaires égal aux dégâts occasionnés par la capacité de canalisation d’énergie. Si le personnage canalise de l’énergie négative et qu’il frappe une créature vivante, cette dernière encaisse un nombre de points de dégâts supplémentaires égal aux dégâts occasionnés par la capacité de canalisation d’énergie. La cible peut effectuer son jet de Volonté habituel pour réduire ces dégâts de moitié. Si l’attaque du personnage échoue, sa capacité de canalisation d’énergie a été utilisée sans effet.", "Le personnage peut canaliser son énergie divine à travers l’arme de contact qu’il tient en main.", "Châtiment canalisé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "châtiment-canalisé-(mythique)", "Lorsqu’il utilise Châtiment canalisé, le personnage gagne un bonus sur son jet d’attaque égal au nombre de d6 conféré par son aptitude de classe canalisation d’énergie. C’est soit un bonus de sainteté si l’énergie canalisée est positive, soit un bonus de malfaisance si l’énergie est négative. Si l’attaque du personnage rate sa cible, il peut dépenser une utilisation de pouvoir mythique par une action libre pour déclencher l’énergie canalisée et produire les effets normaux de son aptitude de classe canalisation d’énergie, en prenant pour centre de la zone d’effet la créature ciblée par le châtiment.", "Les armes du personnage sont de véritables intermédiaires du pouvoir divin.", "Châtiment canalisé (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "casque-bosselé", "Quand le personnage porte un casque, il ajoute +1 à sa CA contre les confirmations de coup critique. Si quelqu’un confirme un critique contre lui, il peut, par une action immédiate, appliquer la moitié des dégâts de l’attaque à son casque au lieu de les appliquer à sa personne. La solidité de l’objet s’applique normalement. Si les dégâts détruisent le casque, les dégâts excédentaires s’appliquent au propriétaire du casque. Si le personnage utilise ce don, il est chancelant jusqu’à la fin de son prochain tour. Il ne peut pas utiliser ce don si son casque est brisé ou si l’attaque ignore les bonus d’armure à la CA.", "Le casque du personnage le protège contre les coups les plus violents.", "Casque bosselé", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "carnage-effroyable-(mythique)", "Lorsqu’il utilise Carnage effroyable, le personnage affecte les ennemis non-mythiques situés à 18 mètres ou moins en plus de la totalité des ennemis situés à 9 mètres ou moins. Le personnage peut dépenser une utilisation de pouvoir mythique avant d’effectuer le test d’Intimidation pour que les ennemis démoralisés par le Carnage effroyable soit effrayés au lieu de simplement secoués pendant la même durée que celle de l’état secoué. Une fois effrayé par cette aptitude, un ennemi subit un malus égal au grade du personnage aux jets d’attaque, de sauvegarde, aux tests de compétence et de caractéristique.", "Les massacres perpétrés par le personnage sont particulièrement effroyables.", "Carnage effroyable (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "capture-de-projectiles", "Quand le personnage utilise le don Parade de projectiles, il peut attraper le projectile au lieu de se contenter de le détourner. Il peut renvoyer immédiatement les armes de jet sur leur lanceur (même si ce n’est pas son tour) ou les garder pour un usage ultérieur.\r\nIl doit avoir au moins une main libre (qui ne tient rien).", "Au lieu de dévier les flèches et les attaques à distance, le personnage est capable de les attraper en plein vol.", "Capture de projectiles", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "capture-de-projectiles-(mythique)", "Lorsqu’il utilise Capture de projectiles pour rattraper une arme de jet qui peut également être utilisée au corps à corps, le personnage peut effectuer une attaque au corps à corps avec cette arme, par une action immédiate, contre un adversaire à portée. Le personnage peut dépenser une utilisation de pouvoir mythique pour effectuer cette attaque sans y consacrer une action immédiate.", "Lorsque le personnage exclue des créatures de sa canalisation, celle-ci est plus efficace envers les créatures affectées.", "Capture de projectiles (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-élémentaire", "L’énergie canalisée par le personnage soigne ou blesse les extérieurs du sous-type choisi au lieu de fonctionner comme à l’accoutumée. Le personnage doit choisir s’il souhaite utiliser ce don ou non à chaque fois qu’il canalise de l’énergie. S’il choisit de soigner ou de blesser une créature du sous-type en question, cette énergie n’aura aucun effet sur les autres créatures. La quantité de dégâts soignés ou infligés et le DD à battre pour réduire les dégâts de moitié ne change pas.", "Le personnage choisit un sous-type élémentaire comme l’air, l’eau, le feu ou la terre. Il peut canaliser l’énergie divine pour blesser ou soigner les extérieurs de ce sous-type.", "Canalisation élémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-élémentaire-(mythique)", "La Canalisation élémentaire du personnage affecte n’importe quel sous-type d’élémentaires, et pas seulement ceux choisis lorsqu’il a sélectionné le don. Avant chaque utilisation de Canalisation élémentaire, il doit choisir un sous-type d’élémentaires. Il peut dépenser une utilisation de pouvoir mythique lorsqu’il utilise Canalisation élémentaire pour choisir un deuxième sous-type d’élémentaires qu’il peut affecter avec ce don.", "La foi du personnage est si puissante qu’elle blesse toutes les créatures de l’alignement abhorré.", "Canalisation élémentaire (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "casque-fendu", "Quand le personnage porte un casque, il ajoute +1 à sa CA contre les confirmations de coup critique. Cet avantage se cumule avec le Casque bosselé. Quand le personnage utilise Casque bosselé pour détourner un coup critique, il peut appliquer tous les dégâts du critique au casque avant de les appliquer à sa personne. Si cela lui évite de subir le moindre dégât, tous les effets supplémentaires comme les dons de critique ou le poison sont annulés.", "Le casque du personnage détourne les coups mortels.", "Casque fendu", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-sélective", ". Quand le personnage canalise de l’énergie, il peut choisir un nombre de cibles égal à son modificateur de Charisme. Elles ne seront pas affectées par l’énergie canalisée.", "Le personnage peut choisir les cibles qu’il affecte quand il canalise de l’énergie.", "Canalisation sélective", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-supplémentaire", "Le personnage canalise l’énergie deux fois de plus par jour.", "Le personnage peut canaliser l’énergie divine plus souvent.", "Canalisation supplémentaire", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-de-force", "Lorsqu’il canalise l’énergie pour infliger des dégâts, l’aasimar peut décider d’affecter une seule cible dans un rayon de 9 mètres. En plus d’infliger des dégâts, si cette cible rate son jet de sauvegarde, l’aasimar peut attirer ou repousser la cible sur une distance maximale de 1,5 mètre par tranche de 2d6 points de dégâts de canalisation d’énergie qu’il est capable d’infliger.\r\n{s:Desambi|Le don Canalisation de force (Channeling force en vo du manuel des races avancées a été renommé Canalisation armée}", "La canalisation de l’aasimar est renforcée par sa foi, ce qui lui permet de déplacer et de blesser ses ennemis.", "Canalisation de force", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-de-force-supérieure", "Lorsqu’il utilise Science de la canalisation de force, l’aasimar peut affecter toutes les créatures situées dans un rayon de 9 mètres.", "Les éruptions de puissance divine de l’aasimar déplacent ses ennemis.", "Canalisation de force supérieure", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-armée", "Par une action rapide, le personnage peut dépenser une utilisation de canalisation d’énergie pour accorder un bonus aux jets de dégâts de ses attaques armées égal au nombre de dés de sa canalisation d’énergie. Ces dégâts supplémentaires sont des dégâts de force. Ce pouvoir dure le temps des trois prochaines attaques armées du personnage ou jusqu’à la fin du combat (ce qui arrive en premier).\r\n{s:Desambi|ce don initialement nommé Canalisation de force a été renommé en raison du fait que la version française utilise déjà ce nom pour un don racial des aasimars}", "Le personnage distille son énergie canalisée dans un écran de force qui entoure son arme pendant une période limitée.", "Canalisation armée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-alignée", "L’énergie canalisée par le personnage soigne ou blesse les extérieurs du sous-type choisi au lieu de fonctionner comme à l’accoutumée. Le personnage doit choisir s’il souhaite utiliser ce don ou non à chaque fois qu’il canalise de l’énergie. S’il choisit de soigner ou de blesser une créature de l’alignement en question, cette énergie n’aura aucun effet sur les autres créatures. La quantité de dégâts soignés ou infligés et le DD à battre pour réduire les dégâts de moitié ne changent pas.", "Le personnage choisit le Bien, le Chaos, la Loi ou le Mal. Il peut canaliser l’énergie divine pour affecter les extérieurs de ce sous-type.", "Canalisation alignée", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-alignée-(mythique)", "L’énergie canalisée par le personnage affecte toutes les créatures de l’alignement désigné lorsqu’il a choisi Canalisation alignée (et pas uniquement les extérieurs et les créatures avec l’alignement en sous-type), mais les soins ou les blessures sont deux fois moins efficaces contre ces cibles supplémentaires. Par ailleurs, le personnage peut dépenser deux utilisations de pouvoir mythique lorsqu’il utilise Canalisation alignée pour considérer que la totalité des cibles de l’alignement choisi avait le sous-type d’alignement correspondant.", "La foi du personnage est si puissante qu’elle blesse toutes les créatures de l’alignement abhorré.", "Canalisation alignée (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "canalisation-sélective-(mythique)", "Lorsque le personnage utilise son aptitude de canalisation d’énergie, les dégâts soignés ou infligés augmentent d’un montant égal à deux fois le nombre de cibles exclues de sa canalisation. Il peut dépenser une utilisation de pouvoir mythique pour augmenter de la moitié de son grade le nombre de cibles exclues de sa canalisation.", "La supériorité du personnage sur les entités élémentaires est pratiquement absolue.", "Canalisation sélective (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "camouflage-au-sol", "Si le personnage est couché depuis la fin de son dernier tour, la pénalité des jets de perception pour que d’autres le repèrent augmente de +1 tous les 6m (4c) qui séparent le personnage de l’observateur, pour un maximum de +5 à 30m (20c). Cette pénalité s’ajoute aux modificateurs normaux de distance.", "Le personnage est encore plus difficile à repérer quand il est à plat ventre.", "Camouflage au sol", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "cavalier-des-bêtes", "Le personnage choisit l’un des types de créature suivant : éléphant, ptéranodon, rhinocéros, stégosaure ou tricératops. Il l’ajoute à sa liste de compagnons animaux ou de montures disponibles. Quand le personnage choisit une créature de ce type pour lui servir de monture ou de compagnon animal, il rajoute deux niveaux à son niveau de druide effectif (pour un niveau maximum égal au niveau du personnage). Si la créature est assez grande pour que le personnage la monte, elle gagne gratuitement monture de combat (voir Dressage).", "Le personnage s’attache les services d’un compagnon ou d’une monture monstrueuse.", "Cavalier des bêtes", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chance-adaptative", "Le personnage peut utiliser la chance adaptable une fois de plus par jour. De plus, quand il l’utilise, le bonus de chance augmente de 2.", "La chance du personnage prend des proportions légendaires.", "Chance adaptative", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chute-ralentie", "Quand il est à portée de main d’un mur, le personnage peut ralentir sa chute, tant qu’il n’est pas en armure lourde. Il reçoit les mêmes dégâts que pour une chute de 3 mètres de moins que la distance réelle de sa chute. Ce pouvoir se cumule avec le pouvoir de classe chute ralentie.", "Le personnage peut réduire le nombre de dégâts de chute qu’il reçoit quand il est proche d’un mur.", "Chute ralentie", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chat-noir", "Une fois par jour et par une action immédiate, lorsqu’il est touché par une attaque de corps à corps, l’homme-félin peut forcer l’adversaire qui a porté l’attaque à refaire son jet avec un malus de -4. L’adversaire doit prendre le résultat du second jet. Il s’agit d’une capacité surnaturelle.", "Ceux qui osent contrarier l’homme-félin sont frappés de malchance.", "Chat noir", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chasseur-de-démons", null, "Le personnage est versé en connaissances démoniaques.", "Chasseur de démons", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charmeur-né", "Le dhampir peut décider de faire 20 à tous ses tests de compétences basées sur le Charisme pour charmer, convaincre, persuader ou séduire les humanoïdes ayant au moins une attitude amicale avec lui. Pour décider de faire 20, il faut quand même exercer la compétence pendant une durée 20 fois plus longue qu’en temps normal.", "Le dhampir possède certains des pouvoirs de domination de son géniteur vampire.", "Charmeur-né", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-renversante-(mythique)", "Au cours d’une charge, le personnage peut ignorer les alliés sur son trajet lorsqu’il détermine s’il peut ou non charger une cible. De plus, après avoir effectué au moins un renversement lors de la charge renversante, il peut dépenser une utilisation de pouvoir mythique par une action libre pour en réaliser un deuxième contre un adversaire différent et situé sur la trajectoire de la charge.", "Le personnage peut traverser les lignes adverses pour charger sa cible.", "Charge renversante (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-explosive", "Lorsqu’il entre dans une rage sanguine, le personnage peut dépenser un emplacement de sort de sanguin à la fin de sa charge par une action rapide pour imprégner son attaque de charge d’un surplus de puissance. Il inflige 1d6 points de dégâts supplémentaires par niveau de l’emplacement de sort dépensé. Ces dégâts supplémentaires sont des dégâts de force qui ne sont pas multipliés sur un coup critique.\r\nSi le lignage sanguin est associé à un type d’énergie spécifique (comme le sont les lignages draconique ou élémentaire), le personnage peut décider d’augmenter les dégâts à 1d8 par niveau de l’emplacement de sort dépensé et ces dégâts supplémentaires sont de ce type d’énergie.\r\n{s:Desambi|Ce don est décrit sous le nom Charge destructrice dans le manuel des classes avancées}", "Le personnage concentre la force de sa rage sanguine en une frappe capable d’exploser de puissance magique.", "Charge explosive", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chair-angélique", "L’aasimar reçoit un malus de -2 à ses tests de Déguisement et de Discrétion, mais gagne l’un des avantages suivants, en fonction de l’affinité métallique de sa chair (il doit en choisir une).\r\n*Acier. Il gagne un bonus d’armure naturelle de +1 à la CA, et ses coups à mains nues ou armes naturelles sont considérés comme étant en fer froid quand il s’agit de passer la réduction de dégâts.\r\n*Airain. Il gagne une résistance de 5 au feu et un bonus de +2 à ses jets de sauvegarde contre les effets de feu.\r\n*Argent. Il gagne un bonus de +2 à ses jets de sauvegarde contre la paralysie, la pétrification et les poisons, ainsi ses coups à mains nues ou armes naturelles sont considérés comme étant en argent quand il s’agit de passer la réduction de dégâts.\r\n*Or. Il reçoit un bonus de +2 à ses jets de sauvegarde contre la cécité, l’éblouissement, les mirages et les effets du registre de la lumière. Lorsqu’il lance des sorts ou pouvoirs magiques de la branche d’illusion (mirage) ou du registre de la lumière, il le fait avec un niveau de lanceur de sorts de +1.", "La peau du personnage brille comme du métal poli.", "Chair angélique", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-dévastatrice", "Quand le personnage lance une charge à cheval (ou sur une autre monture), les dégâts qu’il cause sont doublés s’il utilise une arme de corps à corps (ou triplés s’il s’agit d’une lance d’arçon).", "Les charges montées du personnage provoquent des dégâts terrifiants.", "Charge dévastatrice", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-du-cogneur", "Quand le personnage utilise École du cogneur, il peut charger et faire une attaque de l’École du cogneur à la fin de sa charge au cours de son action de charge.", "Le personnage termine sa charge par un puissant coup de poing.", "Charge du cogneur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-de-la-horde", "Quand le personnage charge au cours du même round qu’un allié qui possède ce don, il gagne un bonus de +2 aux jets d’attaque et de dégâts, en plus des bonus habituels de la charge. S’il a droit à plusieurs attaques par charge, ce bonus s’applique uniquement à la première.", "Le personnage est bien plus dangereux quand il charge avec un allié.", "Charge de la horde", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-de-diversion", "Quand un allié possédant ce don utilise son action de charge et touche son adversaire, le personnage gagne un bonus de +2 à son prochain jet d’attaque contre la cible de cette charge. Ce bonus doit être utilisé avant le tour suivant de l’allié, sinon il est perdu.", "La charge d’un allié crée une ouverture que le personnage peut exploiter", "Charge de diversion", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chant-funèbre-supérieur", "L’effet du chant funèbre du personnage s’attarde sur la cible pendant 2 rounds après que la créature a quitté la zone d’effet. Si le personnage utilise son chant funèbre sur une créature secouée, celle-ci devient effrayée. S’il l’utilise sur une créature effrayée, celle-ci devient paniquée.", "Le son entêtant des intonations sépulcrales du personnage glace les adversaires même les plus solides jusqu’à l’os.", "Chant funèbre supérieur", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chance-insolente", "Une fois par jour, une fois que le personnage a fait 1 à un jet de sauvegarde ou que quelqu’un a confirmé un critique contre lui, il peut refaire son jet de sauvegarde ou obliger la créature qui a confirmé le critique à refaire son jet de confirmation. Cet effet ne se cumule pas avec d’autres effets qui permettent de refaire un jet de sauvegarde ou un jet d’attaque. Le personnage ne peut relancer le dé qu’une seule fois.", "Parfois, le personnage parvient à ignorer des sorts et des attaques qui tueraient une créature moins puissante.", "Chance insolente", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "chance-inexplicable", "Une fois par jour, le personnage peut gagner un bonus de +8 à un unique jet de d20 par une action libre, à faire avant de lancer le dé. Il peut aussi le faire après, mais dans ce cas, le bonus est réduit à +4.", "La chance du personnage surprend toujours les autres.", "Chance inexplicable", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "charge-dévastatrice-(mythique)", "Lorsque le personnage touche un adversaire lors d’une charge montée, cet adversaire doit réussir un jet de Vigueur (DD 10 + ½ du grade du personnage + modificateur de Force du personnage) pour ne pas être chancelant pendant un nombre de rounds égal au grade du personnage. S’il attaque avec une lance d’arçon, augmentez le DD du jet de sauvegarde de +2. Par une action libre avant d’effectuer l’attaque, le personnage peut dépenser une utilisation de pouvoir mythique pour hébéter son adversaire s’il rate son jet de sauvegarde au lieu de le faire chanceler.", "Les charges montées du personnage coupent le souffle et émoussent le courage des créatures suffisamment stupides pour se mettre en travers de sa route.", "Charge dévastatrice (mythique)", null, null });

            migrationBuilder.InsertData(
                table: "Feats",
                columns: new[] { "Id", "Advantage", "Description", "Name", "Normal", "Special" },
                values: new object[] { "création-de-créatures-artificielles", null, "Le personnage peut construire des créatures artificielles (des [[golem|golems]] par exemple).", "Création de créatures artificielles", null, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Feats");
        }
    }
}
