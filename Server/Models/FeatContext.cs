using Microsoft.EntityFrameworkCore;
using pf1e_vtt.Models.Tools;
using pf1e_vtt.Shared.Models;

namespace pf1e_vtt.Models 
{
    public class FeatContext : DbContext
    {
        public FeatContext(DbContextOptions<FeatContext> options) : base(options) 
        {
        }

        public DbSet<Feat> Feats { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var featImporter = new FeatImporter();
            modelBuilder.Entity<Feat>().HasData(
                featImporter.ImportFeats()
            );
        }
    }
}