using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using pf1e_vtt.Shared.Models;

namespace pf1e_vtt.Models.Tools
{
    public class FeatImporter
    {
        private static readonly string _featJsonFile = File.ReadAllText("Resources/Json/feats.json");
        public List<Feat> ImportFeats()
        {
            dynamic featData = JsonConvert.DeserializeObject(_featJsonFile);
            var feats = new List<Feat>();
            foreach (var feat in featData.Feats) 
            {
                var currentFeat = new Feat()
                { 
                    Id = feat.Id,
                    Name = feat.Name,
                    Description = feat.Description,
                    Advantage = feat.Benefit,
                    Normal = feat.Normal,
                    Special = feat.Special
                };
                feats.Add(currentFeat);
            }
            return feats;
        }
    }
}