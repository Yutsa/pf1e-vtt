using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using pf1e_vtt.Shared.Services;
using pf1e_vtt.Shared.Models;

namespace pf1e_vtt.Server.Pages
{
    public class FeatModel : PageModel
    {
        private readonly IFeatsService featsService;

        public IEnumerable<Feat> Feats {get; set;}

        public FeatModel(IFeatsService featsService)
        {
            this.featsService = featsService;
        }

        public async void OnGetAsync()
        {
            Feats =  await featsService.GetFeatsAsync();
        }
    }
}