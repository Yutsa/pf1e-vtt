using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pf1e_vtt.Models;
using pf1e_vtt.Shared.Models;

namespace pf1e_vtt.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeatController : ControllerBase
    {
        private readonly FeatContext _context;

        public FeatController(FeatContext context)
        {
            _context = context;
        }

        // GET: api/Feat
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Feat>>> GetFeats()
        {
            return await _context.Feats.ToListAsync();
        }

        [HttpGet("first")]
        public async Task<ActionResult<IEnumerable<Feat>>> GeatFirstFeats()
        {
            return await _context.Feats
                .OrderBy(feat => feat.Name)
                .Take(20)
                .ToListAsync();
        }

        [HttpGet("last")]
        public async Task<ActionResult<IEnumerable<Feat>>> GeatLastFeats()
        {
            return await _context.Feats
                .OrderBy(feat => feat.Name)
                .Skip(20)
                .ToListAsync();
        }

        // GET: api/Feat/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Feat>> GetFeat(string id)
        {
            var feat = await _context.Feats.FindAsync(id);

            if (feat == null)
            {
                return NotFound();
            }

            return feat;
        }
    }
}
