using System.Threading.Tasks;
using pf1e_vtt.Shared.Models;

namespace pf1e_vtt.Shared.Services
{
    public interface IFeatsService
    {
        Task<Feat[]> GetFeatsAsync();
    }
}