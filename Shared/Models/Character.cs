using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace pf1e_vtt.Shared.Models
{
    public class Character : IFeatOwner
    {
        [Key]
        [JsonIgnore]
        private string Id {get; set;}
        public ICollection<Feat> Feats { get; set; } = new List<Feat>();
        public string Name { get; set; }
    }
}