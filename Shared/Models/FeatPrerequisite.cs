using System.Linq;

namespace pf1e_vtt.Shared.Models
{
    public class FeatPrerequisite : IPrerequisite<IFeatOwner>
    {
        public Feat Feat { get; set; }

        public bool IsSatisfied(IFeatOwner featOwner)
        {
            return featOwner.Feats
            .Any(currentFeat => currentFeat.Name.Equals(Feat.Name));
        }
    }
}