namespace pf1e_vtt.Shared.Models
{
    public class Reference
    {
        public SourceMaterial SourceMaterial { get; private set; }
        public string Link { get; private set; }
    }
}