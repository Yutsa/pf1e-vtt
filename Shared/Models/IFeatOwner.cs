using System.Collections.Generic;

namespace pf1e_vtt.Shared.Models
{
    public interface IFeatOwner
    {
        ICollection<Feat> Feats { get; set; }
    }
}