using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace pf1e_vtt.Shared.Models
{
    public class Feat
    {
        [Key]
        [JsonIgnore]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Advantage { get; set; }
        public string Normal { get; set; }
        public string Special { get; set; }

        public override string ToString()
        {
            return Name;
        }

    }
}