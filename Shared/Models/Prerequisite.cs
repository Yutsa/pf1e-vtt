namespace pf1e_vtt.Shared.Models
{
    public interface IPrerequisite<T>
    {
        bool IsSatisfied(T creature);
    }
}